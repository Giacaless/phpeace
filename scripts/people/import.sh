#! /bin/sh

CURRENTDIR=`dirname $0`
cd $CURRENTDIR

# Input parameters
# filename (in import directory under installation path)
# id_topic (topic to associate imported people to)
# id_geo (default country for imported people)
# id_pt_group (people group to associate)

php $CURRENTDIR/import.php -f $1 -t $2 -g $3 -p $4


