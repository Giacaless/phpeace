<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

$options = getopt("f:t:g:p:");
	
if(is_array($options) && $options['f']!="" && $options['t']>0)
{
	include_once(SERVER_ROOT."/../classes/file.php");
	include_once(SERVER_ROOT."/../classes/db.php");
	include_once(SERVER_ROOT."/../classes/validator.php");
	include_once(SERVER_ROOT."/../classes/people.php");
	include_once(SERVER_ROOT."/../classes/topic.php");
	
	$fm = new FileManager();
	$db =& Db::globaldb();
	$va = new Validator();
	$pe = new People();
	
	$filename = "import/" . $options['f'];
	$id_topic = (int)$options['t'];
	$id_geo = (int)$options['g'];
	$id_pt_group = (int)$options['p'];
	
	$existing = 0;
	$existing_topic = 0;
	$existing_topic_contact = 0;
	$inserted = 0;
	$invalid = 0;
	$t = new Topic($id_topic);
	if(!$fm->Exists($filename))
	{
		echo "no $filename";
		exit;
	}
	$lines = $fm->TextFileLines($filename);
	if(count($lines)>0)
	{
		foreach($lines as $line)
		{
			$vals = explode("|",$line);
			$num_vals = count($vals);
			if($num_vals==6)
			{
				/*
				 * We are assuming the following order in CSV columns:
				 * Contact ID|First Name|Last Name|Role|Company Name|Email
				 * 
				 * We also assume there are no headings (i.e. data begin from first row)
				 */
				$salesforce_id = $db->SqlQuote(trim($vals[0]));
				$name1 = $db->SqlQuote(trim($vals[1]));
				$name2 = $db->SqlQuote(trim($vals[2]));
				$role = $db->SqlQuote(trim($vals[3]));
				$company = $db->SqlQuote(trim($vals[4]));
				$email = $db->SqlQuote(trim($vals[5]));
				if($va->Email($email))
				{
					// check user is there
					$user = $pe->UserGetByEmail($email);
					if($user['id_p']>0)
					{
						$existing ++;
						$id_p = $user['id_p'];
						$topic_user = $t->PersonGet($id_p);
						if($topic_user['id_p']>0)
						{
							$existing_topic++;
							if($topic_user['contact'])
								$existing_topic_contact++;
						}
						else 
						{
							$pe->TopicAssociate($id_p,$id_topic,1,0,false);
						}
					}
					else
					{
						$inserted++;
						$unescaped_params = array();
						if($role!="")
							$unescaped_params['role'] = $role;
						if($company!="")
							$unescaped_params['company'] = $company;
						if($salesforce_id!="")
							$unescaped_params['salesforce_id'] = $salesforce_id;
						$id_p = $pe->UserCreate($name1,$name2,$email,$id_geo,0,$unescaped_params,false,$id_topic,1,false);
					}
					if($id_pt_group>0)
						$pe->TopicGroupAssociate($id_p,$id_pt_group);
					
				}
				else
				{
				    $invalid ++;
				    echo "invalid mail: $email\n";
				}
			}
		}	
	}
	echo "inserted: $inserted\n";
	echo "existing: $existing\n";
	echo "existing_topic: $existing_topic\n";
	echo "existing_topic_contact: $existing_topic_contact\n";
	echo "invalid: $invalid\n";
}


?>
