<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

$_SERVER['DOCUMENT_ROOT'] = realpath("../");
define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

$options = getopt("t:");
	
if(is_array($options) && $options['t']!="")
{
	$id_topic = (int)$options['t'];
	if($id_topic>0)
	{
		$filename = "export/topic_{$id_topic}.xml";
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic($id_topic);
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/db.php");
		$fm = new FileManager();
		$fm->DirAction("export","check");
		$fm->Delete($filename);
		$export = array();
		$export['build'] = PHPEACE_BUILD;
		$export['ts'] = time();
		$export['date'] = getdate();
		ExportAppendTable($export,"topics","id_topic",$id_topic);
		ExportAppendTable($export,"subtopics","id_subtopic",$id_topic);
//		ExportAppendTable($export,"articles","id_article",$id_topic);
		ExportAppendTable($export,"docs_articles","id_doc",$id_topic,"articles","id_article");
		ExportAppendTable($export,"images_articles","id_image",$id_topic,"articles","id_article");
		
		// LINKED TABLES
		// keywords_use
		// keywords
		
		// FILES
		// docs
		// images

		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$xh = new XmlHelper();
		$xml = $xh->Array2Xml($export);
		$fm->WritePage($filename,$xml);
	}
}

function ExportAppendTable(&$array,$table_name,$key,$id_topic,$linked_table="",$linked_key="")
{
	$array[$table_name] = ExportTable($table_name,$key,$id_topic,$linked_table,$linked_key);
	$array[$table_name]['table_name'] = $table_name;
	$array[$table_name]['key'] = $key;
}

function ExportTable($table_name,$key,$id_topic,$linked_table,$linked_key)
{
	$db = Db::globaldb();
	$rows = array();
	$sqlstr = "SELECT * FROM $table_name ";
	if($id_topic>0)
	{
		if($linked_table!="" && $linked_key!="")
		{
			$sqlstr .= " WHERE $linked_key IN (SELECT $linked_key FROM $linked_table WHERE id_topic=$id_topic) ";		
		}
		else
			$sqlstr .= " WHERE id_topic=$id_topic ";
	}
	$sqlstr .= " ORDER BY $key ";
	$db->QueryExe($rows, $sqlstr, false, 1);
	array_unshift($rows,array('0'));
	return $rows;
}
?>
