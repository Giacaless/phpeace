# PhPeace 

PeaceLink Portal Management System

## Release

- update release notes in `dev/release_notes/XYZ.txt` for the current XYZ build
- run `dev/scripts/phdistrib.sh`
- upload the `update_XYZ.tar.gz` file to PhPeace server
- Start next release
  - update build number in `classes/phpeace.php#L49`
  - update version number in `classes/phpeace.php#L56`
  - push
