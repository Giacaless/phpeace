
# LICENSE 

PhPeace is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

PhPeace is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

The GNU General Public License (GPL) is available at http://www.gnu.org/copyleft/gpl.html. A copy can be found in the file COPYING distributed with these scripts.

# 3RD PARTY COMPONENTS

PhPeace is distributed with some third party components. Details about their license and their copyright owner are provided in the file OTHERS.

# REQUIREMENTS 

- Linux
- Apache >= 2.0
- PHP >= 5.2 (compiled with curl, ctype, iconv, mbstring, soap, xsl libraries)
- MySQL 5
- ImageMagick >= 6.3

# SOFTWARE INSTALLATION 

Login as root

Create the directory where PhPeace will be installed

`# mkdir /home/htdocs/myphpeace`

Uncompress the downloded archive (phpeace.tar.gz) in the directory you just created

`# tar xzf phpeace.tar.gz -C /home/htdocs/myphpeace`

Give permissions to the user running PHP (generally the webserver user, apache)

`# chown -R apache:apache /home/htdocs/myphpeace`

# APACHE CONFIGURATION

Set up Apache to work with the two domains you will be using for the public website and for the administration interface.
Let's suppose you have www.mydomain.org and admin.mydomain.org pointing to your machine.
You need to create these two virtual hosts

```
<VirtualHost www.mydomain.org>
    ServerName www.mydomain.org
    DocumentRoot /home/htdocs/myphpeace/pub
    <Directory /home/htdocs/myphpeace/pub>
                Options FollowSymLinks
                AllowOverride All
    </Directory>
</VirtualHost>
```

```
<VirtualHost admin.mydomain.org>
    ServerName admin.mydomain.org
    DocumentRoot /home/htdocs/myphpeace/admin
    <Directory /home/htdocs/myphpeace/admin>
                Options FollowSymLinks
    </Directory>
</VirtualHost>
```

Reload Apache configuration

`# /etc/init.d/apache2 reload`

# PHP CONFIGURATION

Check that the following PHP options are set accordingly in your webserver php.ini
safe_mode = Off
memory_limit = 32M
register_globals = Off
file_uploads = On
upload_max_filesize = 10M
post_max_size = 10M

If you modify any of these settings, restart Apache

`# /etc/init.d/apache2 restart`


# DATABASE CONFIGURATION

Connect to MySql using a user with proper permissions (assuming root)

`# mysql -p`

Create the database for PhPeace (let's call it mydatabase) with utf8 as character set

`mysql> CREATE DATABASE mydatabase DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;`

Create a user for this database (let's call it myusername with password mypassword)

`mysql> USE mysql;`

`mysql> GRANT ALL PRIVILEGES ON mydatabase.* TO 'myusername'@localhost IDENTIFIED BY 'mypassword';`

`mysql> EXIT;`

Write the database information (mydatabase,myusername,mypassword) in `/home/htdocs/myphpeace/custom/config.php`

If you wish, read through config.php and customise the other available options

To complete the installation, connect with a browser to `http://www.mydomain.org/phpeace/install.php`

Once finished the installation, remember to register your PhPeace to receive automatic updates


# SCHEDULER CONFIGURATION

Add this job into your crontab to be executed every 15 minutes

`*/15 * * * * curl http://admin.mydomain.org/gate/cron.php -fs`

and set the scheduler IP in the configuration panel of the administration module


# VIRTUAL HOST EXAMPLE

If you want to setup a virtual host associated to a topic

```
<VirtualHost *:80>
    ServerName www.mytopic.org
    DocumentRoot /home/htdocs/myphpeace/pub/mytopic
    Alias /mytopic /home/htdocs/myphpeace/pub/mytopic
</VirtualHost>
```
