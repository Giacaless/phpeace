<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

if(isset($get['t']))
{
	$token = $get['t'];
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	if($pl->PersonTokenValid($token))
	{
		include_once(SERVER_ROOT."/../classes/layout.php");
		$poll_person = $pl->PersonTokenGet($token);
		$id_poll = (int)$poll_person['id_poll'];
		$id_p = (int)$poll_person['id_p'];
		if($id_poll>0 && $id_p>0)
		{
			$poll = $pl->PollGet($id_poll);
			$id_topic = (int)$poll['id_topic'];
			$l = new Layout;
			$params = array('subtype'=>"result",'id_p'=>$id_p,'token'=>$token);
			echo $l->Output("poll",$id_poll,$id_topic,1,$params);
		}
	}
}


?>
