<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(true,0,false);

$get = $fh->HttpGet();

if(isset($get['mjt']))
{
	$identifier = $get['mjt'];
	if(strlen($identifier)==32)
	{
		include_once(SERVER_ROOT."/../classes/mailjobs.php");
		$mj = new Mailjobs();
		$return = $mj->RecipientUnsubscribe($identifier);
		$id_topic = (int)$return['id_topic'];
		$l = new Layout();
		if($return['id_mail']!="") {
	        if($return['return']) {
	            $fh->va->MessageSet("notice","email_unsubscribe",array($return['email']));
	        } else {
	            $fh->va->MessageSet("error","email_unsubscribe_already",array($return['email']));
	        }
		} else {
		    $fh->va->MessageSet("error","email_unsubscribe_failed",array($l->ini->Get('staff_email')));
		}
		$params = array('subtype'=>'unsub');
		if($id_topic>0)
		{
		    echo $l->Output("topic_home",$id_topic,$id_topic,1,$params);
		}
		else 
		{
		    echo $l->Output("homepage",0,0,1,$params,true);
		}
	}
}
?>
