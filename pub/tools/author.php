<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/layout.php");
$fh = new FormHelper(false,0,false);
$l = new Layout;
$get = $fh->HttpGet();

$login = $get['l'];
$id_translator = (int)$get['t'];
$id_user = (int)$get['u'];
$id_bio = (int)$get['b'];
$id_contact = (int)$get['c'];
$page = (int)$get['p'];
$type = "user";

$params = array();
if($login!="")
	$params['login'] = $login;
elseif($id_user>0)
	$params['u'] =  $id_user;
elseif($id_translator>0)
	$params['t'] =  $id_translator;
elseif($id_bio>0)
	$params['b'] =  $id_bio;
elseif($id_contact>0)
	$params['c'] =  $id_contact;

if ($l->ini->Get("user_show")=="1" && (isset($login) || $id_translator>0 || $id_user>0 || $id_bio>0 || $id_contact>0))
	echo $l->Output($type,0,0,$page,$params,true);
?>

