<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);

include_once(SERVER_ROOT."/../classes/pagetypes.php");			
$pt = new PageTypes();

include_once(SERVER_ROOT."/../classes/layout.php");
$id_topic = (int)$_GET['id_topic'];
$id_type = $_GET['id_type'];
$id = (int)$_GET['id'];
$page = (int)$_GET['p'];
$params = array();
foreach($_GET as $key=>$value)
{
	if ($value!="")
	{
		$params[$key] = $value;
	}
}

if($id_topic>0)
{
	$l = new Layout(false,true);
	
	$xml = $l->PageType($id_type,$id_topic,$id,$page,$params);
	$type = ($params['module']!="")? $params['module'] : array_search($id_type,$pt->types);
	
	$html = $l->xh->Transform($type,$xml,$l->id_style);
	$l->DoctypeHTML5Fix($html);
	echo $html;
}
?>

