<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']); 
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();
include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout();

if(!($l->conf->Get("track_all") || $l->conf->Get("track")))
	exit;

$id_topic = (int)$get['id_t'];
$id_topic_group = (int)$get['id_tg'];
$append = $get['append']=="1";

header('P3P: CP="NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"');
header("Content-type: text/javascript");
$l->NoCache();
?>
var screen_width = "-";
var screen_height = "-";
var screen_color = "-";

if ( document.all || document.getElementById || document.layers )
{
	screen_width = screen.width;
	screen_height = screen.height;
	screen_color = screen.colorDepth;
}

var url;

var cookieEnabled=(navigator.cookieEnabled)? true : false;

if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled)
{
	var  chkcookie = 'test';
	document.cookie = "chkcookie=" + chkcookie + "; path=/";
	cookieEnabled=(document.cookie.indexOf(chkcookie,0)!=-1)? true : false;
}

var cd = "1";
if(cookieEnabled)
	cd = "0";

url = "<?=$l->pub_web;?>/tools/track.php?cd=" + cd + "&id_t=<?=$id_topic;?>&id_tg=<?=$id_topic_group;?>&u=" + encodeURIComponent(window.location)  + "&w=" + screen_width + "&h=" + screen_height + "&c=" + screen_color + "&t=" + encodeURIComponent(document.title) + "&r=" + encodeURIComponent(document.referrer);

<?php if($append) { ?>
$('#tracking-img').append("<img src='" + url + "' class='hidden'  width='0' height='0' />");
<?php } else { ?>
document.write("<img src='" + url + "' class='hidden'  width='0' height='0' />");
<?php }  ?>




