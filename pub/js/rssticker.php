<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$url = $get['url'];
$ttl = (int)$get['ttl'];
$delay = (int)$get['delay'];
$desc = $get['desc']=="1"? "1" : "0";
$div_id = $get['div']!=""? $get['div'] : "ticker-id";

include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout;
$fh->va->URL($url);

header("Content-type: text/javascript; charset=" . $l->conf->Get("encoding"));
if($url!="" && $fh->va->return)
{
	include_once(SERVER_ROOT."/../classes/rss.php");
	$r = new Rss();
	$xml = $r->Get($url,$ttl);
	$xmldata = $l->th->TextReplaceForJavascript($xml,true);
?>

var xmldata = '<?=$xmldata;?>';
var delay = <?=$delay;?>;
var ttl = <?=$ttl;?>;
var div_id = '<?=$div_id;?>';
var showdesc = <?=$desc;?>;

function rss_sticker(xmlstring, cachetime, divId, divClass, delay, showDescription){
this.cachetime=cachetime 
this.tickerid=divId 
this.delay=delay 
this.mouseoverBol=0 
this.showdesc=(showDescription=="1")? true : false
this.pointer=0
	
if (window.ActiveXObject) {
	this.doc=new ActiveXObject("Microsoft.XMLDOM");
	this.doc.async="false";
	this.doc.loadXML(xmlstring);
} else {
	var parser=new DOMParser();
	this.doc=parser.parseFromString(xmlstring,"text/xml");
}
this.initialize()
}

rss_sticker.prototype.initialize=function(){ 
if(this.doc.getElementsByTagName("item").length==0){ 
document.getElementById(this.tickerid).innerHTML="<!-- no news is good news -->"
return
}
var instanceOfTicker=this
this.feeditems=this.doc.getElementsByTagName("item")

for (var i=0; i<this.feeditems.length; i++){
this.feeditems[i].setAttribute("ctitle", this.feeditems[i].getElementsByTagName("title")[0].firstChild.nodeValue)
this.feeditems[i].setAttribute("clink", this.feeditems[i].getElementsByTagName("link")[0].firstChild.nodeValue)
var desc = this.feeditems[i].getElementsByTagName("description")[0].firstChild
this.feeditems[i].setAttribute("cdescription", (desc!=null)? desc.nodeValue:"" )
}
document.getElementById(this.tickerid).onmouseover=function(){instanceOfTicker.mouseoverBol=1}
document.getElementById(this.tickerid).onmouseout=function(){instanceOfTicker.mouseoverBol=0}
this.rotatemsg()
}


rss_sticker.prototype.rotatemsg=function(){
var instanceOfTicker=this
if (this.mouseoverBol==1) 
setTimeout(function(){instanceOfTicker.rotatemsg()}, 100)
else{
var tickerDiv=document.getElementById(this.tickerid)
var tickercontent='<a href="'+this.feeditems[this.pointer].getAttribute("clink")+'">'+this.feeditems[this.pointer].getAttribute("ctitle")+'</a>'
if(this.showdesc)
tickercontent+="<div class=\"tkdesc\">"+this.feeditems[this.pointer].getAttribute("cdescription")+"</div>"
tickerDiv.innerHTML=tickercontent
this.pointer=(this.pointer < this.feeditems.length-1)? this.pointer+1 : 0
setTimeout(function(){instanceOfTicker.rotatemsg()}, this.delay) 
}
}

new rss_sticker(xmldata,ttl,div_id,'ticker',delay,showdesc)

<? } ?>

