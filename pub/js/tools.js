
function getHttpContent(url,elementid) {
	makeHttpRequest(url,'returnContent',elementid);
}

function makeHttpRequest(url, callback_function, output, return_xml) {
	var http_request = false;
	if (window.XMLHttpRequest) {
		http_request = new XMLHttpRequest();
		if (http_request.overrideMimeType) {
			http_request.overrideMimeType('text/xml');
		}
	} else if (window.ActiveXObject) {
		try {
			http_request = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				http_request = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {}
		}
	}

	if (!http_request) {
		return false;
	}
	http_request.onreadystatechange = function() {
		if (http_request.readyState == 4) {
			if (http_request.status == 200) {
				if (return_xml) {
					eval(callback_function + '(\'' +output + '\',http_request.responseXML)');
				} else {
					eval(callback_function + '(\'' + output + '\',http_request.responseText)');
				}
			}
		}
	}
	http_request.open('GET', url, true);
	http_request.send(null);
}

function returnContent(where,what) {
	document.getElementById(where).innerHTML = what;
}

/*
 ======================================================================
 RSS JavaScript Ticker object
 Author: George at JavaScriptKit.com/ DynamicDrive.com
 Created: Feb 5th, 2006. Updated: Feb 5th, 2006
 Adapted for PhPeace by Francesco Iannuzzelli
 ======================================================================
*/

function createAjaxObj(){
var httprequest=false
if (window.XMLHttpRequest){ 
httprequest=new XMLHttpRequest()
if (httprequest.overrideMimeType)
httprequest.overrideMimeType('text/xml')
}
else if (window.ActiveXObject){ 
try {
httprequest=new ActiveXObject("Msxml2.XMLHTTP");
} 
catch (e){
try{
httprequest=new ActiveXObject("Microsoft.XMLHTTP");
}
catch (e){}
}
}
return httprequest
}

// -------------------------------------------------------------------
// Main RSS Ticker Object function
// rss_ticker(RSS_id, cachetime, divId, delay)
// -------------------------------------------------------------------

function rss_ticker(RSS_id, cachetime, divId, delay, showDescription){
this.RSS_id=RSS_id //Array key indicating which RSS feed to display
this.cachetime=cachetime //Time to cache feed, in minutes. 0=no cache.
this.tickerid=divId //ID of ticker div to display information
this.delay=delay //Delay between msg change, in miliseconds.
this.mouseoverBol=0 //Boolean to indicate whether mouse is currently over ticker (and pause it if it is)
this.showdesc=(showDescription=="1")? true : false
this.pointer=0
this.ajaxobj=createAjaxObj()
this.getAjaxcontent()
}

// -------------------------------------------------------------------
// getAjaxcontent()- Makes asynchronous GET request to "rssfetch.php" with the supplied parameters
// -------------------------------------------------------------------

rss_ticker.prototype.getAjaxcontent=function(){
if (this.ajaxobj){
var instanceOfTicker=this
var parameters="url="+encodeURIComponent(this.RSS_id)+"&ttl="+this.cachetime
this.ajaxobj.onreadystatechange=function(){instanceOfTicker.initialize()}
this.ajaxobj.open('GET', "/js/rss_fetch.php?"+parameters, true)
this.ajaxobj.send(null)
}
}

// -------------------------------------------------------------------
// initialize()- Initialize ticker method.
// -Gets contents of RSS content and parse it using JavaScript DOM methods 
// -------------------------------------------------------------------

rss_ticker.prototype.initialize=function(){ 
	if (this.ajaxobj.readyState == 4){ //if request of file completed
		if (this.ajaxobj.status==200){ //if request was successful
			var xmldata=this.ajaxobj.responseXML;
			if(xmldata.getElementsByTagName("item").length==0){ //if no <item> elements found in returned content
				document.getElementById(this.tickerid).innerHTML="<!-- no news is good news -->";
				return
			}
			var instanceOfTicker=this;
			if(xmldata.firstChild.localName=='feature')
			{
				this.feeditems=xmldata.getElementsByTagName("item");
				for (var i=0; i<this.feeditems.length; i++){
					var itemTitle = this.feeditems[i].attributes.getNamedItem('event_type').textContent.toUpperCase() + ' - ' + this.feeditems[i].attributes.getNamedItem('title').textContent;
					this.feeditems[i].setAttribute("ctitle", itemTitle);
					this.feeditems[i].setAttribute("clink", this.feeditems[i].attributes.getNamedItem('url').textContent);
					var itemDesc = this.feeditems[i].attributes.getNamedItem('start_date').textContent;
					if(this.feeditems[i].attributes.getNamedItem('end_date').textContent != this.feeditems[i].attributes.getNamedItem('start_date').textContent){
						itemDesc += ' - ' + this.feeditems[i].attributes.getNamedItem('end_date').textContent;
					}
					itemDesc += ' - ' + this.feeditems[i].attributes.getNamedItem('place').textContent;
					this.feeditems[i].setAttribute("cdescription", itemDesc);
				}
			}
			else
			{
				this.feeditems=xmldata.getElementsByTagName("item");
				for (var i=0; i<this.feeditems.length; i++){
					this.feeditems[i].setAttribute("ctitle", this.feeditems[i].getElementsByTagName("title")[0].firstChild.nodeValue);
					this.feeditems[i].setAttribute("clink", this.feeditems[i].getElementsByTagName("link")[0].firstChild.nodeValue);
					var desc = this.feeditems[i].getElementsByTagName("description")[0].firstChild;
					this.feeditems[i].setAttribute("cdescription", (desc!=null)? desc.nodeValue:"");
				}
			}
		document.getElementById(this.tickerid).onmouseover=function(){instanceOfTicker.mouseoverBol=1;};
		document.getElementById(this.tickerid).onmouseout=function(){instanceOfTicker.mouseoverBol=0;};
		this.rotatemsg();
		}
	}
}

// -------------------------------------------------------------------
// rotatemsg()- Rotate through RSS messages and displays them
// -------------------------------------------------------------------

rss_ticker.prototype.rotatemsg=function(){
var instanceOfTicker=this
if (this.mouseoverBol==1) 
setTimeout(function(){instanceOfTicker.rotatemsg()}, 100)
else{
var tickerDiv=document.getElementById(this.tickerid)
var tickercontent='<a href="'+this.feeditems[this.pointer].getAttribute("clink")+'">'+this.feeditems[this.pointer].getAttribute("ctitle")+'</a>'
if(this.showdesc)
tickercontent+="<div class=\"tkdesc\">"+this.feeditems[this.pointer].getAttribute("cdescription")+"</div>"
tickerDiv.innerHTML=tickercontent
this.pointer=(this.pointer<this.feeditems.length-1)? this.pointer+1 : 0
setTimeout(function(){instanceOfTicker.rotatemsg()}, this.delay) 
}
}

// -------------------------------------------------------------------
// Main RSS Lister Object function
// rss_lister(RSS_id, divId, items_limit)
// -------------------------------------------------------------------

function rss_lister(RSS_id, cachetime, divId, items_limit){
this.RSS_id=RSS_id //Array key indicating which RSS feed to display
this.cachetime=cachetime //Time to cache feed, in minutes. 0=no cache.
this.items_limit=items_limit //How many items to display
this.listerid=divId //ID of ticker div to display information
this.pointer=0
this.ajaxobj=createAjaxObj()
this.getAjaxcontent()
}

// -------------------------------------------------------------------
// getAjaxcontent()- Makes asynchronous GET request to "rssfetch.php" with the supplied parameters
// -------------------------------------------------------------------

rss_lister.prototype.getAjaxcontent=function(){
if (this.ajaxobj){
var instanceOfLister=this
var parameters="url="+encodeURIComponent(this.RSS_id)+"&ttl="+this.cachetime
this.ajaxobj.onreadystatechange=function(){instanceOfLister.initialize()}
this.ajaxobj.open('GET', "/js/rss_fetch.php?"+parameters, true)
this.ajaxobj.send(null)
}
}

// -------------------------------------------------------------------
// initialize()- Initialize lister method.
// -Gets contents of RSS content and parse it using JavaScript DOM methods 
// -------------------------------------------------------------------

rss_lister.prototype.initialize=function(){ 
if (this.ajaxobj.readyState == 4){ //if request of file completed
if (this.ajaxobj.status==200){ //if request was successful
var xmldata=this.ajaxobj.responseXML
if(xmldata.getElementsByTagName("item").length==0){ //if no <item> elements found in returned content
document.getElementById(this.listerid).innerHTML="<!-- no news is good news -->"
return
}
var instanceOfLister=this
this.feeditems=xmldata.getElementsByTagName("item")

var listerDiv=document.getElementById(this.listerid)
var listercontent='<ul class="rss-list">'
this.feeditems=xmldata.getElementsByTagName("item")
for (var i=0; i<Math.min(this.feeditems.length,this.items_limit); i++){
listercontent+='<li><a href="'+this.feeditems[i].getElementsByTagName("link")[0].firstChild.nodeValue+'">'+this.feeditems[i].getElementsByTagName("title")[0].firstChild.nodeValue+'</a>'
var desc = this.feeditems[i].getElementsByTagName("description")[0].firstChild
if(desc!=null)
	listercontent+="<div class=\"ltdesc\">"+desc.nodeValue+"</div>"
listercontent+="</li>"
}
listercontent+="</ul>"

listerDiv.innerHTML=listercontent
}
}
}


// ARTICLES BOXES POPUP 

function open_popup(src,width,height) {
  if ( document.all || document.getElementById || document.layers ) { if ( width>screen.width ) { width = screen.width - 200; } }
  features = 'location=0,statusbar=0,menubar=0,scrollbars=1,width=' + width +',height=' + height + ',screenX=100,screenY=100';
  var theWindow = window.open(src.getAttribute('href'), src.getAttribute('target') || '_blank', features);
  theWindow.focus();
  return theWindow;
}


// GALLERIES COMM //
function sendEvent(swf,typ,prm) { 
  thisMovie(swf).sendEvent(typ,prm); 
};

function thisMovie(swf) {
  if(navigator.appName.indexOf("Microsoft") != -1) {
    return window[swf];
  } else {
    return document[swf];
  }
};


// POLLS
function styleToggle(b)
{
	for (var i = 0; i< b.form.length; i++)
	{
		if (b.form[i].name == b.name)
		{
			b.form[i].parentNode.style.fontWeight = b.form[i].checked? 'bold' : '';
		}
	}
}


String.prototype.htmlEntities = function () {
   return this.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;');
};



/* SWFObject v2.1 <http://code.google.com/p/swfobject/>
	Copyright (c) 2007-2008 Geoff Stearns, Michael Williams, and Bobby van der Sluis
	This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/
var swfobject=function(){var b="undefined",Q="object",n="Shockwave Flash",p="ShockwaveFlash.ShockwaveFlash",P="application/x-shockwave-flash",m="SWFObjectExprInst",j=window,K=document,T=navigator,o=[],N=[],i=[],d=[],J,Z=null,M=null,l=null,e=false,A=false;var h=function(){var v=typeof K.getElementById!=b&&typeof K.getElementsByTagName!=b&&typeof K.createElement!=b,AC=[0,0,0],x=null;if(typeof T.plugins!=b&&typeof T.plugins[n]==Q){x=T.plugins[n].description;if(x&&!(typeof T.mimeTypes!=b&&T.mimeTypes[P]&&!T.mimeTypes[P].enabledPlugin)){x=x.replace(/^.*\s+(\S+\s+\S+$)/,"$1");AC[0]=parseInt(x.replace(/^(.*)\..*$/,"$1"),10);AC[1]=parseInt(x.replace(/^.*\.(.*)\s.*$/,"$1"),10);AC[2]=/r/.test(x)?parseInt(x.replace(/^.*r(.*)$/,"$1"),10):0}}else{if(typeof j.ActiveXObject!=b){var y=null,AB=false;try{y=new ActiveXObject(p+".7")}catch(t){try{y=new ActiveXObject(p+".6");AC=[6,0,21];y.AllowScriptAccess="always"}catch(t){if(AC[0]==6){AB=true}}if(!AB){try{y=new ActiveXObject(p)}catch(t){}}}if(!AB&&y){try{x=y.GetVariable("$version");if(x){x=x.split(" ")[1].split(",");AC=[parseInt(x[0],10),parseInt(x[1],10),parseInt(x[2],10)]}}catch(t){}}}}var AD=T.userAgent.toLowerCase(),r=T.platform.toLowerCase(),AA=/webkit/.test(AD)?parseFloat(AD.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,q=false,z=r?/win/.test(r):/win/.test(AD),w=r?/mac/.test(r):/mac/.test(AD);/*@cc_on q=true;@if(@_win32)z=true;@elif(@_mac)w=true;@end@*/return{w3cdom:v,pv:AC,webkit:AA,ie:q,win:z,mac:w}}();var L=function(){if(!h.w3cdom){return }f(H);if(h.ie&&h.win){try{K.write("<script id=__ie_ondomload defer=true src=//:><\/script>");J=C("__ie_ondomload");if(J){I(J,"onreadystatechange",S)}}catch(q){}}if(h.webkit&&typeof K.readyState!=b){Z=setInterval(function(){if(/loaded|complete/.test(K.readyState)){E()}},10)}if(typeof K.addEventListener!=b){K.addEventListener("DOMContentLoaded",E,null)}R(E)}();function S(){if(J.readyState=="complete"){J.parentNode.removeChild(J);E()}}function E(){if(e){return }if(h.ie&&h.win){var v=a("span");try{var u=K.getElementsByTagName("body")[0].appendChild(v);u.parentNode.removeChild(u)}catch(w){return }}e=true;if(Z){clearInterval(Z);Z=null}var q=o.length;for(var r=0;r<q;r++){o[r]()}}function f(q){if(e){q()}else{o[o.length]=q}}function R(r){if(typeof j.addEventListener!=b){j.addEventListener("load",r,false)}else{if(typeof K.addEventListener!=b){K.addEventListener("load",r,false)}else{if(typeof j.attachEvent!=b){I(j,"onload",r)}else{if(typeof j.onload=="function"){var q=j.onload;j.onload=function(){q();r()}}else{j.onload=r}}}}}function H(){var t=N.length;for(var q=0;q<t;q++){var u=N[q].id;if(h.pv[0]>0){var r=C(u);if(r){N[q].width=r.getAttribute("width")?r.getAttribute("width"):"0";N[q].height=r.getAttribute("height")?r.getAttribute("height"):"0";if(c(N[q].swfVersion)){if(h.webkit&&h.webkit<312){Y(r)}W(u,true)}else{if(N[q].expressInstall&&!A&&c("6.0.65")&&(h.win||h.mac)){k(N[q])}else{O(r)}}}}else{W(u,true)}}}function Y(t){var q=t.getElementsByTagName(Q)[0];if(q){var w=a("embed"),y=q.attributes;if(y){var v=y.length;for(var u=0;u<v;u++){if(y[u].nodeName=="DATA"){w.setAttribute("src",y[u].nodeValue)}else{w.setAttribute(y[u].nodeName,y[u].nodeValue)}}}var x=q.childNodes;if(x){var z=x.length;for(var r=0;r<z;r++){if(x[r].nodeType==1&&x[r].nodeName=="PARAM"){w.setAttribute(x[r].getAttribute("name"),x[r].getAttribute("value"))}}}t.parentNode.replaceChild(w,t)}}function k(w){A=true;var u=C(w.id);if(u){if(w.altContentId){var y=C(w.altContentId);if(y){M=y;l=w.altContentId}}else{M=G(u)}if(!(/%$/.test(w.width))&&parseInt(w.width,10)<310){w.width="310"}if(!(/%$/.test(w.height))&&parseInt(w.height,10)<137){w.height="137"}K.title=K.title.slice(0,47)+" - Flash Player Installation";var z=h.ie&&h.win?"ActiveX":"PlugIn",q=K.title,r="MMredirectURL="+j.location+"&MMplayerType="+z+"&MMdoctitle="+q,x=w.id;if(h.ie&&h.win&&u.readyState!=4){var t=a("div");x+="SWFObjectNew";t.setAttribute("id",x);u.parentNode.insertBefore(t,u);u.style.display="none";var v=function(){u.parentNode.removeChild(u)};I(j,"onload",v)}U({data:w.expressInstall,id:m,width:w.width,height:w.height},{flashvars:r},x)}}function O(t){if(h.ie&&h.win&&t.readyState!=4){var r=a("div");t.parentNode.insertBefore(r,t);r.parentNode.replaceChild(G(t),r);t.style.display="none";var q=function(){t.parentNode.removeChild(t)};I(j,"onload",q)}else{t.parentNode.replaceChild(G(t),t)}}function G(v){var u=a("div");if(h.win&&h.ie){u.innerHTML=v.innerHTML}else{var r=v.getElementsByTagName(Q)[0];if(r){var w=r.childNodes;if(w){var q=w.length;for(var t=0;t<q;t++){if(!(w[t].nodeType==1&&w[t].nodeName=="PARAM")&&!(w[t].nodeType==8)){u.appendChild(w[t].cloneNode(true))}}}}}return u}function U(AG,AE,t){var q,v=C(t);if(v){if(typeof AG.id==b){AG.id=t}if(h.ie&&h.win){var AF="";for(var AB in AG){if(AG[AB]!=Object.prototype[AB]){if(AB.toLowerCase()=="data"){AE.movie=AG[AB]}else{if(AB.toLowerCase()=="styleclass"){AF+=' class="'+AG[AB]+'"'}else{if(AB.toLowerCase()!="classid"){AF+=" "+AB+'="'+AG[AB]+'"'}}}}}var AD="";for(var AA in AE){if(AE[AA]!=Object.prototype[AA]){AD+='<param name="'+AA+'" value="'+AE[AA]+'" />'}}v.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+AF+">"+AD+"</object>";i[i.length]=AG.id;q=C(AG.id)}else{if(h.webkit&&h.webkit<312){var AC=a("embed");AC.setAttribute("type",P);for(var z in AG){if(AG[z]!=Object.prototype[z]){if(z.toLowerCase()=="data"){AC.setAttribute("src",AG[z])}else{if(z.toLowerCase()=="styleclass"){AC.setAttribute("class",AG[z])}else{if(z.toLowerCase()!="classid"){AC.setAttribute(z,AG[z])}}}}}for(var y in AE){if(AE[y]!=Object.prototype[y]){if(y.toLowerCase()!="movie"){AC.setAttribute(y,AE[y])}}}v.parentNode.replaceChild(AC,v);q=AC}else{var u=a(Q);u.setAttribute("type",P);for(var x in AG){if(AG[x]!=Object.prototype[x]){if(x.toLowerCase()=="styleclass"){u.setAttribute("class",AG[x])}else{if(x.toLowerCase()!="classid"){u.setAttribute(x,AG[x])}}}}for(var w in AE){if(AE[w]!=Object.prototype[w]&&w.toLowerCase()!="movie"){F(u,w,AE[w])}}v.parentNode.replaceChild(u,v);q=u}}}return q}function F(t,q,r){var u=a("param");u.setAttribute("name",q);u.setAttribute("value",r);t.appendChild(u)}function X(r){var q=C(r);if(q&&(q.nodeName=="OBJECT"||q.nodeName=="EMBED")){if(h.ie&&h.win){if(q.readyState==4){B(r)}else{j.attachEvent("onload",function(){B(r)})}}else{q.parentNode.removeChild(q)}}}function B(t){var r=C(t);if(r){for(var q in r){if(typeof r[q]=="function"){r[q]=null}}r.parentNode.removeChild(r)}}function C(t){var q=null;try{q=K.getElementById(t)}catch(r){}return q}function a(q){return K.createElement(q)}function I(t,q,r){t.attachEvent(q,r);d[d.length]=[t,q,r]}function c(t){var r=h.pv,q=t.split(".");q[0]=parseInt(q[0],10);q[1]=parseInt(q[1],10)||0;q[2]=parseInt(q[2],10)||0;return(r[0]>q[0]||(r[0]==q[0]&&r[1]>q[1])||(r[0]==q[0]&&r[1]==q[1]&&r[2]>=q[2]))?true:false}function V(v,r){if(h.ie&&h.mac){return }var u=K.getElementsByTagName("head")[0],t=a("style");t.setAttribute("type","text/css");t.setAttribute("media","screen");if(!(h.ie&&h.win)&&typeof K.createTextNode!=b){t.appendChild(K.createTextNode(v+" {"+r+"}"))}u.appendChild(t);if(h.ie&&h.win&&typeof K.styleSheets!=b&&K.styleSheets.length>0){var q=K.styleSheets[K.styleSheets.length-1];if(typeof q.addRule==Q){q.addRule(v,r)}}}function W(t,q){var r=q?"visible":"hidden";if(e&&C(t)){C(t).style.visibility=r}else{V("#"+t,"visibility:"+r)}}function g(s){var r=/[\\\"<>\.;]/;var q=r.exec(s)!=null;return q?encodeURIComponent(s):s}var D=function(){if(h.ie&&h.win){window.attachEvent("onunload",function(){var w=d.length;for(var v=0;v<w;v++){d[v][0].detachEvent(d[v][1],d[v][2])}var t=i.length;for(var u=0;u<t;u++){X(i[u])}for(var r in h){h[r]=null}h=null;for(var q in swfobject){swfobject[q]=null}swfobject=null})}}();return{registerObject:function(u,q,t){if(!h.w3cdom||!u||!q){return }var r={};r.id=u;r.swfVersion=q;r.expressInstall=t?t:false;N[N.length]=r;W(u,false)},getObjectById:function(v){var q=null;if(h.w3cdom){var t=C(v);if(t){var u=t.getElementsByTagName(Q)[0];if(!u||(u&&typeof t.SetVariable!=b)){q=t}else{if(typeof u.SetVariable!=b){q=u}}}}return q},embedSWF:function(x,AE,AB,AD,q,w,r,z,AC){if(!h.w3cdom||!x||!AE||!AB||!AD||!q){return }AB+="";AD+="";if(c(q)){W(AE,false);var AA={};if(AC&&typeof AC===Q){for(var v in AC){if(AC[v]!=Object.prototype[v]){AA[v]=AC[v]}}}AA.data=x;AA.width=AB;AA.height=AD;var y={};if(z&&typeof z===Q){for(var u in z){if(z[u]!=Object.prototype[u]){y[u]=z[u]}}}if(r&&typeof r===Q){for(var t in r){if(r[t]!=Object.prototype[t]){if(typeof y.flashvars!=b){y.flashvars+="&"+t+"="+r[t]}else{y.flashvars=t+"="+r[t]}}}}f(function(){U(AA,y,AE);if(AA.id==AE){W(AE,true)}})}else{if(w&&!A&&c("6.0.65")&&(h.win||h.mac)){A=true;W(AE,false);f(function(){var AF={};AF.id=AF.altContentId=AE;AF.width=AB;AF.height=AD;AF.expressInstall=w;k(AF)})}}},getFlashPlayerVersion:function(){return{major:h.pv[0],minor:h.pv[1],release:h.pv[2]}},hasFlashPlayerVersion:c,createSWF:function(t,r,q){if(h.w3cdom){return U(t,r,q)}else{return undefined}},removeSWF:function(q){if(h.w3cdom){X(q)}},createCSS:function(r,q){if(h.w3cdom){V(r,q)}},addDomLoadEvent:f,addLoadEvent:R,getQueryParamValue:function(v){var u=K.location.search||K.location.hash;if(v==null){return g(u)}if(u){var t=u.substring(1).split("&");for(var r=0;r<t.length;r++){if(t[r].substring(0,t[r].indexOf("="))==v){return g(t[r].substring((t[r].indexOf("=")+1)))}}}return""},expressInstallCallback:function(){if(A&&M){var q=C(m);if(q){q.parentNode.replaceChild(M,q);if(l){W(l,true);if(h.ie&&h.win){M.style.display="block"}}M=null;l=null;A=false}}}}}();



/** 
 * Name:    Highslide JS
 * Version: 4.1.13 (2011-10-06)
 * Config:  default +slideshow +positioning +transitions +packed
 * Author:  Torstein Hønsi
 * Support: www.highslide.com/support
 * License: www.highslide.com/#license
 */
eval(function(p,a,c,k,e,d){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--){d[e(c)]=k[c]||e(c)}k=[function(e){return d[e]}];e=function(){return'\\w+'};c=1};while(c--){if(k[c]){p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c])}}return p}('q(!m){u m={Z:{ah:\'8L\',8E:\'ao...\',8K:\'6u 1H aH\',9r:\'6u 1H aI 1H az\',7k:\'aE 1H aw B (f)\',9V:\'au by <i>9U 9O</i>\',9W:\'al 1H am 9U 9O ba\',aa:\'a0\',8e:\'9o\',8i:\'9q\',8k:\'9S\',8j:\'9S (b9)\',b8:\'b6\',a6:\'9i\',ac:\'9i 1r (9d)\',af:\'9h\',ad:\'9h 1r (9d)\',a8:\'a0 (6t 1a)\',9v:\'9o (6t 2M)\',87:\'9q\',8n:\'1:1\',3e:\'aW %1 aV %2\',7p:\'6u 1H 1Q 2E, b2 a7 aO 1H 3b. aS 6t aR M 1k a7 2R.\'},4p:\'V/be/\',5S:\'bd.4a\',4y:\'b5.4a\',6I:4x,7q:4x,4k:15,8g:15,4o:15,8b:15,45:aK,8D:0.75,ae:J,6D:5,3x:2,bb:3,54:1d,8M:\'3C 2M\',8I:1,7S:J,9M:\'bn://V.aC/\',9N:\'aq\',8q:J,76:[\'a\'],2P:[],9Y:4x,3u:0,6C:50,4l:\'2q\',6O:\'2q\',8m:H,7U:H,7w:J,4O:a9,4L:a9,4A:J,1x:\'ar-at\',81:{2t:\'<1E 3f="V-2t"><86>\'+\'<1V 3f="V-2R">\'+\'<a 2k="#" 26="{m.Z.a8}">\'+\'<21>{m.Z.aa}</21></a>\'+\'</1V>\'+\'<1V 3f="V-35">\'+\'<a 2k="#" 26="{m.Z.ac}">\'+\'<21>{m.Z.a6}</21></a>\'+\'</1V>\'+\'<1V 3f="V-2G">\'+\'<a 2k="#" 26="{m.Z.ad}">\'+\'<21>{m.Z.af}</21></a>\'+\'</1V>\'+\'<1V 3f="V-1k">\'+\'<a 2k="#" 26="{m.Z.9v}">\'+\'<21>{m.Z.8e}</21></a>\'+\'</1V>\'+\'<1V 3f="V-3b">\'+\'<a 2k="#" 26="{m.Z.87}">\'+\'<21>{m.Z.8i}</21></a>\'+\'</1V>\'+\'<1V 3f="V-11-2s">\'+\'<a 2k="#" 26="{m.Z.7k}">\'+\'<21>{m.Z.8n}</21></a>\'+\'</1V>\'+\'<1V 3f="V-1Q">\'+\'<a 2k="#" 26="{m.Z.8j}" >\'+\'<21>{m.Z.8k}</21></a>\'+\'</1V>\'+\'</86></1E>\'},4H:[],77:J,U:[],6W:[\'4A\',\'2N\',\'4l\',\'6O\',\'8m\',\'7U\',\'1x\',\'3x\',\'b7\',\'bk\',\'bm\',\'7W\',\'bj\',\'bg\',\'bh\',\'82\',\'9X\',\'7w\',\'3t\',\'59\',\'2P\',\'3u\',\'L\',\'19\',\'7z\',\'4O\',\'4L\',\'6d\',\'6P\',\'a3\',\'2n\',\'29\',\'9b\',\'9c\',\'1y\'],1z:[],4K:0,6B:{x:[\'8f\',\'1a\',\'3T\',\'2M\',\'9a\'],y:[\'4X\',\'14\',\'7u\',\'3C\',\'5J\']},5y:{},82:{},7W:{},3l:[],4W:[],3F:{},6A:{},5z:[],1Z:/aL\\/4\\.0/.18(4n.51)?8:6v((4n.51.61().2B(/.+(?:98|aU|b0|2d)[\\/: ]([\\d.]+)/)||[0,\'0\'])[1]),2d:(Q.56&&!1A.3j),4S:/b1/.18(4n.51),5o:/aY.+98:1\\.[0-8].+aX/.18(4n.51),$:A(1G){q(1G)D Q.bc(1G)},2l:A(2m,2V){2m[2m.Y]=2V},1e:A(8Y,42,3y,6q,95){u C=Q.1e(8Y);q(42)m.2Z(C,42);q(95)m.S(C,{aT:0,9P:\'1R\',72:0});q(3y)m.S(C,3y);q(6q)6q.2r(C);D C},2Z:A(C,42){M(u x 2z 42)C[x]=42[x];D C},S:A(C,3y){M(u x 2z 3y){q(m.3L&&x==\'1h\'){q(3y[x]>0.99)C.G.aP(\'5b\');I C.G.5b=\'8B(1h=\'+(3y[x]*28)+\')\'}I C.G[x]=3y[x]}},2g:A(C,R,2S){u 3B,4m,3N;q(1n 2S!=\'5i\'||2S===H){u 2U=a4;2S={3p:2U[2],29:2U[3],5q:2U[4]}}q(1n 2S.3p!=\'3e\')2S.3p=4x;2S.29=1b[2S.29]||1b.8v;2S.62=m.2Z({},R);M(u 2Q 2z R){u e=1U m.1B(C,2S,2Q);3B=6v(m.7l(C,2Q))||0;4m=6v(R[2Q]);3N=2Q!=\'1h\'?\'F\':\'\';e.3n(3B,4m,3N)}},7l:A(C,R){q(C.G[R]){D C.G[R]}I q(Q.7m){D Q.7m.9e(C,H).9m(R)}I{q(R==\'1h\')R=\'5b\';u 2V=C.b4[R.23(/\\-(\\w)/g,A(a,b){D b.8c()})];q(R==\'5b\')2V=2V.23(/8B\\(1h=([0-9]+)\\)/,A(a,b){D b/28});D 2V===\'\'?1:2V}},5A:A(){u d=Q,w=1A,4t=d.64&&d.64!=\'7C\'?d.4h:d.3i,3L=m.2d&&(m.1Z<9||1n 8C==\'1T\');u L=3L?4t.7L:(d.4h.7L||5w.bf),19=3L?4t.91:5w.bi;m.3H={L:L,19:19,4P:3L?4t.4P:8C,4Q:3L?4t.4Q:bl};D m.3H},5g:A(C){u p={x:C.8w,y:C.8J};4g(C.8u){C=C.8u;p.x+=C.8w;p.y+=C.8J;q(C!=Q.3i&&C!=Q.4h){p.x-=C.4P;p.y-=C.4Q}}D p},2s:A(a,2H,3n,P){q(!a)a=m.1e(\'a\',H,{1I:\'1R\'},m.2h);q(1n a.4G==\'A\')D 2H;1S{1U m.4M(a,2H,3n);D 1d}1W(e){D J}},8t:A(C,3X,16){u 1f=C.36(3X);M(u i=0;i<1f.Y;i++){q((1U 5k(16)).18(1f[i].16)){D 1f[i]}}D H},8h:A(s){s=s.23(/\\s/g,\' \');u 1M=/{m\\.Z\\.([^}]+)\\}/g,4s=s.2B(1M),Z;q(4s)M(u i=0;i<4s.Y;i++){Z=4s[i].23(1M,"$1");q(1n m.Z[Z]!=\'1T\')s=s.23(4s[i],m.Z[Z])}D s},8X:A(){u 6w=0,5O=-1,U=m.U,z,1o;M(u i=0;i<U.Y;i++){z=U[i];q(z){1o=z.O.G.1o;q(1o&&1o>6w){6w=1o;5O=i}}}q(5O==-1)m.3h=-1;I U[5O].4d()},4R:A(a,53){a.4G=a.30;u p=a.4G?a.4G():H;a.4G=H;D(p&&1n p[53]!=\'1T\')?p[53]:(1n m[53]!=\'1T\'?m[53]:H)},7G:A(a){u 1y=m.4R(a,\'1y\');q(1y)D 1y;D a.2k},4B:A(1G){u 5U=m.$(1G),3D=m.6A[1G],a={};q(!5U&&!3D)D H;q(!3D){3D=5U.6T(J);3D.1G=\'\';m.6A[1G]=3D;D 5U}I{D 3D.6T(J)}},3R:A(d){q(d)m.6Z.2r(d);m.6Z.3A=\'\'},1i:A(z){q(!m.1P){6y=J;m.1P=m.1e(\'1E\',{16:\'V-an V-8G-B\',4b:\'\',30:A(){m.1Q()}},{1c:\'1D\',1h:0},m.2h,J);q(/(aJ|aG|ay|ax)/.18(4n.51)){u 3i=Q.3i;A 6x(){m.S(m.1P,{L:3i.aB+\'F\',19:3i.aF+\'F\'})}6x();m.1X(1A,\'3r\',6x)}}m.1P.G.1I=\'\';u 6y=m.1P.4b==\'\';m.1P.4b+=\'|\'+z.N;q(6y){q(m.5o&&m.84)m.S(m.1P,{85:\'5Z(\'+m.4p+\'aD.7Y)\',1h:1});I m.2g(m.1P,{1h:z.3u},m.6C)}},7J:A(N){q(!m.1P)D;q(1n N!=\'1T\')m.1P.4b=m.1P.4b.23(\'|\'+N,\'\');q((1n N!=\'1T\'&&m.1P.4b!=\'\')||(m.1N&&m.4R(m.1N,\'3u\')))D;q(m.5o&&m.84)m.1P.G.1I=\'1R\';I m.2g(m.1P,{1h:0},m.6C,H,A(){m.1P.G.1I=\'1R\'})},89:A(60,z){u T=z||m.2C();z=T;q(m.1N)D 1d;I m.T=T;m.3K(Q,1A.3j?\'63\':\'66\',m.57);1S{m.1N=60;60.30()}1W(e){m.T=m.1N=H}1S{q(!60||z.2P[1]!=\'3O\')z.1Q()}1W(e){}D 1d},5Y:A(C,1K){u z=m.2C(C);q(z)D m.89(z.7R(1K),z);I D 1d},2R:A(C){D m.5Y(C,-1)},1k:A(C){D m.5Y(C,1)},57:A(e){q(!e)e=1A.1Y;q(!e.20)e.20=e.6i;q(1n e.20.8U!=\'1T\')D J;u z=m.2C();u 1K=H;8r(e.aA){1C 70:q(z)z.5P();D J;1C 32:1K=2;5h;1C 34:1C 39:1C 40:1K=1;5h;1C 8:1C 33:1C 37:1C 38:1K=-1;5h;1C 27:1C 13:1K=0}q(1K!==H){q(1K!=2)m.3K(Q,1A.3j?\'63\':\'66\',m.57);q(!m.8q)D J;q(e.5V)e.5V();I e.cE=1d;q(z){q(1K==0){z.1Q()}I q(1K==2){q(z.1r)z.1r.8H()}I{q(z.1r)z.1r.2G();m.5Y(z.N,1K)}D 1d}}D J},co:A(17){m.2l(m.1z,m.2Z(17,{1L:\'1L\'+m.4K++}))},cp:A(1p){u 2v=1p.2n;q(1n 2v==\'5i\'){M(u i=0;i<2v.Y;i++){u o={};M(u x 2z 1p)o[x]=1p[x];o.2n=2v[i];m.2l(m.4W,o)}}I{m.2l(m.4W,1p)}},7b:A(6m,5X){u C,1M=/^V-O-([0-9]+)$/;C=6m;4g(C.3m){q(C.1G&&1M.18(C.1G))D C.1G.23(1M,"$1");C=C.3m}q(!5X){C=6m;4g(C.3m){q(C.3X&&m.5l(C)){M(u N=0;N<m.U.Y;N++){u z=m.U[N];q(z&&z.a==C)D N}}C=C.3m}}D H},2C:A(C,5X){q(1n C==\'1T\')D m.U[m.3h]||H;q(1n C==\'3e\')D m.U[C]||H;q(1n C==\'7t\')C=m.$(C);D m.U[m.7b(C,5X)]||H},5l:A(a){D(a.30&&a.30.9L().23(/\\s/g,\' \').2B(/m.(cz|e)ct/))},8T:A(){M(u i=0;i<m.U.Y;i++)q(m.U[i]&&m.U[i].55)m.8X()},7d:A(e){q(!e)e=1A.1Y;q(e.cu>1)D J;q(!e.20)e.20=e.6i;u C=e.20;4g(C.3m&&!(/V-(2E|3b|6b|3r)/.18(C.16))){C=C.3m}u z=m.2C(C);q(z&&(z.7x||!z.55))D J;q(z&&e.P==\'aj\'){q(e.20.8U)D J;u 2B=C.16.2B(/V-(2E|3b|3r)/);q(2B){m.2D={z:z,P:2B[1],1a:z.x.E,L:z.x.B,14:z.y.E,19:z.y.B,8Q:e.5E,8R:e.65};m.1X(Q,\'5x\',m.5j);q(e.5V)e.5V();q(/V-(2E|6b)-7e/.18(z.X.16)){z.4d();m.6o=J}D 1d}}I q(e.P==\'a2\'){m.3K(Q,\'5x\',m.5j);q(m.2D){q(m.43&&m.2D.P==\'2E\')m.2D.z.X.G.3U=m.43;u 3v=m.2D.3v;q(!3v&&!m.6o&&!/(3b|3r)/.18(m.2D.P)){z.1Q()}I q(3v||(!3v&&m.cv)){m.2D.z.4D(\'1u\')}m.6o=1d;m.2D=H}I q(/V-2E-7e/.18(C.16)){C.G.3U=m.43}}D 1d},5j:A(e){q(!m.2D)D J;q(!e)e=1A.1Y;u a=m.2D,z=a.z;a.5c=e.5E-a.8Q;a.6h=e.65-a.8R;u 6k=1b.cw(1b.8S(a.5c,2)+1b.8S(a.6h,2));q(!a.3v)a.3v=(a.P!=\'2E\'&&6k>0)||(6k>(m.cx||5));q(a.3v&&e.5E>5&&e.65>5){q(a.P==\'3r\')z.3r(a);I{z.7E(a.1a+a.5c,a.14+a.6h);q(a.P==\'2E\')z.X.G.3U=\'3b\'}}D 1d},ab:A(e){1S{q(!e)e=1A.1Y;u 5M=/cr/i.18(e.P);q(!e.20)e.20=e.6i;q(!e.5W)e.5W=5M?e.ck:e.cj;u z=m.2C(e.20);q(!z.55)D;q(!z||!e.5W||m.2C(e.5W,J)==z||m.2D)D;M(u i=0;i<z.1z.Y;i++)(A(){u o=m.$(\'1L\'+z.1z[i]);q(o&&o.5B){q(5M)m.S(o,{1c:\'1D\',1I:\'\'});m.2g(o,{1h:5M?o.1h:0},o.3o)}})()}1W(e){}},1X:A(C,1Y,2W){q(C==Q&&1Y==\'3z\'){m.2l(m.5z,2W)}1S{C.1X(1Y,2W,1d)}1W(e){1S{C.8V(\'4C\'+1Y,2W);C.cm(\'4C\'+1Y,2W)}1W(e){C[\'4C\'+1Y]=2W}}},3K:A(C,1Y,2W){1S{C.3K(1Y,2W,1d)}1W(e){1S{C.8V(\'4C\'+1Y,2W)}1W(e){C[\'4C\'+1Y]=H}}},5C:A(i){q(m.77&&m.4H[i]&&m.4H[i]!=\'1T\'){u 1t=Q.1e(\'1t\');1t.5m=A(){1t=H;m.5C(i+1)};1t.1y=m.4H[i]}},9J:A(3e){q(3e&&1n 3e!=\'5i\')m.6D=3e;u 2m=m.5e();M(u i=0;i<2m.4c.Y&&i<m.6D;i++){m.2l(m.4H,m.7G(2m.4c[i]))}q(m.1x)1U m.4V(m.1x,A(){m.5C(0)});I m.5C(0);q(m.4y)u 4a=m.1e(\'1t\',{1y:m.4p+m.4y})},6U:A(){q(!m.2h){m.3W=m.2d&&m.1Z<7;m.3L=m.2d&&m.1Z<9;m.5A();M(u x 2z m.5R){q(1n m[x]!=\'1T\')m.Z[x]=m[x];I q(1n m.Z[x]==\'1T\'&&1n m.5R[x]!=\'1T\')m.Z[x]=m.5R[x]}m.2h=m.1e(\'1E\',{16:\'V-2h\'},{1j:\'2f\',1a:0,14:0,L:\'28%\',1o:m.45,ag:\'8L\'},Q.3i,J);m.1O=m.1e(\'a\',{16:\'V-1O\',26:m.Z.8K,3A:m.Z.8E,2k:\'8x:;\'},{1j:\'2f\',14:\'-52\',1h:m.8D,1o:1},m.2h);m.6Z=m.1e(\'1E\',H,{1I:\'1R\'},m.2h);1b.cn=A(t,b,c,d){D c*t/d+b};1b.8v=A(t,b,c,d){D c*(t/=d)*t+b};m.9k=m.3W;m.9g=((1A.3j&&m.1Z<9)||4n.cB==\'cA\'||(m.3W&&m.1Z<5.5))}},3z:A(){q(m.74)D;m.74=J;M(u i=0;i<m.5z.Y;i++)m.5z[i]()},7Q:A(){u C,1f,56=[],4c=[],2L={},1M;M(u i=0;i<m.76.Y;i++){1f=Q.36(m.76[i]);M(u j=0;j<1f.Y;j++){C=1f[j];1M=m.5l(C);q(1M){m.2l(56,C);q(1M[0]==\'m.2s\')m.2l(4c,C);u g=m.4R(C,\'2n\')||\'1R\';q(!2L[g])2L[g]=[];m.2l(2L[g],C)}}}m.47={56:56,2L:2L,4c:4c};D m.47},5e:A(){D m.47||m.7Q()},1Q:A(C){u z=m.2C(C);q(z)z.1Q();D 1d}};m.1B=A(2o,1p,R){k.1p=1p;k.2o=2o;k.R=R;q(!1p.8O)1p.8O={}};m.1B.4N={6F:A(){(m.1B.2X[k.R]||m.1B.2X.8P)(k);q(k.1p.2X)k.1p.2X.96(k.2o,k.3Q,k)},3n:A(79,1H,3N){k.6H=(1U 93()).97();k.3B=79;k.4m=1H;k.3N=3N;k.3Q=k.3B;k.E=k.6E=0;u 5w=k;A t(5D){D 5w.2X(5D)}t.2o=k.2o;q(t()&&m.3l.2l(t)==1){m.90=cD(A(){u 3l=m.3l;M(u i=0;i<3l.Y;i++)q(!3l[i]())3l.cF(i--,1);q(!3l.Y){bo(m.90)}},13)}},2X:A(5D){u t=(1U 93()).97();q(5D||t>=k.1p.3p+k.6H){k.3Q=k.4m;k.E=k.6E=1;k.6F();k.1p.62[k.R]=J;u 6J=J;M(u i 2z k.1p.62)q(k.1p.62[i]!==J)6J=1d;q(6J){q(k.1p.5q)k.1p.5q.96(k.2o)}D 1d}I{u n=t-k.6H;k.6E=n/k.1p.3p;k.E=k.1p.29(n,0,1,k.1p.3p);k.3Q=k.3B+((k.4m-k.3B)*k.E);k.6F()}D J}};m.2Z(m.1B,{2X:{1h:A(1B){m.S(1B.2o,{1h:1B.3Q})},8P:A(1B){1S{q(1B.2o.G&&1B.2o.G[1B.R]!=H)1B.2o.G[1B.R]=1B.3Q+1B.3N;I 1B.2o[1B.R]=1B.3Q}1W(e){}}}});m.4V=A(1x,3J){k.3J=3J;k.1x=1x;u v=m.1Z,5G;k.6Y=m.2d&&m.1Z<7;q(!1x){q(3J)3J();D}m.6U();k.2I=m.1e(\'2I\',{cy:0},{1c:\'1u\',1j:\'2f\',cg:\'bI\',L:0},m.2h,J);u 6Q=m.1e(\'6Q\',H,H,k.2I,1);k.2c=[];M(u i=0;i<=8;i++){q(i%3==0)5G=m.1e(\'5G\',H,{19:\'2q\'},6Q,J);k.2c[i]=m.1e(\'2c\',H,H,5G,J);u G=i!=4?{bH:0,bG:0}:{1j:\'7s\'};m.S(k.2c[i],G)}k.2c[4].16=1x+\' V-W\';k.7Z()};m.4V.4N={7Z:A(){u 1y=m.4p+(m.bE||"bF/")+k.1x+".7Y";u 7X=m.4S&&m.1Z<6V?m.2h:H;k.3c=m.1e(\'1t\',H,{1j:\'2f\',14:\'-52\'},7X,J);u 7N=k;k.3c.5m=A(){7N.80()};k.3c.1y=1y},80:A(){u o=k.1l=k.3c.L/4,E=[[0,0],[0,-4],[-2,0],[0,-8],0,[-2,-8],[0,-2],[0,-6],[-2,-2]],1i={19:(2*o)+\'F\',L:(2*o)+\'F\'};M(u i=0;i<=8;i++){q(E[i]){q(k.6Y){u w=(i==1||i==7)?\'28%\':k.3c.L+\'F\';u 1E=m.1e(\'1E\',H,{L:\'28%\',19:\'28%\',1j:\'7s\',31:\'1u\'},k.2c[i],J);m.1e(\'1E\',H,{5b:"bJ:bK.bO.bN(ch=bM, 1y=\'"+k.3c.1y+"\')",1j:\'2f\',L:w,19:k.3c.19+\'F\',1a:(E[i][0]*o)+\'F\',14:(E[i][1]*o)+\'F\'},1E,J)}I{m.S(k.2c[i],{85:\'5Z(\'+k.3c.1y+\') \'+(E[i][0]*o)+\'F \'+(E[i][1]*o)+\'F\'})}q(1A.3j&&(i==3||i==5))m.1e(\'1E\',H,1i,k.2c[i],J);m.S(k.2c[i],1i)}}k.3c=H;q(m.3F[k.1x])m.3F[k.1x].4J();m.3F[k.1x]=k;q(k.3J)k.3J()},3E:A(E,1l,7V,3o,29){u z=k.z,bL=z.O.G,1l=1l||0,E=E||{x:z.x.E+1l,y:z.y.E+1l,w:z.x.K(\'1F\')-2*1l,h:z.y.K(\'1F\')-2*1l};q(7V)k.2I.G.1c=(E.h>=4*k.1l)?\'1D\':\'1u\';m.S(k.2I,{1a:(E.x-k.1l)+\'F\',14:(E.y-k.1l)+\'F\',L:(E.w+2*k.1l)+\'F\'});E.w-=2*k.1l;E.h-=2*k.1l;m.S(k.2c[4],{L:E.w>=0?E.w+\'F\':0,19:E.h>=0?E.h+\'F\':0});q(k.6Y)k.2c[3].G.19=k.2c[5].G.19=k.2c[4].G.19},4J:A(8s){q(8s)k.2I.G.1c=\'1u\';I m.3R(k.2I)}};m.6c=A(z,1i){k.z=z;k.1i=1i;k.3d=1i==\'x\'?\'bC\':\'bt\';k.3q=k.3d.61();k.4u=1i==\'x\'?\'bs\':\'br\';k.69=k.4u.61();k.6M=1i==\'x\'?\'bp\':\'bq\';k.88=k.6M.61();k.1m=k.2p=0};m.6c.4N={K:A(N){8r(N){1C\'6g\':D k.1J+k.3a+(k.t-m.1O[\'1l\'+k.3d])/2;1C\'6p\':D k.E+k.cb+k.1m+(k.B-m.1O[\'1l\'+k.3d])/2;1C\'1F\':D k.B+2*k.cb+k.1m+k.2p;1C\'48\':D k.3M-k.2J-k.3S;1C\'6G\':D k.K(\'48\')-2*k.cb-k.1m-k.2p;1C\'5a\':D k.E-(k.z.W?k.z.W.1l:0);1C\'7F\':D k.K(\'1F\')+(k.z.W?2*k.z.W.1l:0);1C\'22\':D k.1w?1b.2A((k.B-k.1w)/2):0}},6s:A(){k.cb=(k.z.X[\'1l\'+k.3d]-k.t)/2;k.3S=m[\'72\'+k.6M]},78:A(){k.t=k.z.C[k.3q]?9Z(k.z.C[k.3q]):k.z.C[\'1l\'+k.3d];k.1J=k.z.1J[k.1i];k.3a=(k.z.C[\'1l\'+k.3d]-k.t)/2;q(k.1J==0||k.1J==-1){k.1J=(m.3H[k.3q]/2)+m.3H[\'24\'+k.4u]}},6r:A(){u z=k.z;k.2b=\'2q\';q(z.6O==\'3T\')k.2b=\'3T\';I q(1U 5k(k.69).18(z.4l))k.2b=H;I q(1U 5k(k.88).18(z.4l))k.2b=\'4z\';k.E=k.1J-k.cb+k.3a;q(k.6P&&k.1i==\'x\')z.6d=1b.2O(z.6d||k.11,z.6P*k.11/z.y.11);k.B=1b.2O(k.11,z[\'4z\'+k.3d]||k.11);k.2a=z.4A?1b.2O(z[\'2O\'+k.3d],k.11):k.11;q(z.3k&&z.2N){k.B=z[k.3q];k.1w=k.11}q(k.1i==\'x\'&&m.54)k.2a=z.4O;k.20=z[\'20\'+k.1i.8c()];k.2J=m[\'72\'+k.4u];k.24=m.3H[\'24\'+k.4u];k.3M=m.3H[k.3q]},7D:A(i){u z=k.z;q(z.3k&&(z.2N||m.54)){k.1w=i;k.B=1b.4z(k.B,k.1w);z.X.G[k.69]=k.K(\'22\')+\'F\'}I k.B=i;z.X.G[k.3q]=i+\'F\';z.O.G[k.3q]=k.K(\'1F\')+\'F\';q(z.W)z.W.3E();q(k.1i==\'x\'&&z.1g)z.3V(J);q(k.1i==\'x\'&&z.1r&&z.3k){q(i==k.11)z.1r.3Z(\'11-2s\');I z.1r.3G(\'11-2s\')}},7y:A(i){k.E=i;k.z.O.G[k.69]=i+\'F\';q(k.z.W)k.z.W.3E()}};m.4M=A(a,2H,3n,2K){q(Q.bu&&m.2d&&!m.74){m.1X(Q,\'3z\',A(){1U m.4M(a,2H,3n,2K)});D}k.a=a;k.3n=3n;k.2K=2K||\'2E\';k.3k=!k.bv;m.77=1d;k.1z=[];k.T=m.T;m.T=H;m.6U();u N=k.N=m.U.Y;M(u i=0;i<m.6W.Y;i++){u 2Q=m.6W[i];k[2Q]=2H&&1n 2H[2Q]!=\'1T\'?2H[2Q]:m[2Q]}q(!k.1y)k.1y=a.2k;u C=(2H&&2H.7B)?m.$(2H.7B):a;C=k.a5=C.36(\'1t\')[0]||C;k.5H=C.1G||a.1G;M(u i=0;i<m.U.Y;i++){q(m.U[i]&&m.U[i].a==a&&!(k.T&&k.2P[1]==\'3O\')){m.U[i].4d();D 1d}}q(!m.bA)M(u i=0;i<m.U.Y;i++){q(m.U[i]&&m.U[i].a5!=C&&!m.U[i].5d){m.U[i].6a()}}m.U[N]=k;q(!m.ae&&!m.1N){q(m.U[N-1])m.U[N-1].1Q();q(1n m.3h!=\'1T\'&&m.U[m.3h])m.U[m.3h].1Q()}k.C=C;k.1J=k.a3||m.5g(C);m.5A();u x=k.x=1U m.6c(k,\'x\');x.78();u y=k.y=1U m.6c(k,\'y\');y.78();k.O=m.1e(\'1E\',{1G:\'V-O-\'+k.N,16:\'V-O \'+k.7z},{1c:\'1u\',1j:\'2f\',1o:m.45+=2},H,J);k.O.bz=k.O.bw=m.ab;q(k.2K==\'2E\'&&k.3x==2)k.3x=0;q(!k.1x||(k.T&&k.3k&&k.2P[1]==\'3O\')){k[k.2K+\'6n\']()}I q(m.3F[k.1x]){k.6f();k[k.2K+\'6n\']()}I{k.6R();u z=k;1U m.4V(k.1x,A(){z.6f();z[z.2K+\'6n\']()})}D J};m.4M.4N={7H:A(e){q(m.bx)bP(\'bQ \'+e.c8+\': \'+e.c7);I 1A.c6.2k=k.1y},6f:A(){u W=k.W=m.3F[k.1x];W.z=k;W.2I.G.1o=k.O.G.1o-1;m.3F[k.1x]=H},6R:A(){q(k.5d||k.1O)D;k.1O=m.1O;u z=k;k.1O.30=A(){z.6a()};u z=k,l=k.x.K(\'6g\')+\'F\',t=k.y.K(\'6g\')+\'F\';q(!2i&&k.T&&k.2P[1]==\'3O\')u 2i=k.T;q(2i){l=2i.x.K(\'6p\')+\'F\';t=2i.y.K(\'6p\')+\'F\';k.1O.G.1o=m.45++}4i(A(){q(z.1O)m.S(z.1O,{1a:l,14:t,1o:m.45++})},28)},c9:A(){u z=k;u 1t=Q.1e(\'1t\');k.X=1t;1t.5m=A(){q(m.U[z.N])z.a1()};q(m.ca)1t.cf=A(){D 1d};1t.16=\'V-2E\';m.S(1t,{1c:\'1u\',1I:\'7v\',1j:\'2f\',6d:\'52\',1o:3});1t.26=m.Z.7p;q(m.4S&&m.1Z<6V)m.2h.2r(1t);q(m.2d&&m.ce)1t.1y=H;1t.1y=k.1y;k.6R()},a1:A(){1S{q(!k.X)D;k.X.5m=H;q(k.5d)D;I k.5d=J;u x=k.x,y=k.y;q(k.1O){m.S(k.1O,{14:\'-52\'});k.1O=H}x.11=k.X.L;y.11=k.X.19;m.S(k.X,{L:x.t+\'F\',19:y.t+\'F\'});k.O.2r(k.X);m.2h.2r(k.O);x.6s();y.6s();m.S(k.O,{1a:(x.1J+x.3a-x.cb)+\'F\',14:(y.1J+x.3a-y.cb)+\'F\'});k.9H();k.8p();u 2j=x.11/y.11;x.6r();k.2b(x);y.6r();k.2b(y);q(k.1g)k.3V(0,1);q(k.4A){k.9u(2j);u 1v=k.1r;q(1v&&k.T&&1v.2t&&1v.ai){u E=1v.9B.1j||\'\',p;M(u 1i 2z m.6B)M(u i=0;i<5;i++){p=k[1i];q(E.2B(m.6B[1i][i])){p.E=k.T[1i].E+(k.T[1i].1m-p.1m)+(k.T[1i].B-p.B)*[0,0,.5,1,1][i];q(1v.ai==\'cd\'){q(p.E+p.B+p.1m+p.2p>p.24+p.3M-p.3S)p.E=p.24+p.3M-p.B-p.2J-p.3S-p.1m-p.2p;q(p.E<p.24+p.2J)p.E=p.24+p.2J}}}}q(k.3k&&k.x.11>(k.x.1w||k.x.B)){k.8y();q(k.1z.Y==1)k.3V()}}k.9f()}1W(e){k.7H(e)}},2b:A(p,49){u 3P,2i=p.20,1i=p==k.x?\'x\':\'y\';q(2i&&2i.2B(/ /)){3P=2i.cc(\' \');2i=3P[0]}q(2i&&m.$(2i)){p.E=m.5g(m.$(2i))[1i];q(3P&&3P[1]&&3P[1].2B(/^[-]?[0-9]+F$/))p.E+=9Z(3P[1]);q(p.B<p.2a)p.B=p.2a}I q(p.2b==\'2q\'||p.2b==\'3T\'){u 6X=1d;u 46=p.z.4A;q(p.2b==\'3T\')p.E=1b.2A(p.24+(p.3M+p.2J-p.3S-p.K(\'1F\'))/2);I p.E=1b.2A(p.E-((p.K(\'1F\')-p.t)/2));q(p.E<p.24+p.2J){p.E=p.24+p.2J;6X=J}q(!49&&p.B<p.2a){p.B=p.2a;46=1d}q(p.E+p.K(\'1F\')>p.24+p.3M-p.3S){q(!49&&6X&&46){p.B=1b.2O(p.B,p.K(1i==\'y\'?\'48\':\'6G\'))}I q(p.K(\'1F\')<p.K(\'48\')){p.E=p.24+p.3M-p.3S-p.K(\'1F\')}I{p.E=p.24+p.2J;q(!49&&46)p.B=p.K(1i==\'y\'?\'48\':\'6G\')}}q(!49&&p.B<p.2a){p.B=p.2a;46=1d}}I q(p.2b==\'4z\'){p.E=1b.c2(p.E-p.B+p.t)}q(p.E<p.2J){u 9t=p.E;p.E=p.2J;q(46&&!49)p.B=p.B-(p.E-9t)}},9u:A(2j){u x=k.x,y=k.y,3g=1d,2u=1b.2O(x.11,x.B),2T=1b.2O(y.11,y.B),2N=(k.2N||m.54);q(2u/2T>2j){ 2u=2T*2j;q(2u<x.2a){2u=x.2a;2T=2u/2j}3g=J}I q(2u/2T<2j){ 2T=2u/2j;3g=J}q(m.54&&x.11<x.2a){x.1w=x.11;y.B=y.1w=y.11}I q(k.2N){x.1w=2u;y.1w=2T}I{x.B=2u;y.B=2T}3g=k.9w(k.2N?H:2j,3g);q(2N&&y.B<y.1w){y.1w=y.B;x.1w=y.B*2j}q(3g||2N){x.E=x.1J-x.cb+x.3a;x.2a=x.B;k.2b(x,J);y.E=y.1J-y.cb+y.3a;y.2a=y.B;k.2b(y,J);q(k.1g)k.3V()}},9w:A(2j,3g){u x=k.x,y=k.y;q(k.1g){4g(y.B>k.4L&&x.B>k.4O&&y.K(\'1F\')>y.K(\'48\')){y.B-=10;q(2j)x.B=y.B*2j;k.3V(0,1);3g=J}}D 3g},9f:A(){u x=k.x,y=k.y;k.4D(\'1u\');k.7r(1,{O:{L:x.K(\'1F\'),19:y.K(\'1F\'),1a:x.E,14:y.E},X:{1a:x.1m+x.K(\'22\'),14:y.1m+y.K(\'22\'),L:x.1w||x.B,19:y.1w||y.B}},m.6I)},7r:A(1s,1H,3o){u 4U=k.2P,6K=1s?(k.T?k.T.a:H):m.1N,t=(4U[1]&&6K&&m.4R(6K,\'2P\')[1]==4U[1])?4U[1]:4U[0];q(k[t]&&t!=\'2s\'){k[t](1s,1H);D}q(k.W&&!k.3x){q(1s)k.W.3E();I k.W.4J()}q(!1s)k.7n();u z=k,x=z.x,y=z.y,29=k.29;q(!1s)29=k.9b||29;u 9l=1s?A(){q(z.W)z.W.2I.G.1c="1D";4i(A(){z.5f()},50)}:A(){z.58()};q(1s)m.S(k.O,{L:x.t+\'F\',19:y.t+\'F\'});q(k.9c){m.S(k.O,{1h:1s?0:1});m.2Z(1H.O,{1h:1s})}m.2g(k.O,1H.O,{3p:3o,29:29,2X:A(2V,2U){q(z.W&&z.3x&&2U.R==\'14\'){u 4T=1s?2U.E:1-2U.E;u E={w:x.t+(x.K(\'1F\')-x.t)*4T,h:y.t+(y.K(\'1F\')-y.t)*4T,x:x.1J+(x.E-x.1J)*4T,y:y.1J+(y.E-y.1J)*4T};z.W.3E(E,0,1)}}});m.2g(k.X,1H.X,3o,29,9l);q(1s){k.O.G.1c=\'1D\';k.X.G.1c=\'1D\';k.a.16+=\' V-8A-4l\'}},67:A(1s,1H){k.3x=1d;u z=k,t=1s?m.6I:0;q(1s){m.2g(k.O,1H.O,0);m.S(k.O,{1h:0,1c:\'1D\'});m.2g(k.X,1H.X,0);k.X.G.1c=\'1D\';m.2g(k.O,{1h:1},t,H,A(){z.5f()})}q(k.W){k.W.2I.G.1o=k.O.G.1o;u 5n=1s||-1,1l=k.W.1l,73=1s?3:1l,6S=1s?1l:3;M(u i=73;5n*i<=5n*6S;i+=5n,t+=25){(A(){u o=1s?6S-i:73-i;4i(A(){z.W.3E(0,o,1)},t)})()}}q(1s){}I{4i(A(){q(z.W)z.W.4J(z.bT);z.7n();m.2g(z.O,{1h:0},m.7q,H,A(){z.58()})},t)}},3O:A(1s,1H,79){q(!1s)D;u z=k,T=k.T,x=k.x,y=k.y,2w=T.x,2x=T.y,O=k.O,X=k.X,1g=k.1g;m.3K(Q,\'5x\',m.5j);m.S(X,{L:(x.1w||x.B)+\'F\',19:(y.1w||y.B)+\'F\'});q(1g)1g.G.31=\'1D\';k.W=T.W;q(k.W)k.W.z=z;T.W=H;u 44=m.1e(\'1E\',{16:\'V-\'+k.2K},{1j:\'2f\',1o:4,31:\'1u\',1I:\'1R\'});u 71={9D:T,9G:k};M(u n 2z 71){k[n]=71[n].X.6T(1);m.S(k[n],{1j:\'2f\',9P:0,1c:\'1D\'});44.2r(k[n])}O.2r(44);q(1g){1g.16=\'\';O.2r(1g)}44.G.1I=\'\';T.X.G.1I=\'1R\';q(m.4S&&m.1Z<6V){k.O.G.1c=\'1D\'}m.2g(O,{L:x.B},{3p:m.9Y,2X:A(2V,2U){u E=2U.E,3I=1-E;u R,B={},6z=[\'E\',\'B\',\'1m\',\'2p\'];M(u n 2z 6z){R=6z[n];B[\'x\'+R]=1b.2A(3I*2w[R]+E*x[R]);B[\'y\'+R]=1b.2A(3I*2x[R]+E*y[R]);B.9F=1b.2A(3I*(2w.1w||2w.B)+E*(x.1w||x.B));B.5r=1b.2A(3I*2w.K(\'22\')+E*x.K(\'22\'));B.9E=1b.2A(3I*(2x.1w||2x.B)+E*(y.1w||y.B));B.5s=1b.2A(3I*2x.K(\'22\')+E*y.K(\'22\'))}q(z.W)z.W.3E({x:B.2y,y:B.2F,w:B.4q+B.3w+B.6l+2*x.cb,h:B.4w+B.3s+B.6j+2*y.cb});T.O.G.bX=\'c1(\'+(B.2F-2x.E)+\'F, \'+(B.4q+B.3w+B.6l+B.2y+2*2w.cb-2w.E)+\'F, \'+(B.4w+B.3s+B.6j+B.2F+2*2x.cb-2x.E)+\'F, \'+(B.2y-2w.E)+\'F)\';m.S(X,{14:(B.3s+y.K(\'22\'))+\'F\',1a:(B.3w+x.K(\'22\'))+\'F\',4o:(y.E-B.2F)+\'F\',4k:(x.E-B.2y)+\'F\'});m.S(O,{14:B.2F+\'F\',1a:B.2y+\'F\',L:(B.3w+B.6l+B.4q+2*x.cb)+\'F\',19:(B.3s+B.6j+B.4w+2*y.cb)+\'F\'});m.S(44,{L:(B.9F||B.4q)+\'F\',19:(B.9E||B.4w)+\'F\',1a:(B.3w+B.5r)+\'F\',14:(B.3s+B.5s)+\'F\',1c:\'1D\'});m.S(z.9D,{14:(2x.E-B.2F+2x.1m-B.3s+2x.K(\'22\')-B.5s)+\'F\',1a:(2w.E-B.2y+2w.1m-B.3w+2w.K(\'22\')-B.5r)+\'F\'});m.S(z.9G,{1h:E,14:(y.E-B.2F+y.1m-B.3s+y.K(\'22\')-B.5s)+\'F\',1a:(x.E-B.2y+x.1m-B.3w+x.K(\'22\')-B.5r)+\'F\'});q(1g)m.S(1g,{L:B.4q+\'F\',19:B.4w+\'F\',1a:(B.3w+x.cb)+\'F\',14:(B.3s+y.cb)+\'F\'})},5q:A(){O.G.1c=X.G.1c=\'1D\';X.G.1I=\'7v\';m.3R(44);z.5f();T.58();z.T=H}})},ak:A(o,C){q(!k.T)D 1d;M(u i=0;i<k.T.1z.Y;i++){u 5u=m.$(\'1L\'+k.T.1z[i]);q(5u&&5u.1L==o.1L){k.7A();5u.bY=k.N;m.2l(k.1z,k.T.1z[i]);D J}}D 1d},5f:A(){k.55=J;k.4d();q(k.3u)m.1i(k);q(m.1N&&m.1N==k.a)m.1N=H;k.9I();u p=m.3H,7g=m.5y.x+p.4P,7M=m.5y.y+p.4Q;k.7h=k.x.E<7g&&7g<k.x.E+k.x.K(\'1F\')&&k.y.E<7M&&7M<k.y.E+k.y.K(\'1F\');q(k.1g)k.8Z()},9I:A(){u N=k.N;u 1x=k.1x;1U m.4V(1x,A(){1S{m.U[N].9K()}1W(e){}})},9K:A(){u 1k=k.7R(1);q(1k&&1k.30.9L().2B(/m\\.2s/))u 1t=m.1e(\'1t\',{1y:m.7G(1k)})},7R:A(1K){u 7O=k.5T(),as=m.47.2L[k.2n||\'1R\'];q(as&&!as[7O+1K]&&k.1r&&k.1r.8N){q(1K==1)D as[0];I q(1K==-1)D as[as.Y-1]}D(as&&as[7O+1K])||H},5T:A(){u 2m=m.5e().2L[k.2n||\'1R\'];q(2m)M(u i=0;i<2m.Y;i++){q(2m[i]==k.a)D i}D H},7T:A(){q(k[k.59]){u 2m=m.47.2L[k.2n||\'1R\'];q(2m){u s=m.Z.3e.23(\'%1\',k.5T()+1).23(\'%2\',2m.Y);k[k.59].3A=\'<1E 3f="V-3e">\'+s+\'</1E>\'+k[k.59].3A}}},9H:A(){q(!k.T){M(u i=0;i<m.4W.Y;i++){u 1v=m.4W[i],2v=1v.2n;q(1n 2v==\'1T\'||2v===H||2v===k.2n)k.1r=1U m.7K(k.N,1v)}}I{k.1r=k.T.1r}u 1v=k.1r;q(!1v)D;u N=1v.41=k.N;1v.92();1v.3Z(\'11-2s\');q(1v.2t){k.4e(m.2Z(1v.9B||{},{4f:1v.2t,1L:\'2t\',1o:5}))}q(!k.T&&k.3t)1v.35(J);q(1v.3t){1v.3t=4i(A(){m.1k(N)},(1v.c0||bW))}},6a:A(){m.3R(k.O);m.U[k.N]=H;q(m.1N==k.a)m.1N=H;m.7J(k.N);q(k.1O)m.1O.G.1a=\'-52\'},83:A(){q(k.4I)D;k.4I=m.1e(\'a\',{2k:m.9M,20:m.9N,16:\'V-4I\',3A:m.Z.9V,26:m.Z.9W});k.4e({4f:k.4I,1j:k.9X||\'14 1a\',1L:\'4I\'})},8o:A(7P,9A){M(u i=0;i<7P.Y;i++){u P=7P[i],s=H;q(!k[P+\'4j\']&&k.5H)k[P+\'4j\']=P+\'-M-\'+k.5H;q(k[P+\'4j\'])k[P]=m.4B(k[P+\'4j\']);q(!k[P]&&!k[P+\'7j\']&&k[P+\'9T\'])1S{s=bS(k[P+\'9T\'])}1W(e){}q(!k[P]&&k[P+\'7j\']){s=k[P+\'7j\']}q(!k[P]&&!s){k[P]=m.4B(k.a[\'9Q\'+P+\'4j\']);q(!k[P]){u 1k=k.a.9R;4g(1k&&!m.5l(1k)){q((1U 5k(\'V-\'+P)).18(1k.16||H)){q(!1k.1G)k.a[\'9Q\'+P+\'4j\']=1k.1G=\'1L\'+m.4K++;k[P]=m.4B(1k.1G);5h}1k=1k.9R}}}q(!k[P]&&!s&&k.59==P)s=\'\\n\';q(!k[P]&&s)k[P]=m.1e(\'1E\',{16:\'V-\'+P,3A:s});q(9A&&k[P]){u o={1j:(P==\'5v\')?\'4X\':\'5J\'};M(u x 2z k[P+\'9z\'])o[x]=k[P+\'9z\'][x];o.4f=k[P];k.4e(o)}}},4D:A(1c){q(m.9k)k.5t(\'bU\',1c);q(m.9g)k.5t(\'bV\',1c);q(m.5o)k.5t(\'*\',1c)},5t:A(3X,1c){u 1f=Q.36(3X);u R=3X==\'*\'?\'31\':\'1c\';M(u i=0;i<1f.Y;i++){q(R==\'1c\'||(Q.7m.9e(1f[i],"").9m(\'31\')==\'2q\'||1f[i].9n(\'1u-by\')!=H)){u 2e=1f[i].9n(\'1u-by\');q(1c==\'1D\'&&2e){2e=2e.23(\'[\'+k.N+\']\',\'\');1f[i].4v(\'1u-by\',2e);q(!2e)1f[i].G[R]=1f[i].7a}I q(1c==\'1u\'){u 2Y=m.5g(1f[i]);2Y.w=1f[i].4Y;2Y.h=1f[i].3Y;q(!k.3u){u 9x=(2Y.x+2Y.w<k.x.K(\'5a\')||2Y.x>k.x.K(\'5a\')+k.x.K(\'7F\'));u 9y=(2Y.y+2Y.h<k.y.K(\'5a\')||2Y.y>k.y.K(\'5a\')+k.y.K(\'7F\'))}u 5p=m.7b(1f[i]);q(!9x&&!9y&&5p!=k.N){q(!2e){1f[i].4v(\'1u-by\',\'[\'+k.N+\']\');1f[i].7a=1f[i].G[R];1f[i].G[R]=\'1u\'}I q(2e.9p(\'[\'+k.N+\']\')==-1){1f[i].4v(\'1u-by\',2e+\'[\'+k.N+\']\')}}I q((2e==\'[\'+k.N+\']\'||m.3h==5p)&&5p!=k.N){1f[i].4v(\'1u-by\',\'\');1f[i].G[R]=1f[i].7a||\'\'}I q(2e&&2e.9p(\'[\'+k.N+\']\')>-1){1f[i].4v(\'1u-by\',2e.23(\'[\'+k.N+\']\',\'\'))}}}}},4d:A(){k.O.G.1o=m.45+=2;M(u i=0;i<m.U.Y;i++){q(m.U[i]&&i==m.3h){u 4r=m.U[i];4r.X.16+=\' V-\'+4r.2K+\'-7e\';4r.X.G.3U=m.3W?\'9s\':\'5Q\';4r.X.26=m.Z.9r}}q(k.W)k.W.2I.G.1o=k.O.G.1o-1;k.X.16=\'V-\'+k.2K;k.X.26=m.Z.7p;q(m.4y){m.43=1A.3j?\'5Q\':\'5Z(\'+m.4p+m.4y+\'), 5Q\';q(m.3W&&m.1Z<6)m.43=\'9s\';k.X.G.3U=m.43}m.3h=k.N;m.1X(Q,1A.3j?\'63\':\'66\',m.57)},7E:A(x,y){k.x.7y(x);k.y.7y(y)},3r:A(e){u w,h,r=e.L/e.19;w=1b.4z(e.L+e.5c,1b.2O(k.4O,k.x.11));q(k.3k&&1b.c3(w-k.x.11)<12)w=k.x.11;h=w/r;q(h<1b.2O(k.4L,k.y.11)){h=1b.2O(k.4L,k.y.11);q(k.3k)w=h*r}k.7i(w,h)},7i:A(w,h){k.y.7D(h);k.x.7D(w);k.O.G.19=k.y.K(\'1F\')+\'F\'},1Q:A(){q(k.7x||!k.55)D;q(k.2P[1]==\'3O\'&&m.1N){m.2C(m.1N).6a();m.1N=H}k.7x=J;q(k.1r&&!m.1N)k.1r.2G();m.3K(Q,1A.3j?\'63\':\'66\',m.57);1S{k.X.G.3U=\'c5\';k.7r(0,{O:{L:k.x.t,19:k.y.t,1a:k.x.1J-k.x.cb+k.x.3a,14:k.y.1J-k.y.cb+k.y.3a},X:{1a:0,14:0,L:k.x.t,19:k.y.t}},m.7q)}1W(e){k.58()}},4e:A(o){u C=o.4f;q(1n C==\'7t\')C=m.4B(C);q(o.6b)C=m.1e(\'1E\',{3A:o.6b});q(!C||1n C==\'7t\')D;C.G.1I=\'7v\';o.1L=o.1L||o.4f;q(k.2P[1]==\'3O\'&&k.ak(o,C))D;k.7A();u L=o.L&&/^[0-9]+(F|%)$/.18(o.L)?o.L:\'2q\';q(/^(1a|2M)7f$/.18(o.1j)&&!/^[0-9]+F$/.18(o.L))L=\'bB\';u 17=m.1e(\'1E\',{1G:\'1L\'+m.4K++,1L:o.1L},{1j:\'2f\',1c:\'1u\',L:L,ag:m.Z.ah||\'\',1h:0},k.1g,J);17.2r(C);m.2Z(17,{1h:1,94:0,8d:0,3o:(o.67===0||o.67===1d||(o.67==2&&m.2d))?0:4x});m.2Z(17,o);q(k.8l){k.5I(17);q(!17.5B||k.7h)m.2g(17,{1h:17.1h},17.3o)}m.2l(k.1z,m.4K-1)},5I:A(17){u p=17.1j||\'7u 3T\',68=17.94,6e=17.8d;q(17.3m!=k.1g)k.1g.2r(17);q(/1a$/.18(p))17.G.1a=68+\'F\';q(/3T$/.18(p))m.S(17,{1a:\'50%\',4k:(68-1b.2A(17.4Y/2))+\'F\'});q(/2M$/.18(p))17.G.2M=-68+\'F\';q(/^8f$/.18(p)){m.S(17,{2M:\'28%\',8g:k.x.cb+\'F\',14:-k.y.cb+\'F\',3C:-k.y.cb+\'F\',31:\'2q\'});k.x.1m=17.4Y}I q(/^9a$/.18(p)){m.S(17,{1a:\'28%\',4k:k.x.cb+\'F\',14:-k.y.cb+\'F\',3C:-k.y.cb+\'F\',31:\'2q\'});k.x.2p=17.4Y}q(/^14/.18(p))17.G.14=6e+\'F\';q(/^7u/.18(p))m.S(17,{14:\'50%\',4o:(6e-1b.2A(17.3Y/2))+\'F\'});q(/^3C/.18(p))17.G.3C=-6e+\'F\';q(/^4X$/.18(p)){m.S(17,{1a:(-k.x.1m-k.x.cb)+\'F\',2M:(-k.x.2p-k.x.cb)+\'F\',3C:\'28%\',8b:k.y.cb+\'F\',L:\'2q\'});k.y.1m=17.3Y}I q(/^5J$/.18(p)){m.S(17,{1j:\'7s\',1a:(-k.x.1m-k.x.cb)+\'F\',2M:(-k.x.2p-k.x.cb)+\'F\',14:\'28%\',4o:k.y.cb+\'F\',L:\'2q\'});k.y.2p=17.3Y;17.G.1j=\'2f\'}},8p:A(){k.8o([\'5v\',\'bD\'],J);k.7T();q(k.5v&&k.7w)k.5v.16+=\' V-3b\';q(m.7S)k.83();M(u i=0;i<m.1z.Y;i++){u o=m.1z[i],5F=o.7B,2v=o.2n;q((!5F&&!2v)||(5F&&5F==k.5H)||(2v&&2v===k.2n)){k.4e(o)}}u 5K=[];M(u i=0;i<k.1z.Y;i++){u o=m.$(\'1L\'+k.1z[i]);q(/7f$/.18(o.1j))k.5I(o);I m.2l(5K,o)}M(u i=0;i<5K.Y;i++)k.5I(5K[i]);k.8l=J},7A:A(){q(!k.1g)k.1g=m.1e(\'1E\',{16:k.7z},{1j:\'2f\',L:(k.x.B||(k.2N?k.L:H)||k.x.11)+\'F\',19:(k.y.B||k.y.11)+\'F\',1c:\'1u\',31:\'1u\',1o:m.2d?4:\'2q\'},m.2h,J)},3V:A(7c,8W){u 1g=k.1g,x=k.x,y=k.y;m.S(1g,{L:x.B+\'F\',19:y.B+\'F\'});q(7c||8W){M(u i=0;i<k.1z.Y;i++){u o=m.$(\'1L\'+k.1z[i]);u 7o=(m.3W||Q.64==\'7C\');q(o&&/^(4X|5J)$/.18(o.1j)){q(7o){o.G.L=(1g.4Y+2*x.cb+x.1m+x.2p)+\'F\'}y[o.1j==\'4X\'?\'1m\':\'2p\']=o.3Y}q(o&&7o&&/^(1a|2M)7f$/.18(o.1j)){o.G.19=(1g.3Y+2*y.cb)+\'F\'}}}q(7c){m.S(k.X,{14:y.1m+\'F\'});m.S(1g,{14:(y.1m+y.cb)+\'F\'})}},8Z:A(){u b=k.1g;b.16=\'\';m.S(b,{14:(k.y.1m+k.y.cb)+\'F\',1a:(k.x.1m+k.x.cb)+\'F\',31:\'1D\'});q(m.4S)b.G.1c=\'1D\';k.O.2r(b);M(u i=0;i<k.1z.Y;i++){u o=m.$(\'1L\'+k.1z[i]);o.G.1o=o.1o||4;q(!o.5B||k.7h){o.G.1c=\'1D\';m.S(o,{1c:\'1D\',1I:\'\'});m.2g(o,{1h:o.1h},o.3o)}}},7n:A(){q(!k.1z.Y)D;q(k.1r){u c=k.1r.2t;q(c&&m.2C(c)==k)c.3m.cC(c)}m.3R(k.1g)},8y:A(){q(k.1r&&k.1r.2t){k.1r.3G(\'11-2s\');D}k.5L=m.1e(\'a\',{2k:\'8x:m.U[\'+k.N+\'].5P();\',26:m.Z.7k,16:\'V-11-2s\'});k.4e({4f:k.5L,1j:m.8M,5B:J,1h:m.8I})},5P:A(){1S{q(k.5L)m.3R(k.5L);k.4d();u 2u=k.x.B,2T=k.y.B;k.7i(k.x.11,k.y.11);u 2y=k.x.E-(k.x.B-2u)/2;q(2y<m.4k)2y=m.4k;u 2F=k.y.E-(k.y.B-2T)/2;q(2F<m.4o)2F=m.4o;k.7E(2y,2F);k.4D(\'1u\')}1W(e){k.7H(e)}},58:A(){k.a.16=k.a.16.23(\'V-8A-4l\',\'\');k.4D(\'1D\');q(k.W&&k.3x)k.W.4J();m.3R(k.O);q(k.3u)m.7J(k.N);m.U[k.N]=H;m.8T()}};m.7K=A(41,1p){q(m.cs!==1d)m.7Q();k.41=41;M(u x 2z 1p)k[x]=1p[x];q(k.cl)k.8a()};m.7K.4N={8a:A(){k.2t=m.1e(\'1E\',{3A:m.8h(m.81.2t)},H,m.2h);u 4Z=[\'35\',\'2G\',\'2R\',\'1k\',\'3b\',\'11-2s\',\'1Q\'];k.1q={};u 7N=k;M(u i=0;i<4Z.Y;i++){k.1q[4Z[i]]=m.8t(k.2t,\'1V\',\'V-\'+4Z[i]);k.3G(4Z[i])}k.1q.2G.G.1I=\'1R\'},92:A(){q(k.8N||!k.2t)D;u z=m.U[k.41],4a=z.5T(),1M=/5N$/;q(4a==0)k.3Z(\'2R\');I q(1M.18(k.1q.2R.36(\'a\')[0].16))k.3G(\'2R\');q(4a+1==m.47.2L[z.2n||\'1R\'].Y){k.3Z(\'1k\');k.3Z(\'35\')}I q(1M.18(k.1q.1k.36(\'a\')[0].16)){k.3G(\'1k\');k.3G(\'35\')}},3G:A(1q){q(!k.1q)D;u 8F=k,a=k.1q[1q].36(\'a\')[0],1M=/5N$/;a.30=A(){8F[1q]();D 1d};q(1M.18(a.16))a.16=a.16.23(1M,\'\')},3Z:A(1q){q(!k.1q)D;u a=k.1q[1q].36(\'a\')[0];a.30=A(){D 1d};q(!/5N$/.18(a.16))a.16+=\' 5N\'},8H:A(){q(k.3t)k.2G();I k.35()},35:A(8z){q(k.1q){k.1q.35.G.1I=\'1R\';k.1q.2G.G.1I=\'\'}k.3t=J;q(!8z)m.1k(k.41)},2G:A(){q(k.1q){k.1q.2G.G.1I=\'1R\';k.1q.35.G.1I=\'\'}aQ(k.3t);k.3t=H},2R:A(){k.2G();m.2R(k.1q.2R)},1k:A(){k.2G();m.1k(k.1q.1k)},3b:A(){},\'11-2s\':A(){m.2C().5P()},1Q:A(){m.1Q(k.1q.1Q)}};m.5R=m.Z;u ap=m.4M;q(m.2d&&1A==1A.14){(A(){1S{Q.4h.aZ(\'1a\')}1W(e){4i(a4.aN,50);D}m.3z()})()}m.1X(Q,\'b3\',m.3z);m.1X(1A,\'9j\',m.3z);m.1X(Q,\'3z\',A(){q(m.5S||m.3u){u G=m.1e(\'G\',{P:\'cq/7l\'},H,Q.36(\'c4\')[0]),7I=Q.64==\'7C\';A 4E(6L,6N){q(m.2d&&(m.1Z<9||7I)){u T=Q.9C[Q.9C.Y-1];q(1n(T.4E)=="5i")T.4E(6L,6N)}I{G.2r(Q.av(6L+" {"+6N+"}"))}}A 4F(R){D\'bR( ( ( bZ = Q.4h.\'+R+\' ? Q.4h.\'+R+\' : Q.3i.\'+R+\' ) ) + \\\'F\\\' );\'}q(m.5S)4E(\'.V 1t\',\'3U: 5Z(\'+m.4p+m.5S+\'), 5Q !aM;\');4E(\'.V-8G-B\',m.2d&&(m.1Z<7||7I)?\'1j: 2f; \'+\'1a:\'+4F(\'4P\')+\'14:\'+4F(\'4Q\')+\'L:\'+4F(\'7L\')+\'19:\'+4F(\'91\'):\'1j: ci; L: 28%; 19: 28%; 1a: 0; 14: 0\')}});m.1X(1A,\'3r\',A(){m.5A()});m.1X(Q,\'5x\',A(e){m.5y={x:e.5E,y:e.65}});m.1X(Q,\'aj\',m.7d);m.1X(Q,\'a2\',m.7d);m.1X(Q,\'3z\',m.5e);m.1X(1A,\'9j\',m.9J)}',62,786,'||||||||||||||||||||this||hs||||if||||var|||||exp|function|size|el|return|pos|px|style|null|else|true|get|width|for|key|wrapper|type|document|prop|setStyles|last|expanders|highslide|outline|content|length|lang||full|||top||className|overlay|test|height|left|Math|visibility|false|createElement|els|overlayBox|opacity|dim|position|next|offset|p1|typeof|zIndex|options|btn|slideshow|up|img|hidden|ss|imgSize|outlineType|src|overlays|window|fx|case|visible|div|wsize|id|to|display|tpos|op|hsId|re|upcoming|loading|dimmer|close|none|try|undefined|new|li|catch|addEventListener|event|uaVersion|target|span|imgPad|replace|scroll||title||100|easing|minSize|justify|td|ie|hiddenBy|absolute|animate|container|tgt|ratio|href|push|arr|slideshowGroup|elem|p2|auto|appendChild|expand|controls|xSize|sg|lastX|lastY|xpos|in|round|match|getExpander|dragArgs|image|ypos|pause|params|table|marginMin|contentType|groups|right|useBox|min|transitions|name|previous|opt|ySize|args|val|func|step|elPos|extend|onclick|overflow||||play|getElementsByTagName||||tb|move|graphic|ucwh|number|class|changed|focusKey|body|opera|isImage|timers|parentNode|custom|dur|duration|wh|resize|yp1|autoplay|dimmingOpacity|hasDragged|xp1|outlineWhileAnimating|styles|ready|innerHTML|start|bottom|clone|setPosition|pendingOutlines|enable|page|invPos|onLoad|removeEventListener|ieLt9|clientSize|unit|crossfade|tgtArr|now|discardElement|marginMax|center|cursor|sizeOverlayBox|ieLt7|tagName|offsetHeight|disable||expKey|attribs|styleRestoreCursor|fadeBox|zIndexCounter|allowReduce|anchors|fitsize|moveOnly|cur|owner|images|focus|createOverlay|overlayId|while|documentElement|setTimeout|Id|marginLeft|anchor|end|navigator|marginTop|graphicsDir|xsize|blurExp|matches|iebody|uclt|setAttribute|ysize|250|restoreCursor|max|allowSizeReduction|getNode|on|doShowHide|addRule|fix|getParams|preloadTheseImages|credits|destroy|idCounter|minHeight|Expander|prototype|minWidth|scrollLeft|scrollTop|getParam|safari|fac|trans|Outline|slideshows|above|offsetWidth|buttons||userAgent|9999px|param|padToMinWidth|isExpanded|all|keyHandler|afterClose|numberPosition|opos|filter|dX|onLoadStarted|getAnchors|afterExpand|getPosition|break|object|dragHandler|RegExp|isHsAnchor|onload|dir|geckoMac|wrapperKey|complete|ximgPad|yimgPad|showHideElements|oDiv|heading|self|mousemove|mouse|onReady|getPageSize|hideOnMouseOut|preloadFullImage|gotoEnd|clientX|tId|tr|thumbsUserSetId|positionOverlay|below|os|fullExpandLabel|over|disabled|topmostKey|doFullExpand|pointer|langDefaults|expandCursor|getAnchorIndex|node|preventDefault|relatedTarget|expOnly|previousOrNext|url|adj|toLowerCase|curAnim|keypress|compatMode|clientY|keydown|fade|offX|lt|cancelLoading|html|Dimension|maxWidth|offY|connectOutline|loadingPos|dY|srcElement|yp2|distance|xp2|element|Create|hasFocused|loadingPosXfade|parent|calcExpanded|calcBorders|arrow|Click|parseFloat|topZ|pixDimmerSize|isNew|props|clones|oPos|dimmingDuration|numberOfImagesToPreload|state|update|maxsize|startTime|expandDuration|done|other|sel|ucrb|dec|align|maxHeight|tbody|showLoading|endOff|cloneNode|init|525|overrides|hasMovedMin|hasAlphaImageLoader|garbageBin||names|margin|startOff|isReady||openerTagNames|continuePreloading|calcThumb|from|origProp|getWrapperKey|doWrapper|mouseClickHandler|blur|panel|mX|mouseIsOver|resizeTo|Text|fullExpandTitle|css|defaultView|destroyOverlays|ie6|restoreTitle|restoreDuration|changeSize|relative|string|middle|block|dragByHeading|isClosing|setPos|wrapperClassName|genOverlayBox|thumbnailId|BackCompat|setSize|moveTo|osize|getSrc|error|backCompat|undim|Slideshow|clientWidth|mY|pThis|current|types|updateAnchors|getAdjacentAnchor|showCredits|getNumber|targetY|vis|captionOverlay|appendTo|png|preloadGraphic|onGraphicLoad|skin|headingOverlay|writeCredits|dimmingGeckoFix|background|ul|moveTitle|rb|transit|getControls|marginBottom|toUpperCase|offsetY|nextText|leftpanel|marginRight|replaceLang|moveText|closeTitle|closeText|gotOverlays|targetX|fullExpandText|getInline|getOverlays|enableKeyListener|switch|hide|getElementByClass|offsetParent|easeInQuad|offsetLeft|javascript|createFullExpand|wait|active|alpha|pageXOffset|loadingOpacity|loadingText|sls|viewport|hitSpace|fullExpandOpacity|offsetTop|loadingTitle|ltr|fullExpandPosition|repeat|orig|_default|clickX|clickY|pow|reOrder|form|detachEvent|doPanels|focusTopmost|tag|showOverlays|timerId|clientHeight|checkFirstAndLast|Date|offsetX|nopad|call|getTime|rv||rightpanel|easingClose|fadeInOut|spacebar|getComputedStyle|show|hideIframes|Pause|Play|load|hideSelects|after|getPropertyValue|getAttribute|Next|indexOf|Move|focusTitle|hand|tmpMin|correctRatio|nextTitle|fitOverlayBox|clearsX|clearsY|Overlay|addOverlay|overlayOptions|styleSheets|oldImg|yimgSize|ximgSize|newImg|initSlideshow|prepareNextOutline|preloadImages|preloadNext|toString|creditsHref|creditsTarget|JS|border|_|nextSibling|Close|Eval|Highslide|creditsText|creditsTitle|creditsPosition|transitionDuration|parseInt|Previous|contentLoaded|mouseup|pageOrigin|arguments|thumb|playText|and|previousTitle|200|previousText|wrapperMouseHandler|playTitle|pauseTitle|allowMultipleInstances|pauseText|direction|cssDirection|fixedControls|mousedown|reuseOverlay|Go|the|dimming|Loading|HsExpander|_self|drop||shadow|Powered|createTextNode|actual|iPod|iPhone|front|keyCode|scrollWidth|com|geckodimmer|Expand|scrollHeight|iPad|cancel|bring|Android|1001|Trident|important|callee|drag|removeAttribute|clearTimeout|keys|Use|padding|it|of|Image|Gecko|Macintosh|doScroll|ra|Safari|click|DOMContentLoaded|currentStyle|zoomout|Resize|captionId|resizeTitle|esc|homepage|outlineStartOffset|getElementById|zoomin|graphics|innerWidth|headingText|headingEval|innerHeight|headingId|captionText|pageYOffset|captionEval|http|clearInterval|Right|Bottom|Top|Left|Height|readyState|isHtml|onmouseout|debug||onmouseover|allowSimultaneousLoading|200px|Width|caption|outlinesDir|outlines|fontSize|lineHeight|collapse|progid|DXImageTransform|stl|scale|AlphaImageLoader|Microsoft|alert|Line|expression|eval|preserveContent|SELECT|IFRAME|500|clip|reuse|ignoreMe|interval|rect|floor|abs|HEAD|default|location|message|lineNumber|imageCreate|blockRightClick||split|fit|flushImgSize|oncontextmenu|borderCollapse|sizingMethod|fixed|toElement|fromElement|useControls|attachEvent|linearTween|registerOverlay|addSlideshow|text|mouseover|dynamicallyUpdateAnchors|xpand|button|hasHtmlExpanders|sqrt|dragSensitivity|cellSpacing|htmlE|KDE|vendor|removeChild|setInterval|returnValue|splice'.split('|'),0,{}))

