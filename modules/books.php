<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class Books
{
	public $id_topic;
	
	public $subtypes;
	
	private $covers_thumb;
	private $covers_size;
	private $convert_format;
	private $cover_pub_path;
	private $id_res_type;
	
	function __construct()
	{
		$conf = new Configuration();
		$this->covers_thumb = $conf->Get("covers_thumb");
		$this->covers_size = $conf->Get("covers_size");
		$this->convert_format = $conf->Get("convert_format");
		$paths = $conf->Get("paths");
		$this->cover_pub_path = $paths['graphic'] . "covers";
		$this->id_res_type = "13";
		$this->subtypes = array('publisher','category','book');
		$ini = new Ini();
		$this->id_topic = $ini->GetModule("books","id_topic",0);
	}

	public function ArticleRemove($id_article)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books" );
		$sqlstr = "UPDATE books SET id_article=0 WHERE id_article='$id_article' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function BooksApproved( &$rows, $params )
	{
		if (isset($params['approved']))
			$cond .= " AND b.approved=" . $params['approved'];
		if (isset($params['id_c']))
			$cond .= " AND b.id_category=" . $params['id_c'];
		if (isset($params['id_p']))
			$cond .= " AND b.id_publisher=" . $params['id_p'];
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT b.id_book,b.title,p.name,b.author,p_year,p_month,bc.name AS category,b.id_category,b.id_publisher
		FROM books b
		LEFT JOIN publishers p ON b.id_publisher=p.id_publisher
		LEFT JOIN books_categories bc ON b.id_category=bc.id_category
		WHERE 1=1 $cond ORDER BY b.p_year DESC, b.p_month DESC,b.id_book DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function BooksAll($params=array(),$limit=0)
	{
		$cond = "";
		if ($params['id_publisher']>0)
			$cond .= " AND b.id_publisher={$params['id_publisher']}";
		if ($params['id_category']>0)
			$cond .= " AND b.id_category={$params['id_category']}";
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT b.id_book,b.title,p.name,b.author,b.p_year,b.p_month,b.summary,b.catalog,b.price,
		b.cover_format,'book' AS item_type,'$this->covers_thumb' AS covers_thumb,b.description,b.id_publisher,b.id_category
		FROM books b
		LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
		WHERE b.approved=1 $cond ";
		if($params['sort_by']!="random")
		{
			if($params['sort_by']=="name")
				$sqlstr .= "ORDER BY b.title ASC ";
			else
				$sqlstr .= "ORDER BY b.p_year DESC, b.p_month DESC, b.id_book DESC ";
		}
		if($limit>0)
			$sqlstr .= " LIMIT $limit ";
		$db->QueryExe($rows, $sqlstr);
		if($params['sort_by']=="random" && count($rows)>1)
			shuffle($rows);
		return $rows;
	}

	public function ConfigurationUpdate($path,$books_id_topic,$books_home_type,$books_reviews,$search_books,$show_reviews)
	{
		$ini = new Ini;
		$ini->SetPath("books_path",$path);
		$ini->SetModule("books","id_topic",$books_id_topic);
		$ini->SetModule("books","search_books",$search_books);
		$this->TopicConfigStore(0,$books_home_type,$books_reviews,$show_reviews);
	}
	
	public function TopicConfig($id_topic,$defaults=true)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_topic,books_home_type,books_reviews,show_reviews
				FROM books_config WHERE id_topic=$id_topic";
		$db->query_single($row,$sqlstr);
		if(count($row)==0 && $defaults)
			$row = array('id_topic'=>$id_topic,'books_home_type'=>2,'books_reviews'=>0,'show_reviews'=>4);
		return $row;
	}
	
	public function TopicConfigStore($id_topic,$books_home_type,$books_reviews,$show_reviews)
	{
		$row = $this->TopicConfig($id_topic,false);
		if($row['books_home_type']!=$books_home_type)
			$this->HomepageReset($id_topic);
		if(count($row)>0)
			$sqlstr = "UPDATE books_config SET books_home_type=$books_home_type,books_reviews=$books_reviews,
					show_reviews=$show_reviews WHERE id_topic=$id_topic ";
		else 
			$sqlstr = "INSERT INTO books_config (id_topic,books_home_type,books_reviews,show_reviews) 
					VALUES ($id_topic,$books_home_type,$books_reviews,$show_reviews) ";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books_config" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function HomepageCandidates(&$rows, $hometype,$id_topic)
	{
		$num = 0;
		switch($hometype)
		{
			case "1":
				$sqlstr = "SELECT p.id_publisher,p.name
				FROM publishers p 
				LEFT JOIN books_home bh ON p.id_publisher=bh.id_item AND bh.id_topic='$id_topic'
				WHERE bh.id_item IS NULL
				GROUP BY p.id_publisher ORDER BY p.name";
			break;
			case "2":
				$sqlstr = "SELECT b.id_book,b.title,b.author,b.catalog,p.name,bc.name AS category 
				FROM books b 
				LEFT JOIN books_home bh ON  bh.id_item=b.id_book AND bh.id_topic='$id_topic'
				LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
				LEFT JOIN books_categories bc ON b.id_category=bc.id_category
				WHERE bh.id_item IS NULL
				GROUP BY b.id_book ORDER BY b.id_book DESC";
			break;
		}
		if($hometype=="1" || $hometype=="2")
		{
			$db =& Db::globaldb();
			$num = $db->QueryExe($rows, $sqlstr, true);
		}
		return $num;
	}
	
	public function HomepageContent(&$rows, $id_topic, $hometype, $paged=true, $sort_by="title")
	{
		$num = 0;
		switch($hometype)
		{
			case "0":
				$sqlstr = "SELECT id_publisher,name,address,town,website,email,phone,fax,notes FROM publishers ORDER BY name";
			break;
			case "1":
				$sqlstr = "SELECT bh.id_item,p.name,COUNT(b.id_book) AS counter
				FROM books_home bh 
				INNER JOIN publishers p ON  bh.id_item=p.id_publisher
				LEFT JOIN books b ON p.id_publisher=b.id_publisher 
				WHERE bh.id_topic=$id_topic
				GROUP BY p.id_publisher ORDER BY p.name";
			break;
			case "2":
				$sqlstr = "SELECT bh.id_item,b.id_book,b.title,b.author,
				b.p_year,b.p_month,b.summary,b.catalog,b.price,b.id_publisher,b.id_category,
				b.cover_format,'book' AS item_type,'$this->covers_thumb' AS covers_thumb,b.description,
				p.name 
				FROM books_home bh 
				INNER JOIN books b ON  bh.id_item=b.id_book
				LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
				WHERE bh.id_topic=$id_topic ";
				if($sort_by=="date")
					$sqlstr .= "ORDER BY b.p_year DESC, b.p_month DESC, b.id_book DESC ";
				else
					$sqlstr .= "ORDER BY b.title ASC ";
			break;
		}
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, $paged);
		return $num;
	}
	
	public function HomepageDelete($id_item,$id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books_home" );
		$res[] = $db->query( "DELETE FROM books_home WHERE id_item='$id_item' AND id_topic='$id_topic' " );
		Db::finish( $res, $db);
	}
	
	private function HomepageReset($id_topic)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books_home" );
		$res[] = $db->query( "DELETE FROM books_home WHERE id_topic='$id_topic' " );
		Db::finish( $res, $db);
	}
	
	public function HomepageUpdate($id_item,$id_topic,$reset=false)
	{
		if($id_item>0)
		{
			if($reset)
				$this->HomepageReset();
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "books_home" );
			$sqlstr = "INSERT INTO books_home (id_item,id_topic) VALUES ($id_item,$id_topic)";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
	
	public function KeywordAdd($id_keyword,$role)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "INSERT INTO topic_keywords (id_topic,id_keyword,id_res_type,role) VALUES ('0','$id_keyword','$this->id_res_type','$role')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordDelete($id_keyword)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "DELETE FROM topic_keywords WHERE id_topic='0' AND id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordGet($id_keyword)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description,tk.id_res_type,tk.role 
			FROM topic_keywords tk 
			INNER JOIN keywords k ON tk.id_keyword=k.id_keyword 
			WHERE k.id_keyword='$id_keyword' AND id_topic='0' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	public function KeywordUpdate($id_keyword,$role)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "topic_keywords" );
		$sqlstr = "UPDATE topic_keywords SET id_res_type='$this->id_res_type',role='$role' 
			WHERE id_topic='0' AND id_keyword='$id_keyword' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function KeywordsInternal()
	{
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT tk.id_keyword,k.keyword,k.description,tk.id_res_type,tk.role 
			FROM topic_keywords tk
			INNER JOIN keywords k ON tk.id_keyword=k.id_keyword
			WHERE tk.id_topic='0' ";
		$sqlstr .= " AND (tk.id_res_type=0 OR tk.id_res_type='$this->id_res_type') ";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	public function KeywordsInternalAvailable(&$rows)
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT k.id_keyword,k.keyword,k.description FROM keywords k 
			WHERE k.id_type=4 AND k.id_keyword NOT IN (SELECT id_keyword FROM topic_keywords WHERE id_topic='0') ";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function RepublishCovers()
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$fm->DirAction("pub/$this->cover_pub_path","check");
		$fm->DirAction("pub/$this->cover_pub_path/$this->covers_thumb","check");
		$fm->DirAction("pub/$this->cover_pub_path/$this->covers_size","check");
		$rows = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_book FROM books WHERE cover_format<>'' ORDER BY id_book";
		$db->QueryExe($rows, $sqlstr);
		foreach($rows as $row)
		{
			$filename = "{$row['id_book']}.{$this->convert_format}";
			$fm->Copy("uploads/covers/$this->covers_thumb/$filename","pub/$this->cover_pub_path/$this->covers_thumb/$filename");
			$fm->Copy("uploads/covers/$this->covers_size/$filename","pub/$this->cover_pub_path/$this->covers_size/$filename");
		}
		$fm->PostUpdate();
		return count($rows);
	}
	
	public function Reviews( &$rows, $approved, $id_topic=0 )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_review,review,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,b.title,r.id_book,t.name AS topic_name
		 FROM reviews r 
		 INNER JOIN books b USING(id_book)
		 LEFT JOIN topics t ON r.id_topic=t.id_topic
		  WHERE r.approved=$approved ";
		if($id_topic>0)
			$sqlstr .= " AND r.id_topic=$id_topic ";
		$sqlstr .= " ORDER BY insert_date DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function ReviewsPerson($id_p,$id_topic=0)
	{
		$cond = ($id_topic>0)? " AND FALSE " : "";		
		$rows = array();
		$sqlstr = "SELECT id_review,review,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,
		b.title,r.id_book,UNIX_TIMESTAMP(r.insert_date) AS hdate_ts,
		p.name,b.author,p_year,p_month,bc.name AS category,b.id_category,b.id_publisher
		 FROM reviews r
		  INNER JOIN books b ON b.id_book=r.id_book $cond
		LEFT JOIN publishers p ON b.id_publisher=p.id_publisher
		LEFT JOIN books_categories bc ON b.id_category=bc.id_category
		   WHERE r.approved=1 AND r.id_p=$id_p
		 ORDER BY insert_date DESC";
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Search(&$rows,$word)
	{
		$db =& Db::globaldb();
		$word = $db->SqlQuote($word);
		if (strlen($word) > 0)
			$cond .= " WHERE (b.title LIKE '%$word%' OR b.summary LIKE '%$word%' OR b.author LIKE '%$word%' OR b.description LIKE '%$word%') ";
		$sqlstr = "SELECT b.id_book,b.title,p.name,b.author,b.p_year,b.p_month,b.summary,b.catalog,b.price,b.approved,
		b.cover_format,'book' AS item_type,'$this->covers_thumb' AS covers_thumb,b.description,b.id_publisher,b.id_category
		FROM books b
		LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
		$cond
		ORDER BY b.p_year DESC, b.p_month DESC, b.id_book DESC";
		return $db->QueryExe($rows, $sqlstr,true);
	}

	public function SearchPub(&$rows,$words)
	{
		if(count($words)>0)
		{
			$db =& Db::globaldb();
			$sqljoin = "";
			for ($i = 0; $i < count($words); $i++)
			{
				$sqljoin .= "INNER JOIN search_index2 si$i ON si$i.id = b.id_book AND si$i.id_res={$this->id_res_type} ";
				$sqljoin .= "INNER JOIN search_words sw$i ON sw$i.id_word = si$i.id_word ";
				$wheres[] = "sw$i.word = '$words[$i]'";
				$scores[] = "SUM(si$i.score)";
			}
			$sqlstr = "SELECT b.id_book,b.title,p.name,b.author,b.p_year,b.p_month,b.summary,b.catalog,b.price,
			b.cover_format,'book' AS item_type,'$this->covers_thumb' AS covers_thumb,b.description,b.id_publisher,b.id_category, 
				(" . implode("+",$scores) . ") AS total_score 
			FROM books b $sqljoin
			LEFT JOIN publishers p ON p.id_publisher=b.id_publisher
			 WHERE " . implode(" AND ", $wheres) . "
			 GROUP BY b.id_book
			ORDER BY total_score DESC,b.p_year DESC, b.p_month DESC";
			$num = $db->QueryExe($rows, $sqlstr,true);
		}
		else 
			$num = 0;
		return $num;
	}
}

class Publisher
{
	private $id_topic;
	
	function __construct()
	{
		$ini = new Ini;
		$this->id_topic = $ini->GetModule("books","id_topic",0);
	}

	public function CategoriesAll($id_publisher)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT bc.id_category,bc.name,bc.description,bc.id_image,bc.id_publisher,COUNT(b.id_book) AS books 
		FROM books_categories bc
		LEFT JOIN books b ON bc.id_category=b.id_category
		WHERE bc.id_publisher=$id_publisher GROUP BY bc.id_category ORDER BY bc.name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Categories( &$rows, $id_publisher )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT bc.id_category,bc.name,bc.description,bc.id_image,bc.id_publisher,COUNT(id_book) AS counter 
		FROM books_categories bc
		LEFT JOIN books b USING(id_category) 
		WHERE bc.id_publisher=$id_publisher GROUP BY bc.id_category";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	public function CategoryGet($id_category,$id_publisher)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_category,name,description,id_image,id_publisher FROM books_categories WHERE id_publisher=$id_publisher AND id_category=$id_category";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function CategoryInsert($name,$description,$id_image,$id_publisher)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books_categories" );
		$id_category = $db->nextId( "books_categories", "id_category" );
		$sqlstr = "INSERT INTO books_categories (id_category,name,description,id_image,id_publisher)
		 VALUES ($id_category,'$name','$description',$id_image,$id_publisher)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_category;
	}

	public function CategoryLookup($id_publisher,$category)
	{
		$id_category = 0;
		if(strlen($category)>0 && $id_publisher>0)
		{
			$db =& Db::globaldb();
			$category = $db->SqlQuote($category);
			$sqlstr = "SELECT id_category FROM books_categories WHERE name LIKE '$category' ";
			$row = array();
			$db->query_single($row,$sqlstr);
			if($row['id_category']>0)
				$id_category = $row['id_category'];
			else 
				$id_category = $this->CategoryInsert($category,"",0,$id_publisher);
		}
		return $id_category;
	}
	
	public function CategoryUpdate($name,$description,$id_image,$id_publisher,$id_category)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books_categories" );
		$sqlstr = "UPDATE books_categories SET name='$name',description='$description',id_image=$id_image,
		id_publisher=$id_publisher
		  WHERE id_category=$id_category";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function PublisherGet($id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_publisher,name,address,town,website,email,phone,fax,notes,zipcode,id_prov 
			FROM publishers WHERE id_publisher=$id";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function PublisherInsert( $name,$address,$town,$website,$email,$phone,$fax,$notes,$zipcode,$id_prov )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "publishers" );
		$id_publisher = $db->nextId( "publishers", "id_publisher" );
		$sqlstr = "INSERT INTO publishers (id_publisher,name,address,town,website,email,phone,fax,notes,zipcode,id_prov)
		 VALUES ($id_publisher,'$name','$address','$town','$website','$email','$phone','$fax','$notes','$zipcode',$id_prov)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function PublisherLookup($publisher)
	{
		$id_publisher = 0;
		if(strlen($publisher)>0)
		{
			$db =& Db::globaldb();
			$publisher = $db->SqlQuote($publisher);
			$sqlstr = "SELECT id_publisher FROM publishers WHERE name LIKE '$publisher' ";
			$row = array();
			$db->query_single($row,$sqlstr);
			if($row['id_publisher']>0)
				$id_publisher = $row['id_publisher'];
			else 
				$id_publisher = $this->PublisherInsert($publisher,"","","","","","","","",0);
		}
		return $id_publisher;
	}
	
	public function PublisherUpdate($id_publisher,$name,$address,$town,$website,$email,$phone,$fax,$notes,$zipcode,$id_prov)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "publishers" );
		$sqlstr = "UPDATE publishers SET name='$name',address='$address',town='$town',website='$website',
		email='$email',phone='$phone',fax='$fax',notes='$notes',zipcode='$zipcode',id_prov=$id_prov
		  WHERE id_publisher=$id_publisher";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function AllPublishers()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_publisher,name FROM publishers order by name";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function Publishers( &$rows, $but_id=0 )
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT p.id_publisher,p.name,town,COUNT(id_book) AS num_books
		FROM publishers p 
		LEFT JOIN books USING(id_publisher) ";
		if ($but_id>0)
			$sqlstr .= " WHERE p.id_publisher<>$but_id ";
		$sqlstr .= "GROUP BY p.id_publisher ORDER BY p.name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

}

class Book
{
	public $title;
	public $cover_format;

	private $id;
	private $cover_path;
	private $covers_size;
	private $covers_thumb;
	private $convert_format;
	private $cover_pub_path;

	function __construct($id_book)
	{
		$conf = new Configuration();
		$this->cover_path = "uploads/covers";
		$paths = $conf->Get("paths");
		$this->cover_pub_path = $paths['graphic'] . "covers";
		$this->covers_size = $conf->Get("covers_size");
		$this->covers_thumb = $conf->Get("covers_thumb");
		$this->convert_format = $conf->Get("convert_format");
		$this->id = $id_book;
		if ($id_book>0)
		{
			$book = array();
			$db =& Db::globaldb();
			$db->query_single($book,"SELECT title,cover_format FROM books WHERE id_book=$id_book");
			$this->title = $book['title'];
			$this->cover_format = $book['cover_format'];
		}
	}

	public function BookDelete()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books" );
		$res[] = $db->query( "DELETE FROM books WHERE id_book=$this->id" );
		Db::finish( $res, $db);
		$this->CoverRemove();
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($this->id,$o->types['book']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['book'],$this->id);
	}
	
	public function BookGet()
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT b.id_book,b.title,b.summary,b.description,isbn,id_publisher,p_year,price,b.id_language,b.notes,
			b.id_article,b.approved,cover_format,b.author,series,pages,catalog,b.id_category,b.p_month,'$this->covers_size' AS covers_size
			FROM books b
			WHERE id_book='$this->id' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function BookInsert( $title, $summary, $description, $id_publisher, $p_year, $isbn, $id_language, $notes, $price, $p_month, $approved, $id_article, $author, $series, $pages, $keywords, $catalog, $id_category, $keywords_internal )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books" );
		$id_book = $db->nextId( "books", "id_book" );
		$sqlstr = "INSERT INTO books (id_book,title,summary,description,id_publisher,p_year,isbn,id_language,notes,price,p_month,approved,id_article,author,series,pages,catalog,id_category)
		 VALUES ($id_book,'$title','$summary','$description',$id_publisher,$p_year,'$isbn',$id_language,'$notes',$price,$p_month,$approved,$id_article,'$author','$series',$pages,'$catalog',$id_category)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_book, $o->types['book']);
		$o->InsertKeywordsArray($keywords_internal,$id_book,$o->types['book']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($approved)
			$s->IndexQueueAdd($o->types['book'],$id_book,0,0,1);
	}

	public function BookUpdate($title, $summary, $description, $id_publisher, $p_year, $isbn, $id_language, $notes, $price, $p_month, $approved, $id_article, $author, $series, $pages, $keywords, $catalog, $id_category, $keywords_internal )
	{
		$book_old = $this->BookGet();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books" );
		$sqlstr = "UPDATE books SET title='$title',summary='$summary',description='$description',
		id_publisher=$id_publisher,p_year=$p_year,isbn='$isbn',id_language=$id_language,
		notes='$notes',price=$price,p_month=$p_month,approved=$approved,id_article=$id_article,author='$author',
		series='$series',pages=$pages,catalog='$catalog',id_category=$id_category
		  WHERE id_book=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['book']);
		$o->InsertKeywordsArray($keywords_internal,$this->id,$o->types['book']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		if($approved)
			$s->IndexQueueAdd($o->types['book'],$this->id,0,0,1);
		elseif($book_old['approved'])
			$s->ResourceRemove($o->types['book'],$this->id);
		if($id_article!=$book_old['id_article'])  // TODO detect changes in title and other fields displayed in article's page
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			$a = new Article($id_article);
			$a->ArticleLoad();
			if($a->id_topic>0)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($a->id_topic);
				$t->queue->JobInsert($t->queue->types['article'],$id_article,"update");
			}
		}
	}

	private function CoverConvert($format)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$origfile = "$this->cover_path/orig/" . $this->id . "." . $format;
		$i->ConvertWrapper("cover",$origfile,$this->id);
	}

	private function CoverRemove()
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$fm = new FileManager;
		if ($this->cover_format!="")
		{
			$fm->Delete("$this->cover_path/orig/$this->id".".$this->cover_format");
			$i->RemoveWrapper("cover",$this->id);
		}
	}

	public function CoverUpdate($file)
	{
		$fm = new FileManager;
		$this->CoverRemove();
		$this->SetCoverFormat($file['ext']);
		$fm->MoveUpload($file['temp'],"$this->cover_path/orig/$this->id" . "." . $file['ext']);
		$this->CoverConvert($file['ext']);
		$filename = $this->id . "." . $this->convert_format;
		$fm->Copy("$this->cover_path/$this->covers_thumb/$filename","pub/$this->cover_pub_path/$this->covers_thumb/$filename");
		$fm->Copy("$this->cover_path/$this->covers_size/$filename","pub/$this->cover_pub_path/$this->covers_size/$filename");
		$fm->PostUpdate();
	}

	public function CoverDelete()
	{
		$this->CoverRemove();
		$this->SetCoverFormat("");
	}

	public function ReviewDelete($id_review)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "reviews" );
		$res[] = $db->query( "DELETE FROM reviews WHERE id_review=$id_review" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove(18,$id_review);
	}

	public function ReviewGet($id)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT review,vote,name,email,approved,UNIX_TIMESTAMP(insert_date) AS insert_date_ts,important,id_p,
			rparams,id_language,id_topic,id_book
		FROM reviews WHERE id_review=$id";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function ReviewInsert( $review,$vote,$name,$email,$insert_date,$ip,$approved,$important,$id_p,$id_language,$unescaped_params,$id_topic )
	{
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		$ser_params = $r->ParamsUpdate($unescaped_params,"");
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "reviews" );
		$id_review = $db->nextId( "reviews", "id_review" );
		$res[] = $db->query( "INSERT INTO reviews (id_review,review,id_book,vote,name,email,insert_date,ip,approved,important,id_p,rparams,id_language,id_topic)
		 VALUES ($id_review,'$review',$this->id,$vote,'$name','$email','$insert_date','$ip','$approved','$important','$id_p','$ser_params','$id_language','$id_topic')" );
		Db::finish( $res, $db);
		$book = $this->BookGet();
		if($approved && $book['approved'])
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd(18,$id_review,0,0,1);
		}
	}

	public function ReviewInsertPublic( $review,$vote,$name,$email,$id_p,$id_language,$unescaped_params,$id_topic )
	{
		$db =& Db::globaldb();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$this->ReviewInsert($review,$vote,$name,$email,$db->getTodayTime(),Varia::IP(),0,0,$id_p,$id_language,$unescaped_params,$id_topic);
	}

	public function ReviewUpdate($id_review,$review,$vote,$name,$email,$insert_date,$ip,$approved,$important,$id_language,$unescaped_params,$id_topic)
	{
		$row = $this->ReviewGet($id_review);
		include_once(SERVER_ROOT."/../classes/resources.php");
		$r = new Resources();
		$ser_params = $r->ParamsUpdate($unescaped_params,$row['rparams']);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "reviews" );
		$res[] = $db->query( "UPDATE reviews SET review='$review',vote=$vote,name='$name',email='$email',
			insert_date='$insert_date',ip='$ip',approved=$approved,important=$important,rparams='$ser_params',
			id_language='$id_language',id_topic='$id_topic' 
			WHERE id_review=$id_review" );
		Db::finish( $res, $db);
		$book = $this->BookGet();
		if($approved && $book['approved'])
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd(18,$id_review,0,0,1);
		}
		elseif($row['approved'])
			$s->ResourceRemove(18,$id_review);
	}
	
	public function Reviews( &$rows,$only_important,$only_approved,$id_topic,$topic_only,$id_language,$paged=true )
	{
		$db =& Db::globaldb();
		$sqlstr = "SELECT r.id_review,r.review,UNIX_TIMESTAMP(r.insert_date) AS insert_date_ts,r.name,r.vote,
			r.approved,r.important,r.rparams,t.name AS topic_name 
			FROM reviews r
			LEFT JOIN topics t ON r.id_topic=t.id_topic
			WHERE r.id_book='$this->id' ";
		if($only_important)
			$sqlstr .= " AND r.important=1 AND r.approved=1 ";
		if($only_approved)
			$sqlstr .= " AND r.approved=1 ";
		if($id_language>0)
			$sqlstr .= " AND r.id_language=$id_language ";
		if($id_topic >0)
		{
			if($topic_only)
				$sqlstr .= " AND r.id_topic=$id_topic ";
			else 
				$sqlstr .= " AND (r.id_topic=$id_topic OR r.id_topic=0 )";
		}
		$sqlstr .= " ORDER BY r.insert_date DESC";
		return $db->QueryExe($rows, $sqlstr,$paged);
	}

	private function SetCoverFormat($format)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "books" );
		$sqlstr = "UPDATE books SET cover_format='$format' WHERE id_book=$this->id";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

}
?>
