<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
<xsl:variable name="id_logo" select="'1'"/>
	
<!-- ###############################
     ROOT
     ############################### -->
<xsl:template name="root">
<html xmlns:og="http://opengraphprotocol.org/schema/" >
<xsl:attribute name="lang">
<xsl:choose>
<xsl:when test="/root/topic"><xsl:value-of select="/root/topic/@lang"/></xsl:when>
<xsl:otherwise><xsl:value-of select="/root/site/@lang"/></xsl:otherwise>
</xsl:choose>
</xsl:attribute>
<head>
<xsl:call-template name="head"/>
</head>
<body id="id{/root/publish/@id}">
<xsl:attribute name="class"><xsl:value-of select="/root/publish/@type"/><xsl:if test="string-length(/root/publish/@subtype) &gt;0"><xsl:text> </xsl:text><xsl:value-of select="concat(/root/publish/@type,'-',/root/publish/@subtype)"/></xsl:if></xsl:attribute>
<xsl:if test="/root/preview"><xsl:call-template name="previewToolbar"/></xsl:if>
<div id="main-wrap" >
<div id="top-bar"><xsl:call-template name="topBar" /></div>
<div id="top-nav"><xsl:call-template name="topNav"/></div>
<div id="main">
<xsl:choose>
<xsl:when test="$pagetype='homepage' and /root/publish/@widgets">
<div id="center-widgets"><xsl:call-template name="content" /></div>
</xsl:when>
<xsl:otherwise>
<div id="left-bar"><xsl:call-template name="leftBar" /></div>
<div id="center"><xsl:call-template name="content" /></div>
<div id="right-bar"><xsl:call-template name="rightBar" /></div>
</xsl:otherwise>
</xsl:choose>
</div>
<div id="bottom-bar"><xsl:call-template name="bottomBar" /></div>
</div>
<xsl:call-template name="tracking"/>
</body>
</html>
</xsl:template>


<!-- ###############################
     LEFT BOTTOM
     ############################### -->
<xsl:template name="leftBottom">
	<xsl:if test="/root/publish/@global='1'">
		<xsl:apply-templates select="/root/features/feature[@id_function='3']"/>
	</xsl:if>
	<xsl:if test="/root/topic/lists/list/@feed">
		<xsl:call-template name="topicList"/>
	</xsl:if>
	<xsl:call-template name="userInfo"/>
</xsl:template>


<!-- ###############################
 CSS CUSTOM
 ############################### -->
<xsl:template name="cssCustom">
<!-- TO INCLUDE CUSTOM CSS
<link type="text/css" rel="stylesheet" href="{$css_url}/0/custom_123.css" media="screen"/>
-->

<!-- TO INCLUDE CSS EXTENSION
<link type="text/css" rel="stylesheet" href="{$css_url}/{/root/publish/@style}/custom_123.css" media="screen"/>
-->

<!-- TO INCLUDE CONDITIONAL CUSTOM CSS - EXAMPLE FOR IE6 AND STYLE 0
<xsl:comment><![CDATA[[if lt IE 7]><link rel="stylesheet" type="text/css" media="screen" href="]]><xsl:value-of select="$css_url"/>/0/custom_123.css<![CDATA[" /><![endif]]]></xsl:comment>
-->

<!-- TO INCLUDE CONDITIONAL CUSTOM CSS - EXAMPLE FOR IE6 AND SPECIFIC STYLE
<xsl:comment><![CDATA[[if lt IE 7]><link rel="stylesheet" type="text/css" media="screen" href="]]><xsl:value-of select="$css_url"/>/<xsl:value-of select="/root/publish/@style"/>/custom_123.css<![CDATA[" /><![endif]]]></xsl:comment>
-->

</xsl:template>	


<!-- ###############################
 JAVASCRIPT CUSTOM
 ############################### -->
<xsl:template name="javascriptCustom">
<!-- 
// TO INCLUDE CUSTOM JAVASCRIPT
<script type="text/javascript" src="{/root/site/@base}/js/s{/root/publish/@style}_123.js"></script>

// To destroy session data when user leaves the site
<script type="text/javascript">
$(window).unload(function() { $.get('/js/destroy_session.php', null, function(data, status) { }); });
</script>
-->
</xsl:template>	


</xsl:stylesheet>
