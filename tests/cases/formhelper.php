<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/formhelper.php");

/**
 * Unit Tests for FormHelper class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class FormHelperTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var FormHelper */
	private $obj;
	
	/**
	 * Variables to sanitize
	 *
	 * @var array
	 */
	private $vars;
	
	/**
	 * Expected sanitized variables
	 *
	 * @var array
	 */
	private $sanitized_vars;
	
	/**
	 * Initialize local variables
	 *
	 */
	function setUp()
	{
		$this->obj = new FormHelper();
		$this->prepareVar("Simple string","Simple string");
		$this->prepareVar("String with quote ' ","String with quote \\' ");
		$this->prepareVar("Blah <script>Nasty script</script> Blah","Blah  Blah");
	}
	
	/**
	 * Load variables' values into local variables
	 *
	 * @param string $input				Actual value
	 * @param string $filtered_input	Expected value
	 */
	private function prepareVar($input,$filtered_input)
	{
		$this->vars[] = $input;
		$this->sanitized_vars[] = $filtered_input;
	}
	
	/**
	 * Test $_POST and $_GET input sanitization
	 *
	 */
	function testFiltering()
	{
		$_POST = $this->vars;
		$post = $this->obj->HttpPost(false,true,true);
		$this->assertEquals($this->sanitized_vars,$post,"POST filtering failed");
		$_GET = $this->vars;
		$get = $this->obj->HttpGet(true,true);
		$this->assertEquals($this->sanitized_vars,$get,"GET filtering failed");
	}

}
?>
