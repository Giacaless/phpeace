<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/xsl.php");

/**
 * Unit Tests for XSL
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class XslManagerTest extends PHPUnit\Framework\TestCase 
{

	/**
	 * Test well-formedness of all generic XSL stylesheets
	 *
	 */
	function testXslWellformed()
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		include_once(SERVER_ROOT."/../classes/xmlhelper.php");
		$fm = new FileManager();
		$xh = new XmlHelper();
		foreach($fm->DirFiles(SERVER_ROOT."/../xsl/0",true) as $xsl_file)
		{
			$ext = $fm->Extension($xsl_file);
			if($ext == "xsl")
			{
				$xsl = $fm->TextFileRead($xsl_file,true);
				$this->assertTrue($xh->Check($xsl,false,false),"$xsl_file is not well-formed");			
			}
		}
	}
}
?>
