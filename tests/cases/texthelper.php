<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/texthelper.php");

/**
 * Unit Tests for TextHelper class
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class TextHelperTest extends PHPUnit\Framework\TestCase 
{
	/** 
	 * @var TextHelper */
	private $th;
	
	/**
	 * Initialize local properties
	 *
	 */
	function setUp()
	{
		$this->th = new TextHelper();
	}
	
	/**
	 * Compare two HTML strings
	 *
	 * @param string $string_orig		String to be HTMLised
	 * @param string $string_expected	Expected result
	 */
	private function compareHTMLs($string_orig,$string_expected)
	{
		$string_transformed = $this->th->Htmlise($string_orig,false);
		$this->assertEquals($string_expected,$string_transformed,"HTMLisation failed for $string_orig, got $string_transformed instead of $string_expected");
	}
	
	/**
	 * Test plain text htmlisation
	 *
	 */
	function testHtmliseNone()
	{
		$this->compareHTMLs("text with no link","text with no link");
	}
	
	/**
	 * Test URL htmlisation
	 *
	 */
	function testHtmliseUrl()
	{
		$this->compareHTMLs("link in text http://www.test.org end","link in text <a href=\"http://www.test.org\">http://www.test.org</a> end");
		$this->compareHTMLs("link in parentheses (http://www.test.org) end","link in parentheses (<a href=\"http://www.test.org\">http://www.test.org</a>) end");
		$this->compareHTMLs("link at end of line http://www.test.org","link at end of line <a href=\"http://www.test.org\">http://www.test.org</a>");
		$this->compareHTMLs("http://www.test.org link at beginning of line","<a href=\"http://www.test.org\">http://www.test.org</a> link at beginning of line");
		$this->compareHTMLs("comma,http://www.test.org link after comma","comma,<a href=\"http://www.test.org\">http://www.test.org</a> link after comma");
		$this->compareHTMLs("dot,http://www.test.org link after dot","dot,<a href=\"http://www.test.org\">http://www.test.org</a> link after dot");
		$this->compareHTMLs("colon:http://www.test.org link after colon","colon:<a href=\"http://www.test.org\">http://www.test.org</a> link after colon");
		// $this->compareHTMLs("link followed by colon http://www.test.org: text","link followed by colon <a href=\"http://www.test.org\">http://www.test.org</a>: text");
		$this->compareHTMLs("link followed by semicolon http://www.test.org; text","link followed by semicolon <a href=\"http://www.test.org\">http://www.test.org</a>; text");
		$this->compareHTMLs("link followed by comma http://www.test.org, text","link followed by comma <a href=\"http://www.test.org\">http://www.test.org</a>, text");
		// $this->compareHTMLs("link followed by dot http://www.test.org. text","link followed by dot <a href=\"http://www.test.org\">http://www.test.org</a>. text");
	}

	/**
	 * Test URL htmlisation
	 *
	 */
	function testHtmliseWww()
	{
		$this->compareHTMLs("www in text www.test.org end","www in text <a href=\"http://www.test.org\">www.test.org</a> end");
		// $this->compareHTMLs("www in parentheses (www.test.org) end","www in parentheses (<a href=\"http://www.test.org\">www.test.org</a>) end");
		$this->compareHTMLs("www at end of line www.test.org","www at end of line <a href=\"http://www.test.org\">www.test.org</a>");
		$this->compareHTMLs("www.test.org www at beginning of line","<a href=\"http://www.test.org\">www.test.org</a> www at beginning of line");
		// $this->compareHTMLs("comma,www.test.org www after comma","comma,<a href=\"http://www.test.org\">www.test.org</a> www after comma");
		// $this->compareHTMLs("dot,www.test.org www after dot","dot,<a href=\"http://www.test.org\">www.test.org</a> www after dot");
		// $this->compareHTMLs("colon:www.test.org link after colon","colon:<a href=\"http://www.test.org\">www.test.org</a> www after colon");
		// $this->compareHTMLs("www followed by colon www.test.org: text","www followed by colon <a href=\"http://www.test.org\">www.test.org</a>: text");
		// $this->compareHTMLs("www followed by semicolon www.test.org; text","www followed by semicolon <a href=\"http://www.test.org\">www.test.org</a>; text");
		// $this->compareHTMLs("www followed by comma www.test.org, text","www followed by comma <a href=\"http://www.test.org\">www.test.org</a>, text");
		// $this->compareHTMLs("www followed by dot www.test.org. text","www followed by dot <a href=\"http://www.test.org\">www.test.org</a>. text");
	}

	/**
	 * Test htmlisation of test with existing links
	 *
	 */
	function testHtmliseText()
	{
		$this->compareHTMLs("text with link <a href=\"http://www.test.org\">my link</a> end","text with link <a href=\"http://www.test.org\">my link</a> end");
		$this->compareHTMLs("<a href=\"http://www.test.org\">my link</a> text with link at beginning of line","<a href=\"http://www.test.org\">my link</a> text with link at beginning of line");
		$this->compareHTMLs("text with link at end of line <a href=\"http://www.test.org\">my link</a>","text with link at end of line <a href=\"http://www.test.org\">my link</a>");
		$this->compareHTMLs("text with link (<a href=\"http://www.test.org\">in parentheses</a>) end","text with link (<a href=\"http://www.test.org\">in parentheses</a>) end");
		$this->compareHTMLs("text with same link <a href=\"http://www.test.org\">www.test.org</a> end","text with same link <a href=\"http://www.test.org\">www.test.org</a> end");
	}

	/**
	 * Test email htmlisation
	 *
	 */
	function testHtmliseEmail()
	{
		// $this->compareHTMLs("email with tags <name@test.org> \"Name\" end","email with tags <a href=\"mailto:name@test.org\">name@test.org</a> \"Name\" end");
		$this->compareHTMLs("email in text name@test.org end","email in text <a href=\"mailto:name@test.org\">name@test.org</a>  end");
		$this->compareHTMLs("name@test.org email at beginning","<a href=\"mailto:name@test.org\">name@test.org</a>  email at beginning");
		$this->compareHTMLs("email at end name@test.org","email at end <a href=\"mailto:name@test.org\">name@test.org</a>");
		// $this->compareHTMLs("<i>name@test.org email after tag","<i><a href=\"mailto:name@test.org\">name@test.org</a>  email after tag");
		// $this->compareHTMLs("comma,name@test.org email after comma","comma,<a href=\"mailto:name@test.org\">name@test.org</a>  email after comma");
		// $this->compareHTMLs("colon:name@test.org email after colon","colon:<a href=\"mailto:name@test.org\">name@test.org</a>  email after colon");
	}

	/**
	 * Test htmlisation of text with existing email links
	 *
	 */
	function testHtmliseEmailText()
	{
		$this->compareHTMLs("email in text <a href=\"mailto:name@test.org\">write to me</a> end","email in text <a href=\"mailto:name@test.org\">write to me</a> end");
	}
	
}
?>
