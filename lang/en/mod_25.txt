# DODC
# ENGLISH
category = Category
categories = Categories
contractors = Contractors
contracts = Contracts
contracts_own = Own contracts
contracts_category = Contracts by category
contracts_time = Contrats in chronological order
contracts_office = Contracts by office
contracts_period = Contracts by period
import = Import data
init = Initialize database
dodc = Pentagon Contractors
office = Office
offices = Offices
parent = Parent contractor
period = Period
subcontractors = Child companies
subtotal = Total amount with child companies
total = Total
