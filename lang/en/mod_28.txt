# PEOPLE ENGLISH
admin_notes = Administrator's notes
connections = Connections: %s
domain_change = Domain change
email_check_subj = Email verification
email_check_body = Dear %1$s !\n\nYou receive this message in order to verify the email address %2$s for you account on %3$s management site (%5$s).\n\nPlease click on the following link:\n\n%6$s\n\nThis link, which will allow us to verify your email, will be valid for five days since the date of this message.\n\nFor any clarification please write to %4$s\n\nMany thanks and kind regards\n\nThe staff of %3$s\n\n
email_valid = Valid email
error_check2 = Email not valid
error_check3 = Verification message already sent
last_conn = Last connection: %s
person_deleted = %s has been deleted
registration_check_failed1 = Email verification expired, a new one has just been sent, please check your email
registration_check_failed2 = Email verification failed
registration_check_sent = Verification message sent to %s
registration_date = Registration date: %s
registration_ok = Email verified
send_email_verify = Send message for email verification
start_date = Insert date: %s
user_exists = WARNING: User with email <strong>%s</strong> already exists
verified = Verified

