# DODC
# ITALIAN
category = Categoria
categories = Categorie
contractors = Fornitori
contracts = Contratti
contracts_own = Contratti diretti
contracts_category = Contratti per categoria
contracts_time = Contratti in ordine cronologico
contracts_office = Contratti per ufficio committente
contracts_period = Contratti per periodo
import = Importa dati
init = Prepara database
dodc = Fornitori Pentagono
office = Ufficio committente
offices = Uffici committenti
parent = Subappaltatore
period = Periodo
subcontractors = Sussidiarie
subtotal = Totale considerando anche le sussidiarie
total = Totale