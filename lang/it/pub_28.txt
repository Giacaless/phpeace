# PEOPLE
already_registered_auth = Utente già registrato con l'email %s
already_registered_no_auth = Utente già registrato ma password sbagliata, per ricevere la password inserire il proprio indirizzo email
auth_error = Errore di autenticazione
data_changed = Dati cambiati con successo
deactivate_confirm = Conferma che vuoi veramente rimuovere il tuo account
deactivate_warning = Account rimosso
email_not_found = Spiacente, non risulta nessun utente con l'indirizzo email %s
email_check_subj = Email di verifica
email_check_body = Caro/a %1$s !\n\nRicevi questo messaggio per verificare il tuo indirizzo di posta elettronica %2$s, che hai inserito nel sito %3$s (%5$s).\n\nPer favore clicca sul link sottostante:\n\n%6$s\n\nPer accedere al tuo account su %3$s dovrai utilizzare come login il tuo indirizzo di posta elettronica e la password che hai scelto al momento della registrazione\n\nPer qualsiasi chiarimento scrivi a %4$s\n\nGrazie per il tuo interesse alle attività di %3$s\n\nLo staff di %3$s\n%5$s\n\n
email_check_body2 = Caro/a %1$s !\n\nRicevi questo messaggio per verificare il tuo indirizzo di posta elettronica %2$s, che hai inserito nel sito %3$s (%5$s).\n\nPer favore clicca sul link sottostante:\n\n%6$s\n\nPer qualsiasi chiarimento scrivi a %4$s\n\nGrazie per il tuo interesse alle attività di %3$s\n\nLo staff di %3$s\n%5$s\n\n
error_captcha = Captcha non valido
error_check1 = Utente non autenticato, rieffettuare la login
error_check2 = Email non valida
error_check3 = Messaggio di verifica già inviato
error_check4 = Il messaggio di verifica era scaduto, ne è stato inviato un'altro, controlla la tua posta elettronica
error_email = Indirizzo email %s non valido
error_email_in_use = Email is already in use
error_password = Password troppo corta
error_passwords_equal = Le due password immesse non corrispondono
error_privacy = Senza il consenso al trattamento dei dati non è possibile registrarsi
error_var_empty = %s mancante
login_attempts = Errori di autenticazione
login_attempts_warning = %s ha fallito l'autenticazione per %s volte\n\nCordiali saluti, \n%s\n%s\n
name = Nome
options_changed = Opzioni cambiate con successo
password_changed = Password cambiata
payment_ko = Transazione fallita
payment_ok = Transazione riuscita, grazie!
payment_receipt = Ricevuta del pagamento n. %s
payment_verified = Caro/a %s, grazie per il pagamento; qui di seguito i dettagli:\n\nData: %s\nDescrizione: %s\nCifra: %s\n
registration_check_failed1 = Verifica dell'indirizzo di posta elettronica scaduta, si prega di richiederla nuovamente
registration_check_failed2 = Verifica dell'indirizzo di posta elettronica non riuscita o già effettuata
registration_check_failed3 = Verifica dell'indirizzo di posta elettronica già effettuata
registration_check_failed4 = La verifica precedente è fallita, un nuovo messaggio è stato inviato, si prega di controllare la propria casella di posta elettronica
registration_check_sent = Un messaggio è stato inviato a %s per verificare il tuo indirizzo di posta elettronica. Controlla la tua posta e clicca sul link indicato nel messaggio in modo da completare la procedura di registrazione
registration_ok = Email verificata e registrazione completata
registration_ok_but_check = Registrazione riuscita
registration_ko = Registrazione fallita
remember_logout = Quando hai finito, ricordati di chiudere il browser o di effettuare il logout
reminder_sent = Password inviata all'indirizzo %s
surname = Cognome
