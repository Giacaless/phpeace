<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/modules.php");
include_once(SERVER_ROOT."/../classes/config.php");
// CONSTANTS
define('WS_PATH',"/tools/ws.php"); 
define('WSDL_PATH',"/tools/wsdl.php"); 

class WebService
{
	public $active = false;

	private $wsdl_url;
	private $wsdl;
	private $pub_web;
	private $soap_server;
	private $module;
	private $soap_timeout;
	private $proxy_host;
	private $proxy_port;
	
	function __construct($module,$check_module=false,$server_start=false)
	{
		$this->module = $module;
		$conf = new Configuration();
		$this->soap_timeout = $conf->Get("soap_timeout");
		if($conf->Get("proxy"))
		{
			$this->proxy_host = $conf->Get("proxy_name");
			$this->proxy_port = $conf->Get("proxy_port");
		}
		if($check_module)
		{
			if(Modules::IsActiveByPath($module))
			{
				include_once(SERVER_ROOT."/../classes/file.php");
				include_once(SERVER_ROOT."/../classes/ini.php");
				$ini = new Ini();
				$this->pub_web = $ini->Get("pub_web");
				$fm = new FileManager();
				$filename = "wsdl/{$module}.wsdl";
				if($fm->Exists($filename))
				{
					$this->active = true;
					$this->wsdl_url = $this->pub_web . WSDL_PATH . "?m=$module";
					$wsdl = $fm->TextFileRead($filename);
					$this->wsdl = str_replace("[[url]]",$this->pub_web . WS_PATH . "?m=$module",$wsdl);
					if($server_start)
					{
						$this->ServerStart();
					}
				}
			}		
		}
	}
	
	public function Call($server,$method,$params)
	{
		$client = $this->SoapClient($server . WSDL_PATH . "?m={$this->module}");
		$result = false;
		if($client)
		{
			try
			{
				$result = call_user_func_array(array($client,$method),$params);
			}
			catch (Exception $e)
			{
				$message = $e->getMessage();
				UserError("SOAP call failed", array('error'=>$message,'module'=>$this->module,'server'=>$server,'method'=>$method));
			}
		}
		return $result;
	}
	
	private function SoapClient($url)
	{
        try
        {
        	$options = array('connection_timeout'=>$this->soap_timeout);
        	if($this->proxy_host!="")
        	{
        		$options['proxy_host'] = $this->proxy_host;
        		if($this->proxy_port!="")
        			$options['proxy_port'] = $this->proxy_port;
        	}
			$client = new SoapClient($url,$options);
			return $client;
        }
        catch (SoapFault $e)
        {
            include_once(SERVER_ROOT."/../classes/log.php");
            $log = new Log();
            $log->Write("error", "Webservice down ($url): ". $e->getMessage());
            return false;
        }
	}
	
	private function FunctionsBind()
	{
		switch($this->module)
		{
			case "admin":
				include_once(SERVER_ROOT."/../classes/adminhelper.php");
				$this->soap_server->setClass("AdminWS");
			break;
			case "articles":
				include_once(SERVER_ROOT."/../classes/articles.php");
				$this->soap_server->setClass("ArticlesWS");
			break;
			case "people":
				include_once(SERVER_ROOT."/../classes/people.php");
				$this->soap_server->setClass("PeopleWS");
			break;
			case "phpeace":
				include_once(SERVER_ROOT."/../classes/phpeace.php");
				$this->soap_server->setClass("PhPeaceWS");
			break;
			case "uss":
				include_once(SERVER_ROOT."/../classes/uss.php");
				$this->soap_server->setClass("UrlShorteningWS");
			break;
		}
	}
	
	private function ServerStart()
	{
		$this->soap_server = new SoapServer($this->wsdl_url);
		$this->FunctionsBind();
		$this->soap_server->handle();
	}
	
	public function WSDLGet()
	{
		return $this->wsdl;
	}
}

?>
