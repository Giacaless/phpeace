<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

Class TreeHelper
{
	public $tree;
	public $description;

	function __construct($groups)
	{
		$this->LoadTree($groups);
	}

	public function GroupChildren($id_parent)
	{
		$tree = $this->tree;
		$children = array();
		$child_counter=0;
		for($i=0;$i<count($tree);$i++)
		{
			if ($tree[$i]['id_parent']==$id_parent)
			{
				$children[$child_counter]['id_group']	= $tree[$i]['id_group'];
				$children[$child_counter]['name']		= $tree[$i]['name'];
				$children[$child_counter]['id_parent']	= $tree[$i]['id_parent'];
				$children[$child_counter]['seq']		= $tree[$i]['seq'];
				$children[$child_counter]['visible']	= $tree[$i]['visible'];
				$child_counter = $child_counter + 1;
			}
		}
		return $children;
	}

	public function GroupChildrenAll($id_group, &$groups)
	{
		$children = $this->GroupChildren($id_group);
		while((list(,$child) = each($children))==true)
		{
			$this->GroupChildrenAll($child['id_group'], $groups);
		}
		$groups = array_merge($children, $groups);
	}

	public function GroupData($id_group)
	{
		$group_data = array();
		if (count($this->tree)>0)
		{
			reset($this->tree);
			while((list(,$group) = each($this->tree))==true)
			{
				if ($group['id_group']==$id_group)
				{
					$group_data['id_group']		= $group['id_group'];
					$group_data['name']			= $group['name'];
					$group_data['id_parent']	= $group['id_parent'];
					$group_data['seq']			= $group['seq'];
					$group_data['visible']		= $group['visible'];
				}
			}
		}
		return $group_data;
	}

	public function GroupIsChild($candidate,$current_group)
	{
		$child = 0;
		$children = array();
		$child_counter=0;
		reset($this->tree);
		while((list(,$group) = each($this->tree))==true)
		{
			if ($group['id_parent']==$current_group)
			{
				$children[$child_counter] = $group['id_group'];
				$child_counter ++;
			}
		}
		for($i=0;$i<count($children);$i++)
		{
			if ($children[$i]==$candidate)
				$child = 1;
			else
				$child = $this->GroupIsChild($candidate,$children[$i]);
			if ($child==1)
				break;
		}
		return $child;
	}

	public function GroupParents($id_group, &$parents)
	{
		$group = $this->GroupData($id_group);
		$parents[] = array(	'id_group'	=> $group['id_group'],
							'name'		=> $group['name'],
							'id_parent'	=> $group['id_parent']);
		if ($group['id_parent']!=0)
			$parents = $this->GroupParents($group['id_parent'], $parents);
		return $parents;
	}

	private function LoadTree($groups)
	{
		unset($this->tree);
		$tree = array();
		$counter = 0;
		while((list(,$group) = each($groups))==true)
		{
			$tree[$counter]['id_group']		= $group['id_group'];
			$tree[$counter]['name']			= $group['name'];
			$tree[$counter]['id_parent']	= $group['id_parent'];
			$tree[$counter]['seq']			= $group['seq'];
			$tree[$counter]['visible']		= $group['visible'];
			$counter ++;
			$description[$group['id_group']] = $group['description'];
		}
		$this->description = $description;
		$this->tree = $tree;
	}

}
?>
