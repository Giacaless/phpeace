<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/session.php");
include_once(SERVER_ROOT."/../others/twitteroauth/twitteroauth.php");
include_once(SERVER_ROOT."/../classes/uss.php");

define('TWEET_MAX_LENGTH',140);

/**
 * Manage Twitter accounts and tweets
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class TwitterHelper
{
	/**
	 * Twitter API key
	 * 
	 * @var string
	 */
	private $twitter_consumer_key;
	
	/**
	 * Twitter API password
	 * 
	 * @var string
	 */
	private $twitter_consumer_secret;

	/** 
	 * @var Session */
	private $session;
	
	/** 
	 * @var UrlShorteningClient */
	private $uss;
	
	/**
	 * Initialize local properties 
	 */
	function __construct()
	{
		$conf = new Configuration();
		$this->twitter_consumer_key = $conf->Get("twitter_consumer_key");
		$this->twitter_consumer_secret = $conf->Get("twitter_consumer_secret");
		$this->session = new Session();
		$this->uss = new UrlShorteningClient();
	}
	
	/**
	 * Check if an article is tweetable, i.e. it matches the criteria set by Twitter accounts,
	 * and return the list of all matching accounts
	 * 
	 * @param integer $id_article		Article ID
	 * @param integer $id_topic			Topic ID 
	 * @param integer $id_topic_group	Topic group ID
	 * @param boolean $is_new			Whether the article has been marked as new
	 * @return array					Twitter accounts IDs
	 */
	private function IsArticleTweetable($id_article,$id_topic,$id_topic_group,$is_new)
	{
		$tweets = array();
		if($this->IsTwitterApiKeySet())
		{
			$twitters = array();
			$num_twitters = $this->TwitterAccounts($twitters,$id_topic,$id_topic_group,true,false);
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			if($num_twitters>0)
			{
				while((list(,$twitter) = each($twitters))==true)
				{
					$tweetable = false;
					if($twitter['id_keyword']>0)
					{
						$article_keywords = array();
						$o->GetKeywords($id_article,$o->types['article'],$article_keywords,0,false);
						while((list(,$article_keyword) = each($article_keywords))==true)
						{
							if($article_keyword['id_keyword']==$twitter['id_keyword'])
							{
								$tweetable = $twitter['show_latest_only']? $is_new : true;
							}
						}				
					}
					else
					{
						$tweetable = $twitter['show_latest_only']? $is_new : true;
					}
					if($tweetable)
					{
						$tweets[] = array('id'=>$twitter['id_twitter'],'use_keywords'=>$twitter['use_keywords'],'id_topic'=>$twitter['id_topic']);
					}
				}
			}
		}
		return $tweets;
	}
	
	/**
	 * Check whether Twitter API key has been configured
	 * 
	 * @return boolean
	 */
	public function IsTwitterApiKeySet()
	{
		return $this->twitter_consumer_key!="" && $this->twitter_consumer_secret!="";
	}
	
	/**
	 * Create tweet associated to a specific article and add it to the tweets' queue
	 * 
	 * @param integer $id_article		Article ID
	 * @param integer $id_topic			Topic ID 
	 * @param integer $id_topic_group	Topic group ID
	 * @param boolean $is_new			Whether the article has been marked as new
	 */
	public function TweetArticle($id_article,$id_topic,$id_topic_group,$is_new)
	{
		$twitters = $this->IsArticleTweetable($id_article,$id_topic,$id_topic_group,$is_new); 
		if(count($twitters)>0)
		{
			include_once(SERVER_ROOT."/../classes/article.php");
			include_once(SERVER_ROOT."/../classes/topic.php");
			include_once(SERVER_ROOT."/../classes/irl.php");
			include_once(SERVER_ROOT."/../classes/texthelper.php");
			$article_keywords = array();
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			$o->GetKeywords($id_article,$o->types['article'],$article_keywords,0,true);
			$a = new Article($id_article);
			$th = new TextHelper();
			$a->ArticleLoad();
			$irl = new IRL();
			$topic = new Topic($id_topic);
			$article_url = $irl->PublicUrlTopic("article",array('id'=>$id_article),$topic,false);
			if($this->uss->IsShorteningServiceAvailable())
			{
				$article_url = $this->uss->Shorten($article_url);
			}
			$current_user_id = (int)$this->session->Get("current_user_id");
			$db = Db::globaldb();
			while((list(,$twitter) = each($twitters))==true)
			{
				$title = $twitter['id_topic']>0? $a->headline : $topic->name . ": " . $a->headline; 
				$tweet = $th->StringCut($title,TWEET_MAX_LENGTH - strlen($article_url) - 4) . " " . $article_url;
				if($twitter['use_keywords'])
				{
					foreach($article_keywords as $article_keyword)
					{
						if(strlen($tweet) + strlen($article_keyword['keyword']) + 2 < TWEET_MAX_LENGTH)
						{
							$tweet .= " #" . $article_keyword['keyword'];
						}
					}
				}
				$tweet = $db->SqlQuote($tweet);
				$today_time = $db->GetTodayTime();
				$db->begin();
				$db->lock( "tweets" );
				$res[] = $db->query( "DELETE FROM tweets WHERE id_article='$id_article' AND published=0 AND id_twitter='{$twitter['id']}' " );
				$id_tweet = $db->nextId( "tweets", "id_tweet" );
				$sqlstr = "INSERT INTO tweets (id_tweet,tweet,insert_time,id_article,id_user,id_twitter,published) 
					VALUES ('$id_tweet','$tweet','$today_time','$id_article','$current_user_id','{$twitter['id']}','0')";
				$res[] = $db->query( $sqlstr );
				Db::finish( $res, $db);
			}
		}
	}
	
	/**
	 * Publish the tweet associated to an article
	 * 
	 * @param integer $id_article
	 * @return array	List of Twitter accounts used for publishing this tweet
	 */
	public function TweetArticlePublish($id_article)
	{
		$accounts = array();
		$db =& Db::globaldb();
		$tweets = array();
		$sqlstr = "SELECT twe.id_tweet,twe.tweet,twi.oauth_token,twi.oauth_token_secret,twi.id_twitter,twi.name
			FROM tweets twe
			INNER JOIN twitters twi ON twe.id_twitter=twi.id_twitter
			WHERE twe.id_article='$id_article' AND twe.published=0 AND twi.active=1 
			ORDER BY twe.insert_time ";
		$db->QueryExe($tweets, $sqlstr);
		if(count($tweets)>0)
		{
			while((list(,$tweet) = each($tweets))==true)
			{
				$connection = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_consumer_secret, $tweet['oauth_token'], $tweet['oauth_token_secret']);
				$response = $connection->post('statuses/update',array('status'=>$tweet['tweet']));
				$response_id = (int)$response->id_str;
				if($response_id>0)
				{
					$this->TweetPublishedSet($tweet['id_tweet']);
					$accounts[] = $tweet['name'];
				}
				else 
				{
					$response_errors = array();
					if(isset($response->error))
						$response_errors['error'] = $response->error; 
					UserError("Tweet {$tweet['id_tweet']} failed for article $id_article - Twitter account {$tweet['id_twitter']}",$response_errors);
				}
			}
		}
		return $accounts;
	}
	
	/**
	 * Set the published flag for a speicific tweet
	 * 
	 * @param integer $id_tweet
	 */
	private function TweetPublishedSet($id_tweet)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "tweets" );
		$sqlstr = "UPDATE tweets SET published='1' WHERE id_tweet='$id_tweet'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	/**
	 * Delete all tweets pending to be published for a specific twitter account
	 * 
	 * @param integer $id_twitter
	 */
	private function TweetsPendingDelete($id_twitter)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "tweets" );
		$res[] = $db->query( "DELETE FROM tweets WHERE id_twitter='$id_twitter' AND published=0" );
		Db::finish( $res, $db);
	}
	
	/**
	 * Delete a specific twitter account
	 * 
	 * @param integer $id_twitter
	 */
	public function TwitterAccountDelete($id_twitter)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "twitters" );
		$res[] = $db->query( "DELETE FROM twitters WHERE id_twitter='$id_twitter' " );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "tweets" );
		$res[] = $db->query( "DELETE FROM tweets WHERE id_twitter='$id_twitter' " );
		Db::finish( $res, $db);
	}

	/**
	 * Get a specific twitter account
	 * 
	 * @param integer $id_twitter
	 * @return array
	 */
	public function TwitterAccountGet($id_twitter)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_twitter,id_topic,id_topic_group,id_user,id_keyword,use_keywords,show_latest_only,name,oauth_token,oauth_token_secret,active 
			FROM twitters 
			WHERE id_twitter='$id_twitter'";
		$db->query_single( $row, $sqlstr);
		return $row;
	}

	/**
	 * Store a twitter account definition
	 * 
	 * @param integer	$id_twitter			Twitter ID
	 * @param string 	$name				Name
	 * @param integer 	$id_topic			Associated topic ID
	 * @param integer 	$id_topic_group		Associated group of topics ID
	 * @param boolint 	$show_latest_only	Whether to tweet articles marked as new only
	 * @param integer 	$id_keyword			Keyword ID to filter articles
	 * @param boolint 	$use_keywords		Whether to use article's keywords
	 * @param integer 	$id_user			Admin user ID
	 * @param boolint 	$active				Whether the account is active or not
	 * @return integer
	 */
	public function TwitterAccountStore($id_twitter,$name,$id_topic,$id_topic_group,$show_latest_only,$id_keyword,$use_keywords,$id_user,$active)
	{
		if($id_twitter>0)
		{
			$twitter = $this->TwitterAccountGet($id_twitter);
			if($twitter['active']=="1" && $active=="0")
			{
				$this->TweetsPendingDelete($id_twitter);
			}
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "twitters" );
		if ($id_twitter>0)
		{
			$sqlstr = "UPDATE twitters SET name='$name',id_topic='$id_topic',id_topic_group='$id_topic_group',
				show_latest_only='$show_latest_only',id_keyword='$id_keyword',use_keywords='$use_keywords',active='$active'
				 WHERE id_twitter='$id_twitter'";
		}
		else
		{
			$id_twitter = $db->nextId( "twitters", "id_twitter" );
			$sqlstr = "INSERT INTO twitters (id_twitter,name,id_topic,id_topic_group,id_user,id_keyword,use_keywords,show_latest_only,active) 
				VALUES ('$id_twitter','$name','$id_topic','$id_topic_group','$id_user','$id_keyword','$use_keywords','$show_latest_only','$active')";
		}
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $id_twitter;
	}
	
	/**
	 * Authenticate with a twitter account
	 * Based on example by Abraham Williams at https://github.com/abraham/twitteroauth
	 * 
	 * @param integer $id_twitter	Twitter ID
	 * @param integer $id_topic		Topic ID
	 */
	public function TwitterAccountAuth($id_twitter,$id_topic)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$callback = $ini->Get("admin_web") . "/topics/actions.php?from2=twitter&id=$id_twitter&id_topic=$id_topic";
		$connection = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_consumer_secret);
		$request_token = $connection->getRequestToken($callback);
		$this->session->Set("oauth_twitter_token",$request_token['oauth_token']);
		$this->session->Set("oauth_twitter_token_secret",$request_token['oauth_token_secret']);
		switch ($connection->http_code)
		{
			case 200:
				$url = $connection->getAuthorizeURL($request_token['oauth_token']);
				include_once(SERVER_ROOT."/../classes/translator.php");
				$id_language = $this->session->Get("id_language");
				$tr = new Translator($id_language,0);
				$url .= "&lang=" . $tr->lang;
				header('Location: ' . $url); 
			break;
			default:
				UserError("Could not connect to Twitter",array('http_response'=>$connection->http_code));
		}
	}
	
	/**
	 * Store Twitter Oauth access token
	 * 
	 * @param integer $id_twitter
	 * @param string $oauth_verifier
	 */
	public function TwitterAccountAuthStore($id_twitter,$oauth_verifier)
	{
		if($this->session->IsVarSet("oauth_twitter_token"))
		{
			$oauth_token = $this->session->Get("oauth_twitter_token");		
			$oauth_token_secret = $this->session->Get("oauth_twitter_token_secret");
			$connection = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_consumer_secret, $oauth_token, $oauth_token_secret);
			$access_token = $connection->getAccessToken($oauth_verifier);
			if(is_array($access_token) && isset($access_token['oauth_token']) && isset($access_token['oauth_token_secret']))
			{
				$this->session->Set("oauth_twitter_access_token",$access_token);
				$db =& Db::globaldb();
				$db->begin();
				$db->lock( "twitters" );
				$sqlstr = "UPDATE twitters SET oauth_token='{$access_token['oauth_token']}',oauth_token_secret='{$access_token['oauth_token_secret']}'
					 WHERE id_twitter='$id_twitter'";
				$res[] = $db->query( $sqlstr );
				Db::finish( $res, $db);
			}
		}
	}
	
	/**
	 * Get Twitter account information
	 * 
	 * @param string $token
	 * @param string $token_secret
	 * @return Object
	 */
	public function TwitterAccountContent($token,$token_secret)
	{
		$connection = new TwitterOAuth($this->twitter_consumer_key, $this->twitter_consumer_secret, $token, $token_secret);
		$content = $connection->get('account/verify_credentials');
		return $content;
	}
	
	/**
	 * Get all twitter accounts 
	 * 
	 * @param array		$rows		Accounts
	 * @param integer	$id_topic	Optional topic ID to filter accounts
	 * @param boolean	$paged		Whether results are paginated or not
	 * @return integer
	 */
	public function TwitterAccounts( &$rows, $id_topic, $id_topic_group, $active_only=false, $paged=true )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_twitter,id_topic,id_topic_group,id_user,id_keyword,use_keywords,show_latest_only,
			name,oauth_token,oauth_token_secret,active 
			FROM twitters 
			WHERE id_twitter>0 ";
		if($id_topic>0)
		{
			$sqlstr .= " AND id_topic='$id_topic' OR (id_topic=0 AND id_topic_group='$id_topic_group') ";
		}
		if($active_only)
			$sqlstr .= " AND active=1 ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
}

?>
