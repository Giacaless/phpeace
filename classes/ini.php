<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/sharedmem.php");
define('CONFIGURATION_GLOBAL_VAR',"conf_ini");
define('CONFIGURATION_MODULES_VAR',"conf_modules");
define('MAX_SERIALIZED_CONFIG_ARRAY_LENGTH',800);

class Ini
{
	/** 
	 * @var SharedMem */
	private $mem;
	
	private $conf_ini = array();
	private $conf_modules = array();

	function __construct($load=true)
	{
		$this->mem = new SharedMem();
		if ($load)
		{
			$this->LoadConfiguration();
			$this->LoadModuleConfiguration();
		}
	}
	
	function __destruct()
	{
		unset($this->conf_ini);
		unset($this->conf_modules);
		unset($this->mem);
	}
	
	public function ForceReload()
	{
		$this->LoadConfiguration(true);
		$this->LoadModuleConfiguration(true);
	}
	
	public function Get($var)
	{
		return $this->conf_ini[$var];
	}
	
	public function GetModule($module,$var,$default_value)
	{
		return isset($this->conf_modules[$module][$var])? $this->conf_modules[$module][$var] : $default_value;
	}

	public function GetTimestamp($var)
	{
		$last = array();
		$db =& Db::globaldb();
		$db->query_single( $last, "SELECT UNIX_TIMESTAMP($var) AS {$var}_ts FROM global WHERE id_install=1");
		return $last[$var."_ts"];
	}

	public function Initialise()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "global" );
		$sqlstr = "INSERT INTO global (id_install) VALUES (1)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function LoadConfiguration($force=false)
	{
		$sconf = $this->mem->Get(CONFIGURATION_GLOBAL_VAR);
		if(is_array($sconf) && $sconf['id_install']==1 && !$force)
			$this->conf_ini = $sconf;
		else
		{
			$row = array();
			$db =& Db::globaldb();
			$db->CriticalSet();
			$db->query_single( $row, "SELECT *,
				UNIX_TIMESTAMP(install_date) AS install_date_ts,
				UNIX_TIMESTAMP(last_cron) AS last_cron_ts
				 FROM global WHERE id_install=1");
			if($row['id_install']=="1")
			{
				$this->conf_ini = $row;
				$this->mem->Set(CONFIGURATION_GLOBAL_VAR,$row);
			}
			else
			{
				UserError("Could not load configuration from database",array(),256,true);
			}
		}
	}

	private function LoadModuleConfiguration($force=false)
	{
		$mconf = $this->mem->Get(CONFIGURATION_MODULES_VAR);
		if(is_array($mconf) && count($mconf)>0 && !$force)
			$this->conf_modules = $mconf;
		else
		{
			$sqlstr = "SELECT module_path,config_params FROM modules_config ";
			$rows = array();
			$db =& Db::globaldb();
			$db->QueryExe($rows, $sqlstr);
			$modules_config = array();
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia;		
			foreach($rows as $row)
			{
				$mparams = $v->Deserialize($row['config_params']);
				$modules_config[$row['module_path']] = (is_array($mparams))? $mparams : array();
			}
			$this->conf_modules = $modules_config;
			$this->mem->Set(CONFIGURATION_MODULES_VAR,$modules_config);
		}
	}

	private function Reset()
	{
		$this->mem->Delete(CONFIGURATION_GLOBAL_VAR);
	}
	
	private function ResetModule()
	{
		$this->mem->Delete(CONFIGURATION_MODULES_VAR);
	}
	
	public function Set($var,$value)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock("global");
		$res[] = $db->query( "UPDATE global SET $var='$value' WHERE id_install=1" );
		Db::finish( $res, $db);
		$this->Reset();
		$this->LoadConfiguration(true);
	}
	
	public function SetTimestamp($var,$ts=0)
	{
		$db = Db::globaldb();
		$datetime = $db->getTodayTime($ts);
		$db->begin();
		$db->lock("global");
		$res[] = $db->query( "UPDATE global SET $var='$datetime' WHERE id_install=1" );
		Db::finish( $res, $db);
		$this->Reset();
	}

	public function SetPath($path,$path_value,$check_path=true)
	{
		include_once(SERVER_ROOT."/../classes/formhelper.php");
		$fh = new FormHelper();
		$path_value = $fh->SubmitEscape($path_value);
		$old_path = $this->Get($path);
		if($path_value!=$old_path && $path_value!="")
		{
			include_once(SERVER_ROOT."/../classes/config.php");
			$conf = new Configuration();
			$blackdirs = $conf->Get("blackdirs");
			if(!in_array($path_value,$blackdirs))
			{
				$goahead = true;
				if($check_path)
				{
					include_once(SERVER_ROOT."/../classes/irl.php");
					$irl = new IRL();
					$redirect = $irl->RedirectExists($path_value);
					$goahead = !($redirect>0);
				}
				if($goahead)
				{
					include_once(SERVER_ROOT."/../classes/file.php");
					$fm = new FileManager();
					$rename = $fm->Rename("pub/$old_path","pub/$path_value");
					$fm->PostUpdate();
					if($rename)
						$this->Set($path,$path_value);		
				}
			}
		}
	}

	public function SetModule($module,$var,$unescaped_value)
	{
		$this->LoadModuleConfiguration();
		$current_config = (isset($this->conf_modules[$module]) && is_array($this->conf_modules[$module]))? $this->conf_modules[$module] : array();
		$current_config[$var] = $unescaped_value;
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$mconfig_serialized = $v->Serialize($current_config);
		if(strlen($mconfig_serialized)>MAX_SERIALIZED_CONFIG_ARRAY_LENGTH)
		{
			UserError("Max length for module configuration",array('module'=>$module,'var'=>$var,'value'=>$unescaped_value),512);			
		}
		else 
		{
			$sqlstr = "REPLACE INTO modules_config (module_path,config_params) VALUES ('$module','$mconfig_serialized') ";
			$db =& Db::globaldb();
			$db->begin();
			$db->lock("modules_config");
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
			$this->ResetModule();
			$this->LoadModuleConfiguration(true);
		}
	}
}
?>
