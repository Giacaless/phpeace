<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

use Formapro\TelegramBot\Bot;
use Formapro\TelegramBot\Update;
use Formapro\TelegramBot\SendMessage;
use Formapro\TelegramBot\SendPhoto;
use Formapro\TelegramBot\AnswerCallbackQuery;
use Formapro\TelegramBot\InlineKeyboardButton;
use Formapro\TelegramBot\InlineKeyboardMarkup;
use Formapro\TelegramBot\SetWebhook;
use function GuzzleHttp\Psr7\str;
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/db.php");
require_once(SERVER_ROOT."/../others/vendor/autoload.php");

/**
 * Telegram bot
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class TelegramBot
{
	/**
	 * Telegram Bot API key
	 * 
	 * @var string
	 */
	private $telegram_bot_api_key;
	
	/**
	 * Telegram callback key
	 * (specified in the webook)
	 *
	 * @var string
	 */
	private $telegram_callback_key;

	/**
	 * Chat actions & responses
	 *
	 * @var array
	 */
	private $chat;
	
	private $inline_buttons = array();
	
	private $images = array();
	
	/**
	 * Initialize local properties 
	 */
	function __construct()
	{
		$conf = new Configuration();
		$this->telegram_bot_api_key = $conf->Get("telegram_bot_api_key");
		$this->telegram_callback_key = $conf->Get("telegram_callback_key");
		$filename = SERVER_ROOT."/../custom/telegram.json";
		if(file_exists($filename)) {
			$this->chat = json_decode(file_get_contents(SERVER_ROOT."/../custom/telegram.json"),true);
		} else {
			http_response_code(404);
			exit;
		}
	}
	
	public function KeyCheck($key) {
		return $key !='' && $key == $this->telegram_callback_key;
	}
	
	public function WebhookSet($url) {
		$bot = new Bot($this->telegram_bot_api_key);
		$response = $bot->setWebhook( new SetWebhook($url) );
		return str($response).PHP_EOL;
	}
	
	private function ActionLookup($action) {
		$action = strtolower(trim(trim($action),'/'));
		$response = false;
		$this->menu_items = array();
		foreach($this->chat['actions'] as $a) {
			if($a['action'] == 'boh') {
				$boh = $a['response'];
			}
			if($a['action'] == $action) {
				$response = $a['response'];
				if(isset($a['menu'])) {
					$response .= PHP_EOL . PHP_EOL . $this->MenuLookup($a['menu']);
				}
				if(isset($a['images']) && is_array($a['images'])) {
					$this->images = $a['images'];
				}
			}
		}
		return $response? $response : $boh;
	}

	private function MenuLookup($menu) {
		$response = '';
		foreach($this->chat['menus'] as $m) {
			if($m['menu'] == $menu) {
				$response = $m['description'] . PHP_EOL;
				$this->menu_items = $m['items'];
				foreach($m['items'] as $item) {
					$response .= strtoupper($item['label']) . ' - ' . $item['description'] . PHP_EOL;
				}
			}
		}
		return $response;
	}
	
	public function ProcessRequest($data) {
		$bot = new Bot($this->telegram_bot_api_key);
		$update = Update::create($data);
		if ($callbackQuery = $update->getCallbackQuery()) {
			$answer = new AnswerCallbackQuery($callbackQuery->getId());
			// should do something with the answer
			$answer->setText('Grazie!');
			$bot->answerCallbackQuery($answer);
		} else {
			$req = $update->getMessage();
			$id = $req->getChat()->getId();
			$from = $req->getFrom();
			// get action
			$text = $req->getText();
			// check it's not a bot
			if(!$from->isBot()) {
				$firstname = $from->getFirstName();
				$lastname = $from->getLastName();
				$username = $from->getUsername();
				$fullname = "$firstname $lastname";
				// lookup
				$response = $this->ActionLookup($text);
				if(count($this->images)>0) {
					$sendPhoto = new SendPhoto($id,$this->images[array_rand($this->images)]);
					$sendPhoto->setCaption($response);
					$bot->sendPhoto($sendPhoto);
					$this->images = [];
				} else {
					// variables replacements
					$response = str_replace('%NAME%', $fullname, $response);
					//$msg = new SendMessage($id,"Hi there $fullname ($id - $username)!");
					//$bot->sendMessage($msg);
					$msg = new SendMessage($id,$response);
					// request phone
					/*
					 $button = new KeyboardButton('Telefono');
					 $button->setRequestContact(true);
					 $keyboard = new ReplyKeyboardMarkup([[$button]]);
					 $keyboard->setOneTimeKeyboard(true);
					 */
					// Inline buttons
					if(count($this->inline_buttons)>0) {
						$buttons = [];
						foreach($this->inline_buttons as $item) {
							$buttons[] = InlineKeyboardButton::withTextAsCallbackData(strtoupper($item));
						}
						$keyboard = new InlineKeyboardMarkup([$buttons]);
						$msg->setReplyMarkup($keyboard);
						$this->inline_buttons = [];
					}
					$bot->sendMessage($msg);
				}
				http_response_code(200);
			} else {
				$bot->sendMessage($msg);
				$msg = new SendMessage($id,"Sorry, mama told me not to talk to bots");
				http_response_code(403);
			}
		}
	}
}

?>
