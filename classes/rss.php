<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/cache.php");

class Rss
{
	public $feed_types;
	
	private $type;

	function __construct($type="rss")
	{
		$this->feed_types = array('___','RSS 1.0','RSS 2.0','ATOM','RSS 2.0 with images','RSS 2.0 with content');
		$this->type=$type;
	}

	public function Get($url,$ttl,$limit=0)
	{
		// TODO check url valid 
		$cache = new Cache;
		if ($ttl>0)
			$cache->ttl = $ttl;
		$rss = $cache->Get($this->type,$url);
		$encoding = $this->GetEncoding($rss);
		$conf = new Configuration();
		$default_encoding = $conf->Get("encoding");
		if($encoding!=$default_encoding)
		{
			$rss = mb_convert_encoding($rss,$default_encoding,$encoding);
			$rss = str_replace("encoding=\"$encoding\"","encoding=\"$default_encoding\"",$rss);
		}
		if($limit>0 && $rss!="")
		{
			$dom = new DOMDocument('1.0', $default_encoding);
			$dom->loadXML($rss);
			$items = $dom->getElementsByTagName("item");
			if($items->length > $limit)
			{
				$item_counter = 0;
				$nodes_to_delete = array();
				foreach($items as $item)
				{
					$item_counter ++;
					if($item_counter>$limit)
					{
						$nodes_to_delete[] = $item;
					}
				}
				foreach($nodes_to_delete as $node)
					$node->parentNode->RemoveChild($node);
				$rss = $dom->saveXML();
			}
		}
		return $rss;
	}
	
	private function GetEncoding($xml)
	{
		$encoding = "UTF-8";
		$rx = '/<?xml.*encoding=[\'"](.*?)[\'"].*?>/m';
		$m = array();
		if (preg_match($rx, $xml, $m))
			$encoding = strtoupper($m[1]);
		else 
		{
			if(function_exists('mb_detect_encoding'))
				$encoding = mb_detect_encoding($xml,"UTF-8, ISO-8859-1, GBK");
		}
		return $encoding;
	}
}
?>
