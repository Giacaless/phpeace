<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/docs.php");

/**
 * Length of download tokens
 *
 */
define('DOC_TOKEN_LENGTH',16);

/**
 * Manage attachment data
 * Add/remove from articles
 * Manage download tokens
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Doc
{
	/**
	 * Current document ID
	 *
	 * @var integer
	 */
	private $id;
	
	/**
	 * Local path for uploaded documents
	 *
	 * @var string
	 */
	private $path;
	
	/** 
	 * @var History */
	private $h;

	/**
	 * Initialize local variable
	 *
	 * @param integer $id
	 */
	function __construct( $id )
	{
		$this->id = $id;
		$this->path = "uploads/docs";
		include_once(SERVER_ROOT."/../classes/history.php");
		$this->h = new History();
	}

	/**
	 * If current user has administrator rights on current document
	 *
	 * @return boolean
	 */
	public function AdminRight()
	{
		$right = false;
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$creator = $this->CreatorId();
		if($session->Get("current_user_id") == $creator['id_user'] )
			$right = true;
		else
		{
			include_once(SERVER_ROOT."/../classes/modules.php");
			if (Modules::AmIAdmin(4) || Modules::AmIAdmin(14))
				$right = true;
		}
		return $right;
	}

	/**
	 * All articles associated to current document
	 *
	 * @return array
	 */
	public function Articles()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_article,a.headline,t.name AS topic_name FROM articles a
			INNER JOIN docs_articles da ON a.id_article=da.id_article
			LEFT JOIN topics t ON a.id_topic=t.id_topic 
			WHERE a.id_topic<>" . $ini->Get('temp_id_topic') . " AND da.id_doc=$this->id";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Associate document to article
	 *
	 * @param integer $id_doc
	 * @param integer $id_article
	 * @param integer $seq			Sequence number
	 * @param integer $id_subtopic_form	Option form to fill in before download
	 */
	public function ArticleAdd($id_doc,$id_article,$seq,$id_subtopic_form)
	{
		$docs = $this->DocsArticle($id_article);
		$tot_docs = count($docs);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_articles" );
		$res[] = $db->query( "UPDATE docs_articles SET id_subtopic_form='$id_subtopic_form'	WHERE id_article='$id_article' AND id_doc='$id_doc' " );
		if($tot_docs>0 && $seq<=$tot_docs)
		{
			$res[] = $db->query( "UPDATE docs_articles SET seq=seq+1 WHERE id_article='$id_article' AND seq >= '$seq' " );
		}
		if($seq==0)
		{
			$seq = $tot_docs + 1;
		}
		$res[] = $db->query( "INSERT INTO docs_articles (id_doc,id_article,seq,id_subtopic_form) VALUES ($id_doc,$id_article,$seq,$id_subtopic_form)" );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet($id_article);
	}

	/**
	 * De-associate document from specific article
	 *
	 * @param integer $id_doc
	 * @param integer $id_article
	 */
	public function ArticleDelete($id_doc,$id_article)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_articles" );
		$res[] = $db->query( "DELETE FROM docs_articles WHERE id_doc='$id_doc' AND id_article='$id_article' " );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$a->ArticleLoad();
		if($a->id_topic > 0)
		{
			include_once(SERVER_ROOT."/../classes/irl.php");
			$irl = new IRL();
			include_once(SERVER_ROOT."/../classes/topic.php");
			$irl->topic = new Topic($a->id_topic);
			$format = $this->DocGetFormat($id_doc);
			$filename = $irl->PublicPath("article_doc",array('id'=>$id_doc,'format'=>$format),FALSE,TRUE);
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$fm->Delete($filename);
			$fm->PostUpdate();
		}
		$this->ArticleUpdateSet($id_article);
	}

	/**
	 * Remove document from article and delete it from filesystem
	 *
	 * @param integer $id_doc
	 * @param integer $id_article
	 */
	public function ArticleRemove($id_doc,$id_article)
	{
		$this->ArticleDelete($id_doc,$id_article);
		$this->DocRemove($id_doc);
		$this->ArticleUpdateSet($id_article);
	}

	/**
	 * Associate document to article
	 *
	 * @param integer $id_doc
	 * @param integer $id_article
	 * @param integer $id_subtopic_form	Option form to fill in before download
	 */
	public function ArticleUpdate($id_doc,$id_article,$id_subtopic_form)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_articles" );
		$res[] = $db->query( "UPDATE docs_articles SET id_subtopic_form='$id_subtopic_form'	WHERE id_article='$id_article' AND id_doc='$id_doc' " );
		Db::finish( $res, $db);
		$this->ArticleUpdateSet($id_article);
	}
	
	/**
	 * Propagate document change to article queue and index
	 *
	 * @param integer $id_article
	 */
	private function ArticleUpdateSet($id_article)
	{
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_article);
		$a->ArticleLoad();
		if ($a->visible && ($a->id_topic)>0)
		{
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($a->id_topic);
			$t->queue->JobInsert($t->queue->types['article'],$id_article,"update");
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->IndexQueueAdd($t->queue->types['article'],$id_article,$t->id,$t->id_group,1);
		}
	}

	/**
	 * Reshuffle the sequence of documents associated to a specific article
	 *
	 * @param integer $id_article
	 */
	private function DocArticleReshuffle($id_article)
	{
		$docs = $this->DocsArticle($id_article);
		$counter = 1;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_articles" );
		foreach($docs as $doc)
		{
			$res[] = $db->query( "UPDATE docs_articles set seq=$counter WHERE id_doc='{$doc['id_doc']}' AND id_article=$id_article");
			$counter ++;
		}
		Db::finish( $res, $db);
	}

	/**
	 * Return info about current document creator
	 *
	 * @return array
	 */
	public function CreatorId()
	{
		return $this->h->CreatorId($this->h->types['document'],$this->id);
	}
	
	/**
	 * All documents associated to a specific article
	 *
	 * @param integer $id_article
	 * @return array
	 */
	private function DocsArticle($id_article)
	{
		$db =& Db::globaldb();
		$docs = array();
		$sqlstr = "SELECT id_doc,seq FROM docs_articles WHERE id_article=$id_article ORDER BY seq ";
		$db->QueryExe($docs, $sqlstr);
		return $docs;
	}

	/**
	 * Create a clone of current document
	 *
	 * @return integer	ID of cloned document
	 */
	public function DocClone()
	{
		$db =& Db::globaldb();
		$doc = $this->DocGet();
		$title = $db->SqlQuote($doc['title']);
		$description = $db->SqlQuote($doc['description']);
		$id_language = $doc['id_language'];
		$id_licence = $doc['id_licence'];
		$format = $db->SqlQuote($doc['format']);
		$author = $db->SqlQuote($doc['author']);
		$source = $db->SqlQuote($doc['source']);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs" );
		$id_doc = $db->nextId( "docs", "id_doc" );
		$res[] = $db->query( "INSERT INTO docs (id_doc,title,description,id_language,id_licence,format,author,source)
			VALUES ($id_doc,'$title','$description','$id_language','$id_licence','$format','$author','$source') " );
		Db::finish( $res, $db);
		$fm = new FileManager;
		$filename_orig = "$this->path/{$this->id}.{$doc['format']}";
		$filename_copy = "$this->path/{$id_doc}.{$doc['format']}";
		$fm->HardCopy($filename_orig,$filename_copy);
		$fm->PostUpdate();
		return $id_doc;
	}
	
	/**
	 * Create a JPG cover of a document
	 */
	public function DocCover($id_doc,$extension)
	{
		include_once(SERVER_ROOT."/../classes/config.php");
		include_once(SERVER_ROOT."/../classes/images.php");
		$conf = new Configuration;
		$docs_covers_size = $conf->Get("docs_covers_size");
		if($id_doc>0 && $extension!="" && $docs_covers_size > -1)
		{
			$i = new Images();
			$i->ConvertWrapper("doc", "uploads/docs/{$id_doc}.{$extension}", $id_doc);
		}
	}
	
	/**
	 * Retrieve current document data
	 *
	 * @return array
	 */
	public function DocGet()
	{
		$doc = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_doc,filename,title,description,id_language,author,source,id_licence,format
		FROM docs WHERE id_doc='$this->id' ";
		$db->query_single($doc, $sqlstr);
		return $doc;
	}
	
	/**
	 * Retrieve current document format
	 *
	 * @param integer Document ID
	 * @return string
	 */
	public function DocGetFormat($id_doc)
	{
		$doc = array();
		$db =& Db::globaldb();
		$db->query_single($doc, "SELECT format FROM docs WHERE id_doc='$id_doc' ");
		return $doc['format'];
	}
	
	/**
	 * Retrieve current by download token
	 *
	 * @param string $token
	 * @return array
	 */
	public function DocGetByToken($token)
	{
		$row = array();
		$doc = $this->TokenGet($token);
		if($doc['id_doc']>0 && $doc['active'])
		{
			$row = $doc;
			$counter = (int)$doc['counter'] + 1;
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "docs_tokens" );
			$res[] = $db->query( "UPDATE docs_tokens SET counter='$counter' WHERE token='$token' " );
			Db::finish( $res, $db);
		}
		return $row;
	}

	/**
	 * Insert new document
	 *
	 * @param string	$title
	 * @param string 	$description
	 * @param integer 	$id_language		Language ID
	 * @param string 	$source
	 * @param string 	$author
	 * @param integer 	$id_licence			License ID
	 * @param array 	$keywords			
	 * @param array 	$file				Uploaded file info
	 * @param array 	$keywords_internal	Array of internal keywords IDs
	 * @return integer
	 */
	public function DocInsert($title,$description,$id_language,$source, $author, $id_licence,$keywords,$file,$keywords_internal,$doc_filename="")
	{
		$fm = new FileManager;
		$file_ext = ($doc_filename!="")? $fm->Extension($doc_filename) : $file['ext'];
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs" );
		$id_doc = $db->nextId( "docs", "id_doc" );
		$orig_filename = ($doc_filename!="")? "{$id_doc}.{$file_ext}" : $file['name'];
		$res[] = $db->query( "INSERT INTO docs (id_doc,filename,format,title,description,id_language,source,author,id_licence)
			VALUES ($id_doc, '$orig_filename','$file_ext','$title','$description','$id_language','$source','$author','$id_licence') " );
		Db::finish( $res, $db);
		$filename = "$this->path/" . $id_doc . "." . $file_ext;
		if($doc_filename!="")
			$fm->Rename($doc_filename,$filename);
		else
			$fm->MoveUpload($file['temp'], $filename);
		$this->DocCover($id_doc,$file_ext);
		$fm->PostUpdate();
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_doc, $o->types['document']);
		$o->InsertKeywordsArray($keywords_internal,$id_doc,$o->types['document']);
		$this->h->HistoryAdd($this->h->types['document'],$id_doc,$this->h->actions['create']);
		return $id_doc;
	}

	/**
	 * Remove a document
	 *
	 * @param integer $id_doc
	 * @param boolean $track	Track this action in document history
	 */
	public function DocRemove($id_doc,$track=true)
	{
		$format = $this->DocGetFormat($id_doc);
		$filename = "{$this->path}/{$id_doc}.{$format}";
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$fm->Delete($filename);
		$fm->PostUpdate();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs" );
		$res[] = $db->query( "DELETE FROM docs WHERE id_doc='$id_doc'" );
		Db::finish( $res, $db);
		$db->begin();
		$db->lock( "docs_tokens" );
		$res[] = $db->query( "DELETE FROM docs_tokens WHERE id_doc='$id_doc'" );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->UseDelete($id_doc,$o->types['document']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$s->ResourceRemove($o->types['document'],$id_doc);
		if($track)
			$this->h->HistoryAdd($this->h->types['document'],$id_doc,$this->h->actions['delete']);
	}
	
	/**
	 * Update document data
	 *
	 * @param string	$title
	 * @param string 	$description
	 * @param integer 	$id_language		Language ID
	 * @param string 	$source
	 * @param string 	$author
	 * @param integer 	$id_licence			License ID
	 * @param array 	$keywords			
	 * @param integer	$id_article
	 * @param array 	$file				Uploaded file info
	 * @param array 	$keywords_internal	Array of internal keywords IDs
	 */
	public function DocUpdate($title,$description,$id_language,$source, $author, $id_licence,$keywords,$id_article,$file,$keywords_internal)
	{
		if ($file['ok'])
		{
			$fm = new FileManager;
			$doc = $this->DocGet();
			$fm->Delete("{$this->path}/{$this->id}.{$doc['ext']}");
			$sqlstr = "UPDATE docs SET title='$title',description='$description',id_language='$id_language',
				source='$source',author='$author',id_licence='$id_licence',filename='{$file['name']}',format='{$file['ext']}'
				 WHERE id_doc='$this->id' ";
			$filename = "{$this->path}/{$this->id}.{$file['ext']}";
			$fm->MoveUpload($file['temp'], $filename);
			$this->DocCover($this->id,$file['ext']);
			$fm->PostUpdate();
		}
		else 
		{
			$sqlstr = "UPDATE docs SET title='$title',description='$description',id_language='$id_language',
			source='$source',author='$author',id_licence='$id_licence' WHERE id_doc='$this->id' ";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['document']);
		$o->InsertKeywordsArray($keywords_internal,$this->id,$o->types['document']);
		$this->h->HistoryAdd($this->h->types['document'],$this->id,$this->h->actions['update']);
		if($id_article>0)
			$this->ArticleUpdateSet($id_article);
	}

	/**
	 * Check whether current document is associated to a specific form subtopic for download
	 *
	 * @param integer $id_subtopic
	 * @return boolean
	 */
	public function FormCheck($id_subtopic)
	{
		$db =& Db::globaldb();
		$docs = array();
		$sqlstr = "SELECT id_doc FROM docs_articles WHERE id_doc={$this->id} AND id_subtopic_form=$id_subtopic ";
		$db->QueryExe($docs, $sqlstr);
		return count($docs)>0;
	}
	
	/**
	 * Validate token
	 *
	 * @param string $token
	 * @return boolean
	 */
	public function IsToken($token)
	{
		return strlen($token) == DOC_TOKEN_LENGTH;
	}
	
	/**
	 * Move documents up or down in list of documents associated to a specific article
	 *
	 * @param integer $id_article
	 * @param integer $dir			Direction (1=up, 0=down)
	 */
	public function Move($id_article,$dir)
	{
		$docs = $this->DocsArticle($id_article);
		$num_docs = count($docs);
		$id_doc = $this->id;
		$counter = 1;
		$id_doc_prev = 0;
		if (!($dir==1 && $id_doc==$docs[0]['id_doc']) && !($dir==0 && $id_doc==$docs[$num_docs-1]['id_doc']))
		{
			foreach ($docs as $doc)
			{
				if ($doc['id_doc']==$id_doc && $dir==1)
				{
					$this->UpdateSeq( $id_doc, $id_article, ($doc['seq']-1) );
					$this->UpdateSeq( $id_doc_prev, $id_article, $doc['seq'] );
				}
				if ($id_doc_prev==$id_doc && $dir==0)
				{
					$this->UpdateSeq( $doc['id_doc'], $id_article, ($doc['seq']-1) );
					$this->UpdateSeq( $id_doc_prev, $id_article, $doc['seq'] );
				}
				$id_doc_prev = $doc['id_doc'];
				$counter ++;
			}
		}
	}

	/**
	 * Delete token
	 *
	 * @param string $token
	 */
	public function TokenDelete($token)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_tokens" );
		$res[] = $db->query( "DELETE FROM docs_tokens WHERE token='$token' " );
		Db::finish( $res, $db);
	}

	/**
	 * Generate a new token
	 *
	 * @return string
	 */
	private function TokenGenerate()
	{
  		$hash = "";
		$possible = "123456789abcdefghijkmnpqrstuvwxyzABCDEFGHKLMNPQRSTUVWXYZ"; 
		$i = 0; 
		while ($i < DOC_TOKEN_LENGTH)
		{ 
			$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
			if (!strstr($hash, $char))
			{
				$hash .= $char;
				$i++;
			}
		}
		return $hash;
	}
	
	/**
	 * Retrieve token data
	 *
	 * @param string $token
	 * @return array
	 */
	public function TokenGet($token)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT dt.id_doc,dt.token,dt.expires,UNIX_TIMESTAMP(dt.expire_date) AS expire_date_ts,dt.max_downloads,dt.counter,d.format,
			IF((dt.max_downloads>0 AND dt.counter>=dt.max_downloads) OR (dt.expires AND dt.expire_date<CURDATE()),0,1) AS active
			FROM docs_tokens dt
			INNER JOIN docs d ON dt.id_doc=d.id_doc
			WHERE dt.token='$token' ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	/**
	 * Store token for current document
	 *
	 * @param string	$token			Existing token to update
	 * @param integer	$max_downloads	Maximum number of downloads
	 * @param booling	$expires		Whether it expires or not
	 * @param date		$expire_date	Expiry date
	 * @return string					New token
	 */
	public function TokenStore($token,$max_downloads,$expires,$expire_date)
	{
		if(!$this->IsToken($token))
		{
			$token = $this->TokenGenerate();
			$sqlstr = "INSERT INTO docs_tokens (id_doc,token,max_downloads,expires,expire_date,counter)
				VALUES ('{$this->id}','$token','$max_downloads','$expires','$expire_date',0) ";
		}
		else 
		{
			$sqlstr = "UPDATE docs_tokens SET max_downloads='$max_downloads',expires='$expires',expire_date='$expire_date'	WHERE token='$token' ";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "docs_tokens" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		return $token;
	}
	
	/**
	 * All tokens for current document
	 *
	 * @param array $rows
	 * @param boolean $paged	Optional pagination
	 * @return integer			Num of tokens
	 */
	public function Tokens(&$rows,$paged=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT dt.id_doc,dt.token,dt.expires,UNIX_TIMESTAMP(dt.expire_date) AS expire_date_ts,dt.max_downloads,dt.counter,
			IF((dt.max_downloads>0 AND dt.counter>=dt.max_downloads) OR (dt.expires AND dt.expire_date<CURDATE()),0,1) AS active
			FROM docs_tokens dt
			WHERE dt.id_doc=$this->id";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}
	
	/**
	 * Update sequence number for document in article's documents list
	 *
	 * @param integer $id_doc
	 * @param integer $id_article
	 * @param integer $seq
	 */
	private function UpdateSeq($id_doc,$id_article,$seq)
	{
		if($id_doc>0 && $id_article>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "docs_articles" );
			$res[] = $db->query( "UPDATE docs_articles SET seq=$seq WHERE id_article=$id_article AND id_doc=$id_doc" );
			Db::finish( $res, $db);		
		}
	}

}
?>
