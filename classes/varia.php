<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Varia
{
	public $return_status;
	
	public function Deserialize($value,$encoded=true)
	{
		$var = unserialize($value);
		 if(is_array($var) && $encoded)
			array_walk($var,array($this,'SerialDecoderMulti'));
		return $var;
	}
	
	public function Exec($command)
	{
		$return_var = 0;
		$result = array();
		exec($command, $result, $return_var);
		$this->return_status = $return_var;
		return $result;
	}

	public function ExecEscaped($command,$arguments=array())
	{
		$return_var = 0;
		$result = array();
		$ecommand = escapeshellcmd($command);
		$earguments = "";
		foreach($arguments as $argument)
			$earguments .= " " . escapeshellarg($argument);
		exec($ecommand . $earguments, $result, $return_var);
		$this->return_status = $return_var;
		return $result;
	}
	
	public static function Hash($string,$use_salt=true)
	{
		if($use_salt)
		{
			include_once(SERVER_ROOT."/../classes/ini.php");
			$ini = new Ini();
			$salt = $ini->Get('crypt_password');
			$hash = md5($salt . md5($string . $salt));
		}
		else 
			$hash = md5($string);
		return $hash;
	}

	public static function IP()
	{
		if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
			$ip_list = $_SERVER['HTTP_X_FORWARDED_FOR'];
			// X-Forwarded-For may return a comma+space separated list of IP addresses
			// If this is the case, we need the last one
			// See http://en.wikipedia.org/wiki/X-Forwarded-For
			$ips = explode(',', $ip_list);
			$ip = $ips[count($ips) - 1];
		}
		else 
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return trim($ip);
	}

	public static function MakeSeed()
	{
		list($usec, $sec) = explode(' ', microtime());
		return (float) $sec + ((float) $usec * 100000);
	}

	public static function Referer()
	{
		return $_SERVER['HTTP_REFERER'];
	}
	
	public function SafeMode()
	{
		$safemode = true;
		$sm = ini_get("safe_mode");
		if (!$sm || strtolower($sm)=="off")
			$safemode = false;
		return $safemode;
	}
	
	public function SetTimeLimit($seconds)
	{
		if (!$this->SafeMode())
			set_time_limit($seconds);
	}

	public function Serialize($var,$escape=true,$encode=true)
	{
		if(is_array($var) && $encode)
			array_walk($var,array($this,'SerialEncoderMulti'));
		$ser_var = serialize($var);
		if($escape)
		{
			$ser_var = addslashes($ser_var);
		}
		return $ser_var;
	}
	
	private function SerialEncoderMulti(&$val)
	{
		if (is_array($val))
			array_walk($val,array($this,'SerialEncoderMulti'));
		else
     			$val = base64_encode($val);
	}

	private function SerialDecoderMulti(&$val)
	{
		if (is_array($val))
			array_walk($val,array($this,'SerialDecoderMulti'));
   		else
			$val = base64_decode($val);
	}

	public static function Uid()
	{
		return md5(uniqid(rand(),true));
	}
	
	public static function UserAgent()
	{
		return $_SERVER['HTTP_USER_AGENT'];
	}

}
?>
