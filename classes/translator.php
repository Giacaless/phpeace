<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/sharedmem.php");
include_once(SERVER_ROOT."/../classes/ini.php");
include_once(SERVER_ROOT."/../classes/config.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");

Class Translator
{
	public $id_language;
	public $lang;
	public $lang_code;
	public $languages;
	public $mem_var;
	public $id_module;
	
	/** 
	 * @var SharedMem */
	private $mem;
	private $id_style;
	private $is_admin;
	private $mem_var_base;
	private $notify_errors;
	private $filename;
	
	function __construct($id_language, $id_module, $is_admin=true, $id_style=0)
	{
		if(!$id_language>0 || !$is_admin)
			$ini = new Ini();
		if ($id_language>0)
			$this->id_language = $id_language;
		else
		{
			$this->id_language = $ini->Get("id_language");
		}
		$this->id_module = $id_module;
		$this->id_style = (int)$id_style;
		$this->is_admin = $is_admin;
		$this->languages = array('--','it','en');
		$language_codes = array('--','it_IT','en_GB');
		$this->mem = new SharedMem(false);
		$this->lang = $this->languages[$this->id_language];
		$this->lang_code = $language_codes[$this->id_language];
		$this->mem_var_base = "phpeace" . PHPEACE_BUILD . "_tr" . "_" . $this->lang . "_" . (($is_admin)? "adm":"pub") . $id_module . "_s" . $this->id_style;
		$this->mem_var = $this->mem_var_base;
		$custom_labels = array();
		if(!$is_admin)
		{
			$key = $ini->Get("install_key");
			$mem_var_custom = $this->mem_var . "_" . $key;
			if($this->mem->IsVarSet($mem_var_custom))
				$this->mem_var = $mem_var_custom;
			elseif(!$this->mem->IsVarSet($this->mem_var_base)) 
			{
				$custom_labels = $this->Labels(0,$id_module,$id_language);
				if($this->id_style > 0)
				{
					$custom_labels1 = $this->Labels($this->id_style,$id_module,$id_language);
					if(count($custom_labels1)>0)
					{
						$custom_labels = array_merge($custom_labels,$custom_labels1);
					}
				}
				if(count($custom_labels)>0)
					$this->mem_var = $mem_var_custom;
			}
		}
		$this->filename = $this->Filename($this->id_language,$id_module,$is_admin);
		$vocabolary = $this->mem->Get($this->mem_var);
		if (!is_array($vocabolary) || count($vocabolary) < 2)
			$this->Load($custom_labels);
		$this->notify_errors = TRUE;
		unset($vocabolary);
	}
	
	public function __destruct()
	{
		unset($this->mem);
	}
	
	public function Filename($id_language,$id_module, $is_admin )
	{
		$lang = $this->languages[$id_language];
		return "lang/$lang/" . (($is_admin)? (($id_module>0)?"mod_".$id_module:"main"):(($id_module>0)?"pub_".$id_module:"pub")) . ".txt";
	}

	private function Labels($id_style,$id_module,$id_language)
	{
		$labels = array();
		include_once(SERVER_ROOT."/../classes/db.php");
		$db =& Db::globaldb();
		$sqlstr = "SELECT labels FROM styles_labels WHERE id_module='$id_module' AND id_language='$id_language' AND (id_style='$id_style' OR id_style=0) ORDER BY id_style ";
		$rows = array();
		$num = $db->QueryExe($rows, $sqlstr);
		if($num>0)
		{
			include_once(SERVER_ROOT."/../classes/varia.php");
			$v = new Varia();
			while((list(,$row) = each($rows))==true)
			{
				if(strlen($row['labels'])>1)
				{
					$clabels = $v->Deserialize($row['labels']);
					if(is_array($clabels) && count($clabels)>0)
						$labels = array_merge($labels,$clabels);
				}
			}
		}
		return $labels;
	}
	
	private function Load($custom_labels)
	{
		$this->mem->Delete($this->mem_var);
		$translation = $this->ParseLangFile();
		if($this->is_admin)
		{
			$admin_filename = "custom/m" . $this->id_module . "_" . $this->lang . ".txt";
			$admin_labels = $this->ParseLangFileCustom($admin_filename);
			if(count($admin_labels)>0)
			{
				foreach($admin_labels as $admin_key=>$admin_label)
				{
					$translation[$admin_key] = $admin_label;
				}
			}
		}
		elseif(count($custom_labels)>0)
		{
			foreach($custom_labels as $custom_key=>$custom_label)
			{
				$translation[$custom_key] = $custom_label;
			}
		}
		if(is_array($translation))
		{
			$this->mem->Set($this->mem_var,$translation);
			unset($translation);	
		}
	}
	
	public function ParseLangFile()
	{
		return $this->ParseLangFileCustom($this->filename);
	}
	
	private function ParseLangFileCustom($langfile)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		$lines = $fm->TextFileLines($langfile);
		$translation = array();
		while((list(,$line) = each($lines))==true)
		{
			if(strlen($line)>1 && strpos($line,"=")>0 && $line{0}!="#")
			{
				$elements = explode("=",$line,2);
				$key = trim($elements[0]);
				$value = trim($elements[1]);
				$left_bracket = strpos($value,"{");
				if ($left_bracket>-1)
				{
					$right_bracket = strpos($value,"}",$left_bracket);
					$values = explode("|",trim(substr($value,$left_bracket + 1,$right_bracket - 1)));
					array_walk($values, array($this,'TrimValue'));
					$translation[ $key ] = $values;
				}
				else
					$translation[ $key ] = $value;
			}
		}
		unset($lines);
		return $translation;
	}

	public function Reset($with_delete=true)
	{
		if($with_delete)
		{
			$this->mem->Delete($this->mem_var_base);
			$ini = new Ini();
			$key = $ini->Get("install_key");
			$this->mem->Delete($this->mem_var_base . "_" . $key);
		}		
		$tr = new Translator($this->id_language,$this->id_module,$this->is_admin,$this->id_style);
		$this->mem_var = $tr->mem_var;
	}
	
	public function Translate($word)
	{
		$tr_word = "";
		$voc = $this->mem->Get($this->mem_var);
		if($this->notify_errors && (!is_array($voc) || !array_key_exists($word,$voc)))
		{
			if(!is_array($voc))
			{
				UserError("Translator vocabulary not set",array('mem_var'=>$this->mem_var,'id_language'=>$this->id_language),512);
			}
			else
			{
				UserError("Word not found in Vocabulary: $word",array('mem_var'=>$this->mem_var,'id_language'=>$this->id_language),512);
			}
			$this->Reset();
		}
		else 
			$tr_word = $voc[$word];
		unset($voc);
		return $tr_word;
	}
	
	public function TranslateParams($word,$params)
	{
	    if(!is_array($params)) {
	        $params = [$params];
	    }
		switch(count($params))
		{
			case 0:
				$tword = $this->TranslateTry($word);
			break;
			case 1:
				$tword = sprintf($this->Translate($word),$params[0]);
			break;
			case 2:
				$tword = sprintf($this->Translate($word),$params[0],$params[1]);
			break;
			case 3:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2]);
			break;
			case 4:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2],$params[3]);
			break;
			case 5:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2],$params[3],$params[4]);
			break;
			case 6:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2],$params[3],$params[4],$params[5]);
			break;
			case 7:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2],$params[3],$params[4],$params[5],$params[6]);
			break;
			case 8:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2],$params[3],$params[4],$params[5],$params[6],$params[7]);
			break;
			case 9:
				$tword = sprintf($this->Translate($word),$params[0],$params[1],$params[2],$params[3],$params[4],$params[5],$params[6],$params[7],$params[8]);
			break;
		}
		return $tword;
	}

	public function TranslateTry($word)
	{
		$return_word = $word;
		if (preg_match("/^[a-zA-Z0-9_]+$/i",$word)>0)
		{
			$voc = $this->mem->Get($this->mem_var);
			if (is_array($voc) && array_key_exists($word,$voc))
				$return_word = $voc[$word];
			unset($voc);
		}
		return $return_word;
	}

	private function TrimValue(&$value)
	{
		$value = trim($value);
	}
	
	public function WordExists($word)
	{
		$voc = $this->mem->Get($this->mem_var);
		return preg_match("/^[a-zA-Z0-9_]+$/i",$word)>0 && is_array($voc) && array_key_exists($word,$voc);
	}

}
?>
