<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/config.php");

class Gallery
{
	public $id;
	public $title;
	public $id_user;
	public $visible;
	public $id_topic;
	public $id_topic_group;
	public $id_group;
	public $show_orig;
	public $show_date;
	public $show_author;
	public $author;
	public $sort_order;
	public $slides_order;
	public $thumb_size;
	public $image_size;
	public $images_per_page;
	public $row;
	public $id_licence;
	public $gimages;
	public $captions_html;
	public $topic_admin;

	/** 
	 * @var History */
	private $h;
	private $associated_image;
	private $session_varname;
	private $isCDN;
	
	function __construct($id)
	{
		if ($id>0)
		{
			$gallery = array();
			$this->id = $id;
			$gallery = $this->GalleryGet();
			$this->title = $gallery['title'];
			$this->id_user = $gallery['id_user'];
			$this->visible = $gallery['visible']? 1:0;
			$this->id_topic = $gallery['id_topic'];
			$this->id_topic_group = $gallery['id_topic_group'];
			$this->id_group = $gallery['id_group'];
			$this->show_orig = $gallery['show_orig'];
			$this->show_date = $gallery['show_date'];
			$this->show_author = $gallery['show_author'];
			$this->author = $gallery['author'];
			$this->sort_order = $gallery['sort_order'];
			$this->images_per_page = $gallery['images_per_page'];
			$params = $this->ParamsDeserialize($gallery['params']);
			$this->thumb_size = $params['thumb_size'];
			$this->image_size = $params['image_size'];
			$this->slides_order = (int)$params['slides_order'];
			$this->associated_image = $gallery['associated_image'];
			$this->id_licence = $gallery['id_licence'];
			$this->captions_html = $gallery['captions_html'];
			$this->topic_admin = $gallery['topic_admin'];
			$this->row = $gallery;
			$this->gimages = array();
			$this->session_varname = "gallery" . $id . "_images";
			$conf = new Configuration();
			$cdn = $conf->Get("cdn");
			$this->isCDN = $cdn!='';
		}
		include_once(SERVER_ROOT."/../classes/history.php");
		$this->h = new History();
	}

	public function AmIAdmin($id_user)
	{
		$admin = false;
		if ($id_user>0 && $this->id>0 && $id_user==$this->id_user)
		{
			$admin = true;
		}
		return $admin;
	}
	
	public function AmIUser($id_user)
	{
		$user = false;
		if($id_user>0 && $this->id>0)
		{
			if($this->AmIAdmin($id_user))
			{
				$user = true;
			}
			elseif($this->topic_admin)
			{
				if ($this->id_topic_group > 0)
				{
					include_once(SERVER_ROOT."/../classes/topics.php");
					$tt = new Topics();
					$user = $tt->AmIAdmin($this->id_topic_group);
				}
				if ($this->id_topic > 0)
				{
					include_once(SERVER_ROOT."/../classes/topic.php");
					$t = new Topic($this->id_topic);
					$user = $t->AmIAdmin();
				}
			}		
		}
		return $user;
	}
	
	public function CountImages()
	{
		$count = array();
		$db =& Db::globaldb();
		$db->query_single($count,"SELECT COUNT(id_image) FROM images_galleries WHERE id_gallery='$this->id'");
		return $count[0];
	}

	public function GalleryDelete()
	{
		if(($this->id)>0)
		{
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "images_galleries" );
			$res[] = $db->query( "DELETE FROM images_galleries WHERE id_gallery='$this->id' " );
			Db::finish( $res, $db);
			$db->begin();
			$db->lock( "galleries" );
			$res[] = $db->query( "DELETE FROM galleries WHERE id_gallery='$this->id' " );
			Db::finish( $res, $db);
			$db->begin();
			$db->lock( "images_galleries_import" );
			$res[] = $db->query( "DELETE FROM images_galleries_import WHERE id_gallery='$this->id' " );
			Db::finish( $res, $db);
			$this->Unpublish();
			include_once(SERVER_ROOT."/../classes/ontology.php");
			$o = new Ontology;
			$o->UseDelete($this->id,$o->types['gallery']);
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			$s->ResourceRemove($o->types['gallery'],$this->id);
		}
	}
	
	public function GalleryGet()
	{
		$gallery = array();
		$db =& Db::globaldb();
		$db->query_single($gallery,"SELECT g.id_gallery,g.title,g.description,UNIX_TIMESTAMP(released) AS released_ts,g.id_user,g.visible,show_orig,
			sort_order,show_date,show_author,note,g.id_topic,g.id_group,g.params,g.author,g.id_licence,g.captions_html,
			gu.name AS group_name,g.associated_image,g.images_per_page,gu.visible as visible_group,g.share_images,g.id_topic_group,g.topic_admin
			FROM galleries g
			LEFT JOIN galleries_groups gu ON g.id_group=gu.id_group
			WHERE id_gallery='$this->id' ");
		return $gallery;
	}

	public function GalleryInsert($title,$description,$released,$id_user,$show_orig,$visible,$sort_order,$show_date,$show_author,$note,$id_topic,$keywords,$id_group,$params,$associated_image,$author,$images_per_page,$id_licence,$share_images,$id_topic_group,$captions_html,$topic_admin )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "galleries" );
		$id_gallery = $db->nextId( "galleries", "id_gallery" );
		$sqlstr = "INSERT INTO galleries (id_gallery,title,description,released,id_user,show_orig,visible,sort_order,show_date,show_author,note,id_topic,id_group,params,associated_image,author,images_per_page,id_licence,share_images,id_topic_group,captions_html,topic_admin)
			 VALUES ($id_gallery,'$title','$description','$released',$id_user,$show_orig,$visible,$sort_order,$show_date,$show_author,'$note',$id_topic,$id_group,'$params','$associated_image','$author','$images_per_page','$id_licence','$share_images','$id_topic_group','$captions_html','$topic_admin')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_gallery, $o->types['gallery']);
		$g2 = new Gallery($id_gallery);
		$g2->Publish(false);
		if($g2->visible)
		{
			include_once(SERVER_ROOT."/../classes/search.php");
			$s = new Search();
			include_once(SERVER_ROOT."/../classes/topic.php");
			$t = new Topic($id_topic);
			$s->IndexQueueAdd($o->types['gallery'],$id_gallery,$id_topic,$t->id_group,1);
		}
		$this->h->HistoryAdd($this->h->types['gallery'],$id_gallery,$this->h->actions['create']);
		return $id_gallery;
	}

	public function GalleryUpdate($title,$description,$released,$id_user,$show_orig,$visible,$sort_order,$show_date,$show_author,$note,$id_topic,$keywords,$id_group,$params,$update_sizes,$associated_image,$author,$images_per_page,$id_licence,$share_images,$update_order,$id_topic_group,$captions_html,$topic_admin)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "galleries" );
		$sqlstr = "UPDATE galleries SET title='$title',description='$description',
		id_user=$id_user,released='$released',show_orig=$show_orig,visible=$visible,sort_order=$sort_order,show_date=$show_date,
		show_author=$show_author,author='$author',note='$note',id_topic=$id_topic,id_group=$id_group,params='$params',
		associated_image='$associated_image',images_per_page='$images_per_page',id_licence='$id_licence',share_images='$share_images',
		id_topic_group='$id_topic_group',captions_html='$captions_html',topic_admin='$topic_admin'
		 WHERE id_gallery='$this->id'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $this->id, $o->types['gallery']);
		include_once(SERVER_ROOT."/../classes/search.php");
		$s = new Search();
		$this->h->HistoryAdd($this->h->types['gallery'],$this->id,$this->h->actions['update']);
		$g2 = new Gallery($this->id);
		include_once(SERVER_ROOT."/../classes/topic.php");
		if($g2->visible)
		{
			$t = new Topic($id_topic);
			$s->IndexQueueAdd($o->types['gallery'],$this->id,$id_topic,$t->id_group,1);
			if(!$this->visible || $this->thumb_size!=$g2->thumb_size || $this->image_size!=$g2->image_size || $this->captions_html!=$g2->captions_html)
			{
				$g2->Publish();
			}
			else 
			{
				$t->queue->JobInsert($t->queue->types['gallery'],$this->id,"update_xml");
			}
		}
		elseif($this->visible)
		{
			$s->ResourceRemove($o->types['gallery'],$this->id);
			$this->Unpublish();
		}
		$topics = $this->Topics();
		if (count($topics)>0)
		{
			foreach ($topics as $topic)
			{
				$t = new Topic($topic[id_topic]);
				$t->queue->JobInsert($t->queue->types['subtopic'],$topic['id_subtopic'],"update");
			}
		}
		if($update_sizes)
		{
			include_once(SERVER_ROOT."/../classes/images.php");
			$i = new Images();
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$images = $this->Images();
			$db->begin();
			$db->lock( "images_galleries" );
			foreach($images as $image)
			{
				$filename1 = "uploads/images/{$g2->thumb_size}/{$image['id_image']}.{$i->convert_format}";
				$filename2 = "uploads/images/{$g2->image_size}/{$image['id_image']}.{$i->convert_format}";
				if ($fm->Exists($filename1) && $fm->Exists($filename2))
				{
					$size1 = $fm->ImageSize($filename1);
					$size2 = $fm->ImageSize($filename2);
					if($size1['width']>0 && $size1['height']>0 && $size2['width']>0 && $size2['height']>0)
					{
						$sqlstr = "UPDATE images_galleries SET thumb_height='{$size1['height']}',image_height='{$size2['height']}' 
						WHERE id_image='{$image['id_image']}' AND id_gallery='$this->id' ";
						$res[] = $db->query($sqlstr);
					}
				}
			}
			Db::finish( $res, $db);
			$fm->PostUpdate();
		}
		if($update_order)
		{
			$this->sort_order = $sort_order;
			$this->PrevNext();
			include_once(SERVER_ROOT."/../classes/session.php");
			$session = new Session();
			$session->Delete($this->session_varname);
		}
	}
	
	public function Image()
	{
		$images = $this->Images(0,true);
		$image = $images[0];
		return $image;
	}

	public function ImageGet( $id_image,$fix=true )
	{
		$image = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT i.format,i.source,ig.link,i.image_date,i.author,i.id_licence,i.caption,
		i.image_date,i.id_image,ig.id_gallery,'$this->image_size' AS image_size,ig.image_height,'$this->show_orig' AS show_orig,
		ig.id_gallery_from,'$this->captions_html' AS captions_html,
		igi.id_image AS id_image_import,igi.caption AS caption_import,ig.id_prev,ig.id_next,
		i.width AS orig_width,i.height AS orig_height
		FROM images i
		LEFT JOIN images_galleries ig ON i.id_image=ig.id_image AND ig.id_gallery=$this->id
		LEFT JOIN images_galleries_import igi ON i.id_image=igi.id_image AND ig.id_gallery=igi.id_gallery
		WHERE i.id_image='$id_image'";
		$db->query_single($image, $sqlstr);
		if($fix)
		{
			if($image['id_image_import']>0)
				$image['caption'] = $image['caption_import'];
			$image['show_orig'] = $this->show_orig;
			$image['show_author'] = $this->show_author;
			if($this->show_author)
				$image['gallery_author'] = $this->author;
			$image['show_date'] = $this->show_date;
			if($this->row['note']!="")
				$image['image_footer'] = $this->row['note'];
			$image['id_licence'] = ($image['id_licence']>0)? $image['id_licence'] : $this->id_licence;
		}
		return $image;
	}

	public function Images($howmany=0,$associated=false)
	{
		if(is_array($this->images) && count($this->gimages)>0)
		{
			$images = $this->gimages;
		}
		else
		{
			$images = array();
			$db =& Db::globaldb();
			$sqlstr = "SELECT ig.id_image,ig.id_gallery,ig.link,i.image_date,
			i.format,i.source,IF(igi.id_image IS NULL,i.caption,igi.caption) AS caption,i.id_licence,i.author,'gallery_image' AS item_type,'$this->thumb_size' AS thumb_size,
			'$this->image_size' AS image_size,ig.thumb_height,ig.image_height,'$this->captions_html' as captions_html,'$this->show_author' as show_author,
			i.width AS orig_width,i.height AS orig_height
			FROM images_galleries ig
			INNER JOIN images i ON ig.id_image=i.id_image
			LEFT JOIN images_galleries_import igi ON ig.id_image=igi.id_image AND ig.id_gallery=igi.id_gallery
			WHERE ig.id_gallery='$this->id' ";
			$sort = ($associated)? $this->associated_image : $this->sort_order;
			switch($sort)
			{
				case "0":
					$sqlstr .= " ORDER BY i.id_image";
				break;
				case "1":
					$sqlstr .= " ORDER BY i.id_image DESC";
				break;
				case "2":
					$sqlstr .= " ORDER BY i.image_date";
				break;
				case "3":
					$sqlstr .= " ORDER BY i.image_date DESC";
				break;
			}
			if($howmany>0)
				$sqlstr .= " LIMIT $howmany";
			$db->QueryExe($images, $sqlstr);
			if($sort=="4" && count($images)>1)
				shuffle($images);
		}
		return $images;
	}

	public function ImagePage($id_image)
	{
		$pos = $this->ImagePosition($id_image);
		$page = floor(($pos -1) / $this->images_per_page) + 1;
		return $page;
	}
	
	public function ImagePosition($id_image)
	{
		if ($this->sort_order==4)
			$pos = 1;
		else
		{
			// TODO improve performance of this position finder, caching or denormalizing it in table
			$g_images = $this->Images();
			$counter = 1;
			$pos = 0;
			foreach($g_images as $g_image)
			{
				if($g_image['id_image']==$id_image)
					$pos = $counter;
				$counter ++;
			}
		}
		return $pos;
	}
	
	public function LoadImages()
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		if($session->IsVarSet($this->session_varname))
		{
			$this->gimages = $session->Get($this->session_varname);
		}
		else
		{
			$this->gimages = $this->Images();
			$session->Set($this->session_varname,$this->gimages);
		}
	}

	private function ParamsDeserialize($params)
	{
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		return $v->Deserialize($params);
	}
	
	public function PrevNext()
	{
		$images = $this->Images();
		$id_prev =0;
		$id_next = 0;
		$tot_images = count($images);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "images_galleries" );
		for($i=0;$i<$tot_images;$i++)
		{
			$image= $images[$i];
			if($i==($tot_images - 1))
				$id_next = 0;
			else 
			{
				$image_next = $images[$i+1];
				$id_next = $image_next['id_image'];
			}
			$sqlstr = "UPDATE images_galleries SET id_prev=$id_prev,id_next=$id_next WHERE id_image={$image['id_image']} AND id_gallery={$this->id} ";
			$res[] = $db->query( $sqlstr );
			$id_prev = $image['id_image'];
		}
		Db::finish( $res, $db);
	}
	
	public function Publish($with_images=true)
	{
		if(($this->id)>0)
		{
			include_once(SERVER_ROOT."/../classes/publishmanager.php");
			$pm = new PublishManager();
			$conf = new Configuration();
			$paths = $conf->Get("paths");
			$path = "pub/" . $paths['graphic'] . "galleries";
			$pm->fm->DirAction("$path","check");
			$pm->fm->DirRemove("$path/$this->id");
			$pm->fm->DirAction("$path/$this->id","check");
			$pm->fm->DirAction("$path/$this->id/$this->thumb_size","check");
			$pm->fm->DirAction("$path/$this->id/$this->image_size","check");
			if($this->visible && $with_images)
			{
				$images = $this->Images();
				foreach($images as $image)
				{
					$this->PublishImage($image['id_image']);
				}
				$pm->GalleryXml($this->id);
			}
		}
	}
	
	public function PublishImage($id_image)
	{
	    if(!$this->isCDN) {
	        include_once(SERVER_ROOT."/../classes/file.php");
	        $fm = new FileManager();
	        include_once(SERVER_ROOT."/../classes/irl.php");
	        $irl = new IRL();
	        $orig = $irl->PathAbs("galleries_image",array('id'=>$id_image,'image_size'=>($this->thumb_size)));
	        $copy = $irl->PublicPath("galleries_image",array('id'=>$id_image,'id_gallery'=>($this->id),'image_size'=>($this->thumb_size)),TRUE,TRUE);
	        $fm->Copy($orig,$copy);
	        $orig = $irl->PathAbs("galleries_image",array('id'=>$id_image,'image_size'=>($this->image_size)));
	        $copy = $irl->PublicPath("galleries_image",array('id'=>$id_image,'id_gallery'=>($this->id),'image_size'=>($this->image_size)),TRUE,TRUE);
	        $fm->Copy($orig,$copy);
	        $fm->PostUpdate();
	    }
	}
	
	public function Random()
	{
		$sort_order = $this->sort_order;
		$this->sort_order = 4;
		$images = $this->Images(1);
		$image = $images[0];
		$this->sort_order =$sort_order;
		return $image;	
	}

	public function RemoveImage($id_image)
	{
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager();
		include_once(SERVER_ROOT."/../classes/irl.php");
		$irl = new IRL();
		$copy1 = $irl->PublicPath("galleries_image",array('id'=>$id_image,'id_gallery'=>($this->id),'image_size'=>($this->thumb_size)),TRUE,TRUE);
		$fm->Delete($copy1);
		$copy2 = $irl->PublicPath("galleries_image",array('id'=>$id_image,'id_gallery'=>($this->id),'image_size'=>($this->image_size)),TRUE,TRUE);
		$fm->Delete($copy2);
		$this->PrevNext();
		if($this->visible)
			$this->Publish(true);
		$fm->PostUpdate();
	}
	
	public function Topics()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_topic,id_subtopic FROM subtopics WHERE id_type=9 AND id_item='$this->id'";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	private function Unpublish()
	{
		if(($this->id)>0)
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$conf = new Configuration();
			$paths = $conf->Get("paths");
			$path = "pub/" . $paths['graphic'] . "galleries";
			$fm->DirRemove("$path/$this->id");
			$fm->PostUpdate();
		}
		
	}
}
?>
