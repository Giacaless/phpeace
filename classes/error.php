<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

/**
 * Debug setting (defaults to false)
 * When set to true, error messages are shown instead of logged
 */
include_once(SERVER_ROOT."/../custom/config.php");
$cfg = new Config;
$debug = (isset($cfg->debug) && is_bool($cfg->debug))? $cfg->debug : false;

/**
 * Error message to be shown when debug is disabled
 * It's configurable, see custom/config.php
 */
$error_message = (isset($cfg->error_message))? $cfg->error_message : "An error occurred and the administrator has been notified";
unset($cfg);

/**
 * Set error reporting level: All errors but run-time notices and compatibility suggestions
 * 
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);

/**
 * Custom Error Handler
 *
 * @param integer	$errno		Error level
 * @param string	$errmsg		Error message
 * @param string	$filename	Filename where error was raised
 * @param integer	$linenum	Line number where error was raised
 * @param array		$vars		Context variables
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function ErrorHandler( $errno, $errmsg, $filename, $linenum, $vars )
{
	global $debug;
	global $error_message;
	
	if ($errno!=8 && $errno!=2048 && $errno!=8192 && strpos($errmsg,"headers already sent")===false)
	{
		$errortype = array (	1   => "Error",
					2   => "Warning",
					4   => "Parsing Error",
					8   => "Notice",
					16  => "Core Error",
					32  => "Core Warning",
					64  => "Compile Error",
					128 => "Compile Warning",
					256 => "User Error",
					512 => "User Warning",
					1024=> "User Notice",
					6143 => "All",
					2048 => "Strict",
					4096 => "Recoverable Error",
					8192 => "Deprecated",
					16384 => "User Deprecated" 	);
		$is_error = in_array($errno, array(1,2,4,16,32,64,256));
		$is_user_error = in_array($errno, array(256,512,1024,16384));

		$errarray = array();
		$errarray['msg'] = $errmsg;
		$errarray['type_num'] = $errno;
		$errarray['type'] = strtoupper($errortype[$errno]);
		$errarray['ts'] = time();
		$errarray['datetime'] = date("d.m.Y H:i:s (T)");
		$errarray['filename'] = $filename;
		$errarray['linenum'] = $linenum;
		if ($is_user_error)
		{
			$errarray['user_vars'] = $vars;
		}
		
		$errarray['stack'] = (version_compare(PHP_VERSION, '5.2.5') >= 0)? debug_backtrace(false) : debug_backtrace();
		
		$server_vars = array('REQUEST_URI','QUERY_STRING','HTTP_HOST','DOCUMENT_ROOT','HTTP_REFERER','HTTP_USER_AGENT','HTTP_COOKIE','HTTP_X_FORWARDED_FOR','REMOTE_ADDR');
		$error_server_vars = array();
		while((list(,$var) = each($server_vars))==true)
		{
			if(isset($_SERVER[$var]))
			{
				if(substr($var,0,7)!="PHPEACE")
				{
					$error_server_vars[$var] = $_SERVER[$var];
				}
			}
		}
		$errarray['server_vars'] = $error_server_vars;
		
		$errarray['post'] = $_POST;
		if(isset($errarray['post']['password']))
			unset($errarray['post']['password']);
		if(isset($errarray['post']['pass']))
			unset($errarray['post']['pass']);

		$errarray['get'] = $_GET;

		if ($debug)
		{
			if (array_key_exists('SHELL', $_ENV) || array_key_exists('SHELL', $_SERVER) || preg_match('/cron\.php$/', $_SERVER['SCRIPT_NAME']))
			{
				echo ErrorText($errarray);
			}
			else 
			{
				echo ErrorStyle();
				echo ErrorHTML($errarray);
			}
		}
		else
		{
			if (strpos($_SERVER['REQUEST_URI'],"actions.php")===false && $is_error && $error_message!="")
			{
				echo "<div class=\"error\">$error_message</div>\n";
			}
		}
		set_error_handler("ErrorTrash");
		include_once(SERVER_ROOT."/../classes/log.php");
		$log = new Log($vars['is_fatal']);
		$log->Store($errarray);
		if($vars['is_fatal'])
		{
			exit;
		}
	}
}

/**
 * Empty error handler to trash errors
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function ErrorTrash()
{
	// Do nothing
}

/**
 * Show error details in HTML
 *
 * @param array $error
 * @return string
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function ErrorHTML($error)
{
	$err = "<div class=\"error-report\"><h2>ERROR REPORT</h2>";
	$err .= "<div class=\"error\">";
	$err .= "<div>{$error['type']} - {$error['datetime']}</div>\n";
	$err .= "<h3>{$error['msg']}";
	if(isset($error['user_vars']['is_fatal']) && $error['user_vars']['is_fatal'])
		$err .= " [FATAL]";
	$err .= "</h3>\n";
	if ($error['filename']!="")
		$err .= "<div>in: <b>{$error['filename']}</b> line <b>{$error['linenum']}</b></div>\n";
	if (isset($error['user_vars']))
	{
		$err .= "<ul>";
		while ((list ($key, $val) = each ($error['user_vars']))==true)
		{
			if ($key!="password" && $key!="is_fatal")
			{
				$err .= "<li>" . ((strpos($key,"pub_msg")===false)? "$key: " : "");
				$err .= "<code>" . nl2br(htmlspecialchars(trim(var_export($val,true)))) . "</code></li>\n";
			}
		}
		$err .= "</ul>";
	}
	$err .= "</div>\n";
	
	if(is_array($error['stack']) && count($error['stack'])>0)
	{
		$err .= "<div class=\"stack\">";
		$err .= "\n<h4>STACK</h4>\n";
		$err .= "<ul>";
		while((list(,$call) = each($error['stack']))==true)
		{
			$err .= "<li>";
			if(isset($call['file']))
				$err .= $call['file'];
			if(isset($call['line']))
				$err .= " line " . $call['line'];
			$err .= " - {$call['function']}";
			if(isset($call['args']) && is_array($call['args']) && count($call['args'])>0)
			{
				$err .= " (" . ErrorStackCallArgs($call['args']) . ")\n";
			}
			$err .= "</li>\n";
		}
		$err .= "</ul>\n";
		$err .= "</div>\n";
	}
	
	if (is_array($error['server_vars']) && count($error['server_vars'])>0)
	{
		$err .= "<div class=\"server_vars\">";
		$err .= "<h4>SERVER VARS</h4>\n";
		$err .= "<ul>";
		while((list($varname,$value) = each($error['server_vars']))==true)
		{
			$err .= "<li>$varname: <code>" . var_export($value,true) . "</code></li>\n";
		}
		$err .= "</ul>\n";
		$err .= "</div>\n";
	}

	if (is_array($error['post']) && count($error['post'])>0)
	{
		$err .= "<div class=\"post\">";
		$err .= "<h4>POST VARS</h4>\n";
		$err .= "<ul>";
		while((list($key,$value) = each($error['post']))==true)
		{
			$err .= "<li>$key: <code>" . var_export($value,true) . "</code></li>\n";
		}
		$err .= "</ul>\n";
		$err .= "</div>\n";
	}
	if (is_array($error['get']) && count($error['get'])>0)
	{
		$err .= "<div class=\"get\">";
		$err .= "<h4>GET VARS</h4>\n";
		$err .= "<ul>";
		while((list($key,$value) = each($error['get']))==true)
		{
			$err .= "<li>$key: <code>" . var_export($value,true) . "</code></li>\n";
		}
		$err .= "</ul>\n";
		$err .= "</div>\n";
	}
	$err .= "</div>\n";
	
	return $err;
}

/**
 * CSS styling for HTML error report
 *
 * @return string
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function ErrorStyle()
{
	return '<style type="text/css">
.error-report {font-family:sans-serif;font-size:0.9em;margin: 1em;padding: 0.2em;border: 1px inset #500;}
.error-report h2 {margin:0 0 0.5em 0;padding:0.2em;color: #fff;background-color: #a00;}
.error-report h4 {margin-bottom:0;}
.error-report ul {margin-top:0.3em;}
.error-report li {margin-bottom:0.2em;}
.error,.stack,.server_vars,.post,.get {padding:0 0.5em;}
</style>
';
}

/**
 * Show error details as plain text
 *
 * @param array $error
 * @return string
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function ErrorText($error)
{
	$err = "##### ERROR REPORT #####\n";
	$err .= "{$error['type']} - {$error['datetime']}\n\n";
	$err .= strtoupper($error['msg']) . "\n";
	if ($error['filename']!="")
		$err .= "in: {$error['filename']} line {$error['linenum']}\n";
	if (isset($error['user_vars']))
	{
		while ((list ($key, $val) = each ($error['user_vars']))==true)
		{
			if ($key!="password" && $key!="is_fatal")
				$err .= "- $key: " . nl2br(htmlspecialchars(trim(var_export($val,true)))) . "\n";
		}
	}
	$err .= "\n";
	
	if(count($error['stack'])>0)
	{
		$err .= "\n## STACK ##\n";
		while((list(,$call) = each($error['stack']))==true)
		{
			$err .= "- ";
			if(isset($call['file']))
				$err .= $call['file'];
			if(isset($call['line']))
				$err .= " line " . $call['line'];
			$err .= " - {$call['function']}";
			if(count($call['args'])>0)
			{
				$err .= " (" . ErrorStackCallArgs($call['args']) . ")\n";
			}
		}
		$err .= "\n";
	}
	
	if (count($error['server_vars'])>0)
	{
		$err .= "\n## SERVER VARS ##\n";
		while((list($varname,$value) = each($error['server_vars']))==true)
		{
			$err .= "- $varname: " . var_export($value,true) . "\n";
		}
	}

	if (count($error['post'])>0)
	{
		$err .= "\n## POST VARS ##\n";
		while((list($key,$value) = each($error['post']))==true)
		{
			$err .= "- $key: " . var_export($value,true) . "\n";
		}
	}
	if (count($error['get'])>0)
	{
		$err .= "\n## GET VARS ##\n";
		while((list($key,$value) = each($error['get']))==true)
		{
			$err .= "- $key: " . var_export($value,true) . "\n";
		}
	}
	$err .= "\n\n";
	return $err;
}

/**
 * Info on argument of a function call
 *
 * @param mixed $call_args
 * @return string
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function ErrorStackCallArgs($call_args)
{
	$args = "";
	if(is_array($call_args))
	{
		while((list(,$arg) = each($call_args))==true)
		{
			if (!empty($args))
				$args .= ', ';
			switch (gettype($arg))
			{
				case 'integer':
				case 'double':
					$args .= $arg;
				break;
				case 'string':
					if (substr($arg,0,5)=="<?xml")
						$arg = "[[XML]]";
					else
						$arg = htmlspecialchars(substr($arg, 0, 200)).((strlen($arg) > 200) ? '...' : '');
					$args .= "\"$arg\"";
				break;
				case 'array':
					$args .= 'Array('.count($arg).')';
				break;
				case 'object':
					$args .= 'Object('.get_class($arg).')';
				break;
				case 'resource':
					$args .= 'Resource('.get_resource_type($arg).')';
				break;
				case 'boolean':
					$args .= $arg ? 'True' : 'False';
				break;
				case 'NULL':
					$args .= 'Null';
				break;
				default:
					$args .= 'Unknown';
			}
		}				
	}
	return $args;
}

/**
 * Trigger user error
 *
 * @param string	$msg		Error message
 * @param array		$vars		Error message variables
 * @param integer	$type		Error type 	(256="User Error", 512="User Warning")
 * @param boolean	$is_fatal	Fatal error (stop execution)
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
function UserError($msg, $vars,$type=256,$is_fatal=false)
{
	$vars['is_fatal'] = $is_fatal;
	ErrorHandler($type, $msg, "", "", $vars);
}

// Set custom error handler
$old_eh = set_error_handler("ErrorHandler");

?>
