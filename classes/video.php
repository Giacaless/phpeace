<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/media.php");

class Video extends Media
{
	public $video_thumbs;
	public $video_max_size;
	public $video_resize_width;
	public $video_resize_height;

	function __construct()
	{
		$conf = new Configuration();		
		$this->video_max_size = $conf->Get("video_max_size");
		$this->video_resize_width = $conf->Get("video_resize_width");
		$this->video_resize_height = $conf->Get("video_resize_height");
		$this->video_thumbs = $conf->Get("video_thumbs");
		parent::__construct("video");
	}
	
	public function EmbedHtml($hash)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		return "<embed src=\"" . $ini->Get("pub_web") . "/tools/mediaplayer.swf\" width=\"{$this->video_resize_width}\" height=\"{$this->video_resize_height}\" allowscriptaccess=\"always\" allowfullscreen=\"false\" flashvars=\"file=" . $this->FlashVarsEncode($this->irl->PublicUrlGlobal("video_xml",array('hash'=>$hash))) . "\" />";
	}
	
	protected function EncodePost($id_video,$enc_file)
	{
		$enc_file_abs = $this->fm->RealPath($enc_file);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		for($j=1;$j<=$this->video_thumbs;$j++)
		{
			$image_file_abs = $this->irl->PathAbs("video_image",array('id'=>$id_video,'seq'=>$j));
			$image_file = $this->fm->RealPath($image_file_abs);
			$image_command = $this->FfmpegSnapshotCommand($enc_file_abs,$image_file,$j);
			$v->Exec($image_command);
			if($j==1)
				$i->ConvertWrapper("video_thumb",$image_file_abs, $id_video);
		}
		$this->VideoInfoUpdate($id_video,$enc_file,false);
	}
	
	protected function FfmpegEncodeCommand($input_file,$output_file)
	{
		$conf = new Configuration();
		return "nice -n {$this->ffmpeg_niceness} ffmpeg -i $input_file -t " . $conf->Get("video_length_limit") . " -b " . $conf->Get("video_bitrate") . " -r " . $conf->Get("video_frame_rate") . " -ab " . $conf->Get("video_audio_bitrate") . " -ar " . $conf->Get("video_audio_sample_rate") . " -s " . $conf->Get("video_resize_width") . "x" . $conf->Get("video_resize_height") . " " . $conf->Get("ffmpeg_options") . " -y $output_file 2>&1 ";
	}
	
	protected function FfmpegSnapshotCommand($input_file,$output_file,$sequence)
	{
		$time = "00:0" . ($sequence-1) . ":03";
		return "nice -n {$this->ffmpeg_niceness} ffmpeg -i $input_file -f mjpeg -an -vframes 1 -y -ss $time $output_file ";
	}
	
	private function HashUnique()
	{
		$hash = $this->Hash();
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_video FROM videos WHERE hash='$hash' ");
		return $row['id_video']>0? $this->HashUnique(): $hash ;
	}
	
	protected function MediaGet($id_media)
	{
		return $this->VideoGet($id_media);
	}
	
	public function ImageAssociate($image_seq,$id_video,$file)
	{
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		if($file['ok'])
		{
			$filename = $this->irl->PathAbs("video_image",array('id'=>$id_video,'seq'=>"0"));
			$this->fm->MoveUpload($file['temp'], $filename);
			$i->Convert($this->video_resize_width, $filename, $filename);
			$image_seq = "0";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "videos" );
		$res[] = $db->query( "UPDATE videos SET image_seq=$image_seq WHERE id_video=$id_video " );
		Db::finish( $res, $db);
		$image_file = $this->irl->PathAbs("video_image",array('id'=>$id_video,'seq'=>$image_seq));
		$i->ConvertWrapper("video_thumb",$image_file, $id_video);
		$row = $this->VideoGet($id_video);
		if($row['approved'] && $row['encoded'])
			$this->Publish($id_video,$image_seq,$row['hash']);
	}
	
	public function Publish($id_video,$image_seq,$hash,$hash2="",$title="")
	{
		if($hash2!="")
		{
			$this->PublishMedia($id_video,$hash,$hash2,$title);		
		}
		$image_orig = $this->irl->PathAbs("video_image",array('id'=>$id_video,'seq'=>$image_seq));
		$image_pub = $this->irl->PublicPath("video_image",array('hash'=>$hash),true,true);
		$this->fm->Copy($image_orig,$image_pub);
		$thumb_orig = $this->irl->PathAbs("video_thumb",array('id'=>$id_video));
		$thumb_pub = $this->irl->PublicPath("video_thumb",array('hash'=>$hash),true,true);
		$this->fm->Copy($thumb_orig,$thumb_pub);
		$this->fm->PostUpdate();
	}
	
	protected function Tracklist($hash,$title)
	{
		$tl = array();
		$tl['version'] = "1";
		$tl['xmlns'] = "http://xspf.org/ns/0/";
		$tl['width'] = $this->video_resize_width;
		$tl['height'] = $this->video_resize_height;
		$video_array = array('xname'=>"track");
		$video_array['identifier'] = array('xvalue'=>$hash);
		$video_array['title'] = array('xvalue'=>$title);
		$video_array['image'] = array('xvalue'=>$this->irl->PublicUrlGlobal("video_image",array('hash'=>$hash),true));
		$video_array['location'] = array('xvalue'=>$this->irl->PublicUrlGlobal("video_enc_link",array('hash'=>$hash),true));
		$video_array['logo'] = array('xvalue'=>$this->irl->PublicUrlGlobal("watermark",array()));
		$video_array['meta'] = array('rel'=>"type",'xvalue'=>"flv");
		$vt = array('xname'=>"trackList");
		$vt['track'] = $video_array;
		$tl['trackList'] = $vt;
		return $tl;
	}

	public function VideoDelete($id_video)
	{
		$row = $this->VideoGet($id_video);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "videos" );
		$res[] = $db->query( "DELETE FROM videos WHERE id_video=$id_video" );
		Db::finish( $res, $db);
		$this->MediaDelete($id_video,$row['format']);
		$this->VideoDeletePub($row['hash'],$row['hash2']);
		for($i=0;$i<=$this->video_thumbs;$i++)
		{
			$image_file = $this->irl->PathAbs("video_image",array('id'=>$id_video,'seq'=>$i));
			$this->fm->Delete($image_file);
		}
		$thumb_file = $this->irl->PathAbs("video_thumb",array('id'=>$id_video));
		$this->fm->Delete($thumb_file);
		$this->fm->PostUpdate();
	}
	
	private function VideoDeletePub($hash,$hash2)
	{
		$pub_file = $this->irl->PublicPath("video_enc",array('hash2'=>$hash2),true,true);
		$image_file = $this->irl->PublicPath("video_image",array('hash'=>$hash),true,true);
		$thumb_file = $this->irl->PublicPath("video_thumb",array('hash'=>$hash),true,true);
		$xml_file = $this->irl->PublicPath("video_xml",array('hash'=>$hash),true,true);
		$this->fm->Delete($pub_file);
		$this->fm->Delete($image_file);
		$this->fm->Delete($thumb_file);
		$this->fm->Delete($xml_file);
		$this->fm->PostUpdate();
	}
	
	public function VideoGet($id_video,$approved_only=false)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT v.id_video,UNIX_TIMESTAMP(v.insert_date) AS insert_date_ts,v.format,v.title,v.description,v.author,v.source,
			v.bytes,v.id_licence,v.id_topic,v.encoded,v.approved,v.hash,v.hash2,mi.original,mi.encoded AS 'encoded_info',v.image_seq,v.bytes_enc,
			v.length,v.link,v.views,v.id_language,v.auto_start
			FROM videos v 
			INNER JOIN media_info mi ON v.id_video=mi.id_media AND mi.id_type='{$this->id_type}'
			WHERE v.id_video='$id_video' ";
		if($approved_only)
			$sqlstr .= " AND v.approved=1 AND v.encoded=1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function VideoGetByHash($hash)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT v.id_video,UNIX_TIMESTAMP(v.insert_date) AS insert_date_ts,v.title,v.description,v.author,v.source,
			v.bytes,v.id_licence,v.id_topic,v.hash,v.image_seq,v.hash2,v.length,v.link,v.views,v.id_language,v.auto_start
			FROM videos v 
			WHERE v.hash='$hash' AND v.approved=1 AND v.encoded=1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function VideoInsert($insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$approved,$file,$auto_start,$encode)
	{
		$hash = $this->HashUnique();
		$hash2 = $this->Hash2();
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("videos","media_info"));
		$id_video = $db->nextId( "videos", "id_video" );
		$sqlstr1 = "INSERT INTO videos (id_video,insert_date,format,title,description,author,source,id_language,link,id_licence,id_topic,bytes,encoded,approved,hash,hash2,auto_start) 
			VALUES ($id_video,'$insert_date','{$file['ext']}','$title','$description','$author','$source','$id_language','$link','$id_licence',0,'{$file['size']}',0,'$approved','$hash','$hash2','$auto_start') ";
		$res1[] = $db->query( $sqlstr1 );
		$sqlstr2 = "INSERT INTO media_info (id_media,id_type,original,encoded) VALUES ($id_video,'{$this->id_type}','','')";
		$res1[] = $db->query( $sqlstr2 );
		Db::finish( $res1, $db);

		$filename = $this->irl->PathAbs("video_orig",array('size'=>-1,'id'=>$id_video,'format'=>$file['ext']));
		$this->fm->MoveUpload($file['temp'], $filename);
		$this->VideoInfoUpdate($id_video,$filename,true);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_video, $this->id_res_type);
		$this->h->HistoryAdd($this->id_res_type,$id_video,$this->h->actions['create']);
		if($approved)
			$this->h->HistoryAdd($this->id_res_type,$id_video,$this->h->actions['approve']);
		if($encode)
			$this->EncodeQueueAdd($id_video);
		else 
		{
			$enc_file = $this->irl->PathAbs("video_enc",array('id'=>$id_video));
			$this->fm->Copy($filename,$enc_file);
			$this->EncodePost($id_video,$enc_file);
			if($approved)
				$this->Publish($id_video,1,$hash,$hash2,$title);
		}
		$this->fm->PostUpdate();
		return $id_video;
	}
	
	private function VideoInfoUpdate($id_video,$filename,$is_orig)
	{
		$media_info = $this->MediaInfo($filename);
		$db =& Db::globaldb();
		$info = $db->SqlQuote($media_info['info']);
		$length = $db->SqlQuote($media_info['length']);
		$this->MediaInfoUpdate($id_video,$info,$is_orig);
		if(!$is_orig)
		{
			$enc_info = $this->fm->FileInfo($filename);
			$db->begin();
			$db->lock( "videos" );
			$res[] = $db->query( "UPDATE videos SET encoded=1,image_seq=1,bytes_enc='{$enc_info['size']}',length='$length' WHERE id_video=$id_video " );
			Db::finish( $res, $db);		
		}
	}
	
	public function VideoUpdate($id_video,$insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$keywords_internal,$approved,$auto_start)
	{
		$row = $this->VideoGet($id_video);
		$sqlstr = "UPDATE videos SET insert_date='$insert_date',title='$title',description='$description',author='$author',source='$source',
			link='$link',id_licence='$id_licence',approved='$approved',id_language='$id_language',auto_start='$auto_start'
			WHERE id_video=$id_video" ;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "videos" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_video, $this->id_res_type);
		$o->InsertKeywordsArray($keywords_internal,$id_video,$this->id_res_type);
		$this->h->HistoryAdd($this->id_res_type,$id_video,$this->h->actions['update']);
		if($approved!=$row['approved'])
		{
			if($approved)
			{
				$this->Publish($id_video,$row['image_seq'],$row['hash'],$row['hash2'],$row['title']);
				$this->h->HistoryAdd($this->id_res_type,$id_video,$this->h->actions['approve']);
			}
			else 
			{
				$this->VideoDeletePub($row['hash'],$row['hash2']);
				$this->h->HistoryAdd($this->id_res_type,$id_video,$this->h->actions['reject']);
			}
		}
		if($approved && $row['encoded'])
			$this->Index($id_video);
	}
	
	public function VideoUpdateOrig($id_video,$file,$encode)
	{
		$row = $this->VideoGet($id_video);
		$filename = $this->irl->PathAbs("video_orig",array('id'=>$id_video,'format'=>$file['ext']));
		$this->fm->MoveUpload($file['temp'], $filename);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "videos" );
		$res[] = $db->query( "UPDATE videos SET approved='0',encoded='0',bytes='{$file['size']}',format='{$file['ext']}'  WHERE id_video=$id_video" );
		Db::finish( $res, $db);
		$this->VideoInfoUpdate($id_video,$filename,true);
		if($encode)
		{
			$this->EncodeQueueAdd($id_video);
		}
		else 
		{
			$enc_file = $this->irl->PathAbs("video_enc",array('id'=>$id_video));
			$this->fm->Copy($filename,$enc_file);
			$this->EncodePost($id_video,$enc_file);
		}
		$this->fm->PostUpdate();
		$this->h->HistoryAdd($this->id_res_type,$id_video,$this->h->actions['create']);
		if($row['approved'])
			$this->VideoDeletePub($row['hash'],$row['hash2']);
	}
	
	public function VideoView($id_video)
	{
		$session_var = "video_$id_video";
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		if(!$session->IsVarSet($session_var))
		{
			$db =& Db::globaldb();
			$sqlstr =  "UPDATE LOW_PRIORITY videos SET views=views+1 WHERE id_video='$id_video' ";
			$db->query( $sqlstr );
			$session->Set($session_var, 1 );
		}
	}
	
	public function VideosAll(&$rows,$id_topic=0,$paged=true,$only_approved=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT v.id_video,UNIX_TIMESTAMP(v.insert_date) AS insert_date_ts,v.format,v.title,v.description,v.author,v.source,
			v.bytes,v.id_licence,v.id_topic,v.encoded,v.approved,'video' AS item_type,v.hash,v.length,v.bytes_enc,v.image_seq,v.hash2,v.views
			FROM videos v 
			WHERE id_video>0 ";
		if($id_topic>0)
			$sqlstr .= " AND v.id_topic=$id_topic ";
		if($only_approved)
			$sqlstr .= " AND v.approved=1 AND v.encoded=1 ";
		$sqlstr .= " ORDER BY v.insert_date DESC, v.id_video DESC ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	public function Watermark( $file )
	{
		$this->fm->MoveUpload($file['temp'], "uploads/custom/watermark.png");
		$watermark = $this->irl->PublicPath("watermark",array(),true,true);
		$this->fm->Copy("uploads/custom/watermark.png",$watermark);
		$this->fm->PostUpdate();
	}

}

?>
