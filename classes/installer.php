<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/lock.php");
include_once(SERVER_ROOT."/../classes/file.php");
define('MIN_MEMORY_LIMIT',64);

class Installer
{
	/**
	 * @var Translator */
	public $trm;
	
	/**
	 * @var Lock */
	public $lock;

	public $step;
	public $steps = 6;
	public $messages = array();
	public $test_cases = array('main_paths','path_rights','php_version','db_conn','is_installed','php_register_globals','php_file_uploads','domxml','ctype','curl','iconv','imap','json','multibyte','imagemagick','imagemagick_create','imagemagick_resize','xslt','tar');
	public $test_msg;
	public $test_details;
	public $errors;
		
	function __construct($step,$id_language)
	{
		$this->step = (is_numeric($step) && $step>0)? $step : 0;
		$this->trm = new Translator($id_language,17);
		$this->lock = new Lock("installer");
		$conf = new Configuration("",true);
	}

	private function ContentInit()
	{
		$fm = new FileManager();
		// XSL & CSS
		include_once(SERVER_ROOT."/../classes/xsl.php");
		include_once(SERVER_ROOT."/../classes/css.php");
		include_once(SERVER_ROOT."/../classes/pagetypes.php");			
		$pt = new PageTypes();
		$xslm = new XslManager("global");
		$csm = new CssManager("global");
		foreach($pt->gtypes as $type=>$id)
		{
			$xsl = $fm->TextFileRead("xsl/0/$type.xsl");
			$xslm->id_pagetype = $id;
			$xslm->XslInsert($id,0,"",$xsl,1);
			$css = $fm->TextFileRead("pub/css/0/$type.css");
			$csm->id_pagetype = $id;
			$csm->CssInsert($id,0,"",$css);
		}
		$xslm = new XslManager();
		$csm = new CssManager();
		foreach($pt->types as $type=>$id)
		{
			$xsl = $fm->TextFileRead("xsl/0/$type.xsl");
			$xslm->id_pagetype = $id;
			$xslm->XslInsert(0,0,"",$xsl,1);
			$css = $fm->TextFileRead("pub/css/0/$type.css");
			$csm->id_pagetype = $id;
			$csm->CssInsert(0,0,"",$css);
		}
		include_once(SERVER_ROOT."/../classes/modules.php");
		$modules = Modules::AvailableModules();
		$xslm = new XslManager("module");
		$csm = new CssManager("module");
		foreach($modules as $module)
		{
			if($module['layout']=="1" && $module['internal']=="0" && !$module['global'])
			{
				$xsl = $fm->TextFileRead("xsl/0/{$module['path']}.xsl");
				$xslm->id_pagetype = $module['id_module'];
				$xslm->XslInsert(0,0,"",$xsl,1);
				$css = $fm->TextFileRead("pub/css/0/{$module['path']}.css");
				$csm->id_pagetype = $module['id_module'];
				$csm->CssInsert(0,0,"",$css);
			}
		}
		include_once(SERVER_ROOT."/../classes/phpeace.php"); 
		$phpeace = new PhPeace(); 
		$phpeace->CssStore(CSS_EMPTY); 
		// Style
		include_once(SERVER_ROOT."/../classes/styles.php");
		$s = new Styles();
		$s->StyleInsert($this->trm->Translate("style_title"),$this->trm->Translate("style_desc"),0,0);
		$s->BoxesTypeStore(0,0,'text');
		// Features
		$pt0 = new PageTypes();
		$pt0->FeatureStore( 0,$this->trm->Translate("topics_groups"),$this->trm->Translate("topics_groups_desc"),0,0,0,13,1,0,"",0,"",0,0 );
		$pt0->FeatureStore( 0,$this->trm->Translate("galleries_groups"),$this->trm->Translate("galleries_groups_desc"),0,0,0,14,1,0,"",0,"",0,0 );
		$id_feature = $pt0->GlobalFeatureStore( 0,$this->trm->Translate("latest_articles"),$this->trm->Translate("latest_articles"),0,1,0,0,1,0,0);
		$params = array('limit'=>10,'id_topic_group'=>1);
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia();
		$pt0->PageFunctionsParamsStore($id_feature,$v->Serialize($params));
		$id_feature = $pt0->FeatureStore( 0,$this->trm->Translate("next_events"),$this->trm->Translate("next_events"),0,0,0,4,1,0,"",0,"",0,0 );
		$params = array('limit'=>5,'id_topic'=>"id_topic");
		$pt0->PageFunctionsParamsStore($id_feature,$v->Serialize($params));
		// Scheduler
		include_once(SERVER_ROOT."/../classes/scheduler.php");
		$sc = new Scheduler();
		$sc->ScheduleStore(0,0,1,1,0);
		$sc->ScheduleStore(0,1,1,1,0);
		$sc->ScheduleStore(0,2,0,1,0);
		$sc->ScheduleStore(0,3,1,1,0);
		$sc->ScheduleStore(0,2,1,1,0);
		$sc->ScheduleStore(0,4,1,1,0);
		$sc->ScheduleStore(0,5,1,1,0);
		$sc->ScheduleStore(0,6,1,1,0);
		$sc->ScheduleStore(0,7,0,1,0);
		$sc->ScheduleStore(0,8,1,0,0);
		$sc->ScheduleStore(0,9,1,1,0);
		$sc->ScheduleStore(0,15,1,1,0);
		$id_schedule = $sc->ScheduleStore(0,10,1,1,0);
		$sc->SchedulableActionStore($id_schedule,$v->Serialize(array('wday'=>1)));
		$sc->ScheduleStore(0,19,0,1,0);
		include_once(SERVER_ROOT."/../classes/services.php");
		$se = new Services();
		$se->ServiceStatusUpdate("scheduler",1,"");
		// Favicon
		$fm->HardCopy("install/favicon.ico","uploads/custom/favicon.ico");
		$fm->Copy("uploads/custom/favicon.ico","admin/favicon.ico");
		$fm->HardCopy("uploads/custom/favicon.ico","uploads/graphics/favicon/0.ico");
		$fm->Copy("uploads/graphics/favicon/0.ico","pub/favicon.ico");
		$fm->HardCopy("install/robots.txt","pub/robots.txt");
		$fm->WritePage("uploads/custom/admin.js","");
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$ini->SetTimestamp("last_cron");
		$ini->Set("topic_store",$this->trm->Translate("topic_store"));
		include_once(SERVER_ROOT."/../classes/topics.php");
		$tt = new Topics();
		$tt->gh->GroupInsert($this->trm->Translate("topics"),$this->trm->Translate("topics_desc"),0,1,"");
		include_once(SERVER_ROOT."/../classes/galleries.php");
		$gg = new Galleries();
		$gg->gh->GroupInsert($this->trm->Translate("galleries_title"),$this->trm->Translate("galleries_desc"),0,1,"");
		include_once(SERVER_ROOT."/../classes/topic.php");
		$t = new Topic(0);
		$id_topic1 = $t->TopicInsert($this->trm->Translate("topic_test"),$this->trm->Translate("topic_test"),"","test",$this->trm->id_language,1,1,1,"",1,1,1,0,"",0,1,0,0,1,"","",0,0);
		$t1 = new Topic($id_topic1);
		$t1->TopicUpdateGraphic(1,15,2,3,1,1,0,1,0);
		$t = new Topic(0);
		$id_topic2 = $t->TopicInsert($this->trm->Translate("trashbin"),$this->trm->Translate("trashbin_desc"),"","temp",$this->trm->id_language,0,1,0,"",0,0,0,0,"",0,1,0,0,0,"","",0,0);
		$ini->Set("temp_id_topic",$id_topic2);
		include_once(SERVER_ROOT."/../classes/banners.php");
		$banners = new Banners();
		$banner_groups = $this->trm->Translate("banner_groups");
		$banners->GroupInsert($banner_groups[0],"88","31","",0);
		$banners->GroupInsert($banner_groups[1],"120","60","",0);
		$banners->GroupInsert($banner_groups[2],"234","60","",0);
		$banners->GroupInsert($banner_groups[3],"468","60","",0);
		$banners->GroupInsert($banner_groups[4],"120","120","",0);
		$params = array();
		$params['fp_1'] = array('id_param'=>1,'label'=>"name",'type'=>"text",'type_params'=>"",'mandatory'=>0);
		$params['fp_2'] = array('id_param'=>2,'label'=>"surname",'type'=>"text",'type_params'=>"",'mandatory'=>0);
		$params['fp_3'] = array('id_param'=>3,'label'=>"email",'type'=>"text",'type_params'=>"",'mandatory'=>1);
		$params['fp_4'] = array('id_param'=>4,'label'=>"geo_name",'type'=>"geo",'type_params'=>"",'mandatory'=>0);
		$params['fp_5'] = array('id_param'=>5,'label'=>"comments",'type'=>"textarea",'type_params'=>"",'mandatory'=>0);
		$ser_params = $v->Serialize($params);
		include_once(SERVER_ROOT."/../classes/forms.php");
		$fo = new Forms();
		$id_form1 = $fo->FormStore(0,0,1,0,'Contact',0,0,"",0,0,'',0,'',0,1,0,3,"","",0,0);
		$fo->ParamStore($id_form1,$ser_params);
		$params2 = array();
		$params2['fp_1'] = array('id_param'=>1,'label'=>"name",'type'=>"text",'type_params'=>"",'mandatory'=>0);
		$params2['fp_2'] = array('id_param'=>2,'label'=>"surname",'type'=>"text",'type_params'=>"",'mandatory'=>0);
		$params2['fp_3'] = array('id_param'=>3,'label'=>"email",'type'=>"text",'type_params'=>"",'mandatory'=>1);
		$ser_params2 = $v->Serialize($params2);
		$id_form2 = $fo->FormStore(0,0,1,1,'Newsletter',0,0,"",1,0,'',0,'',0,1,0,3,"","",0,0);
		$fo->ParamStore($id_form2,$ser_params2);
		$ini->Set("search_config","1|1|1|0|1|1|1|1|1|0|1|0|1|1|0|0|0|0|0|0|0");
		$sc->pm->JavaScriptCustom();
	}
	
	public function DatabaseInit()
	{
		include_once(SERVER_ROOT."/../classes/db.php");
		$db =& Db::globaldb();
		$db->Restore("install/dbschema.sql");
		$db->Restore("install/init.sql");
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini(false);
		$ini->Initialise();
		$db->Restore("install/init{$this->trm->id_language}.sql");
		$this->lock->LockSet();
		include_once(SERVER_ROOT."/../classes/varia.php");
		$v = new Varia;
		$key1 = $v->Uid();
		$key2 = $v->Uid();
		$ini->Set("install_key",$key1);
		$ini->Set("crypt_password",$key2);
		$ini->Set("id_language",$this->trm->id_language);
		switch($this->trm->id_language)
		{
			case "1":
				$ini->Set("geo_location",1);
				$ini->Set("default_currency",2);
			break;			
			case "2":
				$ini->Set("geo_location",6);
				$ini->Set("default_currency",3);
			break;			
		}
		$ini->SetModule("galleries","title",$this->trm->Translate("galleries"));
		$ini->SetModule("galleries","description","");
		$today = $db->getTodayTime();
		$ini->Set("install_date",$today);
		include_once(SERVER_ROOT."/../classes/mail.php");
		$mail = new Mail();
		$mail->SetLastNewsletter();
		include_once(SERVER_ROOT."/../classes/phpeace.php");
		$ini->Set("db_version",PHPEACE_DB_VERSION);
		$ini->Set("script_version",PHPEACE_BUILD);
	}
	
	public function DirectoriesInit()
	{
		$fm = new FileManager;
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini();
		$ini->ForceReload();
		include_once(SERVER_ROOT."/../classes/config.php");
		$conf = new Configuration();
		$dev = $conf->Get("dev");
		if(!$dev)
		{
			include_once(SERVER_ROOT."/../classes/phpeace.php"); 
			$phpeace = new PhPeace(); 
			foreach($phpeace->PublicPaths() as $path=>$db_path)
			{
				if ($path!="phpeace" && $path!="tools")
				{
					$fm->Rename("pub/_".$path,"pub/$db_path",false,false,false,false);
				}
			}		
		}
		$ini->SetPath("map_path",$ini->Get('map_path'),false);
		$fm->DirAction("log","check");
		$fm->DirAction("cache","check");
		$fm->DirAction("uploads","check");
		$fm->DirAction("uploads/banners","check");
		$fm->DirAction("uploads/banners/orig","check");
		$fm->DirAction("uploads/banners/0","check");
		$fm->DirAction("uploads/covers","check");
		$fm->DirAction("uploads/covers/orig","check");
		$fm->DirAction("uploads/covers/0","check");
		$fm->DirAction("uploads/covers/1","check");
		$fm->DirAction("uploads/covers/2","check");
		$fm->DirAction("uploads/events","check");
		$fm->DirAction("uploads/events/orig","check");
		$fm->DirAction("uploads/events/0","check");
		$fm->DirAction("uploads/events/1","check");
		$fm->DirAction("uploads/events/2","check");
		$fm->DirAction("uploads/forms","check");
		$fm->DirAction("uploads/orgs","check");
		$fm->DirAction("uploads/orgs/docs","check");
		$fm->DirAction("uploads/orgs/orig","check");
		$fm->DirAction("uploads/orgs/0","check");
		$fm->DirAction("uploads/orgs/1","check");
		$fm->DirAction("uploads/users","check");
		$fm->DirAction("uploads/users/orig","check");
		$fm->DirAction("uploads/users/0","check");
		$fm->DirAction("uploads/products","check");
		$fm->DirAction("uploads/products/orig","check");
		$fm->DirAction("uploads/products/0","check");
		$fm->DirAction("uploads/products/1","check");
		$fm->DirAction("uploads/widgets","check");
		$fm->DirAction("uploads/widgets/orig","check");
		$fm->DirAction("uploads/widgets/0","check");
		$fm->DirAction("uploads/custom","check");
		$fm->DirAction("uploads/docs","check");
		$fm->DirAction("uploads/docs/covers","check");
		$fm->DirAction("uploads/graphics","check");
		$fm->DirAction("uploads/graphics/favicon","check");
		$fm->DirAction("uploads/graphics/orig","check");
		$fm->DirAction("uploads/graphics/0","check");
		$fm->DirAction("uploads/images","check");
		$fm->DirAction("uploads/images/orig","check");
		$fm->DirAction("uploads/images/0","check");
		$fm->DirAction("uploads/images/1","check");
		$fm->DirAction("uploads/images/2","check");
		$fm->DirAction("uploads/images/3","check");
		$fm->DirAction("uploads/keywords","check");
		$fm->DirAction("uploads/keywords/orig","check");
		$fm->DirAction("uploads/videos","check");
		$fm->DirAction("uploads/videos/orig","check");
		$fm->DirAction("uploads/videos/flv","check");
		$fm->DirAction("uploads/videos/images","check");
		$fm->DirAction("uploads/videos/thumbs","check");
		$fm->DirAction("uploads/audios","check");
		$fm->DirAction("uploads/audios/orig","check");
		$fm->DirAction("uploads/audios/enc","check");
		$fm->DirAction("uploads/mailjobs","check");
		$fm->DirAction("uploads/ebooks","check");
		$fm->DirAction("xsl","check");
		$fm->DirAction("pub","check");
		$fm->DirAction("pub/feeds","check");
		$fm->DirAction("pub/jsc","check");
		$fm->DirAction("pub/js/custom","check");
		$fm->DirAction("pub/css","check");
		$fm->DirAction("pub/banners","check");
		if(!$dev)
		{
			$fm->Rename("css0","pub/css/0");
			$fm->Rename("xsl0","xsl/0");			
		}
		$paths = $conf->Get("paths");
		$fm->DirAction("pub/" . $paths['graphic'] ,"check");
		$fm->DirAction("pub/" . $paths['graphic'] . "covers","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "galleries","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "covers/" . $conf->Get("covers_thumb"),"check");
		$fm->DirAction("pub/" . $paths['graphic'] . "covers/" . $conf->Get("covers_size"),"check");
		$fm->DirAction("pub/" . $paths['graphic'] . "docs","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "orgs","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "orgs/0","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "orgs/1","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "orgs/orig","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "users","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "users/0","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "products","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "products/0","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "products/1","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "videos","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "videos/f","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "videos/i","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "videos/t","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "videos/x","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "audios","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "audios/e","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "audios/x","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "widgets","check");
		$fm->DirAction("pub/" . $paths['graphic'] . "widgets/0","check");
		$fm->DirAction("pub/" . $paths['docs'] ,"check");
		$fm->DirAction("pub/" . $paths['docs'] . "orgs","check");
		$fm->DirAction("pub/" . $ini->Get("map_path"),"check");
		$fm->DirAction("distrib","check");
	}

	public function IsInstalled()
	{
		$is_installed = false;
		include_once(SERVER_ROOT."/../classes/db.php");
		if (Db::test())
		{
			$row = array();
			$db =& Db::globaldb();
			$db->query_single($row,"show tables like 'global'");
			if (count($row)>0)
			{
				$row2 = array();
				$db->query_single($row2,"SELECT id_install FROM global");
				if ($row2['id_install']>0)
					$is_installed = true;
			}
		}
		return $is_installed;
	}
	
	public function SetLanguage($id_language)
	{
		$this->trm->id_language = $id_language;
	}
	
	public function Test($case)
	{
		$test = true;
		$this->test_msg = "";
		$paths = array('.','admin','classes','modules','others','custom','pub');
		$this->errors = array();
		$this->details = "";
		switch($case)
		{
			case "main_paths":
				foreach($paths as $path)
				{
					if (!file_exists("../../$path"))
					{
						$test = false;
						$this->errors[] = $this->trm->TranslateParams("error_main_path",array(realpath("../../") . "/$path"));
					}
				}
			break;
			case "path_rights":
				foreach($paths as $path)
				{
					if (($path!='custom') && !is_writeable("../../$path"))
					{
						$test = false;
						$this->errors[] = $this->trm->TranslateParams("error_path_rights",array(realpath("../../") . "/$path"));
					}
				}
			break;
			case "php_version":
				$this->test_msg = phpversion();
				if (version_compare($this->test_msg,"7.2.0.","<") || version_compare($this->test_msg,"8.0.0",">=") )
				{
					$test = false;
					$this->errors[] = $this->trm->TranslateParams("error_php_version",array($this->test_msg));
				}
			break;
			case "db_conn":
				include_once(SERVER_ROOT."/../classes/db.php");
				$db =& Db::test();
				if (!$db)
				{
					$test = false;
					$this->errors[] = $this->trm->TranslateParams("error_db_conn",array(mysql_error()));
				}
			break;
			case "is_installed":
				include_once(SERVER_ROOT."/../classes/db.php");
				$db =& Db::test();
				if(!$db)
					$test = false;
				else
				{
					if($this->IsInstalled())
					{
						$test = false;
						$this->errors[] = $this->trm->Translate("error_is_installed");
					}
				}
			break;
			case "ldap":
				$test = false;
				include_once(SERVER_ROOT."/../classes/config.php");
				$conf = new Configuration();
				$ldap_config = $conf->Get("ldap");
				set_error_handler("ErrorTrash");
				$connect = ldap_connect("ldap://".$ldap_config['server']);
				if ($connect>0)
				{
					$bind=ldap_bind($connect);
					if ($bind)
					{
						ldap_close($connect);
						$test = true;
					}
				}
				set_error_handler("ErrorHandler");
				if(!$test)
					$this->errors[] = $this->trm->TranslateParams("error_ldap",array($ldap_config['server']));
			break;
			case "php_safe_mode":
				include_once(SERVER_ROOT."/../classes/varia.php");
				$va = new Varia();
				if ($va->SafeMode())
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_php_safe_mode");
				}
			break;
			case "php_memory_limit":
				$memory_limit = str_replace("M","",get_cfg_var("memory_limit"));
				if ($memory_limit<MIN_MEMORY_LIMIT)
				{
					$test = false;
					$this->errors[] = $this->trm->TranslateParams("error_php_memory_limit",array($memory_limit,MIN_MEMORY_LIMIT));
				}
			break;
			case "php_register_globals":
				$ini_reg_globals = ini_get("register_globals");
				if ($ini_reg_globals || strtolower($ini_reg_globals)=="on")
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_php_register_globals");
				}
			break;
			case "php_file_uploads":
				if (ini_get("file_uploads")!="1")
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_php_file_uploads");
				}
			break;
			case "domxml":
				if (!class_exists("DomDocument"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_domxml");
				}
			break;
			case "curl":
				$curl = @curl_version();
				if (!isset($curl['version_number']))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_curl");
				}
			break;
			case "ctype":
				if(!( function_exists("ctype_print") &&  function_exists("ctype_alnum")))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_ctype");
				}
			break;
			case "iconv":
				if(!function_exists("iconv"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_iconv");
				}
			break;
			case "imap":
				if(!function_exists("imap_open"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_imap");
				}
			break;
			case "json":
				if(!function_exists("json_encode"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_json");
				}
			break;
			case "multibyte":
				if(!( function_exists("mb_convert_encoding") &&  function_exists("mb_detect_encoding")))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_multibyte");
				}
			break;
			case "imagemagick":
				include_once(SERVER_ROOT."/../classes/images.php");
				$i = new Images(false);
				$this->test_msg = $i->ImageMagickVersion();
				if (!stristr($this->test_msg,"imagemagick"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_imagemagick");
				}
			break;
			case "imagemagick_create":
				include_once(SERVER_ROOT."/../classes/images.php");
				$i = new Images(false);
				$fm = new FileManager();
				$fm->DirAction("cache","check");
				$output = "cache/test.jpg";
				$i->LogoCreate("prova","ciao",$output);
				if (!$fm->Exists($output))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_imagemagick_create");
				}
				else
					$fm->Delete($output);
			break;
			case "imagemagick_resize":
				include_once(SERVER_ROOT."/../classes/images.php");
				$i = new Images(false);
				$fm = new FileManager();
				$fm->DirAction("cache","check");
				$output = "cache/test2.jpg";
				$resize = 100;
				$i->Convert($resize,"pub/logos/phpeace.png",$output);
				if($fm->Exists($output))
					$size = $fm->ImageSize($output);
				if (!($fm->Exists($output) && $size['width']==$resize))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_imagemagick_resize");
				}
				else
					$fm->Delete($output);
			break;
			case "tar":
				set_error_handler("ErrorTrash");
				include_once("Archive/Tar.php");
				if (!class_exists("Archive_Tar"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_tar");
				}
				set_error_handler("ErrorHandler");
			break;
			case "xslt":
				if (!class_exists("XSLTProcessor"))
				{
					$test = false;
					$this->errors[] = $this->trm->Translate("error_xslt");
				}
			break;
		}
		return $test;
	}
	
	public function TestCritical($case)
	{
		return $case=="is_installed" || $case=="db_conn" || $case=="domxml" || $case=="xslt" || $case=="curl"  || $case=="tar" || $case=="json" || $case=="multibyte" || $case=="imagemagick_resize";
	}
	
	public function Validate()
	{
		include_once(SERVER_ROOT."/../classes/formhelper.php");
		$fh = new FormHelper();
		if($this->step > 0)
			$post = $fh->HttpPost();
		$validate = true;
		switch($this->step)
		{
			case 3:
				include_once(SERVER_ROOT."/../classes/ini.php");
				$ini = new Ini;
				foreach($post as $key=>$value)
				{
					if(strpos($key,"_path")!==false)
					{
						if($value=="")
						{
							$validate = false;
							$this->messages[] = $this->trm->TranslateParams("validate_error",array($this->trm->Translate($key)));
						}
						else
						{
							$ini->Set($key,$fh->String2Path($value));
						}
					}
				}
			break;
			case 4:
				$values = array('admin_web','pub_web','title','description','staff_email');
				foreach($values as $value)
				{
					if($post[$value]=="")
					{
						$validate = false;
						$this->messages[] = $this->trm->TranslateParams("validate_error",array($this->trm->Translate($value)));
					}
				}
				if($validate)
				{
					include_once(SERVER_ROOT."/../classes/ini.php");
					$ini = new Ini;
					$admin_web = $fh->String2Url($post['admin_web']);
					$ini->Set("admin_web",$admin_web);
					$ini->Set("pub_web",$fh->String2Url($post['pub_web']));
					$ini->Set("title",$post['title']);
					$ini->Set("description",$post['description']);
					$ini->Set("staff_email",$post['staff_email']);
					include_once(SERVER_ROOT."/../classes/publishmanager.php");
					$pm = new PublishManager();
					$pm->CrossDomain();
					include_once(SERVER_ROOT."/../classes/images.php");
					$i = new Images(false);
					$i->LogoCreate($post['title'],$post['description'],"uploads/custom/logo.jpg",true);
				}
			break;
			case 5:
				$values = array('login','password');
				include_once(SERVER_ROOT."/../classes/config.php");
				$conf = new Configuration();
				if ($conf->Get("user_auth")=="internal")
					$values = array('login','password','name','email');
				foreach($values as $value)
				{
					if($post[$value]=="")
					{
						$validate = false;
						$this->messages[] = $this->trm->TranslateParams("validate_error",array($this->trm->Translate($value)));
					}
				}
				if ($conf->Get("user_auth")=="ldap")
				{
					include_once(SERVER_ROOT."/../classes/adminhelper.php");
					$ah = new AdminHelper();
					$auth = $ah->UserAuthLdap($post['login'],$post['password']);
					if (!$auth)
					{
						$validate = false;
						$this->messages[] = $this->trm->Translate("auth_error");
					}
				}
				if($validate)
				{
					include_once(SERVER_ROOT."/../classes/user.php");
					$u = new User;
					if ($conf->Get("user_auth")=="ldap")
						$id_user = $u->UserInsertLdap($post['login'],$this->trm->id_language);
					else
					{
						$id_user = $u->UserInsert($post['name'],$post['email'],$post['login'],$post['password'],1,"",$post['id_language']);
					}
					$u->id = $id_user;
					$u->Verified();
					$u->ModuleAdd(1,1);
					$u->ModuleAdd(3,1);
					$u->ModuleAdd(4,1);
					$u->ModuleAdd(7,1);
					$u->ModuleAdd(10,1);
					$u->ModuleAdd(12,1);
					$u->ModuleAdd(14,1);
					$u->ModuleAdd(17,1);
					$u->ServiceRegister("scheduler",0);
					include_once(SERVER_ROOT."/../classes/session.php");
					$session = new Session();
					$session->Set("current_user_id",$id_user);
					$session->Set("id_language",$post['id_language']);
					$this->ContentInit();
				}
			break;
		}
		return $validate;
	}
}
?>
