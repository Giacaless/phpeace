<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/grouphelper.php");
include_once(SERVER_ROOT."/../classes/ini.php");
		
/**
 * General galleries management
 * 
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class Galleries
{
	/** 
	 * @var GroupHelper */
	public $gh;
	
	/**
	 * Type of homepage
	 * 
	 * @var integer
	 */
	public $hometype;

	/**
	 * Limit of galleries to show in homepage
	 * 
	 * @var integer
	 */
	public $homelimit;

	/**
	 * Initialize local variables 
	 */
	function __construct()
	{
		$this->gh = new GroupHelper("galleries");
		$ini = new Ini;
		$this->gh->top_name = $ini->GetModule("galleries","title","Galleries");
		$this->gh->top_description = $ini->GetModule("galleries","description","");
		$this->hometype = $ini->GetModule("galleries","hometype",0);
		$this->homelimit = $ini->GetModule("galleries","homelimit",6);
	}

	/**
	 * Get all galleries
	 * 
	 * @param boolean $only_visible			Whether to return visible galleries only
	 * @param boolean $sharing_images_only	Whether to return galleries sharing images only
	 * @return array
	 */
	public function AllGalleries($only_visible=false,$sharing_images_only=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT g.id_gallery,g.title AS name,g.id_group,gg.name AS group_name
		 FROM galleries g
		INNER JOIN galleries_groups gg ON g.id_group=gg.id_group 
		WHERE g.id_gallery>0 ";
		if($only_visible)
			$sqlstr .= " AND g.visible=1 ";
		if($sharing_images_only)
			$sqlstr .= " AND g.share_images=1 ";
		$sqlstr .= " ORDER BY gg.id_group,gg.seq,g.title";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Get all galleries (paginated)
	 * 
	 * @param array $rows	Galleries
	 * @return integer		Num of galleries
	 */
	public function AllGalleriesP( &$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(released) AS released_ts,g.id_gallery,g.title,u.name,
			count(ig.id_image) AS counter,g.id_topic,g.id_topic_group,g.id_group
			FROM galleries g
			LEFT JOIN users u ON g.id_user=u.id_user
			LEFT JOIN images_galleries ig ON g.id_gallery=ig.id_gallery 
			GROUP BY g.id_gallery ORDER BY released DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	/**
	 * Return all galleries' names
	 * 
	 * @return array
	 */
	public function AllGalleriesByName()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT g.id_gallery,g.title
			FROM galleries g
			ORDER BY title";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	/**
	 * Update configuration of galleries module
	 * 
	 * @param string $path			Public path
	 * @param string $title			Title
	 * @param string $description	Main description
	 * @param integer $hometype		Type of homepage
	 * @param integer $homelimit	Limit of galleries in homepage
	 */
	public function ConfigurationUpdate($path, $title, $description, $hometype, $homelimit)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$ini->SetModule("galleries","title",$title);
		$ini->SetModule("galleries","description",$description);
		$ini->SetPath("gallery_path",$path);
		$ini->SetModule("galleries","hometype",$hometype);
		$ini->SetModule("galleries","homelimit",$homelimit);
	}

	/**
	 * Get the gallery an image belongs
	 * 
	 * @param integer $id_image	Image ID
	 * @return array			Gallery data
	 */
	public function GalleryImage( $id_image )
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT ig.id_image,ig.id_gallery,ig.link,i.caption,i.source,i.author,i.format,i.image_date,i.image_date,ig.thumb_height,ig.image_height  
			FROM images_galleries ig INNER JOIN images i USING(id_image) WHERE ig.id_image='$id_image' ";
		$db->query_single($row,$sqlstr);
		return $row;
	}
	
	/**
	 * Get the galleries to show in homepage
	 * 
	 * @return array
	 */
	public function Homepage()
	{
		switch($this->hometype)
		{
			case "0":
				$sqlstr = "SELECT g.id_gallery AS id_item,g.title AS name,g.description,COUNT(ig.id_image) AS counter,
				UNIX_TIMESTAMP(g.released) AS released_ts,g.show_date,g.author
				FROM galleries g
				INNER JOIN galleries_groups gg ON g.id_group=gg.id_group
				LEFT JOIN images_galleries ig ON g.id_gallery=ig.id_gallery
				WHERE g.visible=1 AND gg.visible=1
				GROUP BY g.id_gallery ORDER BY g.released DESC ";
				if(is_numeric($this->homelimit) && $this->homelimit>0)
					$sqlstr .= " LIMIT $this->homelimit";
			break;
			case "1":
				$sqlstr = "SELECT g.id_gallery AS id_item,g.title AS name,g.description,COUNT(ig.id_image) AS counter,
				UNIX_TIMESTAMP(g.released) AS released_ts,g.show_date,g.author,MAX(ig.id_image) AS last_image_id
				FROM galleries g
				INNER JOIN galleries_groups gg ON g.id_group=gg.id_group
				INNER JOIN galleries_home gh ON g.id_gallery=gh.id_gallery
				LEFT JOIN images_galleries ig ON g.id_gallery=ig.id_gallery
				WHERE g.visible=1 AND gg.visible=1
				GROUP BY g.id_gallery ORDER BY last_image_id DESC,g.released DESC";
			break;
		}
		$db =& Db::globaldb();
		$rows = array();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}
	
	/**
	 * Add a specific gallery to the homepage
	 * 
	 * @param integer $id_gallery	Gallery ID
	 */
	public function HomepageAdd($id_gallery)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "galleries_home" );
		$sqlstr = "INSERT INTO galleries_home (id_gallery) VALUES ($id_gallery)";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Get all galleries available to be added to the homepage
	 * 
	 * @param array $rows	Galleries
	 * @return integer		Num of galleries
	 */
	public function HomepageAvailable(&$rows)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT UNIX_TIMESTAMP(released) AS released_ts,g.id_gallery,g.title,
			count(ig.id_image) AS counter,t.name AS topic_name,g.id_group
			FROM galleries g
			LEFT JOIN topics t ON g.id_topic=t.id_topic
			LEFT JOIN images_galleries ig ON g.id_gallery=ig.id_gallery 
			LEFT JOIN galleries_home gh ON gh.id_gallery=g.id_gallery
			WHERE g.visible=1  AND gh.id_gallery IS NULL
			GROUP BY g.id_gallery ORDER BY released DESC";
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	/**
	 * Remove a specific gallery from homepage
	 * 
	 * @param integer $id_gallery
	 */
	public function HomepageDelete($id_gallery)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "galleries_home" );
		$sqlstr = "DELETE FROM galleries_home WHERE id_gallery='$id_gallery' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	/**
	 * Images available to be imported
	 * 
	 * @param array $rows					Images
	 * @param integer $id_gallery_exclude	Gallery ID to exclude images from 
	 * @return integer						Num of images
	 */
	public function Images( &$rows,$id_gallery_exclude=0 )
	{
		if($id_gallery_exclude>0)
			$sqlstr = "SELECT COUNT(ig.id_image) AS counter ,g.title,g.id_gallery 
			FROM galleries g 
			INNER JOIN images_galleries ig ON g.id_gallery=ig.id_gallery 
			LEFT JOIN images_galleries ig2 ON ig.id_image=ig2.id_image AND ig2.id_gallery=$id_gallery_exclude 
			WHERE  ig2.id_gallery_from IS NULL AND g.share_images=1 
			GROUP BY g.id_gallery  ORDER BY g.released DESC";
		else
			$sqlstr = "SELECT COUNT(ig.id_image) AS counter, g.title,g.id_gallery
			FROM images_galleries ig
			INNER JOIN galleries g ON ig.id_gallery=g.id_gallery 
			WHERE g.share_images=1
			 GROUP BY g.id_gallery ORDER BY g.released DESC";
		$db =& Db::globaldb();
		$rows = array();
		return $db->QueryExe($rows, $sqlstr, true);
	}
	
	/**
	 * Check if the original image can be shown
	 * 
	 * @param integer $id_image	Image ID
	 * @return boolean
	 */
	public function IsOrigAvailable($id_image)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT ig.id_image FROM images_galleries ig INNER JOIN galleries g ON ig.id_gallery=g.id_gallery AND ig.id_image='$id_image' AND g.show_orig=1";
		$db->query_single($row,$sqlstr);
		return ($row['id_image']>0);
	}
	
	/**
	 * Placeholder for the latest functionality 
	 * 
	 * @param integer $id_group
	 * @return array
	 */
	public function Latest($id_group)
	{
		return array();
	}

	/**
	 * Get all visible galleries
	 * 
	 * @return array
	 */
	public function Visible()
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT g.id_gallery,g.title,g.description,COUNT(ig.id_image) AS counter,
		UNIX_TIMESTAMP(g.released) AS released_ts,g.show_date,g.author
		FROM galleries g
		INNER JOIN galleries_groups gg ON g.id_group=gg.id_group AND gg.visible=1
		LEFT JOIN images_galleries ig ON g.id_gallery=ig.id_gallery
		WHERE g.visible=1 
		GROUP BY g.id_gallery ORDER BY g.released DESC";
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

}
?>
