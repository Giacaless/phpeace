<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/config.php");
define('XSL_PATH',"xsl");
define('XSL_DEFAULT_OUTPUT_DOCTYPE',"xhtml10_transitional");

class XmlHelper
{
	public $root_element;
	public $encoding;
	public $header;

	private $debug;

	function __construct($debug = false)
	{
		$this->root_element = "root";
		$conf = new Configuration();
		$this->encoding = $conf->Get("encoding");
		$this->header = "<?xml version=\"1.0\" encoding=\"" . $this->encoding . "\"?>\n";
		$this->debug = $debug;
	}

	public function Array2Xml( $array, $with_header=true, $attribute_format=true )
	{
		if ($attribute_format)
            $xml = $this->NameValuePairs($this->root_element, $array);
        else
            $xml = $this->NameValuePairsElement($this->root_element, $array);
		unset($array);
		return $with_header? $this->header . $xml : $xml;
	}

	public function Check($xmlstring,$catch=TRUE,$notify=true)
	{
		$check = false;
		if($xmlstring!="")
		{
			libxml_clear_errors();
			libxml_use_internal_errors(true);
			$doc = new DOMDocument('1.0', $this->encoding);
			$doc->loadXML($xmlstring);
			$errors = libxml_get_errors();
			if (empty($errors))
			{
				$check = true;
			}
			$error = $errors[0];
			if ($error->level < 3)
			{
				$check = true;
			}
		}
		if(!$check && ($catch || $notify))
		{
			$user_error = array();
			if($xmlstring!="")
			{
				$user_error['error'] = $error->message.' at line '.$error->line;
				$bad_line = (int)$error->line;
				$badxml_lines = explode("\n", $xmlstring);
				if(count($badxml_lines)>0 && $bad_line>0 && $bad_line < count($badxml_lines)) {
					$user_error['line ' . ($bad_line - 1)] = $badxml_lines[$bad_line-1];
					$user_error['line ' . ($bad_line)] = $badxml_lines[$bad_line];
					$user_error['line ' . ($bad_line + 1)] = $badxml_lines[$bad_line+1];
				}
			}
			else 
			{
				$user_error['error'] = "Empty XML";
			}
			if ($catch)
			{
				UserError("Non-well-formed XML",$user_error);
			}
			elseif($notify)
			{
				include_once(SERVER_ROOT."/../classes/adminhelper.php");
				$ah = new AdminHelper;
				foreach($user_error as $l)
					$message .= "$l";
				$ah->MessageSet($message);
			}
		}
		return $check;
	}
	
	public function LibXMLDisplayErrors($show_file_info=false)
	{ 
		$return = "";
		$errors = libxml_get_errors(); 
		foreach ($errors as $error)
		{
			switch ($error->level)
			{
				case LIBXML_ERR_WARNING: 
					$return .= "Warning $error->code: "; 
				break;
				case LIBXML_ERR_ERROR: 
					$return .= "Error $error->code: "; 
				break; 
				case LIBXML_ERR_FATAL: 
					$return .= "Fatal Error $error->code: "; 
				break; 
			} 
			$return .= trim($error->message); 
			if ($show_file_info && $error->file)
			{ 
				$return .= " in \"$error->file\"  on line $error->line"; 
			} 
			$return .= "\n";
		}
		libxml_clear_errors(); 
		return $return;
	}

    private function NameValuePairs($name,$values)
    {
        $is_complex = FALSE;
        $xstr = "";
        if (count($values)>0)
        {
            $ename = isset($values['xname'])? $values['xname'] : $name;
            $xstr = "<" . $ename;
            $xstr_inner = "";
            while((list($key,$value) = each($values))==true)
            {
                if ($key != "xname" && $key != "xvalue" && $key != "xxml")
                {
                    if (is_array($value))
                    {
                        $xstr_inner .= $this->NameValuePairs($key,$value);
                        $is_complex = TRUE;
                    }
                    else
                        $xstr .= " $key=\"" . htmlspecialchars($value) . "\"";
                }
            }
            if ($is_complex)
                $xstr .= ">\n" . $xstr_inner . "</" . $ename . ">\n";
            elseif (isset($values['xvalue']))
            {
            	$x_content = $values['xvalue'];
            	$matches = array();
				preg_match_all('/<!\[cdata\[(.*?)\]\]>/is', $x_content, $matches);
				$x_content = str_replace($matches[0], $matches[1], $x_content); 
                $xstr .= "><![CDATA[" . $x_content . "]]></" . $ename . ">\n";
            }
            elseif (isset($values['xxml']))
                $xstr .= ">" . preg_replace('/<\?xml(.*)?\?'.'>/', '', $values['xxml'])  . "</" . $ename . ">\n";
            else
                $xstr .= " />\n";
        }
        return $xstr;
    }

    private function NameValuePairsElement($name,$values)
    {
        $xstr = "";
        if (count($values)>0)
        {
            $ename = isset($values['xname'])? $values['xname'] : $name;
            $xstr = "<" . $ename . ">";
            $xstr_inner = "";
            while((list($key,$value) = each($values))==true)
            {
                if ($key != "xname" && $key != "xvalue" && $key != "xxml")
                {
                    if (is_array($value))
                        $xstr_inner .= $this->NameValuePairsElement($key,$value);
                    else
                        $xstr .= " \n<$key>" . htmlspecialchars($value) . "</$key>";
                }
            }
            if (isset($values['xvalue']))
                $xstr .= "<![CDATA[" . $values['xvalue'] . "]]></" . $ename . ">\n";
            elseif (isset($values['xxml']))
                $xstr .= preg_replace('/<\?xml(.*)?\?'.'>/', '', $values['xxml'])  . "</" . $ename . ">\n";
            else
                $xstr .= "\n" . $xstr_inner . "</" . $ename . ">\n";
        }
        return $xstr;
    }
	
	public function StripDoctype(&$xml,$substitute_with="")
	{
		$xml = preg_replace('/<!DOCTYPE\b[^>]*?>/si', $substitute_with, $xml); 
	}

	public function Transform( $type, $xml, $id_style=0, $params=array() )
	{
		if($this->debug)
		{
			$str = "";
			if (strpos($xml,$this->root_element)!==false)
			{
				if ( $this->Check($xml))
					$str = $this->TransformXml($this->XslFileName($type, $id_style),$xml,true,$params);
				else
					UserError("XSL trasformation failed because of non-well-formed XML",array("type"=>$type,"id_style"=>$id_style));
			}
		}
		else 
			$str = $this->TransformXml($this->XslFileName($type, $id_style),$xml,true,$params);
		unset($xml);
		return $str;
	}

	private function TransformXml( $xslfile, $xmlstring, $catch_errors=true, $params=array() )
	{
		$xp = new XSLTProcessor();
		$xsldoc = new DOMDocument();
		$xsldoc->load($xslfile);
		$xp->importStylesheet($xsldoc);
		$xmldoc = new DOMDocument('1.0',$this->encoding);
		if($catch_errors)
			$xmldoc->loadxml($xmlstring);
		else
			$xmldoc->loadxml($xmlstring,LIBXML_NOERROR);
		if(count($params)>0)
		{
			foreach($params as $name=>$value)
			{
				$xp->setParameter('',$name,$value);
			}
			//$xp->setParameter('',$params);
		}
		$xml = $xp->transformToXml($xmldoc);
		if ($this->debug && !$xml)
		{
			UserError("XSL transformation failed",array());
		}
		return $xml;
	}
	
	public function XmlFragmentDelete($id_xml)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "xml" );
		$sqlstr = "DELETE FROM xml WHERE id_xml='$id_xml'";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function XmlFragmentGet($id_xml)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT id_xml,name,xml FROM xml WHERE id_xml='$id_xml'";
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function XmlFragments( &$rows )
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT id_xml,name FROM xml ORDER BY name";
		return $db->QueryExe($rows, $sqlstr, true);
	}

	public function XmlFragmentStore($id_xml,$name,$unescaped_xml)
	{
		if ($this->Check($unescaped_xml,FALSE))
		{
			$db =& Db::globaldb();
			$xml = $db->SqlQuote($unescaped_xml);
			$db->begin();
			$db->lock( "xml" );
			if ($id_xml>0)
				$sqlstr = "UPDATE xml SET name='$name',xml='$xml' WHERE id_xml='$id_xml' ";
			else
			{
				$id_xml = $db->nextId( "xml", "id_xml" );
				$sqlstr = "INSERT INTO xml (id_xml,name,xml) VALUES ($id_xml,'$name','$xml')";
			}
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
	
	private function XslFileName($type,$id_style)
	{
		$filename = XSL_PATH . "/$id_style/{$type}.xsl";
		include_once(SERVER_ROOT."/../classes/file.php");
		$fm = new FileManager;
		$filename = $fm->RealPath($filename);
		return $filename;
	}
	
}

?>
