<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/media.php");

class Audio extends Media
{
	public $audio_max_size;

	function __construct()
	{
		$conf = new Configuration();		
		$this->audio_max_size = $conf->Get("audio_max_size");
		parent::__construct("audio");
	}
	
	public function EmbedHtml($hash)
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		return "<embed src=\"" . $ini->Get("pub_web") . "/tools/mediaplayer.swf\" allowscriptaccess=\"always\" allowfullscreen=\"false\" flashvars=\"file=" . $this->FlashVarsEncode($this->irl->PublicUrlGlobal("audio_xml",array('hash'=>$hash))) . "\" />";
	}
	
	protected function EncodePost($id_audio,$enc_file)
	{
		$this->AudioInfoUpdate($id_audio,$enc_file,false);
	}
	
	protected function FfmpegEncodeCommand($input_file,$output_file)
	{
		$conf = new Configuration();
		return "nice -n {$this->ffmpeg_niceness} ffmpeg -i $input_file -ab " . $conf->Get("audio_bitrate") . " -ar " . $conf->Get("audio_sample_rate") . " " . $conf->Get("ffmpeg_audio_options") . " -y $output_file 2>&1 ";
	}
	
	protected function FfmpegInfoCommand($input_file)
	{
		return "ffmpeg -i $input_file  2>&1 | tail -n 3 | head -n 2 ";
	}
	
	private function HashUnique()
	{
		$hash = $this->Hash();
		$row = array();
		$db =& Db::globaldb();
		$db->query_single($row, "SELECT id_audio FROM audios WHERE hash='$hash' ");
		return $row['id_audio']>0? $this->HashUnique(): $hash ;
	}
	
	protected function MediaGet($id_media)
	{
		return $this->AudioGet($id_media);
	}
	
	public function Publish($id_audio,$hash,$hash2="",$title="")
	{
		if($hash2!="")
		{
			$this->PublishMedia($id_audio,$hash,$hash2,$title);
			$this->fm->PostUpdate();		
		}
	}
	
	public function AudioDelete($id_audio)
	{
		$row = $this->AudioGet($id_audio);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "audios" );
		$res[] = $db->query( "DELETE FROM audios WHERE id_audio=$id_audio" );
		Db::finish( $res, $db);
		$this->MediaDelete($id_audio,$row['format']);
		$this->AudioDeletePub($row['hash'],$row['hash2']);
		$this->fm->PostUpdate();
	}
	
	private function AudioDeletePub($hash,$hash2)
	{
		$pub_file = $this->irl->PublicPath("audio_enc",array('hash2'=>$hash2),true,true);
		$xml_file = $this->irl->PublicPath("audio_xml",array('hash'=>$hash),true,true);
		$this->fm->Delete($pub_file);
		$this->fm->Delete($xml_file);
		$this->fm->PostUpdate();
	}
	
	public function AudioGet($id_audio,$approved_only=false)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_audio,UNIX_TIMESTAMP(a.insert_date) AS insert_date_ts,a.format,a.title,a.description,a.author,a.source,
			a.bytes,a.id_licence,a.id_topic,a.encoded,a.approved,a.hash,a.hash2,mi.original,mi.encoded AS 'encoded_info',a.bytes_enc,
			a.length,a.link,a.views,a.id_language,a.auto_start,a.download
			FROM audios a 
			INNER JOIN media_info mi ON a.id_audio=mi.id_media AND mi.id_type='{$this->id_type}'
			WHERE a.id_audio='$id_audio' ";
		if($approved_only)
			$sqlstr .= " AND a.approved=1 AND a.encoded=1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function AudioGetByHash($hash)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT a.id_audio,UNIX_TIMESTAMP(a.insert_date) AS insert_date_ts,a.title,a.description,a.author,a.source,
			a.bytes,a.id_licence,a.id_topic,a.hash,a.hash2,a.length,a.link,a.views,a.id_language,a.auto_start,a.download
			FROM audios a 
			WHERE a.hash='$hash' AND a.approved=1 AND a.encoded=1 ";
		$db->query_single($row, $sqlstr);
		return $row;
	}
	
	public function AudioInsert($insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$approved,$file,$auto_start,$encode,$download)
	{
		$hash = $this->HashUnique();
		$hash2 = $this->Hash2();
		$db =& Db::globaldb();
		$db->begin();
		$db->LockTables(array("audios","media_info"));
		$id_audio = $db->nextId( "audios", "id_audio" );
		$sqlstr1 = "INSERT INTO audios (id_audio,insert_date,format,title,description,author,source,id_language,link,id_licence,id_topic,bytes,encoded,approved,hash,hash2,auto_start,download) 
			VALUES ($id_audio,'$insert_date','{$file['ext']}','$title','$description','$author','$source','$id_language','$link','$id_licence',0,'{$file['size']}',0,'$approved','$hash','$hash2','$auto_start','$download') ";
		$res1[] = $db->query( $sqlstr1 );
		$sqlstr2 = "INSERT INTO media_info (id_media,id_type,original,encoded) VALUES ($id_audio,'{$this->id_type}','','')";
		$res1[] = $db->query( $sqlstr2 );
		Db::finish( $res1, $db);

		$filename = $this->irl->PathAbs("audio_orig",array('id'=>$id_audio,'format'=>$file['ext']));
		$this->fm->MoveUpload($file['temp'], $filename);
		$this->AudioInfoUpdate($id_audio,$filename,true);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_audio, $this->id_res_type);
		$this->h->HistoryAdd($this->id_res_type,$id_audio,$this->h->actions['create']);
		if($approved)
			$this->h->HistoryAdd($this->id_res_type,$id_audio,$this->h->actions['approve']);
		if($encode)
			$this->EncodeQueueAdd($id_audio);
		else
		{
			$enc_file = $this->irl->PathAbs("audio_enc",array('id'=>$id_audio));
			$this->fm->Copy($filename,$enc_file);
			$this->EncodePost($id_audio,$enc_file);
			if($approved)
				$this->Publish($id_audio,$hash,$hash2,$title);		
		}
		$this->fm->PostUpdate();
		return $id_audio;
	}
	
	private function AudioInfoUpdate($id_audio,$filename,$is_orig)
	{
		$media_info = $this->MediaInfo($filename);
		$db =& Db::globaldb();
		$info = $db->SqlQuote($media_info['info']);
		$length = $db->SqlQuote($media_info['length']);
		$this->MediaInfoUpdate($id_audio,$info,$is_orig);
		if(!$is_orig)
		{
			$enc_info = $this->fm->FileInfo($filename);
			$db->begin();
			$db->lock( "audios" );
			$res[] = $db->query( "UPDATE audios SET encoded=1,bytes_enc='{$enc_info['size']}',length='$length' WHERE id_audio=$id_audio " );
			Db::finish( $res, $db);		
		}
	}
	
	public function AudioUpdate($id_audio,$insert_date,$title,$description,$author,$source,$id_language,$link,$id_licence,$keywords,$keywords_internal,$approved,$auto_start,$download)
	{
		$row = $this->AudioGet($id_audio);
		$sqlstr = "UPDATE audios SET insert_date='$insert_date',title='$title',description='$description',author='$author',source='$source',
			link='$link',id_licence='$id_licence',approved='$approved',id_language='$id_language',auto_start='$auto_start',
			download='$download'
			WHERE id_audio=$id_audio" ;
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "audios" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
		include_once(SERVER_ROOT."/../classes/ontology.php");
		$o = new Ontology;
		$o->InsertKeywords($keywords, $id_audio, $this->id_res_type);
		$o->InsertKeywordsArray($keywords_internal,$id_audio,$this->id_res_type);
		$this->h->HistoryAdd($this->id_res_type,$id_audio,$this->h->actions['update']);
		if($approved!=$row['approved'])
		{
			if($approved)
			{
				$this->Publish($id_audio,$row['hash'],$row['hash2'],$row['title']);
				$this->h->HistoryAdd($this->id_res_type,$id_audio,$this->h->actions['approve']);
			}
			else 
			{
				$this->AudioDeletePub($row['hash'],$row['hash2']);
				$this->h->HistoryAdd($this->id_res_type,$id_audio,$this->h->actions['reject']);
			}
		}
		if($approved && $row['encoded'])
			$this->Index($id_audio);
	}
	
	public function AudioUpdateOrig($id_audio,$file,$encode)
	{
		$row = $this->AudioGet($id_audio);
		$filename = $this->irl->PathAbs("audio_orig",array('id'=>$id_audio,'format'=>$file['ext']));
		$this->fm->MoveUpload($file['temp'], $filename);
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "audios" );
		$res[] = $db->query( "UPDATE audios SET approved='0',encoded='0',bytes='{$file['size']}',format='{$file['ext']}'  WHERE id_audio=$id_audio" );
		Db::finish( $res, $db);
		$this->AudioInfoUpdate($id_audio,$filename,true);
		if($encode)
		{
			$this->EncodeQueueAdd($id_audio);
		}
		else 
		{
			$enc_file = $this->irl->PathAbs("audio_enc",array('id'=>$id_audio));
			$this->fm->Copy($filename,$enc_file);
			$this->EncodePost($id_audio,$enc_file);
		}
		$this->fm->PostUpdate();
		$this->h->HistoryAdd($this->id_res_type,$id_audio,$this->h->actions['create']);
		if($row['approved'])
			$this->AudioDeletePub($row['hash'],$row['hash2']);
	}
	
	public function AudioView($id_audio)
	{
		$session_var = "audio_$id_audio";
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		if(!$session->IsVarSet($session_var))
		{
			$db =& Db::globaldb();
			$sqlstr =  "UPDATE LOW_PRIORITY audios SET views=views+1 WHERE id_audio='$id_audio' ";
			$db->query( $sqlstr );
			$session->Set($session_var, 1 );
		}
	}
	
	public function AudiosAll(&$rows,$id_topic=0,$paged=true,$only_approved=false)
	{
		$db =& Db::globaldb();
		$rows = array();
		$sqlstr = "SELECT a.id_audio,UNIX_TIMESTAMP(a.insert_date) AS insert_date_ts,a.format,a.title,a.description,a.author,a.source,
			a.bytes,a.id_licence,a.id_topic,a.encoded,a.approved,'audio' AS item_type,a.hash,a.length,a.bytes_enc,a.hash2,a.views
			FROM audios a 
			WHERE a.id_audio>0 ";
		if($id_topic>0)
			$sqlstr .= " AND a.id_topic=$id_topic ";
		if($only_approved)
			$sqlstr .= " AND a.approved=1 AND a.encoded=1 ";
		$sqlstr .= " ORDER BY a.insert_date DESC, a.id_audio DESC ";
		return $db->QueryExe($rows, $sqlstr, $paged);
	}

	protected function Tracklist($hash,$title)
	{
		$tl = array();
		$tl['version'] = "1";
		$tl['xmlns'] = "http://xspf.org/ns/0/";
		$audio_array = array('xname'=>"track");
		$audio_array['identifier'] = array('xvalue'=>$hash);
		$audio_array['title'] = array('xvalue'=>$title);
		$audio_array['location'] = array('xvalue'=>$this->irl->PublicUrlGlobal("audio_enc_link",array('hash'=>$hash),true));
		$audio_array['meta'] = array('rel'=>"type",'xvalue'=>"mp3");
		$vt = array('xname'=>"trackList");
		$vt['track'] = $audio_array;
		$tl['trackList'] = $vt;
		return $tl;
	}
}

?>
