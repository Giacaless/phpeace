<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/config.php");

class Graphic
{
	private $id;
	private $path;
	private $pub_path;
	/** 
	 * @var FileManager */
	private $fm;

	function __construct( $id )
	{
		$conf = new Configuration();
		$this->id = $id;
		$this->path = "uploads/graphics";
		$paths = $conf->Get("paths");
		$this->pub_path = "pub/" . $paths['graphic'];
		$this->fm = new Filemanager;
	}

	private function Convert($origfile)
	{
		$i = new Images();
		$i->ConvertWrapper("graphic",$origfile, $this->id);
	}

	/**
	 * Update favicon
	 *
	 * @param integer $id_topic Topic ID
	 * @param array $file Uploaded file
	 */
	public function FaviconUpdate( $id_topic, $file )
	{
		if (count($file)>0)
		{
			$fm = new FileManager;
			$orig_filename = "uploads/graphics/favicon/{$id_topic}.ico";
			$fm->MoveUpload($file['temp'],$orig_filename);
			if($id_topic>0)
			{
				include_once(SERVER_ROOT."/../classes/topic.php");
				$t = new Topic($id_topic);
				$fm->Copy($orig_filename,"pub/{$t->path}/favicon.ico");
			}
			else
			{
				$fm->Copy($orig_filename,"pub/favicon.ico");				
			}
			$fm->PostUpdate();
		}
	}
	
	public function GraphicDelete()
	{
		$this->GraphicRemove();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "graphics" );
		$res[] = $db->query( "DELETE FROM graphics WHERE id_graphic=$this->id" );
		Db::finish( $res, $db);
	}

	public function GraphicGet()
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT name,width,height,format,id_style,exclude_lookup FROM graphics WHERE id_graphic=$this->id";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function GraphicInsert( $file, $name, $id_style,$exclude_lookup=0 )
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "graphics" );
		$this->id = $db->nextId( "graphics", "id_graphic" );
		$origfile = "$this->path/orig/" . $this->id . "." . $file['ext'];
		$this->fm->MoveUpload($file['temp'], $origfile);
		$size = $this->fm->ImageSize($origfile);
		$res[] = $db->query( "INSERT INTO graphics (id_graphic,name,width,height,format,id_style,exclude_lookup)
		 VALUES ($this->id,'$name','{$size['width']}','{$size['height']}','{$file['ext']}',$id_style,$exclude_lookup)" );
		Db::finish( $res, $db);
		$this->Convert($origfile);
		$this->Publish($file['ext']);
	}

	private function GraphicRemove()
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT format FROM graphics WHERE id_graphic=$this->id";
		$db->query_single($row,$sqlstr);
		$this->fm->Delete("$this->path/orig/$this->id" . "." . $row['format']);
		include_once(SERVER_ROOT."/../classes/images.php");
		$i = new Images();
		$this->fm->Delete("{$this->pub_path}$this->id" . "." . $row['format']);
		$i->RemoveWrapper("graphic",$this->id);
	}

	public function GraphicUpdate( $file, $name, $id_style,$exclude_lookup=0 )
	{
		if ($file['ok'])
		{
			$this->GraphicRemove();
			$origfile = "$this->path/orig/$this->id" . "." . $file['ext'];
			$this->fm->MoveUpload($file['temp'], $origfile);
			$size = $this->fm->ImageSize($origfile);
			$this->Convert($origfile);
			$this->Publish($file['ext']);
			$sqlstr = "UPDATE graphics SET name='$name',width='{$size['width']}',height='{$size['height']}',
				format='{$file['ext']}',id_style=$id_style,exclude_lookup=$exclude_lookup
				WHERE id_graphic='$this->id' ";
		}
		else
			$sqlstr = "UPDATE graphics SET name='$name',id_style='$id_style',exclude_lookup='$exclude_lookup' WHERE id_graphic='$this->id' ";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "graphics" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function Publish($ext)
	{
		$this->fm->Copy("$this->path/orig/$this->id" . "." . $ext,"{$this->pub_path}$this->id" . "." . $ext);
	}

}
?>
