<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

/**
 * How many seconds in a day
 */
define('SECONDS_PER_DAY',86400);

/**
 * Generic function for date/time
 *
 * @package PhPeace
 * @author Francesco Iannuzzelli <francesco@phpeace.org>
 */
class DateTimeHelper
{
	/**
	 * Months names
	 *
	 * @var array
	 */
	private $months;
	
	/**
	 * Weekdays names
	 *
	 * @var array
	 */
	private $weekdays;
	
	/**
	 * hours
	 *
	 * @var string
	 */
	private $hours;
	
	/**
	 * Initialize local variables
	 *
	 * @param Translator $tr	A translator instance
	 */
	function __construct($months,$weekdays,$hours)
	{
		$this->months = $months;
		$this->weekdays = $weekdays;
		$this->hours = $hours;		
	}
	
	/**
	 * Numeric day of the week
	 *
	 * @param timestamp $mydate_ts
	 * @return integer
	 */
	public function DayOfWeek($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		return $this->weekdays[$work_date['wday']];
	}

	/**
	 * Name of day
	 *
	 * @param timestamp $mydate_ts
	 * @return string
	 */
	public function DayNameByTs($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		$this->weekdays[] = $this->weekdays[0];
		return $this->weekdays[$work_date['wday']];
	}

	/**
	 * Name of day
	 *
	 * @param integer $mydow	Day of the week
	 * @return string
	 */
	public function DayName($mydow)
	{
		$this->weekdays[] = $this->weekdays[0];
		return $this->weekdays[$mydow];
	}
	
	/**
	 * Numeric day of week (1=Monday, 7=Sunday)
	 *
	 * @param timestamp $mydate_ts
	 * @return integer
	 */
	public function Dow($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		$wday = $work_date['wday'];
		return ($wday==0)? 7 : $wday;
	}

	/**
	 * Numeric day of the month
	 *
	 * @param timestamp $mydate_ts
	 * @return integer
	 */
	public function DayOfMonth($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		return $work_date['mday'];
	}

	/**
	 * Format a timestamp returning its year number (4 digits)
	 *
	 * @param timestamp $mydate_ts
	 * @return integer
	 */
	public function Year($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		return $work_date['year'];
	}

	/**
	 * Format a date
	 *
	 * @param mixed $ts
	 * @param boolean $is_ts	Is $ts a tiemstamp or a date to try parsing
	 * @return string
	 */
	public function FormatDate($ts, $is_ts=TRUE)
	{
		$fdate = "";
		if ($is_ts)
		{
            if ($ts > 0)
            {
                $work_date = getdate($ts);
                $month_number = $work_date['mon'];
                $fdate = $work_date['mday'] . " " . $this->months[$month_number-1] . " " . $work_date['year'];
            }
		}
		else
		{
			$adate = preg_split("/-/",$ts);
			if (checkdate($adate[1],$adate[2],$adate[0]))
				$fdate = "$adate[2] " . $this->months[($adate[1] - 1)] . " " . $adate[0];
		}
		return $fdate;
	}

	/**
	 * Format a timestamp returning year and month number
	 *
	 * @param timestamp $ts
	 * @return string
	 */
	public function FormatMonth($ts,$with_year=true)
	{
		$work_date = getdate($ts);
		$month_number = $work_date['mon'];
		return  ($with_year? $work_date['year'] . " " : "") . $this->months[$month_number-1];
	}

	/**
	 * Format a timestamp returning hours and minutes
	 *
	 * @param timestamp $mytime
	 * @return string
	 */
	public function FormatTime($mytime_ts)
	{
		$work_time = getdate($mytime_ts);
		return $this->hours . " " . sprintf("%02d:%02d",$work_time['hours'],$work_time['minutes']);
	}

	/**
	 * Format a timestamp returning hours and minutes
	 *
	 * @param timestamp $mytime_ts
	 * @return string
	 */
	public function FormatTimeSeconds($mytime_ts)
	{
		$work_time = getdate($mytime_ts);
		return sprintf("%02d:%02d:%02d",$work_time['hours'],$work_time['minutes'],$work_time['seconds']);
	}

	/**
	 * Format a timestamp returning its month name
	 *
	 * @param timestamp $mydate_ts
	 * @return string
	 */
	public function Month($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		return $this->months[$work_date['mon']-1];
	}

	/**
	 * Format a timestamp returning its month number
	 *
	 * @param timestamp $mydate_ts
	 * @return integer
	 */
	public function MonthNumber($mydate_ts)
	{
		$work_date = getdate($mydate_ts);
		return $work_date['mon'];
	}

	/**
	 * Format a timestamp returning its week number
	 *
	 * @param timestamp $ts
	 * @return integer
	 */
	public function WeekNumber($ts)
	{
		return date("W",$ts);
	}

}

/**
 * How many seconds in an interval
 *
 * @param integer $id_frequency		ID of interval
 * @return integer
 */
function FrequencySeconds($id_frequency)
{
	switch ($id_frequency)
	{
	case "1": // daily
		$seconds = SECONDS_PER_DAY;
		break;
	case "2": // weekly
		$seconds = SECONDS_PER_DAY * 7;
		break;
	case "3": // fortnightly
		$seconds = SECONDS_PER_DAY * 14;
		break;
	case "4": // monthly
		$seconds = SECONDS_PER_DAY * 30;
		break;
	}
	return $seconds;
}

?>
