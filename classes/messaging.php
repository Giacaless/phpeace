<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");

class Messaging
{
	public function DeleteFromInbox($id_message)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "message_users" );
		$res[] = $db->query( "DELETE FROM message_users WHERE id_message='$id_message' AND id_receiver='" . $session->Get('current_user_id') . "'" );
		Db::finish( $res, $db);
	}

	public function DeleteFromOutbox($id_message)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "messages" );
		$res[] = $db->query( "UPDATE messages SET deleted=1 WHERE id_message='$id_message'" );
		Db::finish( $res, $db);
	}

	public function DeleteMessages($id_message)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "messages" );
		$res[] = $db->query( "DELETE FROM messages WHERE id_message='$id_message'" );
		Db::finish( $res, $db);
	}

	public function DeleteOld()
	{
		include_once(SERVER_ROOT."/../classes/ini.php");
		$ini = new Ini;
		$limit = (int)($ini->GetModule("mail","messages_expire",30));
		if ($limit>0)
		{
			$db =& Db::globaldb();
			$rows = array();
			$cond = " WHERE date_read>0 AND (DATE_ADD(date_read, INTERVAL $limit DAY))<CURRENT_DATE";
			$sqlstr = "SELECT DISTINCT id_message FROM message_users $cond";
			$db->QueryExe($rows, $sqlstr);
			foreach($rows as $row)
				$this->DeleteMessages($row['id_message']);
			$db =& Db::globaldb();
			$db->begin();
			$db->lock( "message_users" );
			$res[] = $db->query( "DELETE FROM message_users $cond" );
			Db::finish( $res, $db);
		}
	}

	public function IsMessageDeleted($id_message)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT deleted FROM messages WHERE id_message='$id_message'";
		$db->query_single($row,$sqlstr);
		return $row['deleted'];
	}

	public function MessageGet($id_message)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT m.id_message,subject,body,id_sender,m.id_user,id_module,UNIX_TIMESTAMP(sent) AS sent,
				u.name,UNIX_TIMESTAMP(mu.date_read) AS date_read
			FROM messages m
			LEFT JOIN users u ON m.id_sender=u.id_user
			LEFT JOIN message_users mu ON mu.id_message=m.id_message
			WHERE m.id_message='$id_message'";
		$db->query_single($row,$sqlstr);
		return $row;
	}

	public function MessageSend($id_sender,$id_recipient,$unescaped_subject,$unescaped_body,$urgent,$notify)
	{
		$db =& Db::globaldb();
		$subject = $db->SqlQuote($unescaped_subject);
		$body = $db->SqlQuote($unescaped_body);
		$today = $db->getTodayTime();
		$db->begin();
		$db->lock( "messages" );
		$id_message = $db->nextId( "messages", "id_message" );
		$sqlstr = "INSERT INTO messages (id_message,subject,body,id_sender,id_user,id_module,sent,deleted)
			VALUES ($id_message,'$subject','$body',$id_sender,$id_recipient,0,'$today',0)";
		$res[] = $db->query($sqlstr);
		Db::finish( $res, $db);
		$db->lock( "message_users" );
		$res[] = $db->query( "INSERT INTO message_users (id_message,id_receiver) VALUES ($id_message,$id_recipient)");
		Db::finish( $res, $db);
		if ($urgent)
			$this->SendUrgent($id_recipient,$unescaped_subject,$unescaped_body);
		if ($notify)
		{
			include_once(SERVER_ROOT."/../classes/user.php");
			$u = new User;
			$u->id = $id_recipient;
			$receiver = $u->UserGet();
			$ah = new AdminHelper();
			$ah->MessageSet("message_sent",array($receiver['name']));
		}
	}
	
	public function Messages($id_message)
	{
		$row = array();
		$db =& Db::globaldb();
		$sqlstr = "SELECT COUNT(id_message) AS count_messages FROM message_users WHERE id_message='$id_message'";
		$db->query_single($row,$sqlstr);
		return $row['count_messages'];
	}

	public function ReplySubject( $subject )
	{
		if (substr($subject,0,3)!="Re: ")
			$subject = "Re: $subject";
		return $subject;
	}

	public function SetRead($id_message)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$current_user_id = $session->Get("current_user_id");
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "message_users" );
		$res[] = $db->query( "UPDATE message_users SET date_read='".$db->GetTodayDate()."' WHERE id_message='$id_message' AND id_receiver='$current_user_id'" );
		Db::finish( $res, $db);
	}

	public function SendUrgent($id_receiver,$subject,$body)
	{
		include_once(SERVER_ROOT."/../classes/user.php");
		include_once(SERVER_ROOT."/../classes/mail.php");
		include_once(SERVER_ROOT."/../classes/translator.php");
		$mail = new Mail();
		$u = new User;
		$sender = $u->UserGet();
		$u->id = $id_receiver;
		$receiver = $u->UserGet();
		$tr = new Translator($receiver['id_language'],0);
		$message = $tr->TranslateParams("message_urgent2",array($sender['name']));
		$message .= "\n-------------------------\n\n";
		$message .= $body;
		$message .= "\n\n-------------------------\n";
		$message .= "PhPeace $mail->title\n";
		$message .= $mail->admin_web."\n";
		$extra = array();
		$extra['name'] = $sender['name'];
		$extra['email'] = $sender['email'];
		$mail->SendMail($receiver['email'], $receiver['name'], $subject, $message, $extra);
	}

}
?>
