<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

include_once(SERVER_ROOT."/../classes/db.php");
include_once(SERVER_ROOT."/../classes/ini.php");

class Queue
{
	public $types = array(	'article'	=> 1,
				'article_box'	=> 2,
				'event'		=> 3,
				'link'		=> 4,
				'book'		=> 5,
				'gallery'		=> 6,
				'xdomain'	=> 7,
				'subtopic'	=> 8,
				'topic_home'	=> 9,
				'latest'	=> 10,
				'homepage'	=> 11,
				'map'		=> 12,
				'all'		=> 13
				);

	private $id_topic;
	
	private $hometype;

	function __construct($id_topic)
	{
		$this->id_topic = $id_topic;
		$ini = new Ini();
		$this->hometype = $ini->GetModule("homepage","hometype",0);
	}

	public function Confirm($id_user)
	{
		$db =& Db::globaldb();
		$row = array();
		$sqlstr = "SELECT id_job FROM queue WHERE id_topic=$this->id_topic AND id_type='{$this->types['all']}' AND action='confirmed' ";
		$db->query_single($row, $sqlstr);
		if(!$row['id_job']>0)
		{
			$db->begin();
			$db->lock( "queue" );
			$sqlstr = "UPDATE queue SET action='confirmed',id_user='$id_user' 
				WHERE id_topic='$this->id_topic' AND id_type='{$this->types['all']}' ";
			$res[] = $db->query( $sqlstr );
			Db::finish( $res, $db);
		}
	}
	
	public function ConfirmAll()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "queue" );
		$sqlstr = "UPDATE queue SET action='confirmed' WHERE id_type='{$this->types['all']}' ";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}
	
	public function JobDelete( $id_job, $id_type )
	{
		if ($id_type==$this->types['homepage'] || $id_type==$this->types['map'])
			$sqlstr = "DELETE FROM queue WHERE id_type=$id_type";
		else
			$sqlstr = "DELETE FROM queue WHERE id_job=$id_job";
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "queue" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function JobInsert($id_type,$id_item,$action,$param="")
	{
		if ($action=="delete" || (($id_type==$this->types['latest'] || $id_type==$this->types['xdomain'] || ($id_type==$this->types['homepage'] && $this->hometype==0) || $id_type==$this->types['map']) || !$this->JobPresentAll()))
		{
			if ($id_type==$this->types['all'])
			{
				$this->JobsDelete(true);
				$this->JobInsertOk($id_type,$id_item,$action,$param);
			}
			elseif ($this->JobPresent($id_type,$id_item,$action,$param)==0)
			{
				$this->JobInsertOk($id_type,$id_item,$action,$param);
			}
		}
	}

	private function JobInsertOk($id_type,$id_item,$action,$param)
	{
		include_once(SERVER_ROOT."/../classes/session.php");
		$session = new Session();
		$id_user = (int)($session->Get("current_user_id"));
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "queue" );
		$id_job = $db->nextId( "queue", "id_job" );
		$sqlstr = "INSERT INTO queue (id_job,id_topic,id_type,id_item,action,param,id_user)
			VALUES ($id_job,$this->id_topic,$id_type,$id_item,'$action','$param','$id_user')";
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	private function JobPresent($id_type,$id_item,$action,$param)
	{
		$present = 0;
		$row = array();
		$sqlstr = "SELECT id_job,param FROM queue WHERE id_topic=$this->id_topic
			AND id_type=$id_type AND id_item=$id_item AND action='$action'";
		$db =& Db::globaldb();
		$db->query_single($row, $sqlstr);
		if ($row['id_job']>0)
		{
			if ($db->SqlQuote($row['param'])!=$param)
				$this->JobUpdate($row['id_job'], $param);
			$present = (int)$row['id_job'];
		}
		return $present;
	}

	public function JobPresentAll()
	{
		$present = false;
		$row = array();
		$sqlstr = "SELECT id_job FROM queue WHERE id_topic=$this->id_topic AND id_type=" . $this->types['all'];
		$db =& Db::globaldb();
		$db->query_single($row, $sqlstr);
		if (isset($row['id_job']) && $row['id_job']>0)
			$present = true;
		return $present;
	}

	public function JobPresentParam($id_type,$id_item,$action,$param)
	{
		$row = array();
		$present = 0;
		$sqlstr = "SELECT id_job FROM queue WHERE id_topic=$this->id_topic
			AND id_type=$id_type AND id_item=$id_item AND action='$action' AND param='$param'";
		$db =& Db::globaldb();
		$db->query_single($row, $sqlstr);
		if ($row['id_job']>0)
			$present = $row['id_job'];
		return $present;
	}

	public function JobsAll()
	{
		$sqlstr = "SELECT id_job,id_type,id_item,action,param,id_user FROM queue WHERE id_topic=$this->id_topic ORDER BY id_type";
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function JobsAllP( &$rows )
	{
		$sqlstr = "SELECT id_type,id_item,action,param,id_user FROM queue WHERE id_topic=$this->id_topic ORDER BY id_job DESC";
		$rows = array();
		$db =& Db::globaldb();
		$num = $db->QueryExe($rows, $sqlstr, true);
		return $num;
	}

	public function JobsAllByType($id_type)
	{
		$sqlstr = "SELECT id_job,id_type,id_item,action,param,id_user FROM queue WHERE id_type=$id_type ORDER BY id_job ";
		$rows = array();
		$db =& Db::globaldb();
		$db->QueryExe($rows, $sqlstr);
		return $rows;
	}

	public function JobsDelete($topic_only=false,$all_included=false)
	{
		$sqlstr = "DELETE FROM queue WHERE id_topic=$this->id_topic";
		if($topic_only)
		{
			$sqlstr .= " AND ((id_type<{$this->types['latest']} AND action<>'delete')";
			if($all_included)
			{
				$sqlstr .= " OR id_type={$this->types['all']} ";
			}
			$sqlstr .= ")";
		}
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "queue" );
		$res[] = $db->query( $sqlstr );
		Db::finish( $res, $db);
	}

	public function JobsDeleteAll()
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "queue" );
		$res[] = $db->query( "DELETE FROM queue" );
		Db::finish( $res, $db);
	}

	private function JobUpdate($id_job, $param)
	{
		$db =& Db::globaldb();
		$db->begin();
		$db->lock( "queue" );
		$res[] = $db->query( "UPDATE queue SET param='$param' WHERE id_job=$id_job" );
		Db::finish( $res, $db);
	}

	public function NextJobAll()
	{
		$row = array();
		$sqlstr = "SELECT q.id_job,q.id_topic,q.id_user,u.name 
			FROM queue q 
			LEFT JOIN users u ON q.id_user=u.id_user
			WHERE q.id_type='{$this->types['all']}' AND q.action='confirmed'
			ORDER BY q.id_job ASC LIMIT 1 ";
		$db =& Db::globaldb();
		$db->query_single($row, $sqlstr);
		return $row;
	}

	public function Type($id_type)
	{
		return array_search($id_type,$this->types);
	}
}
?>
