<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

class Session
{
	/** 
	 * @var DbMysql */
	private $session_db;
	
	private $using_db;
	
	function __construct()
	{
		$this->using_db = false;
		
		if(isset($_SERVER['PHPEACE_SESSION_DB']) && $_SERVER['PHPEACE_SESSION_DB']=="1")
		{
			$this->using_db = true;
			include_once(SERVER_ROOT."/../classes/dbmysql.php" );
			session_set_save_handler(
				        array(& $this, 'SessionDbOpen'),
				        array(& $this, 'SessionDbClose'),
				        array(& $this, 'SessionDbRead'),
				        array(& $this, 'SessionDbWrite'),
				        array(& $this, 'SessionDbDestroy'),
				        array(& $this, 'SessionDbClean')
			        ); 			
		}
		if(isset($_SERVER['PHPEACE_SESSION_DOMAIN']))
		{
			ini_set('session.cookie_domain',$_SERVER['PHPEACE_SESSION_DOMAIN']);
		}
		if (session_status() == PHP_SESSION_NONE)
		{
			$session_started = session_start();
			if(!$session_started)
			{
				$this->RegenerateId(true);
				session_start();
			}
		}
		if (!isset($_SESSION['initiated'])) 
		{
			$this->RegenerateId();
 			$this->Set("initiated",true);
		}
	}
	
	public function All()
	{
		return $_SESSION;
	}

	public function Cleanup($cookie_names = null)
	{
		$_SESSION = array();

		$cookie_names = (array)$cookie_names;
		if (count($cookie_names) > 0)
		{
			$domain = '';
			if (isset($_SERVER['PHPEACE_SESSION_DOMAIN']))
			{
				$domain = $_SERVER['PHPEACE_SESSION_DOMAIN'];
			}
			foreach ($cookie_names as $cookie_name)
			{
				setcookie($cookie_name, '', 1, "/", $domain != '' ? $domain : '');
			}
		}
	}

	public function Delete($var)
	{
		unset($_SESSION[$var]);
	}
	
	public function DeleteAll()
	{
		session_unset();
	}
	
	public function Destroy()
	{
		include_once(SERVER_ROOT."/../classes/error.php");
		set_error_handler("ErrorTrash");
		$_SESSION=array();
		if (isset($_COOKIE[session_name()]))
			setcookie(session_name(), '', time()-42000, '/');
		session_destroy();
		set_error_handler("ErrorHandler");
	}
	
	public function Get($var)
	{
		return isset($_SESSION[$var])? $_SESSION[$var] : "";
	}
	
	public function GetDataBySessionId($session_id)
	{
		if($this->using_db)
		{
			$this->SessionDbOpen();
			$content = $this->SessionDbRead($session_id);
		}
		else
		{
			$session_file = session_save_path() . "/sess_" . $session_id;
			$content = file_get_contents($session_file);
		}
		session_start;
		session_decode($content);
		return $_SESSION;
	}
	
	public function ID()
	{
		return session_id();
	}
	
	public function IsVarSet($var)
	{
		return array_key_exists($var,$_SESSION);
	}
	
	public function RegenerateId($delete_old_session=false)
	{
		session_regenerate_id($delete_old_session);
	}

	public function Set($var,$value)
	{
		$_SESSION[$var] = $value;
	}
	
	public function SessionDbOpen()
	{
		global $session_db_conn;
		
		if($session_db_conn)
		{
			if(!$session_db_conn->Ping())
			{
				// DB connection is broken
				// properly close DB Connection
				$session_db_conn->Close();
			}
			else
			{
				// DB connection is OK
				// return DB Connection
				$this->session_db = $session_db_conn;
				return true;
			}
		}
			

		$session_db_conn = new DbMysql( $_SERVER['PHPEACE_SESSION_DB_HOST'], $_SERVER['PHPEACE_SESSION_DB_NAME'], $_SERVER['PHPEACE_SESSION_DB_USER'], $_SERVER['PHPEACE_SESSION_DB_PASSWORD'],false);
		$this->session_db = $session_db_conn;
		return true;
	}

	public function SessionDbClose()
	{
		if(isset($this->session_db))
		{
			$this->session_db->close();
		}
		return true;
	}

	public function SessionDbRead($id)
	{
		$id = $this->session_db->SqlQuote($id);
		$row = array();
		$sqlstr = "SELECT data FROM sessions WHERE id='$id'";
		$this->session_db->query_single($row,$sqlstr);
		return isset($row['data'])? $row['data'] : "";
	}

	public function SessionDbWrite($id,$data)
	{
		$access = time();
		$id = $this->session_db->SqlQuote($id);
		$data = $this->session_db->SqlQuote($data);
		$access = $this->session_db->SqlQuote($access);
		$sqlstr = "REPLACE INTO sessions VALUES ('$id','$access','$data') ";
		return $this->session_db->query($sqlstr);
	}

	public function SessionDbDestroy($id)
	{
		$id = $this->session_db->SqlQuote($id);
		$sqlstr = "DELETE FROM sessions WHERE id='$id' ";
		return $this->session_db->query($sqlstr);
	}

	public function SessionDbClean($max)
	{
		$old = time() - $max;
		$old = $this->session_db->SqlQuote($old);
		$sqlstr = "DELETE FROM sessions WHERE access<'$old' ";
		return $this->session_db->query($sqlstr);
	}
}
?>
