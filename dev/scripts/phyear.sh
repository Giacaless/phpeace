#!/bin/bash
#
# Search and replace for copyright statement
#
#################

START_DIR="/var/www/phpeace"

#SEARCH_FOR="\$_SERVER\['DOCUMENT_ROOT'\]"
#REPLACE_WITH="SERVER_ROOT"

#SEARCH_FOR="include_once(SERVER_ROOT.\"/include/header.php\");"
#REPLACE_WITH="include_once(SERVER_ROOT.\"/include/header.php\");"

SEARCH="   (C) 2003-2019 Francesco Iannuzzelli"
SEARCH_FOR="   (C) 2003-2019 Francesco Iannuzzelli"
REPLACE_WITH="   (C) 2003-2020 Francesco Iannuzzelli"

cd $START_DIR
find . -type f -name '*.php' -print | while read i
do
    MATCH=`grep -c "$SEARCH" $i`
    if [ $MATCH -gt 0 ]
    then
        sed "s|$SEARCH_FOR|$REPLACE_WITH|g" $i > $i.tmp && mv $i.tmp $i
        echo "Changing $i"
    fi
done

find . -type f -name '*.xsl' -print | while read i
do
    MATCH=`grep -c "$SEARCH" $i`
    if [ $MATCH -gt 0 ]
    then
        sed "s|$SEARCH_FOR|$REPLACE_WITH|g" $i > $i.tmp && mv $i.tmp $i
        echo "Changing $i"
    fi
done
