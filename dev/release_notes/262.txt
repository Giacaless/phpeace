## General notes ##

reCAPTCHA v1 is being discontinued and PhPeace now supports v2 only.
Securimage has also been disabled.
Please get API keys from https://www.google.com/recaptcha/admin and add them to your configuration.

Captcha has been added to all public forms.

Added link to directly delete visitors from search listing

Updated copyright statement to 2018

## Defects fixed ##

Various issues with mailer for visitors module

Fixed defect with Apache 2.4 directory options in .htaccess

## Modules ##

Removed module Tour Operators
