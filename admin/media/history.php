<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$trm9 = new Translator($hh->tr->id_language,9);

$id_media = $_GET['id'];
$id_type = $_GET['id_type'];


if($id_type==28)
{
	include_once(SERVER_ROOT."/../classes/audio.php");
	$au = new Audio();
	$row = $au->AudioGet($id_media);
	$title[] = array($trm9->Translate("audios"),'audios.php');
	$title[] = array($row['title'],'audio.php?id='.$id_media);
}
else 
{
	include_once(SERVER_ROOT."/../classes/video.php");
	$vi = new Video();
	$row = $vi->VideoGet($id_media);
	$title[] = array($trm9->Translate("videos"),'videos.php');
	$title[] = array($row['title'],'video.php?id='.$id_media);
}
$title[] = array('history','');

echo $hh->ShowTitle($title);

$tabs = array();
if($id_type==28)
{
	$tabs[] = array($trm9->Translate("audio_enc"),'audio.php?id='.$id_media);
	$tabs[] = array("details",'audio_details.php?id='.$id_media);
	$tabs[] = array($trm9->Translate("audio_orig"),'audio_orig.php?id='.$id_media);
}
else 
{
	$tabs[] = array($trm9->Translate("video_enc"),'video.php?id='.$id_media);
	$tabs[] = array("details",'video_details.php?id='.$id_media);
	$tabs[] = array("image_associated",'video_thumbs.php?id='.$id_media);
	$tabs[] = array($trm9->Translate("video_orig"),'video_orig.php?id='.$id_media);
}
$tabs[] = array('history','');
echo $hh->Tabs($tabs);

include_once(SERVER_ROOT."/../classes/history.php");
$h = new History;

$num = $h->HistoryAll( $row, $id_type, $id_media );

$table_headers = array('date','ip','user','action');
$table_content = array('{FormatDateTime($row[ts_time])}','$row[ip]','$row[name]','{HistoryAction($row[action])}');

if($module_admin)
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
