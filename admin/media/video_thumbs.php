<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/video.php");

$trm9 = new Translator($hh->tr->id_language,9);
$hhf = new HHFunctions();

$vi = new Video();

$id_video = $_GET['id'];

$row = $vi->VideoGet($id_video);

$title[] = array($trm9->Translate("videos"),'videos.php');
$title[] = array($row['title'],'video.php?id='.$id_video);
$title[] = array("image_associated",'');

if($vi->AdminRight($ah->current_user_id,$id_video) || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm9->Translate("video_enc"),'video.php?id='.$id_video);
$tabs[] = array("details",'video_details.php?id='.$id_video);
$tabs[] = array("image_associated",'');
$tabs[] = array($trm9->Translate("video_orig"),'video_orig.php?id='.$id_video);
if($module_admin)
	$tabs[] = array('history','history.php?id_type='.$ah->r->types['video'].'&id='.$id_video);
echo $hh->Tabs($tabs);

?>

<script type="text/javascript"><!--
function radioclick(radio_id)
{
	var list = document.getElementsByTagName ('li');
	for (var i = 0; i < list.length; ++i)
	if (list[i].id.match('aimage') != '')
            		list[i].className = '';	
	radio_selected = document.getElementById ('aimage' + radio_id);
	radio_selected.className = 'associated';
}
//-->
</script>
<?php

$fm = new FileManager();
$maxfilesize = $fm->MaxFileSize();
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_video",$id_video);
echo $hh->input_hidden("from","video_thumbs");
echo $hh->input_table_open();

$queued = $vi->IsInEncodeQueue($id_video);
if($queued)
{
	echo $hh->input_note($trm9->Translate("waiting_enc"));
}
else 
{
	$irl = new IRL();
	echo "<tr><td>" . $hh->tr->Translate("image_associated") . "</td><td>";
	echo "<ul class=\"article-images\">";
	for($j=0;$j<=($vi->video_thumbs);$j++)
	{
		$image_file = "videos/images/{$id_video}_{$j}.jpg";
		echo "<li id=\"aimage{$j}\" " . ($row['image_seq']==$j? " class=\"associated\" ":"") . ">";
		if($j==0)
		{
			if($fm->Exists("uploads/$image_file"))
			{
				echo "<input type=\"radio\" name=\"associate\"  value=\"$j\" " . (($row['image_seq']==$j)? "checked=\"checked\" ":"") . " class=\"input-radio\" onclick=\"radioclick($j)\">";
				echo "<img src=\"/images/upload.php?src=$image_file\" width=\"{$vi->video_resize_width}\" style=\"float:none;\"><br>\n";
				if($input_right)
				{
					echo $hh->tr->Translate("substitute_with") . "<br>";
					echo "<input type=\"file\" name=\"img[]\" size=\"30\">";
				}
			}
			elseif($input_right) 
			{
				echo $hh->tr->Translate("choose_file") . "<br>";
				echo "<input type=\"file\" name=\"img[]\" size=\"30\">";
			}
		}
		else
		{
			echo "<input type=\"radio\" name=\"associate\"  value=\"$j\" " . (($row['image_seq']==$j)? "checked=\"checked\" ":"") . " class=\"input-radio\" onclick=\"radioclick($j)\">";
			echo "<img src=\"/images/upload.php?src=$image_file\" width=\"{$vi->video_resize_width}\" height=\"{$vi->video_resize_height}\">\n";
		}
		echo "</li>\n";
	}
	echo "</ul>";
	echo "</td></tr>";
	
	$actions = array();
	$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
	echo $hh->input_actions($actions,$input_right);
}


echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

