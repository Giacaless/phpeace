<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

$trm9 = new Translator($hh->tr->id_language,9);

echo "<h3>" . $trm9->Translate("videos") . "</h3>\n";
echo "<ul>\n";
echo "<li><a href=\"videos.php\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"video_insert.php\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>\n";

echo "<h3>" . $trm9->Translate("audios") . "</h3>\n";
echo "<ul>\n";
echo "<li><a href=\"audios.php\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"audio_insert.php\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>\n";

if ($module_admin)
{
	echo "<h3>" . $hh->tr->Translate("administration") . "</h3>\n";
	echo "<ul>\n";
	echo "<li><a href=\"queue.php\">" . $trm9->Translate("queue") . "</a></li>\n";
	// echo "<li><a href=\"pears.php\">" . $trm9->Translate("pears") . "</a></li>\n";
	echo "<li><a href=\"watermark.php\">Watermark</a></li>\n";
	echo "<li><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></li>\n";
	echo "</ul>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
