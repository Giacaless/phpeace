<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/audio.php");

$trm9 = new Translator($hh->tr->id_language,9);
$hhf = new HHFunctions();

$au = new Audio();

$id_audio = $get['id'];

$row = $au->AudioGet($id_audio);
$title[] = array($trm9->Translate("audios"),'audios.php');
$title[] = array($row['title'],'audio.php?id='.$id_audio);
$title[] = array("details",'');

if($au->AdminRight($ah->current_user_id,$id_audio) || $module_admin)
	$input_right = 1;

if($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm9->Translate("audio_enc"),'audio.php?id='.$id_audio);
$tabs[] = array("details",'');
$tabs[] = array($trm9->Translate("audio_orig"),'audio_orig.php?id='.$id_audio);
if($module_admin)
	$tabs[] = array('history','history.php?id_type='.$ah->r->types['audio'].'&id='.$id_audio);
echo $hh->Tabs($tabs);

?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			title: "required"
		}
	});
});
</script>

<?php
$fm = new FileManager();
echo $hh->input_form_open();
echo $hh->input_hidden("id_audio",$id_audio);
echo $hh->input_hidden("from","audio");
echo $hh->input_table_open();

echo $hh->input_text("ID","id_id",$id_audio,10,0,0);
echo $hh->input_text("hash","hash",$row['hash'],20,0,0);
echo $hh->input_date("insert_date","insert_date",$row['insert_date_ts'],$input_right);
echo $hh->input_text("title","title",$row['title'],60,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,5,"",$input_right);
echo $hh->input_text("author","author",$row['author'],40,0,$input_right);
echo $hh->input_textarea("source","source",$row['source'],60,3,"",$input_right);
echo $hh->input_array("language","id_language",$row['id_language'],$hh->tr->Translate("languages"),$input_right);
echo $hh->input_text("link","link",$row['link'],50,0,$input_right);
echo $hh->input_checkbox($trm9->Translate("auto_start"),"auto_start",$row['auto_start'],0,$input_right);
echo $hh->input_checkbox($trm9->Translate("download"),"download",$row['download'],0,$input_right);

if ($hh->ini->Get("licences"))
	echo $hh->input_array("licence","id_licence",$row['id_licence'],$hh->tr->Translate("licences"),$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id_audio,$o->types['audio'],$keywords,$input_right);

$tikeywords = $au->KeywordsInternal();
echo $hh->input_internal_keywords($id_audio,$tikeywords,"audio",$input_super_right,$input_right);

echo $hh->input_separator("administration");
echo $hh->input_checkbox("approved","approved",$row['approved'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_audio>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

