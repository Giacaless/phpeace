<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/video.php");

$vi = new Video();

$trm9 = new Translator($hh->tr->id_language,9);

$title[] = array('Watermark','');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

$fm = new FileManager();
$maxfilesize = $fm->MaxFileSize();
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("from","watermark");
echo $hh->input_table_open();
echo $hh->input_note($trm9->Translate("watermark_note"));

$filename = "uploads/custom/watermark.png";
if ($fm->Exists($filename))
{
	$size = $fm->ImageSize($filename);
	$video_width = $vi->video_resize_width;
	$video_height = $vi->video_resize_height;
	$td_width = $video_width*2 + 100;
	echo "<tr><td align=\"right\">Watermark<br />{$size['width']}x{$size['height']} px</td><td><img src=\"/images/upload.php?src=custom/watermark.png\" width=\"{$size['width']}\"></td></tr>";
	echo $hh->input_upload("substitute_with","img",50,$input_right);
	echo $hh->input_submit("submit","",$input_right);
	echo $hh->input_separator($trm9->Translate("examples"));
	echo "<tr><td colspan=\"2\" width=\"$td_width\">";
	echo "<div style=\"float:left;margin:10px;padding:10px;width:{$video_width}px;height:{$video_height}px;color:#fff;background:#000 url(/images/upload.php?src=custom/watermark.png) no-repeat 95% 5%;\" />DARK</div>";
	echo "<div style=\"float:left;margin:10px;padding:10px;width:{$video_width}px;height:{$video_height}px;border:solid 1px #ccc;color:#000;background:#FFF url(/images/upload.php?src=custom/watermark.png) no-repeat 95% 5%;\" />WHITE</div>";
	echo "</td></tr>\n";
}
else 
{
	echo $hh->input_upload("choose_file","img",50,$input_right);
	echo $hh->input_submit("submit","",$input_right);
}

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
