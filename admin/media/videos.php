<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/video.php");

$trm9 = new Translator($hh->tr->id_language,9);

$vi = new Video();

$title[] = array($trm9->Translate("videos"),'');

echo $hh->ShowTitle($title);

$num = $vi->VideosAll($rows,0);

$table_headers = array('video','date','title','length','size','encoded','approved',$trm9->Translate("views"));
$table_content = array('{ThumbVideo($row[id_video])}','{FormatDate($row[insert_date_ts])','{LinkTitle("video.php?id=$row[id_video]",$row[title])}',
'{FormatLength($row[length])}','{FormatSize($row[bytes_enc])}','{Bool2YN($row[encoded])}',
'{Bool2YN($row[approved])}','<div class=\"right\">$row[views]</div>');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if ($module_right)
	echo "<p><a href=\"video_insert.php\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

