<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/user.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/phpeace.php");
include_once(SERVER_ROOT."/../classes/translator.php");
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../classes/rights.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$ah = new AdminHelper;
$phpeace = new PhPeace;
$conf = new Configuration();
$ini = new Ini;
if (isset($_GET['id_l']) && $_GET['id_l']>0)
	$ah->session->Set("id_language",$_GET['id_l']);

$hh = new HtmlHelper();

$fh = new FormHelper();
$get = $fh->HttpGet();
$unescaped_get = $fh->HttpGet(false);

// PAGING GLOBALS
$records_per_page = $conf->Get("records_per_page");
$current_page = (isset($_GET['p']) && $_GET['p']> 0)? $_GET['p'] : 1;

$ui = $conf->Get("ui");
$input_right = 0;
$input_super_right = 0;
$input_super_super_right = 0;

$at_work = FALSE;
if ($ini->Get("at_work")==1)
{
	$at_work = TRUE;
	if (!$phpeace->AmIAdmin())
		$ah->auth->Remove();
}
$page_title = "PhPeace - ".$ini->Get('title');
include_once(SERVER_ROOT."/../admin/include/head.php");
?>
<body>
<div id="main">
<div id="head">
<div id="custom-logo"><a href="/gate/index.php" title="Homepage"><img src="/images/custom.jpg" alt="<?=htmlspecialchars($page_title);?>"></a></div>
<div id="phpeace-logo">
<?php
echo "<img src=\"/images/" . ($conf->Get("ips")?"ips.png\" width=\"280\"":"phpeace.png\" width=\"99\"") . " height=\"67\" alt=\"PhPeace\"></div>";
?>
</div>
<div id="maintitle">
<div id="user-info">
<?php
if ($ah->current_user_id>0)
{
	if ($ah->session->Get("real_user_id")>0)
	{
		echo $hh->tr->Translate("user") . ": <strong>" . $ah->session->Get("current_user_login") . "</strong>";
		echo " (<a href=\"/users/index.php\">by <strong>" . $ah->session->Get("real_user_login") . "</strong></a>)";
	}
	else 
	{
		echo $hh->tr->Translate("user") . ": <strong><a href=\"/gate/user.php\">" . $ah->session->Get("current_user_login") . "</a></strong>";
		echo " [<a href=\"/gate/logout.php\">log-out</a>]";
		
	}
	$ah->SetModule();
}
?>
</div>
<div id="phpeace-version">
<?php
echo $hh->tr->Translate("phpeace_powered") . " PhPeace {$phpeace->version}";
?>
</div>
</div>
<div id="main-content">
<?php
if ($ah->auth->Check())
{
	$filepath = explode("/",$_SERVER['SCRIPT_NAME']);
	$dev_right = false;
	echo "<div id=\"nav\">\n";
	if ($at_work)
		echo "<ul class=\"msg\"><li>" . $hh->tr->Translate("mainteinance") . "</li></ul>\n";
	$admin_text = $hh->th->AdminContentGet(2);
	if($admin_text['active'])
	{
		echo "<div class=\"admin-text\">\n";
		echo "<h2>" . $admin_text['title'] . "</h2>";
		echo $admin_text['description'];
		echo "</div>\n";
	}
	if ($conf->Get("dev"))
	{
		$dev_right = true;
		echo "<ul class=\"msg\"><li>Development</li></ul>\n";
		$ah->DevSet();
	}
	echo "<ul id=\"modules\">\n";

	$current_url = $_SERVER['REQUEST_URI'];

	$current_id_topic = 0;
	if (substr_count($current_url,"/topics")>0)
	{
		if (isset($_GET['id_topic']) && $_GET['id_topic']>0)
			$current_id_topic = $_GET['id_topic'];
		elseif(isset($_GET['id']) && !isset($_GET['id_topic']))
			$current_id_topic = $_GET['id'];
	}

	$ri = new Rights();
	$module_right = $ri->ModuleUser();
	$module_admin = $ri->ModuleAdmin();
	$adminModules = array();
	$u = new User();
	$userModules = $u->Modules();
	$tr_modules = $hh->tr->Translate("modules_names");
	foreach($userModules as $userModule)
	{
		if ($userModule['is_admin']==1)
			$adminModules[] = $userModule['id_module'];
		$current_module = $ah->session->Get("module_id");
		if(isset($_GET['w']))
		{
			if($_GET['w']=="topics")
				$current_module = 4;
			if($_GET['w']=="tra")
				$current_module = 21;
		}
		echo "<li" . (($userModule['id_module']==$current_module)? " class=\"selected\"":"") . ">";
		echo "<a href=\"/{$userModule['path']}/index.php\">" . $tr_modules[$userModule['id_module']] . "</a>";
		if ( $userModule['id_module'] == "2" )
		{
			$u = new User();
			$newmessages = $u->NewMessages();
			if($newmessages>0)
				echo " - <span class=\"new\">NEW!</span>";
		}
		if ( $userModule['id_module'] == "4" )
		{
			$userTopics = $u->Topics();
			if (count($userTopics)>0)
			{
				$group = "";
				$counter = 0;
				echo "\n<ul id=\"topics-groups\">\n";
				foreach($userTopics as $userTopic)
				{
					if($userTopic['group_name']!=$group)
					{
						if($counter>0)
						{
							echo "</ul>\n";
							echo "</li>\n";
						}
						echo "<li class=\"topic-group\">{$userTopic['group_name']}";
						echo "\n<ul class=\"topics\">\n";
					}
					$topic_selected = ($ah->session->Get("module_id")==4 && $userTopic['id_topic']==$current_id_topic && $userTopic['is_admin']=="1");
					echo "<li " . ($topic_selected? "class=\"selected\"":"") . "><a href=\"/{$userModule['path']}/ops.php?id={$userTopic['id_topic']}\" title=\"" . htmlspecialchars($userTopic['description']) . "\">{$userTopic['name']}</a>";
					if ($topic_selected)
					{
						echo "\n<ul>\n";
						echo "<li><a href=\"/articles/article.php?w=topics&id=0&id_topic={$userTopic['id_topic']}\">" . $hh->tr->Translate("article_new") . "</a></li>\n";
						echo "<li><a href=\"/topics/publish.php?id={$userTopic['id_topic']}\">" . $hh->tr->Translate("publish") . "</a></li>\n";
						echo "<li><a href=\"/topics/preview.php?id_topic={$userTopic['id_topic']}&id_type=1\" target=\"_blank\">" . $hh->tr->Translate("preview") . "</a></li>\n";
						echo "</ul>\n";
					}
					echo "</li>\n";
					$group = $userTopic['group_name'];
					$counter++;
				}
				if($counter==count($userTopics))
				{
					echo "</ul>\n";
					echo "</li>\n";
				}
				echo "</ul>\n";
			}
		}
		echo "</li>\n";
	}
	echo "<li" . (($_SERVER['REQUEST_URI']==("/users/user.php?id=".$ah->current_user_id))? " class=\"selected\">":">");
	echo "<a href=\"/gate/user.php\">" . $hh->tr->Translate("your_account") . "</a></li>\n";
	if($conf->Get("submit_defects"))
	{
		echo "<li><a href=\"/gate/defect.php?id=0&amp;id_module=" . $ah->session->Get("module_id") . "\">" . $hh->tr->Translate("bug_submit") . "</a></li>\n";	
	}
	if ($ah->session->Get("real_user_id")>0)
		echo "<li><a href=\"/users/actions.php?from2=user&action3=emulationend\">" . $hh->tr->Translate("simulation_end") . "</a></li>\n";
	else
		echo "<li><a href=\"/gate/logout.php\">" . $hh->tr->Translate("logout") . "</a></li>\n";
	echo "</ul>";
	if ($conf->Get("debug") && $phpeace->AmIAdmin())
	{
		echo "\n<hr>\n<div class=\"notes\">\n";
		echo "<b>SESSION INFO</b><ul id=\"session-info\">\n";
		foreach($ah->session->All() as $var=>$value)
			echo "<li>$var: <b>" . ((is_array($value))? " [".count($value)."]":$value) . "</b></li>\n";
		echo "<li>module_right: <b>$module_right</b></li>\n";			
		echo "<li>module_admin: <b>$module_admin</b></li>\n";			
		echo "</ul>\n";
		echo "</div>\n";
	}
	echo "</div>\n";
	$title = array();
	//--end auth
}
else 
{
	$admin_text = $hh->th->AdminContentGet(1);
	if($admin_text['active'])
	{
		echo "<div id=\"admin-text\" class=\"admin-text\" >\n";
		echo "<h2>" . $admin_text['title'] . "</h2>";
		echo $admin_text['description'];
		echo "</div>\n";
	}
}

$module_path = $ah->session->Get("module_path");
echo "<div id=\"content\"" . (($module_path!="")? " class=\"mod-$module_path\" ":"") . ">";

if (!($ah->auth->Check()))
{
	if (((int)$_GET['reminder'])>0 && $conf->Get("user_auth")=="internal")
	{
	?>
<script type="text/javascript">
function checkform1()
{
	f = document.forms['form1'];
	if (f.email.value=="" || f.email.value.indexOf('@',0)==-1)
	{
		alert("<?=$hh->tr->Translate("missing_email");?>\n");
		f.email.focus();
	}
	else
		f.submit();
}
</script>
<h1><?=$hh->tr->Translate("password_forgotten");?></h1>
<p><?=$hh->tr->Translate("password_forgotten_note");?></p>
<form name="form1" action="/gate/actions.php" method="post" onsubmit="checkform1(); return false;">
<input type="hidden" name="action" value="reminder">
<table border="0" cellpadding="2" cellspacing="2">
<tr><td align="right"><?=$hh->tr->Translate("email");?></td><td><input type="text" size="30" name="email"></td></tr>
<tr><td align="right">&nbsp;</td><td><input type="submit" class="input-submit" value="<?=$hh->tr->Translate("submit");?>"></td></tr>
</table>
<script type="text/javascript">
document.form1.email.focus();
</script>
</form>

<p><a href="/"><?=$hh->tr->Translate("back_gate");?></a></p>
<p>&nbsp;</p>

<?php
	}
	else
	{
		if ($at_work)
			$ah->MessageSet("at_work_warning");
		echo $hh->MessageShow();
?>
<script type="text/javascript">
function checkform2()
{
	f = document.forms['form2'];
	if (f.user.value=="" || f.pass.value=="")
		alert("<?=$hh->tr->Translate("missing_login_pass");?>\n");
	else
		f.submit();
}

var cookieEnabled = (navigator.cookieEnabled)? true : false;
if(typeof navigator.cookieEnabled=="undefined" && !cookieEnabled) {
	document.cookie="testcookie";
	cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false;
}
if(!cookieEnabled) {
	document.write('<ul class="msg"><li><?=$hh->th->TextReplaceForJavascript($hh->tr->TranslateParams("cookies_disabled_warning",array($ini->Get("admin_web"))));?></li></ul>');
}
</script>
<noscript>
<ul class="msg"><li><?=$hh->tr->Translate("javascript_disabled_warning");?></li></ul>
</noscript>
<?php
if (($conf->Get("no_more_ie6")) && $ah->IsIe6())
{
	echo "<div id=\"no_more_ie6\">" . $hh->tr->Translate("no_more_ie6") . "</div>\n";
}

echo "<p id=\"welcome\">" . $hh->tr->TranslateParams("welcome_gate",array($ini->Get('title'))) . "</p>\n";
?>

<form name="form2" action="/gate/actions.php" method="post" onsubmit="checkform2(); return false;">
<input type="hidden" name="action" value="login">
<?php
$token = md5(uniqid(rand(),true));
$ah->session->Set("token",$token);
$ah->session->Set("token_time",time());
echo "<input type=\"hidden\" name=\"token\" value=\"" . $token . "\">\n";

if ($ah->session->Get("uri")!="")
{
	$uri = $ah->session->Get("uri");
	$ah->session->Delete("uri");
}
else
	$uri = $_SERVER['REQUEST_URI'];

echo "<input type=\"hidden\" name=\"uri\" value=\"$uri\">\n";
$disable_autocomplete = $conf->Get("disable_autocomplete");
?>
<table border="0" cellpadding="2" cellspacing="2">
<tr><td class="input-label"><?=$hh->tr->Translate("name");?></td><td><input type="text" size="20" name="user" <?php if($disable_autocomplete) { echo "autocomplete=\"off\""; } ?> onkeydown="k=(document.all)?event.keyCode:event.which;if(k==13)document.form2.pass.focus();"></td></tr>
<tr><td class="input-label">Password</td><td><input type="password" size="20" name="pass" <?php if($disable_autocomplete) { echo "autocomplete=\"off\""; } ?> onkeydown="k=(document.all)?event.keyCode:event.which;if(k==13)document.form2.submit();"></td></tr>
<tr><td>&nbsp;</td><td><input type="submit" class="input-submit" value="<?=$hh->tr->Translate("enter");?>"></td></tr>
</table>
</form>
<script type="text/javascript">
document.form2.user.focus();
</script>
<?php
	if($conf->Get("user_auth")=="internal")
	{
		echo $hh->tr->Translate("password_forgotten");
		echo "<div><a href=\"/gate/index.php?reminder=1\">" . $hh->tr->Translate("password_send") . "</a></div>";
	}
}
?>
</div>
</div>
</div>
</body>
</html>
<?php
	exit;
}
elseif(!$module_right)
{
	$hh->tr->Translate("user_no_auth");
	exit;
}

?>
