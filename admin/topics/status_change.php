<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/scheduler.php");

$id_topic = $get['id_topic'];
$id_res = $get['id_res'];
$id = $get['id'];
$date = $get['date'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);

if ($id_res==$ah->r->types['campaign'])
{
	include_once(SERVER_ROOT."/../classes/campaign.php");
	$c = new Campaign($id);
	$row = $c->CampaignGet();
	
	$title[] = array('campaigns','campaigns.php?id='.$id_topic);
	$title[] = array($row['name'],'campaign.php?id='.$id.'&id_topic='.$id_topic);
	$title[] = array('status_changes','campaign_status.php?id='.$id);
}
if ($id_res==$ah->r->types['poll'])
{
	include_once(SERVER_ROOT."/../classes/polls.php");
	$pl = new Polls();
	$poll = $pl->PollGet($id);
	
	$title[] = array('polls','polls.php?id='.$id_topic);
	$title[] = array($poll['title'],'poll.php?id='.$id.'&id_topic='.$id_topic);
	$title[] = array('status_changes','poll_status.php?id='.$id);
}
$title[] = array($date!=""?$date:"add_new",'');

if($date!="")
{
	include_once(SERVER_ROOT."/../classes/scheduler.php");
	$sc = new Scheduler();
	$row = $sc->StatusChangeGetByDate($id_res,$id,$date);
}

echo $hh->ShowTitle($title);

$input_right = ($module_admin || $t->AmIAdmin());

?>
<script type="text/javascript">
$().ready(function() {
	$("#form1").validate({
		rules: {
			change_date: "required"
		}
	});
	$("input#change_date-field").datepicker({ dateFormat: 'dd-mm-yy' });
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","status_change");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_res",$id_res);
echo $hh->input_hidden("id",$id);
echo $hh->input_table_open();
echo $hh->input_date_text("date","change_date",$row['change_date'],10,0,$input_right);
echo $hh->input_array("status","status",$row['status'],$hh->tr->Translate("status_options"),$input_right);	
$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id>0 && $date!="");
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
