<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('settings_main','topic.php?id='.$id_topic);
$title[] = array('keywords_internal','');
echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;

$ikeywords = $t->KeywordsInternal(0);
echo "<ul>";
$resources = $hh->tr->Translate("resources");
foreach($ikeywords as $ikeyword)
{
	echo "<li>" . $hh->Wrap($ikeyword['keyword'],"<a href=\"topic_keyword.php?id_topic=$id_topic&id={$ikeyword['id_keyword']}\">","</a>",$input_right);
	if($ikeyword['id_res_type']>0)
	{
		$role_options = $hh->tr->Translate("role_options");
		echo " ({$resources[$ikeyword['id_res_type']]} - " . $hh->tr->Translate("management") . ": " . $role_options[$ikeyword['role']] . ")";
	}
	if($ikeyword['description']!="")
		echo "<div class=\"notes\">{$ikeyword['description']}</div>";
	echo "</li>";
}
echo "</ul>";

echo "<p><a href=\"topic_keyword.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

