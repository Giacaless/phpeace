<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/template.php");

$id_topic = ($_GET['id_topic']>0)? $_GET['id_topic']:0;
$id_template = $_GET['id_template'];
$id_template_param = $_GET['id'];

if ($module_admin)
	$input_right = 1;

$te = new Template();
$template = $te->TemplateGet($id_template);
$te->ResourceSet($template['id_res']);

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($id_topic==$template['id_topic'] && $t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$trs = new Translator($hh->tr->id_language,0,false,$t->id_style);
}
else 
	$trs = new Translator($hh->tr->id_language,0,false,0);

$title[] = array('templates','templates.php?id_topic='.$id_topic);

$title[] = array($template['name'],'template.php?id='.$id_template.'&id_topic='.$id_topic);

$title[] = array('hidden_fields','');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","template_hide");
echo $hh->input_hidden("id_template",$id_template);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
$resource_types = $hh->tr->Translate("resources");
echo $hh->input_array("resources_types","id_res_type",$template['id_res'],$resource_types,0);
echo $hh->input_list_labels("hidden_fields","hidden_fields",$template['hide_fields'],$te->HideableFields(),$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);

echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
