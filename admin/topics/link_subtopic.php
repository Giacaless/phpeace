<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
$hh = new HtmlHelper();

$id_topic = $_GET['id_topic'];
$id_subtopic = $_GET['id_subtopic'];

$hh->TopicLoad($id_topic);
$page_title = $hh->tr->Translate("placing");
include_once(SERVER_ROOT."/../admin/include/head.php");
?>
<body>
<script type="text/javascript">
function set_subtopic(id_subtopic,subtopic)
{
	opener.document.forms['form1'].id_subtopic.value = id_subtopic;
	opener.document.forms['form1'].descriz_id_subtopic.value = subtopic;
	self.close();
}
</script>
<div class="popup-tree">
<?php
echo "<p><b>{$hh->topic->name}</b>\n<br>" . $hh->tr->Translate("placing_link_choose") . "</p>\n";
echo $hh->ShowSubtopicsTreeForLink(0, $id_subtopic, $id_topic);
?>
<p><input type="button" onClick="self.close()" value="<?=$hh->tr->Translate("close");?>"></p>
</div>
</body>
</html>
