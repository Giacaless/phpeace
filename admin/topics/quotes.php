<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/quotes.php");

$approved = $_GET['approved'];
if ($approved=="")
	$approved = 1;

$id_topic = (int)$_GET['id_topic'];

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
}

$title[] = array((($approved==1)? "quotes_approved" : "quotes_to_approve"),'');
echo $hh->ShowTitle($title);

$qq = new Quotes();
$num = $qq->QuotesApproved( $row, $id_topic, $approved );

$table_headers = array('topic','author','quote','&nbsp;');
$table_content = array('$row[name]','$row[author]<div>$row[notes]</div>','{nl2br($row[quote])}',
'{LinkTitle("quote.php?id=$row[id_quote]&approved='.$approved.'&id_topic='.$id_topic.'",' . $hh->tr->Translate("open") . ')}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"quote.php?id=0&id_topic=$id_topic\">" . $hh->tr->Translate("quotes_new") . "</a></p>\n";

if ($id_topic>0 && $approved)
	echo $hh->input_code("quote_html","quote_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/quote.php?id_topic=$id_topic\"></script>",60,2);

include_once(SERVER_ROOT."/include/footer.php");
?>
