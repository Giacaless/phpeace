<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);

$id_topic = $_GET['id_topic'];
$id_subtopic = $_GET['id_subtopic'];
$id_parent = $_GET['id_parent'];
$hh = new HtmlHelper;

$page_title = $hh->tr->Translate("subtopic_parent");
include_once(SERVER_ROOT."/../admin/include/head.php");
?>
<body>
<script type="text/javascript">
function set_subtopic(id_subtopic,subtopic)
{
	opener.document.forms['form1'].id_parent.value = id_subtopic;
	opener.document.forms['form1'].descriz_id_parent.value = subtopic;
	self.close();
}
</script>
<div class="popup-tree">
<?php
$hh->TopicLoad($id_topic);
if (count($hh->topic->tree)>0)
{
	echo "<p>" . $hh->tr->Translate("subtopic_parent_choose") . "</p>\n";
	$stree = "<dl>\n";
	if ($id_parent>0)
		$stree .= "<dt><dd><a href=\"#\"  onClick=\"set_subtopic('0','nessuno');\">{$hh->topic->name}</a></dd></dt>";
	else
		$stree .= "<dt><dd><b>{$hh->topic->name}</b></dd></dt>";
	$stree .= $hh->ShowSubtopicsTreeForSubtopic(0, $id_subtopic, $id_parent, $id_subtopic);
	$stree .= "</dl>\n";
	echo $stree;
}
else
	echo "<p>" . $hh->tr->Translate("missing_subtopics") . "</p>\n<p><input type=button onClick=\"self.close()\" value=\"" . $hh->tr->Translate("close") . "\"></p>\n";
?>
</div>
</body>
</html>

