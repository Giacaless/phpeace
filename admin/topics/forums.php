<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('forum','');

echo $hh->ShowTitle($title);

$num = $t->ForumsP( $row );

$table_headers = array('date_start','forum','status');
$table_content = array('{FormatDate($row[start_date_ts])}','{LinkTitle("forum.php?id=$row[id_topic_forum]&id_topic='.$id_topic.'",$row[name])}','{StatusLookup($row[active])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($input_right)
	echo "<p><a href=\"forum.php?id=0&id_topic=$id_topic\">".$hh->tr->Translate("add_new")."</a>\n";

if($ah->ModuleAdmin(10))
	echo "<p><a href=\"features.php?id=$id_topic&id_type=8\">" . $hh->tr->Translate("features") ."</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");

?>

