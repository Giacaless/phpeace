<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/links.php");


$id_topic = $_GET['id'];
$appr = $_GET['appr'];
$error = $_GET['error'];

$t = new Topic($id_topic);

$approved = 1;
if (($appr=="0") && ($module_admin || $t->AmIAdmin()))
	$approved = 0;

$approved = 1;
$title1 = "links_list_approved";

if ($appr=="0")
{
	$approved = 0;
	$title1 = "links_list_to_approve";
}

if ($appr=="1" || $error=="1")
{
	$error404 = 1;
	$title1 = "links_list_to_check";
}
else
	$error404 = 0;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($title1,'');
echo $hh->ShowTitle($title);

$l = new Links;
$num = $l->TopicApprove( $row, $id_topic, $approved, $error404 );

$table_headers = array('date','title','link','in');
$table_content = array('{FormatDate($row[insert_date_ts])}','{LinkTitle("link.php?id=$row[id_link]&id_topic='.$id_topic.'&error='.$error404.'",$row[title])}','$row[url]','{PathToSubtopic('.$id_topic.',$row[id_subtopic])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);
?>
<p><hr><a href="link.php?id=0&id_topic=<?=$id_topic;?>"><?=$hh->tr->Translate("link_add");?></a></p>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

