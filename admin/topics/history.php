<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id_topic'];
$id_type = $_GET['id_type'];
$id = $_GET['id'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
if ($id_type==$ah->r->types['article'])

{
	$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic);
	include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($id);
	$article = $a->ArticleGet();
	$title[] = array($article['headline'],'/articles/article.php?w=topics&id='.$id);
}
if ($id_type==$ah->r->types['subtopic'])
{
	$title[] = array('subtopics_tree','subtopics.php?id='.$id_topic);
	$row = $t->SubtopicGet($id);
	$title[] = array($row['name'],'subtopic.php?id='.$id.'&id_topic='.$id_topic);

}
if ($id_type==$ah->r->types['event'])
{
	$title[] = array('events','events.php?id='.$id_topic);
	include_once(SERVER_ROOT."/../classes/event.php");
	$e = new Event();
	$row = $e->EventGet($id);
	$title[] = array($row['title'],'event.php?id='.$id.'&id_topic='.$id_topic);

}
$title[] = array('history','');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/history.php");
$h = new History;

$num = $h->HistoryAll( $row, $id_type, $id );

$table_headers = array('date','ip','user','action');
$table_content = array('{FormatDateTime($row[ts_time])}','$row[ip]','$row[name]','{HistoryAction($row[action])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
