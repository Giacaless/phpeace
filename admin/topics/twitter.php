<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/twitter.php");

$hhf = new HHFunctions();

$id_topic = (int)$get['id_topic'];
$id_twitter = (int)$get['id'];

if(isset($get['verified']))
{
	$trm4 = new Translator($hh->tr->id_language,4);
	$ah->MessageSetTranslated($trm4->Translate($get['verified']=="1"?"twitter_ok":"error_twitter"));
}

if($id_topic>0)
{
	$t = new Topic($id_topic);
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	if ($t->AmIAdmin())
		$input_right = 1;
}

if ($module_admin)
	$input_right = 1;

$title[] = array('Twitter','twitters.php?id='.$id_topic);

$tw = new TwitterHelper();
if ($id_twitter>0)
{
	$row = $tw->TwitterAccountGet($id_twitter);
	$title[] = array($row['name'],'');
	if($input_right && $id_topic>0 && $id_topic != $row['id_topic'])
		$input_right = 0;
	$twitter_topic = $row['id_topic'];
	$twitter_group = $row['id_topic_group'];
	$id_user = $row['id_user'];
	$show_latest_only = $row['show_latest_only'];
	$active = $row['active'];
}
else
{
	$title[] = array('add_new','');
	if($id_topic>0)
		$twitter_topic = $id_topic;
	$id_user = $ah->current_user_id;
	$show_latest_only = 1;
	$active = 1;
}

echo $hh->ShowTitle($title);

if($tw->IsTwitterApiKeySet())
{
	
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required"
		}
	});
});
</script>

<?php

echo $hh->input_form_open();
echo $hh->input_hidden("from","twitter");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_twitter",$id_twitter);
echo $hh->input_table_open();

echo $hh->input_text("name","name",$row['name'],30,0,$input_right);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($twitter_topic,$twitter_group,$tt->User($ah->current_user_id),"all_option",$input_right,true,"id_topic_or_group");

echo $hh->input_checkbox("show_latest_only","show_latest_only",$show_latest_only,0,$input_right);
echo $hh->input_text("keyword_id_filter","id_keyword",$row['id_keyword'],6,0,$input_right);
echo $hh->input_checkbox("use_keywords","use_keywords",$row['use_keywords'],0,$input_right);

if($id_twitter>0)
{
	$bind_label = "bind_account";
	echo "<tr><td class=\"input-label\">Twitter Account</td><td>";
	if($row['oauth_token']!="" && $row['oauth_token_secret']!="")
	{
		$twitter_content = $tw->TwitterAccountContent($row['oauth_token'],$row['oauth_token_secret']);
		if(isset($twitter_content->id_str))
		{
			echo "<img src=\"" . $twitter_content->profile_image_url . "\" align=\"left\"/>";
			echo "&nbsp;<strong>" . $twitter_content->name . "</strong>";
			$twitter_url = "twitter.com/" . $twitter_content->screen_name;
			echo "<br/>&nbsp;" . $twitter_content->statuses_count . " tweets on <a href=\"http://$twitter_url\" target=\"_blank\">$twitter_url</a>";
			echo "<br/>&nbsp;Apps " . $hh->tr->Translate("management") . ": <a href=\"http://twitter.com/settings/connections?lang={$hh->tr->lang}\" target=\"_blank\">http://twitter.com/settings/connections</a>";
			$bind_label = "change_account";
		}
		else
		{
			echo "<em>Revoked</em>";
		}
	}
	echo "</td></tr>\n";
}

echo $hh->input_separator("administration");
echo $hh->input_text("author","id_user",$hhf->UserLookup($id_user),50,0,0);
echo $hh->input_checkbox("active","active",$active,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"bind_account",'label'=>$bind_label,'right'=>$input_right && $id_twitter>0);
$actions[] = array('action'=>"delete",'label'=>'delete','right'=>$input_right && $id_twitter>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

}
else
{
	echo $hh->tr->Translate("twitter_api");
}

include_once(SERVER_ROOT."/include/footer.php");
?>
