<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/template.php");
include_once(SERVER_ROOT."/../classes/resources.php");

$id_topic = ($_GET['id_topic']>0)? $_GET['id_topic']:0;
$id_template = $_GET['id_template'];
$id_template_param = $_GET['id'];

if ($module_admin)
	$input_right = 1;

$te = new Template();
$template = $te->TemplateGet($id_template);
$te->ResourceSet($template['id_res']);

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($id_topic==$template['id_topic'] && $t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$trs = new Translator($hh->tr->id_language,0,false,$t->id_style);
}
else 
	$trs = new Translator($hh->tr->id_language,0,false,0);

$title[] = array('templates','templates.php?id_topic='.$id_topic);

$title[] = array($template['name'],'template.php?id='.$id_template.'&id_topic='.$id_topic);

if ($id_template_param>0)
{
	$row = $te->ParamGet($id_template_param);
	$title[] = array($row['label'],'');
	$public = $row['public'];
}
else
{
	$title[] = array('add_new','');
	$public = 1;
}

echo $hh->ShowTitle($title);

$r = new Resources();
$types = $r->ParamsTypes(true);

echo $hh->input_form_open();
echo $hh->input_hidden("from","template_param");
echo $hh->input_hidden("id_template",$id_template);
echo $hh->input_hidden("id_template_param",$row['id_template_param']);
echo $hh->input_hidden("id_res",$template['id_res']);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_type_old",array_search($row['type'],$types));
echo $hh->input_table_open();
$param_label = "";

echo $hh->input_text("name","label",$row['label'],20,0,$input_right,$param_label);
$t_types = array();
foreach($types as $key=>$type)
	$t_types[$key] = $hh->tr->Translate("paramtype_" . $type);

echo $hh->input_array("type","id_type",array_search($row['type'],$types),$t_types,$input_right);

if($row['type']=="dropdown" || $row['type']=="mchoice")
{
	if($row['params']=="")
		echo $hh->input_note("select_insert");
	echo $hh->input_text("parameters","type_params",$row['type_params'],20,0,$input_right);
}
if($row['type']=="geo")
{
	echo $hh->input_array("geo_location","type_params",($row['type_params']>0)?$row['type_params']:$hh->ini->Get("geo_location"),$hh->tr->Translate("geo_options"),$input_right);
}
if($row['type']=="textarea")
{
	echo $hh->input_checkbox("htmlise","type_params",$row['type_params'],0,$input_right);
}
echo $hh->input_text("default","default_value",$row['default_value'],20,0,$input_right);
echo $hh->input_checkbox("public","public",$public,0,$input_right);
echo $hh->input_checkbox("index_include","index_include",$row['index_include'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_template_param>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
