<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");
$fo = new Forms();

$id_topic = (int)$_GET['id_topic'];
$id_form = $_GET['id'];

if ($module_admin)
{
	$input_right = 1;
}

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$users = $t->Users();
}
else 
{
	include_once(SERVER_ROOT."/../classes/users.php");
	$uu = new Users();
	$users = $uu->Active();
	$t = new Topic(0);
}
$users[] = array('0'=>0,'id_user'=>0,'1'=>"--" . $hh->tr->Translate("contact_main") . "--");

$title[] = array('forms','forms.php?id='.$id_topic);

$row = $fo->FormGet($id_form);
$title[] = array($row['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('fields','');

$id_user = $row['id_user'];
if ($id_topic>0 && $id_topic==$row['id_topic'] && $t->AmIAdmin())
	$input_right = 1;
$form_topic = $row['id_topic'];

echo $hh->ShowTitle($title);

if($id_form>0)
{
	$tabs = array();
	$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
	$tabs[] = array("fields","");
	if($row['store'])
	{
		$posts = array();
		$num_posts = $fo->Posts($posts,$id_form,$id_topic);
		$tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
        $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
	}
	if($row['weights'])
		$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
	$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
	echo $hh->Tabs($tabs);
	
	$hhf = new HHFunctions();
	echo "<h3>" . $hh->tr->Translate("fields");
	if($input_right)
		echo " (<a href=\"form_param.php?id_form=$id_form&id_topic=$id_topic&id=0\">" . $hh->tr->Translate("add_new") . "</a>)";
	echo "</h3>";
	$params = $fo->Params($id_form);
	if($row['profiling']>1)
	{
		$email_found = false;
		foreach($params as $param)
			if($param['label']=="email")
				$email_found = true;
		if(!$email_found)
			echo $hh->tr->Translate("form_param_email");
	}
	if(count($params)>0)
	{
		echo "<ul class=\"tree\">\n";
		$counter = 1;
		foreach($params as $param)
		{
			echo "<li>";
			if($input_right)
			{
				if ($counter>1)
					echo "<a href=\"actions.php?from2=form_param&id_form=$id_form&id={$param['id_param']}&action3=up\">{$hh->img_arrow_up_on}</a>";
				else
					echo $hh->img_arrow_up_off;
				if ($counter<count($params))
					echo "<a href=\"actions.php?from2=form_param&id_form=$id_form&id={$param['id_param']}&action3=down\">{$hh->img_arrow_down_on}</a>";
				else
					echo $hh->img_arrow_down_off;
				echo "&nbsp;";
			}
			echo $hh->Wrap($hhf->LinkTitle("form_param.php?id={$param['id_param']}&id_form=$id_form&id_topic=$id_topic",$param['label']),"<strong>","</strong>",$param['mandatory']) . " (" . $hh->tr->Translate("paramtype_" . $param['type']);
			if($param['type']=="text" && $param['type_params']>0)
			{
				$text_uses = $hh->tr->Translate("form_param_text_uses");
				echo " - " . $text_uses[$param['type_params']];
			}
			echo ")</li>\n";
			$counter ++;
		}
		echo "</ul>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>

