<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_p = (int)$get['id_p'];
$id_topic = $get['id_topic'];

$t = new Topic($id_topic);
$row = $t->row;

if ($t->AmIAdmin() || $module_admin)
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('visitors','visitors.php?id='.$id_topic);

include_once(SERVER_ROOT."/../classes/people.php");
$pe = new People();
$row = $pe->UserGetDetailsById($id_p);
$title[] = array($row['name1'] . " " . $row['name2'],'visitor.php?id_p='.$id_p.'&id='.$id_topic);

$title[] = array('groups','');

echo $hh->ShowTitle($title);

$pgroups = $pe->Groups($id_p,false,$id_topic);
$pgroups_ids = array();
foreach($pgroups as $pgroup)
	$pgroups_ids[] = $pgroup['id_pt_group'];

$tgroups = array();
$t->PeopleGroups($tgroups);

echo $hh->input_form_open();
echo $hh->input_hidden("from","visitor_groups");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("id_p",$id_p);

echo "<ul>";
foreach($tgroups as $tgroup)
{
	echo "<li>";
	if ($input_right==1)
	{
		echo "<input type=\"checkbox\"  class=\"input-checkbox\" name=\"group{$tgroup['id_pt_group']}\"";
		
		if (in_array($tgroup['id_pt_group'],$pgroups_ids))
			echo " checked ";
		echo ">";
	}
	echo "{$tgroup['name']}</li>";
}
echo "</ul>";
if ($input_right==1)
	echo "<p><input type=\"submit\" class=\"input-submit\" value=\"" . $hh->tr->Translate("submit") . "\"></p>\n";

echo $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
