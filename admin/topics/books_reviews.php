<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id_topic'];
$approved = (int)$_GET['approved'];

$t = new Topic($id_topic);

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($trm16->Translate($approved? "reviews_approved":"reviews_to_approve"),'');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../modules/books.php");
$bb = new Books();
$num = $bb->Reviews( $row, $approved, $id_topic );
$table_headers = array('date','book','recensione');
$table_content = array('{FormatDate($row[insert_date_ts])}','$row[title]',
'{LinkTitle("books_review.php?id=$row[id_review]&id_book=$row[id_book]&id_topic='.$id_topic.'&approved='.$approved.'",$row[review],20)}');


echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

