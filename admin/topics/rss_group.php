<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/rssmanager.php");

$id_group = (int)$_GET['id'];
$id_topic = (int)$_GET['id_topic'];

if($module_admin)
	$input_right = 1;
	
if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	if($t->AmIAdmin())
		$input_right = 1;	
}

$title[] = array('RSS feeds','rss_groups.php?id_topic='.$id_topic);

$rsm = new RssManager();

if ($id_group>0)
{
	$row = $rsm->GroupGet($id_group);
	$title[] = array($row['name'],'');
	$items = $row['items'];
	$ttl = $row['ttl'];
	$feeds = array();
	$num_feeds = $rsm->RssAll($feeds,$id_group,false);
}
else
{
	$title[] = array('add_new','');
	$items = 15;
	$ttl = 60;
}

echo $hh->ShowTitle($title);

if($id_group>0)
{
	$url = $ini->Get("pub_web") . "/js/rss_group.php?id=$id_group";
	echo "<p>" . $hh->tr->Translate("link") .": <a href=\"$url\" target=\"_blank\">$url</a></p>";
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","rss_group");
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_table_open();
if($id_group>0)
	echo $hh->input_text("ID","id_group",$id_group,50,0,0);
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_array("type","group_type",$row['group_type'],$rsm->group_types,$input_right);
echo $hh->input_text("limit","items",$items,3,0,$input_right);
echo $hh->input_text("ttl","ttl",$ttl,3,0,$input_right,"(minutes)");
echo $hh->input_checkbox("show_title","show_title",$row['show_title'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_group>0 && $num_feeds==0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id_group>0)
{
	if($num_feeds>0)
	{
		echo "<h3>Feeds (<a href=\"rss_group_preview.php?id=$id_group&id_topic=$id_topic\" target=\"_blank\">" . $hh->tr->Translate("preview") . "</a>)</h3>";
		echo "<ul>";
		foreach($feeds as $feed)
			echo "<li><a href=\"rss_feed.php?id={$feed['id_feed']}&id_group=$id_group&id_topic=$id_topic\">{$feed['url']}</a></li>";
		echo "</ul>";
	}
	
	if ($input_right)
		echo "<p><a href=\"rss_feed.php?id=0&id_group=$id_group&id_topic=$id_topic\">" . $hh->tr->Translate("feed_add") . "</a>\n";
	
}

include_once(SERVER_ROOT."/include/footer.php");
?>
