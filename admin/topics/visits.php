<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id'];
$id_p = $_GET['id_p'];

$t = new Topic($id_topic);
$row = $t->row;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('visits','');

echo $hh->ShowTitle($title);

if($t->profiling)
{
	if(($module_admin || $t->AmIAdmin()) && ($conf->Get("track") || $conf->Get("track_all")))
	{
		include_once(SERVER_ROOT."/../classes/tracker.php");
		$tk = new Tracker();

		$num = $tk->Visits( $row, 0);
		$qs = "&id_topic=$id_topic";
		$table_headers = array('date','ip','name','pages','length','cookie');
		$table_content = array('{FormatDateTime($row[visit_date_ts])}','$row[ip]',
		'{LinkTitle("visitor_visit.php?id=$row[id_visit]&id_p=$row[id_p]'.$qs.'","$row[name]")}','<div class=\"right\">$row[pages]</div>','<div class=\"right\">$row[length]</div>','{Bool2YN($row[cookie])}');

		echo $hh->ShowTable($row, $table_headers, $table_content, $num);

	}
}


include_once(SERVER_ROOT."/include/footer.php");
?>

