<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id_topic = $_GET['id_topic'];
$id = $_GET['id'];
$id_book = $_GET['id_book'];
$approved = (int)$_GET['approved'];

$b = new Book($id_book);

$t = new Topic($id_topic);

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($t->name,'ops.php?id='.$id_topic);

if ($module_admin || $t->AmIAdmin())
{
	$input_right = 1;
	$input_super_right = 1;
}

if ($id>0)
{
	$action2 = "update";
	$review = $b->ReviewGet($id);
	$approved = $review['approved'];
	$vote = $review['vote'];
	$id_language = $review['id_language'];
	$title[] = array($trm16->Translate($approved? "reviews_approved":"reviews_to_approve"),'books_reviews.php?approved='.$approved.'&id_topic='.$id_topic);
	$title[] = array($b->title,'');
	$review_topic = $review['id_topic'];
}
else
{
	$action2 = "insert";
	$approved = $input_right;
	$vote = 3;
	$id_language = $t->id_language;
	if ($t->AmIUser())
		$input_right = 1;
	$title[] = array($trm16->Translate("review_add"),'');
	$review_topic = $id_topic;
}

echo $hh->ShowTitle($title);
?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			review: "required"
		}
	});
});
</script>

<?php
echo $hh->input_form("post","/books/actions.php");
echo $hh->input_hidden("from","review");
echo $hh->input_hidden("module","topics");
echo $hh->input_hidden("rapproved",$approved);
echo $hh->input_hidden("id_review",$id);
echo $hh->input_hidden("from_topic",$id_topic);
echo $hh->input_table_open();

$bb = new Books();
echo $hh->input_row($trm16->Translate("book"),"id_book",$id_book,$bb->BooksAll(array('sort_by'=>"name")),"",0,$input_right);

echo $hh->input_date("insert_date","insert_date",$review['insert_date_ts'],$input_right);
echo $hh->input_textarea($trm16->Translate("review"),"review",$review['review'],80,10,"",$input_right);

$combo_values = array('5'=>'*****','4'=>'****','3'=>'***','2'=>'**','1'=>'*');
echo $hh->input_array($trm16->Translate("vote"),"vote",$vote,$combo_values,$input_right);

if($review['id_p']>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$user = $pe->UserGetById($review['id_p']);
	echo $hh->input_text("name","name",$user['name1'],50,0,0);
	echo $hh->input_text("email","email",$user['email'],50,0,0);
}
else
{
	echo $hh->input_text("name","name",$review['name'],50,0,$input_right);
	echo $hh->input_text("email","email",$review['email'],50,0,$input_right);
}
echo $hh->input_array("language","id_language",$id_language,$phpeace->Languages($hh->tr->id_language),$input_right);
include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo $hh->input_topics($review_topic,0,$tt->AllTopics(),"all_option",$input_right);

echo $hh->input_checkbox($trm16->Translate("important"),"important",$review['important'],0,$input_right);
echo $hh->input_checkbox("approved","approved",$approved,0,$input_right);

include_once(SERVER_ROOT."/../classes/varia.php");
$v =new Varia();
$deparams = $v->Deserialize($review['rparams']);

include_once(SERVER_ROOT."/../classes/resources.php");
$r = new Resources();
$rparams = $r->Params("review");
if(count($rparams)>0)
{
	echo $hh->input_separator("additional_fields");
	foreach($rparams as $rparam)
		echo $hh->input_keyword_param($rparam['id_resource_param'],$rparam['label'],$rparam['type'],$deparams['rp_'.$rparam['id_resource_param']],$input_right,$rparam['public'],$rparam['params']);
}

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($id>0 && $input_super_right));
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

