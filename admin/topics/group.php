<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topics.php");


$id = $_GET['id'];
$title[] = array('topics_tree','tree.php');

if ($id>0)
{
	$action2 = "update";
	$topics = new Topics;
	$row = $topics->gh->GroupGet($id);
	$title[] = array($row['name'],'');
	$parent_name = $row['parent_name'];
	if ($row['id_parent']=="0")
		$parent_name = $topics->gh->top_name;
}
else
{
	$action2 = "insert";
	$title[] = array('topic_group_new','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			name: "required"
		}
	});
});
</script>
<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","group");
echo $hh->input_hidden("id_group",$id);
echo $hh->input_hidden("id_parent_old",$row['id_parent']);
echo $hh->input_table_open();

if($id>0)
	echo $hh->input_text("ID","ID",$id,5,0,0);
echo $hh->input_text("name","name",$row['name'],20,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],50,3,"",$input_right);
echo $hh->input_link("son_of","id_parent","'group_group.php?&id_group='+document.forms['form1'].id_group.value+'&id_parent='+document.forms['form1'].id_parent.value",$row['id_parent'],$parent_name,$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id,$o->types['topics_group'],$keywords,$input_right);

echo $hh->input_checkbox("visible","visible",$row['visible'],0,$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
if ($id>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);

echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

