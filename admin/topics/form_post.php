<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");

$id_topic = $get['id_topic'];
$id_form = $get['id_form'];
$id_post = $get['id'];

if ($module_admin)
	$input_right = 1;

$fo = new Forms();
$form = $fo->FormGet($id_form);

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($id_topic==$form['id_topic'] && $t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('forms','forms.php?id='.$id_topic);
$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('posts','form_posts.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('#'.$id_post,'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
	$posts = array();
	$num_posts = $fo->Posts($posts,$id_form,$id_topic);
	$tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
    $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
}
if($form['weights'])
	$tabs[] = array("actions","form_actions.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);

$post = $fo->PostGet($id_post);
if($id_topic>0 && $post['id_topic']==$id_topic && $t->AmIAdmin())
	$input_right = 1;

if($input_right)
{
    if($post['id_p']>0)
    {
        include_once(SERVER_ROOT."/../classes/people.php");
        $pe = new People();
        $user = $pe->UserGetById($post['id_p'],false);
        $hhf = new HHFunctions();
        
        if(count($post['post'])>0)
        {
            $strUser = "<ul>";
            foreach($post['post'] as $post_item)
            {
                $strUser .= "<li>{$post_item['name']}: <em>{$post_item['value']}</em></li>";
            }
            $strUser .= "</ul>";
        }
    }
    
    echo "<p>" . $hhf->LinkTitle( ($id_topic>0 && $t->profiling? "visitor.php?id=$id_topic&id_p={$post['id_p']}" : "/people/person.php?id={$post['id_p']}"),"{$user['email']}") . "</p>";;
    echo $strUser;
    
    if($form['weights'])
	{
		echo "<p>" . $hh->tr->Translate("score"). ": {$post['score']}</p>";
	}
	if($post['id_payment']>0 && $ah->ModuleUser(15))
	{
		$trm15 = new Translator($hh->tr->id_language,15);
		include_once(SERVER_ROOT."/../classes/payment.php");
		$pa = new Payment();
		$payment = $pa->PaymentGet($post['id_payment']);
		echo "<p>" . $trm15->Translate("payment"). ": <a href=\"/payments/payment.php?id={$post['id_payment']}\">{$payment['amount']} {$pa->default_currency_desc}</a></p>";
	}
	if($post['ext']!="")
	{
		if(is_numeric($post['ext']))
		{
			$id_image = (int)$post['ext'];
			if($id_image>0)
			{
				$id_gallery = $fo->Gallery($id_form);
				$filename_copy = "images/2/{$id_image}." . $conf->Get("convert_format");
				$img_sizes = $conf->Get("img_sizes");
				echo $hh->Wrap("<img src=\"/images/upload.php?src=$filename_copy\" width=\"{$img_sizes[2]}\">", "<a href=\"/galleries/image.php?id=$id_image&id_gallery=$id_gallery\">", "</a>", $id_gallery>0 && Modules::AmIUser(7));
			}
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/file.php");
			$fm = new FileManager();
			$filename = "forms/$id_form/{$id_post}.{$post['ext']}";
			if($fm->Exists("uploads/$filename"))
			{
				echo "<p>File: <a href=\"/docs/upload.php?src=$filename\" target=\"_blank\">$id_post.{$post['ext']}</a>";
				echo " (" . $fm->Size("uploads/$filename") . ")</p>";
			}
		}
	}
	echo "<p>" . $hh->tr->Translate("insert_date") . ": " . $hh->FormatDateTime($post['post_time_ts']) . "</p>";
	echo "<p>IP: {$post['ip']}</p>";

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","form_post");
	echo $hh->input_hidden("id_post",$id_post);
	echo $hh->input_hidden("id_form",$id_form);
	echo $hh->input_hidden("id_topic",$id_topic);
	echo $hh->input_table_open();
	$actions = array();
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_post>0);
	echo $hh->input_actions($actions,$input_right);
	
	echo $hh->input_table_close() . $hh->input_form_close();
}

include_once(SERVER_ROOT."/include/footer.php");
?>

