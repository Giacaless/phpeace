<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

include_once(SERVER_ROOT."/../classes/xmlhelper.php");

$xh = new XmlHelper();

$id = $_GET['id'];

$title[] = array('XML fragments','xml_fragments.php');

if ($id>0)
{
	$action2 = "update";
	$row = $xh->XmlFragmentGet($id);
	$title[] = array($row['name'],'');
}
else
{
	$action2 = "insert";
	$title[] = array('New fragment','');
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<form method="post" action="actions.php" name="form1">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="from" value="xml_fragment">
<input type="hidden" name="id_xml" value="<?=$id;?>">
<table border=0 cellpadding=0 cellspacing=7>
<?php
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("xml","xml",$row['xml'],80,30,"",$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && $id>0));
echo $hh->input_actions($actions,$input_right);

?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

