<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forms.php");

$id_topic = $get['id_topic'];
$id_form = $get['id'];

if ($module_admin)
	$input_right = 1;

$fo = new Forms();
$form = $fo->FormGet($id_form);

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($id_topic==$form['id_topic'] && $t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
}

$title[] = array('forms','forms.php?id='.$id_topic);
$title[] = array($form['name'],'form.php?id='.$id_form.'&id_topic='.$id_topic);
$title[] = array('actions','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("form","form.php?id=$id_form&id_topic=$id_topic");
$tabs[] = array("fields","form_params.php?id=$id_form&id_topic=$id_topic");
if($form['store'])
{
	$posts = array();
	$num_posts = $fo->Posts($posts,$id_form,$id_topic);
	$tabs[] = array("Posts ($num_posts)","form_posts.php?id=$id_form&id_topic=$id_topic");
    $tabs[] = array("Reports","form_summary.php?id=$id_form&id_topic=$id_topic");
}
$tabs[] = array("actions",'');
$tabs[] = array("usage","form_use.php?id=$id_form&id_topic=$id_topic");
echo $hh->Tabs($tabs);
	
if($input_right)
{
	$num = $fo->Actions($row,$id_form);
	$table_headers = array('min','max','redirect','email','');
	$table_content = array('<div class=right>$row[score_min]</div>','<div class=right>$row[score_max]</div>','$row[redirect]','$row[recipient]',
	'{LinkTitle("form_action.php?id=$row[id_action]&id_topic=' . $id_topic . '&id_form='.$id_form.'","'.$hh->tr->Translate("change").'")}');
	echo $hh->showTable($row, $table_headers, $table_content, $num);
	
	echo "<p><a href=\"form_action.php?id=0&id_form=$id_form&id_topic=$id_topic\">" . $hh->tr->Translate("add_new") . "</a>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>

