<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
$pt = new PageTypes();


$id_topic = ($_GET['id']>0)? $_GET['id']:0;
$id_type = ($_GET['id_type']!="")? (int)$_GET['id_type']:-1;

if ($module_admin)
	$input_right = 1;

if ($id_topic>0)
{
	$t = new Topic($id_topic);
	if ($t->AmIAdmin())
		$input_right = 1;
	$title[] = array($t->name,'ops.php?id='.$id_topic);
	$num = $pt->ft->PageFeaturesStyle($row,$t->id_style,$id_type);
}
else
{
	$num = $pt->ft->PageFeaturesStyle($row,0,$id_type);
}

$title[] = array('Features','');
echo $hh->ShowTitle($title);

if ($id_type>=0)
{
	$page_types = $hh->tr->Translate("page_types");	
	echo "<p>" . $hh->tr->Translate("features") . ": <b>" . $page_types[$id_type] . "</b></p>";
}

$table_headers = array('feature','description','page_type','function','active','author');
$table_content = array('{LinkTitle("feature.php?id=$row[id_feature]&id_topic='.$id_topic.'",$row[name])}',
'$row[description]','{PageType($row[id_type],$row[id_module])}','{PageFunction($row[id_function])}','{Bool2YN($row[active])}','{UserLookup($row[id_user])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"feature.php?id=0&id_topic=$id_topic" . (($id_type>=0)? "&id_type=$id_type":"") . "\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

