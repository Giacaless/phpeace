<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");

$id = $_GET['id'];

$t = new Topic($id);
$row = $t->row;

$title[] = array($t->name,'ops.php?id='.$id);
$title[] = array('layout_settings','');

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

echo $hh->ShowTitle($title);

?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			id_style: {
				required: true,
				min: 1
			}
		}
	});
});
</script>
<?php

echo $hh->input_form_open();
echo $hh->input_hidden("from","topic_gra");
echo $hh->input_hidden("id_topic",$id);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
echo $hh->input_row($hh->Wrap("graphic_style","<a href=\"/layout/style.php?id={$row['id_style']}\">","</a>",$row['id_style']>0 && (($input_right && $t->edit_layout) || $module_admin) ),"id_style",$row['id_style'],$s->StylesAll(),"choose_option",0,$input_right);

echo "<tr><td class=\"input-label\">" . $hh->tr->Translate("image_associated") . "</td><td valign=middle>";
if ($row['id_image']>0)
{
	include_once(SERVER_ROOT."/../classes/images.php");
	$i = new Images();
	echo "<img src=\"/images/upload.php?src=images/0/{$row['id_image']}".".".$i->convert_format."\" width=\"{$i->img_sizes[0]}\" border=\"0\" align=\"left\" >&nbsp;\n";
}
echo "(<a href=\"topic_images.php?id=$id\">" . $hh->tr->Translate("change") . "</a>)</td></tr>\n";

echo $hh->input_text("articles_per_page","articles_per_page",$row['articles_per_page'],5,0,$input_right);

$combo_values = array('1'=>'1','2'=>'2');
echo $hh->input_array("menu_depth","menu_depth",$row['menu_depth'],$combo_values,$input_right);
echo $hh->input_array("homepage_type","home_type",$row['home_type'],$hh->tr->Translate("topic_home_types"),$input_right);
echo $hh->input_array("sort_articles_by","home_sort_by",$row['home_sort_by'],$hh->tr->Translate("sort_by_options"),$input_right);
echo $hh->input_checkbox("homepage_show_path","show_path",$row['show_path'],0,$input_right);
include_once(SERVER_ROOT."/../classes/images.php");
$i = new Images();
echo $hh->input_array("associated_image_size","associated_image_size",$row['associated_image_size'],$i->img_sizes,$input_right);
echo $hh->input_array("Toolbar","show_print",$row['show_print'],$hh->tr->Translate("toolbar_options"),$input_right);
echo $hh->input_checkbox("Editor HTML WYSIWYG","wysiwyg",$row['wysiwyg'],0,$input_right);

echo "<tr><td class=\"input-label\">" . $hh->tr->Translate("texts") . "</td>\n<td>";
foreach($hh->th->types as $type=>$id_type)
{
	if ($id_type>0)
	{
		$texts_types = $hh->tr->Translate("texts_types");
		echo "<div><a href=\"text.php?id_topic=$id&id_type=$id_type\">" . $texts_types[$id_type] . "</a></div>";
	}
} 
echo "</td></tr>\n";

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

