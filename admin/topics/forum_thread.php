<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/forum.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_forum = (int)$_GET['id_forum'];
$id_thread = (int)$_GET['id'];
$approved = (int)$_GET['approved'];

$f = new Forum($id_forum);
$forum = $f->ForumGet();
$id_topic = $forum['id_topic'];

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('forum','forums.php?id='.$id_topic);
$title[] = array($forum['name'],'forum.php?id='.$id_forum.'&id_topic='.$id_topic);
$title[] = array('threads','forum_threads.php?id='.$id_forum.'&id_topic='.$id_topic.'&approved='.$approved.'&p='.$current_page);

if($id_thread>0)
{
	$row = $f->ThreadGet($id_thread);
	$title[] = array($row['title'],'');
}
else
{
	$title[] = array("add_new",'');
}

echo $hh->ShowTitle($title);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

if($id_thread>0)
{
	$url = $ini->Get("pub_web") . "/" . $ini->Get("forum_path") . "/thread.php?id=$id_forum&id_topic=$id_topic&id_thread=$id_thread";
	echo "<p>" . $hh->tr->Translate("link") .": <a href=\"$url\" target=\"_blank\">$url</a></p>";

	$co = new Comments("thread",$id_thread);
	$co_pending = $co->CommentsPending(0,$t->id);
	$co_approved = $co->CommentsPending(1,$t->id);
	if($forum['comments']>0 || ($co_approved + $co_pending)>0 || $module_admin)
	{
		echo "<div class=\"box\">";
		echo "<h3><a href=\"comments_tree.php?type=thread&id_item=$id_thread\">" . $hh->tr->Translate("comments") . "</a></h3>\n";
		echo "<ul>";
		if($co_approved>0)
			echo "<li><a href=\"comments.php?type=thread&id_item=$id_thread&appr=1\">" . $hh->tr->Translate("approveds") . "</a> (" . $co_approved . ")</li>\n";
		if($co_pending>0)
			echo "<li><a href=\"comments.php?type=thread&id_item=$id_thread&appr=0\">" . $hh->tr->Translate("to_approve") . "</a> (" . $co_pending . ")</li>\n";
		echo "<li><a href=\"comment.php?id=0&type=thread&id_item=$id_thread\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
		echo "</ul>";
		echo "</div>";
	}
}

echo $hh->input_form_open();
echo $hh->input_hidden("id_thread",$id_thread);
echo $hh->input_hidden("id_forum",$id_forum);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("approved",$approved);
echo $hh->input_hidden("p",$current_page);
echo $hh->input_hidden("from","thread");
echo $hh->input_table_open();

echo $hh->input_date("date_insert","insert_date",$row['insert_date_ts'],$input_right);
echo $hh->input_text("title","title",$row['title'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,4,"",$input_right);
echo $hh->input_wysiwyg("description_long","description_long",$row['description_long'],$row['is_html'],15,$input_right,$t->wysiwyg);

if($id_thread>0 && $row['id_p']>0)
{
	echo $hh->input_text("IP","ip",$row['ip'],15,0,0);
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$user = $pe->UserGetById($row['id_p']);
	echo $hh->input_text("author","author","{$user['name1']} {$user['name2']}",30,0,0);
}
if ($forum['source'])
{
	echo $hh->input_separator("source");
	echo $hh->input_textarea("source","source",$row['source'],60,3,"",$input_right);
}
echo $hh->input_separator("administration");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
if ($id_topic>0)
	$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
if ($id_forum>0)
	$o->GetKeywords($id_forum,$o->types['forum'], $keywords,0,true);
echo $hh->input_keywords($id_thread,$o->types['thread'],$keywords,$input_right);

echo $hh->input_checkbox("approved","thread_approved",$row['thread_approved'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_thread>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
