<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_poll = $_GET['id_poll'];
$id_question = $_GET['id_question'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);
$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
$title[] = array('poll_questions',"poll_questions.php?id=$id_poll&approved=-1");

$row = $pl->QuestionGet($id_question);
$title[] = array($row['question'],"poll_question.php?id_poll=$id_poll&id=$id_question");
$title[] = array('poll_answers_weights','');
echo $hh->ShowTitle($title);


if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);
}

echo $hh->input_form_open();
echo $hh->input_hidden("id_question",$id_question);
echo $hh->input_hidden("id_poll",$id_poll);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","poll_question_weights");
echo $hh->input_table_open();

$votenames = $pl->QuestionVotesWeights($id_poll,$id_question);
foreach($votenames as $votename)
{
	echo $hh->input_text($votename['name'],"question_weight_{$votename['vote']}",is_numeric($votename['weight'])?$votename['weight']:$votename['seq'],5,0,$input_right);
}

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
