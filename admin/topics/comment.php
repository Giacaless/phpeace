<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_item = $_GET['id_item'];
$type = $_GET['type'];
$id_comment = $_GET['id'];
$id_parent = $_GET['id_parent'];

switch($type)
{
	case "article":
		include_once(SERVER_ROOT."/../classes/article.php");
		$a = new Article($id_item);
		$article = $a->ArticleGet();
		$id_topic = $article['id_topic'];
		$t = new Topic($id_topic);
		$title[] = array($t->name,'ops.php?id='.$id_topic);
		$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic);
		$title[] = array($article['headline'],'/articles/article.php?w=topics&id='.$id_item);
	break;
	case "question":
		include_once(SERVER_ROOT."/../classes/polls.php");
		$pl = new Polls();
		$question = $pl->QuestionGet($id_item);
		$id_poll = $question['id_poll'];
		$poll = $pl->PollGet($id_poll);
		$id_topic = $poll['id_topic'];
		$t = new Topic($id_topic);
		$title[] = array($t->name,'ops.php?id='.$id_topic);
		$title[] = array('polls','polls.php?id='.$id_topic);
		$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
		$row = $pl->QuestionGet($id_item);
		$title[] = array($row['question'],'poll_question.php?id='.$id_item.'&id_poll='.$id_poll);
	break;
	case "thread":
		include_once(SERVER_ROOT."/../classes/forum.php");
		$f = new Forum(0);
		$thread = $f->ThreadGet($id_item);
		$f->id = $thread['id_topic_forum'];
		$id_forum = $f->id;
		$forum = $f->ForumGet();
		$id_topic = $forum['id_topic'];
		$t = new Topic($id_topic);
		$title[] = array($t->name,'ops.php?id='.$id_topic);
		$title[] = array('forum','forums.php?id='.$id_topic);
		$title[] = array($forum['name'],'forum.php?id='.$id_forum.'&id_topic='.$id_topic);
		$title[] = array('threads','forum_threads.php?id='.$id_forum.'&id_topic='.$id_topic.'&p='.$current_page);
		$title[] = array($thread['title'],'forum_thread.php?id='.$id_item.'&id_forum='.$id_forum.'&id_topic='.$id_topic);
	break;
}


if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

$title[] = array("comments",'comments_tree.php?type='.$type.'&id_item='.$id_item);

$co = new Comments($type,$id_item);

if ($id_comment>0)
{
	$row = $co->CommentGet($id_comment);
	$approved = $row['approved'];
	$title[] = array($row['title'],'');
	$action2 = "update";
	$co_title = $row['title'];
}
else
{
	if ($t->AmIUser())
		$input_right = 1;
	$pub_user = $ah->session->Get("user");
	$id_p = (int)$pub_user['id'];
	if(!$id_p>0)
	{
		$input_right = 0;
		$ah->MessageSet("comment_add_warning");
	}
	$title[] = array($id_parent>0?"reply":"add_new",'');
	$action2 = "insert";
	$approved = 1;
}
echo $hh->ShowTitle($title);


if($id_parent>0)
{
	$parent = $co->CommentGet($id_parent);
	echo "<p>" . $hh->tr->Translate("reply_to") . ": <i><a href=\"comment.php?id={$parent['id_comment']}&type=$type&id_item=$id_item&id_parent={$parent['id_parent']}\">{$parent['title']}</a></i></p>";
	if(!$id_comment>0)
		$co_title = (substr($parent['title'],0,3)!="Re:")? "Re: {$parent['title']}" : $parent['title'];
}

echo $hh->input_form_open();
echo $hh->input_hidden("id_comment",$id_comment);
echo $hh->input_hidden("id_parent",$id_parent);
echo $hh->input_hidden("id_item",$co->id_item);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("type",$type);
echo $hh->input_hidden("from","comment");
echo $hh->input_table_open();

echo $hh->input_date("date","insert_date",$row['date'],$input_right);
echo $hh->input_time("hour","insert_date",$row['date'],$input_right);
if($row['id_p']>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$user = $pe->UserGetById($row['id_p']);
	echo $hh->input_text("author","author","{$user['name1']} {$user['name2']}",30,0,0);
}

echo $hh->input_text("title","title",$co_title,75,0,$input_right);
echo $hh->input_textarea("content","comment",$row['text'],75,5,"",$input_right);
echo $hh->input_checkbox("approved","approved",$approved,0,$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
$co->LoadTree();
$children = $co->Children($id_comment);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_comment>0 && !count($children)>0);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if($id_comment>0 && $approved)
	echo "<p><a href=\"comment.php?id=0&id_item=$id_item&type=$type&id_parent=$id_comment\">" . $hh->tr->Translate("reply") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>


