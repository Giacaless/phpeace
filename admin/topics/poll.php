<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");

$id_poll = $_GET['id'];
$id_topic = $_GET['id_topic'];

$t = new Topic($id_topic);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);

$pl = new Polls();

if ($id_poll>0)
{
	$row = $pl->PollGet($id_poll);
	$title[] = array($row['title'],'');
	$active = $row['active'];
}
else
{
	$title[] = array('add_new','');
	$active = 1;
}

$profiling_options = $hh->tr->Translate("profiling_options");

if($ini->Get("profiling")=="0")
{
	unset($profiling_options[1]);
	unset($profiling_options[2]);
}
echo $hh->ShowTitle($title);

if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll','');
	if($row['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions","poll_questions.php?id=$id_poll&approved=-1");
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($row['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);

	$url = $ini->Get("pub_web") . "/" . $ini->Get("poll_path") . "/index.php?id=$id_poll&id_topic=$id_topic";
	echo "<p>" . $hh->tr->Translate("link") .": <a href=\"$url\" target=\"_blank\">$url</a></p>";
	
	$row2 = array();
	$num_questions_to_approve = $pl->Questions($row2,$id_poll,1,0,0,true);
	if($num_questions_to_approve>0)
		echo "<p>". $hh->tr->Translate("poll_questions") . ": <a href=\"poll_questions.php?id=$id_poll&approved=0\"><strong>$num_questions_to_approve " . $hh->tr->Translate("to_approve") . "</strong></a></p>";
}

echo $hh->input_form_open();
echo $hh->input_hidden("from","poll");
echo $hh->input_hidden("id_poll",$id_poll);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($id_topic,0,$tt->AllTopics(),"",$input_super_right);

echo $hh->input_date("date_start","start_date",$row['start_date_ts'],$input_right);
echo $hh->input_array("type","id_type",$row['id_type'],$hh->tr->Translate("polls_types"),$input_right);
echo $hh->input_text("title","title",$row['title'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],80,10,"",$input_right);
echo $hh->input_textarea("intro_text","intro",$row['intro'],80,5,"",$input_right);
echo $hh->input_textarea("thanks_text","thanks",$row['thanks'],80,4,"",$input_right);
echo $hh->input_array("open_to","users_type",$row['users_type'],$profiling_options,$input_right);
echo $hh->input_array("sort_by","sort_questions_by",$row['sort_questions_by'],$hh->tr->Translate("poll_sort_by"),$input_right);

echo $hh->input_array("poll_questions_submit","submit_questions",$row['submit_questions'],$profiling_options,$input_right);

if($id_poll>0)
{
	if($row['id_type']=="2" || $row['id_type']=="3")
	{
		$polls_types = $hh->tr->Translate("polls_types");
		echo $hh->input_separator($polls_types[2]);
		for($i=0;$i<=10;$i++)
			$combo_values["".$i.""] = $i;
		echo $hh->input_array("vote_min","vote_min",$row['vote_min'],$combo_values,$input_right);
		echo $hh->input_array("vote_max","vote_max",$row['vote_max'],$combo_values,$input_right);
		echo $hh->input_text("vote_threshold","vote_threshold",$row['vote_threshold'],3,0,$input_right);
	}
	else 
	{
		echo $hh->input_hidden("vote_min",$row['vote_min']);
		echo $hh->input_hidden("vote_max",$row['vote_max']);
		echo $hh->input_hidden("vote_threshold",$row['vote_threshold']);
	}
}

echo $hh->input_separator("comments");
echo $hh->input_array("allow_comments","comments",$row['comments'],$profiling_options,$input_right);
echo $hh->input_checkbox("moderate_comments","moderate_comments",$row['moderate_comments'],0,$input_right);

echo $hh->input_separator("promoter");
echo $hh->input_text("name","promoter",$row['promoter'],50,0,$input_right);
echo $hh->input_text("email","promoter_email",$row['promoter_email'],50,0,$input_right);
echo $hh->input_checkbox("admin_notify","notify_answers",$row['notify_answers'],0,$input_right);

echo $hh->input_separator("administration");
include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
$o->GetKeywords($id_topic,$o->types['topic'], $keywords,0,true);
echo $hh->input_keywords($id_poll,$o->types['poll'],$keywords,$input_right);
echo $hh->input_array("status","active",$active,$hh->tr->Translate("status_options"),$input_right);
echo $hh->input_checkbox("private","is_private",$row['is_private'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_poll>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
