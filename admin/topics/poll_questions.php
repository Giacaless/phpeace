<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/polls.php");

$id_poll = $_GET['id'];

$pl = new Polls();
$poll = $pl->PollGet($id_poll);
$id_topic = $poll['id_topic'];
$approved = isset($_GET['approved'])? $_GET['approved'] : 1;

$t = new Topic($id_topic);

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array('polls','polls.php?id='.$id_topic);
$title[] = array($poll['title'],'poll.php?id='.$id_poll.'&id_topic='.$id_topic);
$title[] = array('poll_questions','');
echo $hh->ShowTitle($title);

if ($module_admin || $t->AmIAdmin())
	$input_right = 1;

if($id_poll>0)
{
	$tabs = array();
	$tabs[] = array('poll',"poll.php?id=$id_poll&id_topic=$id_topic");
	if($poll['id_type']=="4")
		$tabs[] = array("groups","poll_questions_groups.php?id=$id_poll");
	$tabs[] = array("poll_questions",'');
	
	include_once(SERVER_ROOT."/../classes/comments.php");
	$co = new Comments("question",0);
	$co_pending = $co->CommentsPending(0,$t->id,$id_poll);
	if($co_pending>0)
		$tabs[] = array('comments_to_approve',"comments_poll.php?id=$id_poll&approved=0");

	if($poll['id_type']=="4")
		$tabs[] = array('answers',"poll_answers.php?id_poll=$id_poll");
	$tabs[] = array('votes',"poll_question_answers.php?id_poll=$id_poll");
	$tabs[] = array('chart',"poll_chart.php?id=$id_poll");
	if ($input_right)
		$tabs[] = array('mailjob',"poll_mail.php?act=filter&id_item=$id_poll");
	$tabs[] = array('status_changes',"poll_status.php?id=$id_poll");
	echo $hh->Tabs($tabs);
}

$row = array();
$num = $pl->Questions( $row, $id_poll, $poll['sort_questions_by'], $approved, 0, true );

if($approved!="0" && $poll['sort_questions_by']=="1")
{
	$table_headers = array('seq','move','date','question','comments','approved');
	$table_content = array('$row[pos]','{PollQuestionMove($row[pos],'.$num.',$row[id_question])}',
	'{FormatDate($row[insert_date_ts])}','{LinkTitle("poll_question.php?id=$row[id_question]&id_poll='.$id_poll.'",$row[question])}','<div class=\"right\">$row[comments]</div>','{Bool2YN($row[approved])}');
}
else 
{
	$table_headers = array('date','question','comments','approved');
	$table_content = array('{FormatDate($row[insert_date_ts])}','{LinkTitle("poll_question.php?id=$row[id_question]&id_poll='.$id_poll.'",$row[question])}','<div class=\"right\">$row[comments]</div>','{Bool2YN($row[approved])}');
}

if($poll['id_type']=="4")
{
	unset($table_headers[4]);
	unset($table_content[4]);
	if($poll['sort_questions_by']=="1")
	{
		unset($table_headers[1]);
		unset($table_content[1]);	
	}
	$table_headers[] = 'group';
	$table_headers[] = 'weight';
	$table_content[] = '$row[group_name]';
	$table_content[] = '<div class=\"right\">$row[weight]</div>';
}

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"poll_question.php?id=0&id_poll=$id_poll\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

