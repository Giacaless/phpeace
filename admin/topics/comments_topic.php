<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/comments.php");

$id_topic = $_GET['id'];
$appr = $_GET['appr'];

$t = new Topic($id_topic);

$approved = 1;
if (($appr=="0") && ($module_admin || $t->AmIAdmin()))
	$approved = 0;

$approved = 1;
$title1 = "comments_approved";

if ($appr=="0")
{
	$approved = 0;
	$title1 = "comments_to_approve";
}

$title[] = array($t->name,'ops.php?id='.$id_topic);
$title[] = array($title1,'');
echo $hh->ShowTitle($title);

$row = array();
$co = new Comments("article",0);
$num = $co->CommentsApproved( $row, $approved, $id_topic );

$table_headers = array('date','name','article','comment','approved');
$table_content = array('{FormatDate($row[insert_date_ts])}','$row[name]','$row[headline]',
'{LinkTitle("comment.php?type=article&id=$row[id_comment]&id_item=$row[id_item]",$row[title])}',
'{Bool2YN($row[approved])}');

echo $hh->showTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

