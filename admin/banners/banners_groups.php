<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/banners.php");

$title[] = array('banner_groups','');
echo $hh->ShowTitle($title);

$banners = new Banners();
$num = $banners->Groups( $row );

$table_headers = array('group','banners_active','size','mantainer','&nbsp;');
$table_content = array('{LinkTitle("banners.php?id=$row[id_group]",$row[name],0,$row[description])}',
'<div class=\"right\">$row[active] / $row[counter]</div>',
'<div class=\"right\">$row[width]x$row[height]</div>','{UserLookup($row[id_user],false)}',
'{LinkTitle("banners_group.php?id=$row[id_group]","' . $hh->tr->Translate("change") . '")}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_admin)
	echo "<p><a href=\"banners_group.php?id=0\">" . $hh->tr->Translate("banner_group_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
