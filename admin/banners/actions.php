<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];

if ($from=="banners_group")
{
	$action = $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/banners.php");
	$id_group	= (int)$post['id_group'];
	$name	= $post['name'];
	$description	= $post['description'];
	$id_user	= (int)$post['id_user'];
	$width	= $post['width'];
	$height	= $post['height'];
	$b = new Banners;
	if ($action=="store")
	{
		if($id_group>0)
			$b->GroupUpdate($id_group,$name,$width,$height,$description,$id_user);
		else 
			$id_group = $b->GroupInsert($name,$width,$height,$description,$id_user);
	}
	if ($action=="delete")
		$b->GroupDelete($id_group);
	header("Location: banners_groups.php");
}

if ($from=="banner")
{
	$action = $fh->ActionGet($post);
	$id_group	= $post['id_group'];
	$id_banner	= $post['id_banner'];
	$link		= $fh->String2Url($post['link']);
	$alt_text	= $post['alt_text'];
	$start_date	= $post['start_date_y']."-".$post['start_date_m']."-".$post['start_date_d'];
	$can_expire	= $fh->Checkbox2bool($post['can_expire']);
	$expire_date	= $post['expire_date_y']."-".$post['expire_date_m']."-".$post['expire_date_d'];
	$popup	= $fh->Checkbox2bool($post['popup']);
	$file		= $fh->UploadedFile("img",true);
	include_once(SERVER_ROOT."/../classes/banners.php");
	$b = new Banners;
	if ($action=="store")
	{
		if($id_banner>0)
			$b->BannerUpdate($id_banner,$link,$alt_text,$start_date,$can_expire,$expire_date,$id_group,$file,$popup);
		elseif($file['ok'])
			$b->BannerInsert($link,$alt_text,$start_date,$can_expire,$expire_date,$id_group,$file,$popup);
	}
	if ($action=="delete")
		$b->BannerDelete($id_banner,$id_group);
	header("Location: banners.php?id=$id_group");
}

?>
