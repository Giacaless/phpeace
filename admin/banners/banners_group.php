<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id = $_GET['id'];
$title[] = array('banner_groups','banners_groups.php');

if ($id>0)
{
	include_once(SERVER_ROOT."/../classes/banners.php");
	$b = new Banners();
	$row = $b->GroupGet($id);
	$title[] = array($row['name'],'');
	$row2 = array();
	$num_banners = $b->GroupBanners( $row2, $id );
}
else
{
	$title[] = array('banner_group_add','');
}

if ($module_admin || $row['id_user']==$ah->current_user_id)
	$input_right = 1;
if($module_admin)
	$input_super_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form("post","actions.php");
echo $hh->input_hidden("id_group",$id);
echo $hh->input_hidden("from","banners_group");
echo $hh->input_table_open();

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],75,0,"",$input_right);
echo $hh->input_text("width","width",$row['width'],5,0,$input_right);
echo $hh->input_text("height","height",$row['height'],5,0,$input_right);
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users();
$users = $uu->ModuleUsers(24);
echo $hh->input_user("mantainer",$users,$row['id_user'],$input_super_right,"id_user",true);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>($input_right));
if ($id>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_super_right && !$num_banners>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

