<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");
include_once(SERVER_ROOT."/../classes/file.php");

$dodc = new DodContractors();
$trm25 = new Translator($hh->tr->id_language,25);

$title[] = array("download",'');

echo $hh->ShowTitle($title);

if($module_admin)
{
	$ah->SetTimeLimit();
	$fm = new FileManager();
	$categories = array();
	$dodc->Categories($categories);
	$years = array("2011");
	foreach($years as $year)
	{
		$fm->DirCreate("import/dodc_download/$year");
		foreach($categories as $category)
		{
			$filename = "import/dodc_download/$year/cat_{$category['code']}.xml";
			$url_api = "http://www.usaspending.gov/fpds/fpds.php?datype=X&detail=4&maj_agency_cat=97&placeOfPerformanceCountryCode=IT&fiscal_year=$year";
			$url_api .= "&psc_cat={$category['code']}";
			$fm->DownLoad($url_api,$filename);
			$year_dump = new DOMDocument();
			$year_dump->loadXML($fm->TextFileRead($filename));
			$records = $year_dump->getElementsByTagName("record");
			echo "$year: Cat. {$category['code']} - {$category['description']} - <b>" . $records->length . "</b> records<br>";
		}
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>

