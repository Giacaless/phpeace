<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/dodc.php");
include_once(SERVER_ROOT."/../classes/file.php");

$dodc = new DodContractors();
$trm25 = new Translator($hh->tr->id_language,25);

$title[] = array("Database",'database.php');
$title[] = array($trm25->Translate("import"),'');

echo $hh->ShowTitle($title);

if($module_admin)
{
	$fm = new FileManager();
	if($fm->Exists("import/dodc"))
	{
		$ah->SetTimeLimit();
		if($fm->Exists("import/dodc/duns"))
		{
			$num_duns = $dodc->ImportDuns();
			echo "$num_duns DUNS<br>";
		}
		$files = $fm->DirFiles("import/dodc");
		if(count($files)>0)
		{
			sort($files);
			foreach($files as $file)
			{
				$basename = basename($file);
				preg_match('/([0-9]*)([0-9]*)\.txt/i',$basename,$match);
				$year = $match[1];
				if((int)$year>0)
					echo nl2br($dodc->ImportYearCsv($basename,$year));
			}
			for($year=2007;$year<=2011;$year++)
			{
				echo nl2br($dodc->ImportYearXml($year));
				echo nl2br($dodc->ImportYearContractors($year));
			}
			$dodc->ImportFixes();
		}
		else 
			echo "<p>No files available</p>";
	}
	else 
		echo "<p>No files available</p>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>

