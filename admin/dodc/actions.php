<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../modules/dodc.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper(true,25);
$post = $fh->HttpPost();

$from = $post['from'];
$action = $fh->ActionGet($post);

$dodc = new DodContractors();

if ($from=="office_merge")
{
	$id_from	= $post['id_from'];
	$id_to		= $post['id_to'];
	if($id_from>0 && $id_to>0 && $module_admin)
		$dodc->OfficeMerge($id_from,$id_to);
	header("Location: office.php?id=$id_to");
}

?>
