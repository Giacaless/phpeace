<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting_slot = $_GET['id'];
$id_meeting = $_GET['id_meeting'];

$meeting = $me->MeetingGet($id_meeting);

if($id_meeting_slot>0)
	$row = $me->MeetingSlotGet($id_meeting_slot);

if ($module_admin || $me->AmIAdmin($id_meeting,$ah->current_user_id))
	$input_right = 1;

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($meeting['title'],'meeting.php?id='.$id_meeting);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'meeting_slots.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("participants"),'meeting_participants.php?id='.$id_meeting);
echo $hh->Tabs($tabs);

echo $hh->input_form_open();
echo $hh->input_hidden("from","meeting_slot");
echo $hh->input_hidden("id_meeting",$id_meeting);
echo $hh->input_hidden("id_meeting_slot",$id_meeting_slot);
echo $hh->input_table_open();

echo $hh->InputDateEvent("date","start_date",$row['start_date_ts'],$input_right," onChange=\"reconcile_dates()\"",1);
echo $hh->input_time("hour","start_date",$row['start_date_ts'],$input_right);
echo $hh->input_text("event_length","length",$row['length'],5,0,$input_right);
echo $hh->input_text("title","title",$row['title'],70,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],50,5,"",$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_meeting_slot>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();


include_once(SERVER_ROOT."/include/footer.php");
?>
