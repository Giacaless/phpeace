<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/meetings.php");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;

$trm23 = new Translator($hh->tr->id_language,23);

$me = new Meetings();

$id_meeting = $_GET['id'];
$status = isset($_GET['status'])? (int)$_GET['status'] : -1; 
$id_slot = (int)$_GET['id_slot'];

$row = $me->MeetingGet($id_meeting);

if ($module_admin || ($id_meeting>0 && $me->AmIAdmin($id_meeting,$ah->current_user_id)))
	$input_right = 1;

$title[] = array($trm23->Translate("meetings"),'meetings.php');
$title[] = array($row['title'],'meeting.php?id='.$id_meeting);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm23->Translate("meeting"),'meeting.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("slots"),'meeting_slots.php?id='.$id_meeting);
$tabs[] = array($trm23->Translate("participants"),'meeting_participants.php?id='.$id_meeting);
echo $hh->Tabs($tabs);

$statuses = $trm23->Translate("participation");

if($id_slot>0)
{
	$slot = $me->MeetingSlotGet($id_slot);
	echo "<p>" . $trm23->Translate("slot") . ": " . $hh->FormatDateTime($slot['start_date_ts']) . ": ";
	echo $hh->Wrap("<b>{$slot['title']}</b>","<a href=\"meeting_participants.php?id=$id_meeting&id_slot=$id_slot\">","</a>",$status>-1) . "</p>";
	echo "<h3>Status: {$statuses[$status]}</h3>";
	if($status>-1)
	{
		$num = $me->MeetingParticipants($rows,$id_slot,$status);
		
		$table_headers = array('date','name','comments');
		$table_content = array('{FormatDate($row[join_date_ts])}',
		'{LinkTitle("meeting_participant_slot.php?id_p=$row[id_p]&id_meeting_slot='.$id_slot.'",$row[name])}','<em>$row[comments]<em>');
		
		echo $hh->ShowTable($rows, $table_headers, $table_content, $num);
	}
	else 
	{
		echo "<ul>";
		foreach($statuses as $key=>$status)
		{
			echo "<li>";
			$num_p = $me->MeetingParticipants($rows,$id_slot,$key);
			echo "<a href=\"meeting_participants.php?id=$id_meeting&id_slot=$id_slot&status=$key\">$num_p $status</a></li>";
		}
		echo "</ul>";
	}
}
else 
{
	$slots = array();
	$num_slots = $me->MeetingSlots($slots,$id_meeting,false);
	if(count($slots)>0)
	{
		echo "<ul>";
		foreach($slots as $slot)
		{
			echo "<li>";
			echo $hh->FormatDateTime($slot['start_date_ts']) . ": <b>{$slot['title']}</b>";
			echo "<ul>";
			foreach($statuses as $key=>$status)
			{
				echo "<li>";
				$num_p = $me->MeetingParticipants($rows,$slot['id_meeting_slot'],$key);
				echo "<a href=\"meeting_participants.php?id=$id_meeting&id_slot={$slot['id_meeting_slot']}&status=$key\">$num_p $status</a></li>";
			}
			echo "</ul>";
			echo "</li>";
		}
		echo "</ul>";
	}
	if($input_right)
	{
		echo $hh->input_form("get","search.php");
		echo $hh->input_hidden("id_meeting",$id_meeting);
		echo $hh->input_table_open();
		echo $hh->input_separator("search");
		echo $hh->input_text("name","name","","30",0,$input_right);
		echo $hh->input_text("email","email","","30",0,$input_right);
		$statuses = $trm23->Translate("participation");
		$statuses[-1] = $hh->tr->Translate("all_option");
		ksort($statuses);
		echo $hh->input_array("status","status",-1,$statuses,$input_right);
		$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
		echo $hh->input_actions($actions,$input_right);
		echo $hh->input_table_close() . $hh->input_form_close();
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
