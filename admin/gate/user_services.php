<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");
include_once(SERVER_ROOT."/../classes/topics.php");

$u = new User;
$id = $u->id;
$row = $u->UserGet();
$active = $row['active'];
if ($active!=1)
{
	$ah->MessageSet("user_no_active_warn");
}
$title[] = array($row['name'],'user.php');
$title[] = array('services','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/gate/user.php');
if($hh->ini->Get("user_show"))
	$tabs[] = array('user_link','/gate/user_page.php');
$tabs[] = array('image_associated','/gate/user_image.php');
$tabs[] = array('responsibilities','/gate/user_roles.php');
$tabs[] = array('services','');
$tabs[] = array('user_stats','/gate/user_stats.php');
echo $hh->Tabs($tabs);

$num = $u->Services($rows);

$table_headers = array('name','mobile');
$table_content = array('{LinkTitle("user_service.php?id=$row[id_user_service]",$row[service_name])}','{Bool2YN($row[mobile])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if($ah->ModuleAdmin(1))
	echo "<p><a href=\"/users/user_service.php?id={$u->id}&id_user_service=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>


