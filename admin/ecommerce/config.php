<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");
$trm15 = new Translator($hh->tr->id_language,15);
$ec = new eCommerce();

if ($module_admin)
	$input_right=1;

$title[] = array('configuration','');
echo $hh->ShowTitle($title);

if($ec->store_payments)
{
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","config_update");
	echo $hh->input_table_open();
	include_once(SERVER_ROOT."/../classes/payment.php");
	$p = new Payment();
	$types = array();
	$p->Types($types,$id_topic,false);
	echo $hh->input_row($trm15->Translate("payment_type"),"id_payment_type",$ec->id_payment_type,$types,"choose_option",0,$input_right);

	$accounts = $p->AccountsByType($ec->pgw=="paypal"?3:0);
	echo $hh->input_row("account","id_account",$ec->id_account,$accounts,"choose_option",0,$input_right);
	
	echo $hh->input_submit("submit","",$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();	
}

include_once(SERVER_ROOT."/include/footer.php");
?>

