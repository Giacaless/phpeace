<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
    define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id = $_GET['id'];
$name = $_GET['name'];
$w = $_GET['w'];

switch($w)
{
    case 'p':
        $title[] = array('list','products.php');
        $title[] = array('Product','product.php?id='.$id);
        break;
    case 'o':
        $title[] = array('list','orders.php');
        $title[] = array('Order','order.php?id='.$id);
        break;
}
$title[] = array("delete",'');

if($module_right)
    $input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("id",$id);
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("from","confirm_delete");

switch($w)
{
    case 'p':
        echo "<p>Delete product \"" . $name . "\"</p>";
        break;
    case 'o':
        echo "<p>Delete order \"#" . $id . "\"</p>";
        break;
}

echo $hh->input_table_open();
echo $hh->input_separator("are_you_sure");
$actions = array();
$actions[] = array('action'=>"delete_ok",'label'=>"yes",'right'=>$input_right);
$actions[] = array('action'=>"delete_no",'label'=>"no",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


