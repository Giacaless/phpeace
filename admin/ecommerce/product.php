<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
$tinymce = true;
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

$trm27 = new Translator($hh->tr->id_language,27);
$ec = new eCommerce();

$id_product = $_GET['id'];
$error = isset($_GET['error']);

$title[] = array($trm27->Translate("products"),'products.php');
if($id_product>0)
{
	$row = $ec->ProductGet($id_product);
	$title[] = array($row['name'],'');
}
else 
{
	$title[] = array("add_new",'');
}

if($module_right)
	$input_right = 1;
	
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array($trm27->Translate("product"),'');
$tabs[] = array("image",$id_product>0?'product_image.php?id='.$id_product:"");
echo $hh->Tabs($tabs);


echo $hh->input_form_open();
echo $hh->input_hidden("from","product");
echo $hh->input_hidden("id_product",$id_product);
echo $hh->input_table_open();

if($error)
    echo $hh->input_note('No product info entered',true);

if($id_product>0)
{
	
	echo $hh->input_note("ID: $id_product");
	include_once(SERVER_ROOT."/../classes/file.php");
	$fm = new FileManager();
	include_once(SERVER_ROOT."/../classes/images.php");
	$i = new Images();
	$filename = "products/0/$id_product".".".$i->convert_format;
	if($fm->Exists("uploads/$filename"))
		echo "<tr><td align=\"right\">&nbsp;</td><td><a href=\"product_image.php?id=$id_product\"><img src=\"/images/upload.php?src=$filename\" width=\"{$i->img_sizes[$ec->product_thumb]}\"></a></td></tr>\n";
	
}
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
$enter = "
";
$row['description'] = stripcslashes(str_replace('\r\n', $enter, $row['description']));
echo $hh->input_textarea("description","description",$row['description'],50,5,"",$input_right);
echo $hh->input_text("price","price",$row['price'],10,0,$input_right,$ec->currency);
echo $hh->input_checkbox("In stock","stock",$row['stock'],0,$input_right);
echo $hh->input_checkbox("active","active",$row['active'],0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$module_admin && $id_product>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


