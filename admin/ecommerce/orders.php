<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

$id_status = $get['id_status'];
$start_date = $get['start_date'];
$end_date = $get['end_date'];

$trm27 = new Translator($hh->tr->id_language,27);
$status_list = array_merge($trm27->Translate("order_status"),array("All"));

if ($id_status == '')
    $id_status = count($status_list) - 1;

$ec = new eCommerce();

$title[] = array($trm27->Translate("orders"),'');

echo $hh->ShowTitle($title);

if ($id_status == count($status_list) - 1)
    $num = $ec->Orders($row,-1,array('start_date'=>$start_date, 'end_date'=>$end_date));
else
    $num = $ec->Orders($row,$id_status,array('start_date'=>$start_date, 'end_date'=>$end_date));

//$table_headers = array('date','name','status','payment date');
//$table_content = array('{FormatDate($row[order_date_ts])}','{LinkTitle("order.php?id=$row[id_order]","#".$row[id_order])}','{OrderStatus($row[id_status])}','{FormatDate($row[payment_date_ts])}');
$table_headers = array('Order Date','name','status');
$table_content = array('{FormatDate($row[order_date_ts])}','{LinkTitle("order.php?id=$row[id_order]","#".$row[id_order])}','{OrderStatus($row[id_status])}');
if ($id_status > 0)
{
    $table_headers[] = 'payment date';
    $table_content[] = '{FormatDate($row[payment_date_ts])}';    
}

$input_right = 1;
echo $hh->input_form_open();
echo $hh->input_hidden("from","orders");
echo $hh->input_array("Status","status",$id_status, $status_list,$input_right);
echo $hh->input_date("Start Date","start_date",strtotime($get['start_date']),$input_right,true);
echo $hh->input_date("End Date","end_date",strtotime($get['end_date']),$input_right,true);
$actions = array();
$actions[] = array('action'=>"list",'label'=>"list",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
$hh->input_form_close(); 

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

