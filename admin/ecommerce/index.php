<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

echo $hh->ShowTitle($title);

$trm27 = new Translator($hh->tr->id_language,27);
$ec = new eCommerce();

$input_right = $module_right;

echo "<div class=\"box2c\">";
echo "<h2>" . $trm27->Translate("products") . "</h2>\n";
echo "<ul>";
echo "<li><a href=\"products.php\">" . $hh->tr->Translate("list") . "</a></li>\n";
echo "<li><a href=\"product.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>";
echo "<h3>" . "Product Search" . "</h3>";
echo "<ul>";
echo $hh->input_form("get","search.php","");
echo $hh->input_table_open();
echo $hh->input_text("name","name","","28",0,$input_right);
echo $hh->input_array("In Stock","stock",1,$hh->tr->Translate("any_yes_no"),1);
echo $hh->input_checkbox("active_only","active",1,0,$input_right);
$actions[] = array('action'=>"search",'label'=>"search",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();
echo "</ul>";
echo "</div>";

echo "<div class=\"box\">";
echo "<h2>" . $trm27->Translate("orders") . "</h2>\n";
echo "<ul>";
$statuses = $trm27->Translate("order_status");
$total_row = 0;
foreach($statuses as $id_status=>$status)
{
	$rows = array();
	$num = $ec->Orders($rows,$id_status);
    $total_row += $num;
	echo "<li>" . $hh->Wrap($status,"<a href=\"orders.php?id_status=$id_status\">","</a>",$num>0) . " ($num)</li>\n";
}
$status = count($statuses);
echo "<li>" . $hh->Wrap('All',"<a href=\"orders.php?id_status=$status\">","</a>",$total_row>0) . " ($total_row)</li>\n";
echo "<li><a href=\"order.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
echo "</ul>";
echo "</div>";

if ($module_admin && $ec->store_payments)
{
	echo "<div class=\"box\">";
	echo "<h2>" . $hh->tr->Translate("management") . "</h2>";
	echo "<ul>";
	echo "<li><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></li>\n";
	echo "</ul>\n";
	echo "</div>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
