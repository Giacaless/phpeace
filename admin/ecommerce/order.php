<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

$trm27 = new Translator($hh->tr->id_language,27);
$ec = new eCommerce();

$id_order = $_GET['id'];

$title[] = array($trm27->Translate("orders"),'orders.php');
if($id_order>0)
{
	$row = $ec->OrderGet($id_order,true);
	$title[] = array($trm27->Translate("order") . ' #' . $id_order,'');
	$products = $ec->OrderProducts($id_order);
	$num_products = count($products);
}
else 
{
	$title[] = array("add_new",'');
}

if($module_admin)
	$input_right = 1;
	
echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","order");
echo $hh->input_hidden("id_order",$id_order);
echo $hh->input_table_open();

if($id_order>0)
{
	if(!$row['id_p']>0)
		echo $hh->input_note($trm27->Translate("order_user_missing"),true);
	if($num_products==0)
		echo $hh->input_note($trm27->Translate("order_products_missing"),true);
	echo $hh->input_text("#","number",$id_order,50,0,0);
	if($row['id_transaction']!="")
		echo $hh->input_text("Transaction ID","id_transaction",$row['id_transaction'],50,0,0);
}
else 
	echo $hh->input_note($trm27->Translate("order_create"));

echo $hh->input_date("Order Date","order_date",$row['order_date_ts'],$id_order==0 && $input_right);
echo $hh->input_array("status","id_status",$row['id_status'],$trm27->Translate("order_status"),$id_order>0 && $input_right);
if ($id_order>0)
    echo $hh->input_date("Payment Date","payment_date",$row['payment_date_ts'],$input_right,true);   
echo $hh->input_textarea($trm27->Translate("delivery_notes"),"delivery_notes",$row['delivery_notes'],50,2,"",$input_right);

$pub_username = "----";
if($row['id_p']>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$pub_user = $pe->UserGetById($row['id_p']);
	$pub_username = "{$pub_user['name1']} {$pub_user['name2']}";
}

echo $hh->input_text("pub_user","pub_user",$pub_username,20,0,0,($id_order>0)? " (<a href=\"order_user.php?id_order=$id_order&id_p={$row['id_p']}\">" . $hh->tr->Translate($row['id_p']>0?"details":"change") . "</a>)" : "");
	
$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$module_admin && $id_order>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id_order>0)
{
	echo "<h3>" . $trm27->Translate("products") . "</h3>";
	echo "<ul>";
	foreach($products as $product)
	{
		echo "<li><a href=\"order_product.php?id_order=$id_order&id_product={$product['id_product']}\">{$product['quantity']} x {$product['name']}</a></li>\n";
	}
	if($num_products<$ec->products_per_order || $ec->products_per_order==0)
	{
		echo "<li><a href=\"order_product.php?id_order=$id_order&id_product=0\">" . $hh->tr->Translate("add_new") . "</a></li>\n";
	}
	echo "</ul>";
	
	echo "<h3>" . $trm27->Translate("total") . ": " . number_format($row['total'],2) . "</h3>";
}
include_once(SERVER_ROOT."/include/footer.php");
?>


