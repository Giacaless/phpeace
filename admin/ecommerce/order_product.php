<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

$trm27 = new Translator($hh->tr->id_language,27);
$ec = new eCommerce();

$id_product = (int)$_GET['id_product'];
$id_order = (int)$_GET['id_order'];

$title[] = array($trm27->Translate("orders"),'orders.php');
$order = $ec->OrderGet($id_order);
$title[] = array($trm27->Translate("order") . ' #' . $id_order,'order.php?id='.$id_order);


$title[] = array($trm27->Translate("products"),'');
echo $hh->ShowTitle($title);

if($id_product>0)
{
	$input_right = $module_admin;
	$row = $ec->OrderProductGet($id_order,$id_product);
	$product = $ec->ProductGet($id_product);
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","order_product");
	echo $hh->input_hidden("id_order",$id_order);
	echo $hh->input_hidden("id_product",$id_product);
	echo $hh->input_table_open();
	echo $hh->input_text("name","name","<a href=\"product.php?id=$id_product\">{$product['name']}</a>",50,0,0);
	echo $hh->input_money($trm27->Translate("price_original"),"price_original",$product['price'],10,0,0);
	$price = $row['price']>0? $row['price'] : $product['price'];
	echo $hh->input_money("price","price",$price,10,0,$input_right);
	if($ec->products_per_order>0)
	{
		echo $hh->input_text($trm27->Translate("quantity"),"quantity",$ec->products_per_order,3,0,0);
	}
	else 
	{
		echo $hh->input_text($trm27->Translate("quantity"),"quantity",$row['id_product']>0?$row['quantity']:1,3,0,$input_right);	
	}
	$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $row['id_product']>0);
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
}
else
{
	$num = $ec->ProductsAvailable($id_order,$row);
	
	$table_headers = array('name','price','stock');
	$table_content = array('{LinkTitle("order_product.php?id_order='.$id_order.'&id_product=$row[id_product]",$row[name])}','<div class=right>$row[price]</div>','{Bool2YN($row[stock])}');
	
	echo $hh->ShowTable($row, $table_headers, $table_content, $num);
}

include_once(SERVER_ROOT."/include/footer.php");
?>


