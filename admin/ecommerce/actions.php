<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/ecommerce.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper;
$post = $fh->HttpPost();
$get = $fh->HttpGet();

$from = $post['from'];
$from2 = $get['from2'];

$ec = new eCommerce();

if ($from=="product")
{
	$action = $fh->ActionGet($post);
	$id_product 	= $post['id_product'];
	$name 			= $post['name'];
	$description	= strip_tags(html_entity_decode($post['description']));;
	$price 			= $fh->String2Number($post['price']);
	$stock	 		= $fh->Checkbox2bool($post['stock']);
	$active 		= $fh->Checkbox2bool($post['active']);
	if ($action=="delete")
	{
        $url = "confirm_delete.php?id=$id_product&name=$name&w=p";
//		$ec->ProductDelete( $id_product );
//		$url = "products.php";
	}
	if ($action=="update")
	{
		if ($name != '')
        {
            $id_product_new = $ec->ProductStore( $id_product,$name,$description,$price,$stock,$active );
            $url = $id_product>0? "products.php":"product.php?id=$id_product_new";
        }
        else
            $url = "product.php?error";
	}
	header("Location: $url");
}

if ($from=="confirm_delete")
{
    $action = $fh->ActionGet($post);
    $id = $post['id'];
    $w = $post['w'];
    if ($action == "delete_ok")
    {
        $ec = new eCommerce();
        switch ($w)
        {
            case 'p' :
                $ec->ProductDelete( $id );
                $url = "products.php";
                break;
            case 'o' :
                $ec->OrderDelete( $id );
                $url = "orders.php";
                break;
        }
    }
    else
    {
        switch ($w)
        {
            case 'p' :
                $url = "product.php?id=$id";
                break;
            case 'o' :
                $url = "order.php?id=$id";
                break;
        }
        
    }
    header("Location: " . $url);
}

if ($from=="product_image")
{
	$id_product 	= $post['id_product'];
	$file		= $fh->UploadedFile("img",true);
	$action = $fh->ActionGet($post);
	if ($action=="store" && $file['ok'])
		$ec->ProductImageUpdate($id_product,$file);
	if ($action=="delete")
		$ec->ProductImageDelete($id_product);
	header("Location: product.php?id=$id_product");
}

if ($from=="order")
{
	$action = $fh->ActionGet($post);
	$id_order 	= $post['id_order'];
	$order_date	= $fh->DateMerge("order_date",$post);
    $payment_date    = $fh->DateMerge("payment_date",$post);   
	$delivery_notes	= $post['delivery_notes'];
	$id_status 		= $fh->String2Number($post['id_status']);
	if ($action=="delete")
	{
        $url = "confirm_delete.php?id=$id_order&w=o";
//		$ec->OrderDelete( $id_order );
//		$url = "orders.php";
	}
	if ($action=="update")
	{
		$id_order_new = $ec->OrderStore( $id_order,$order_date,$id_status,$delivery_notes,$payment_date );
		$url = $id_order>0? "orders.php?id_status=$id_status":"order.php?id=$id_order_new";
	}
	header("Location: $url");
}

if ($from=="orders")
{
    $status = $post['status'];   
    $start_date = $fh->DateMerge("start_date",$post);
    $end_date = $fh->DateMerge("end_date",$post);
    $url = "orders.php?id_status=$status"; 
    if ($start_date != '')
        $url .= "&start_date=$start_date";
    if ($end_date != '')
        $url .= "&end_date=$end_date";
    header("Location: $url");
}

if ($from=="order_product")
{
	$action = $fh->ActionGet($post);
	$id_order 	= $post['id_order'];
	$id_product 	= $post['id_product'];
	$quantity 		= $fh->String2Number($post['quantity']);
	$price 			= $fh->String2Number($post['price']);
	if ($action=="delete")
	{
		$ec->OrderProductDelete( $id_order,$id_product );
	}
	if ($action=="update")
	{
		$ec->OrderProductStore($id_order,$id_product,$quantity,$price);
	}
	header("Location: order.php?id=$id_order");
}

if ($from2=="order_user" && $module_admin)
{
	$id_order 	= $get['id_order'];
	$id_p 	= $get['id_p'];
	if($id_order>0 && $id_p>0)
	{
		$ec->OrderAssociate($id_order,$id_p);
	}
	header("Location: order.php?id=$id_order");
}

if ($from=="config_update")
{
	$id_payment_type	= $post['id_payment_type'];
	$id_account			= $post['id_account'];
	$ec->ConfigurationUpdate($id_payment_type,$id_account);
	header("Location: index.php");
}

?>
