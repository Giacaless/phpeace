<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array($hh->tr->Translate("messages_sent"),'');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;
$num = $u->MessagesSent( $row );

$table_headers = array($hh->tr->Translate("date"),$hh->tr->Translate("recipients"),$hh->tr->Translate("subject"),'&nbsp;');
$table_content = array('{FormatDateTime($row[sent_ts])}','{UserLookup($row[id_user])}',
'{LinkTitle("message.php?id=$row[id_message]&from=o",$row[subject])}',
'{LinkTitle("actions.php?action3=delete&id=$row[id_message]&from=o",' . $hh->tr->Translate("delete") . ')}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
