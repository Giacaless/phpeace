<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "</p>" . $hh->tr->Translate("simulation_no_msg") . "</p>\n";
else
{
	echo "<p><a href=\"inbox.php\">" . $hh->tr->Translate("messages_received") . "</a>\n";

	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User;

	$new_messages = $u->NewMessages();
	if ( $new_messages>0 )
		echo " <i>(" . $hh->tr->TranslateParams("messages_new",array($new_messages)) . ")</i>\n";

	echo "</p>";

	echo "<p><a href=\"outbox.php\">" . $hh->tr->Translate("messages_sent") . "</a></p>\n";

	echo "<p><a href=\"message.php?id=0\">" . $hh->tr->Translate("message_write") . "</a></p>\n";

	$expire = $hh->ini->GetModule("mail","messages_expire",30);
	if($expire>0)
		echo "<p>" . $hh->tr->TranslateParams("messages_note",array($expire)) . "</p>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
