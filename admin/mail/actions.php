<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/messaging.php");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();
$unescaped_post = $fh->HttpPost(false,false);

$action2	= $post['action2'];
$action3	= $_GET['action3'];
$from		= $_GET['from'];

$me = new Messaging();

if ($action2=="send")
{
	$subject	= $unescaped_post['subject'];
	$body		= $unescaped_post['body'];
	$id_user	= $post['id_user'];
	$id_module	= $post['id_module'];
	$urgent 	= $fh->Checkbox2bool($post['urgent']);
	$me->MessageSend($ah->current_user_id,$id_user,$subject,$body,$urgent,true);
	header("Location: index.php");
}

if ($action3=="delete")
{
	$deleted = 0;
	$id_message	= $_GET['id'];
	if ($from=="i")
	{
		$me->DeleteFromInbox($id_message);
		$goto = "inbox.php";
	}
	if ($from=="o")
	{
		$me->DeleteFromOutbox($id_message);
		$goto = "outbox.php";
		$deleted = 1;
	}
	if ($deleted==0)
		$deleted = $me->IsMessageDeleted($id_message);
	$messages = $me->Messages($id_message);
	if ($messages==0 && $deleted==1)
		$me->DeleteMessages($id_message);
	header("Location: $goto");
}

?>
