<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/users.php");
include_once(SERVER_ROOT."/../classes/messaging.php");
$me = new Messaging();
$hhf = new HHFunctions();


$id = $get['id'];
$from = $get['from'];

if ($id>0 && $from!="r")
{
	$row = $me->MessageGet($id);
	$msg_subject = $row['subject'];
	$msg_body = $row['body'];
	$msg_user_id = $row['id_user'];
	$msg_module_id = $row['id_module'];
	$msg_sender_name = $row['id_sender']>0? $row['name'] : $hhf->UserLookup(0);
	$msg_sender_id = $row['id_sender'];
	if ((!$row['date_read']>0) && $row[id_user]==$ah->current_user_id)
		$me->SetRead($id);
	if ($from=="o")
		$title[] = array($hh->tr->Translate("messages_sent"),'outbox.php');
	if ($from=="i" || $from=="r")
		$title[] = array($hh->tr->Translate("messages_received"),'inbox.php');
	$title[] = array($msg_subject,'');
	$input_right = 0;
}
else
{
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User;
	$user = $u->UserGet();
	$msg_sender_name = $user['name'];
	$msg_sender_id = $u->id;
	$msg_user_id = $get['tou'];
	$msg_module_id = $get['tom'];
	$msg_subject = $get['subject'];
	$action2 = "send";
	$title[] = array($hh->tr->Translate("message_write"),'');
	$input_right = 1;
	if ($from=="r")
	{
		$row = $me->MessageGet($id);
		$row = $me->MessageGet($id);
		$msg_subject = $me->ReplySubject($row['subject']);
		$msg_body = $hh->tr->TranslateParams("you_wrote",array($hh->FormatDate($row['sent']))) . ":\n> " . str_replace("\n","\n> ",wordwrap($row['body'], 70)) . "\n\n";
	}
}

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0 || ($msg_user_id!=$ah->current_user_id && $msg_sender_id!=$ah->current_user_id))
	echo "<p>" . $hh->tr->Translate("user_no_auth") . "</p>\n";
else
{
?>
<form name="form1" action="actions.php" method="post">
<input type="hidden" name="action2" value="<?=$action2;?>">
<table border=0 cellpadding=2 cellspacing=2>
<?php
echo $hh->input_date("date","sent",$row['sent'],0);
echo $hh->input_time("hour","sent",$row['sent'],0);
echo $hh->input_text("sender","sender",$msg_sender_name,50,0,0);
$users = new Users();
echo $hh->input_row("to_whom","id_user",$msg_user_id,$users->Active(),"choose_option",0,$input_right);
echo $hh->input_text("subject","subject",$msg_subject,50,0,$input_right);
echo $hh->input_textarea("text","body",$msg_body,70,20,"",$input_right,"",$row['id_sender']>0 && $id>0 && $from!="r");

if ($input_right==1)
{
	echo "<tr><td>&nbsp;</td><td><table border=0 cellpadding=0 cellspacing=0 width=\"100%\"><tr><td valign=top>\n";
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") . "\" onClick=\"Send()\">";
	echo "&nbsp;<input type=reset value=\"" . $hh->tr->Translate("cancel") . "\">\n";
	echo "</td>	<td align=\"right\"><input type=\"checkbox\" class=\"input-checkbox\" name=\"urgent\" onClick=\"SetUrgent()\"> " . $hh->tr->Translate("urgent") . "</td></tr>\n";
	echo "</table></td></tr>\n";
}
else
{
	if ($from!="o" && $msg_sender_id>0)
		echo "<tr><td colspan=\"2\"><a href=\"message.php?tou=$msg_sender_id&id=$id&from=r\">" . $hh->tr->Translate("reply") . "</a></td></tr>\n";
	echo "<tr><td colspan=\"2\"><a href=\"actions.php?action3=delete&id=$id&from=$from\">" . $hh->tr->Translate("delete") . "</a>";
	echo "</td></tr>\n";
}
?>
</table>
</form>
<script type="text/javascript">
	f = document.forms['form1'];

	function SetUrgent()
	{
		if (f.urgent.checked)
			alert("<?=$hh->tr->Translate("message_urgent");?>");
	}

	function Send()
	{
		allright = true;
		strAlert = "";
		//if (f.id_user.selectedIndex==0 && f.id_module.selectedIndex==0)
		if (f.id_user.selectedIndex==0)
		{
			allright = false;
			strAlert += "<?=$hh->tr->Translate("message_v_to");?>\n";
		}
		if (f.subject.value=="")
		{
			allright = false;
			strAlert += "<?=$hh->tr->Translate("message_v_subject");?>\n";
		}
		if (f.body.value=="")
		{
			allright = false;
			strAlert += "<?=$hh->tr->Translate("message_v_body");?>\n";
		}
		if (allright)
			f.submit();
		else
			alert(strAlert);
	}
</script>
<?php
if($from=="r")
	echo "<script type=\"text/javascript\">\ndocument.form1.body.focus()\n</script\n";
}
include_once(SERVER_ROOT."/include/footer.php");
?>
