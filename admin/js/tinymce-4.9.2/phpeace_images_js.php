<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);

header("Content-type: text/javascript; charset=UTF-8");

include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);

$conf = new Configuration();

$id_article = (int)$_GET['id'];
if($id_article>0)
{
	include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($id_article);
	$images = $a->ImageGetAll();
	if(count($images)>0)
	{
		$convert_format = $conf->Get("convert_format");
		
		echo "var tinyMCEImageList = new Array(\n";
		$counter = 0;
		foreach($images as $image)
		{
			$size = $image['size']=="-1"? "orig":$image['size'];
			$format = $image['size']=="-1"? $image['format'] : $convert_format;
			echo "[\"{$image['caption']}\",\"/images/upload.php?src=images/{$size}/{$image['id_image']}.{$format}\"]";
			$counter++;
			if($counter!=count($images))
				echo ",";
		}
		echo ");\n";
	}
}
?>
