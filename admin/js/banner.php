<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth();

include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$id_banner = (int)$get['id'];
$id_group = (int)$get['id_g'];
$async = (int)$get['a'];
$id_div = isset($get['div'])? $get['div'] : "";
$json = isset($get['json']);

include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout(false,false,false);
echo $l->BannerWrapper($id_banner,$id_group,$async=="1",$id_div,$json);
?>
