<?php
/********************************************************************
  
   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/layout.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$fh = new FormHelper(false,0,false);
$get = $fh->HttpGet();

$l = new Layout(false,false,false);
$async = (int)$get['a'];
$id_div = isset($get['div'])? $get['div'] : "";

$params = array();

$type = "random_item";
$params['subtype'] = "book";

if ((int)$get['id_p']>0)
	$params['id_publisher'] = (int)$get['id_p'];

if ((int)$get['id_c']>0)
	$params['id_category'] = (int)$get['id_c'];

if ((int)$get['id_k']>0)
	$params['id_keyword'] = (int)$get['id_k'];

if ((int)$get['limit']>0)
	$params['limit'] = (int)$get['limit'];

if ($get['size']>0)
	$params['subtype'] = "book_big";

$html = $l->Output($type,0,0,1,$params);

print $l->Javascript($html,true,$async=="1",$id_div);
?>
