<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/books.php");

$title[] = array('homepage_content','');
echo $hh->ShowTitle($title);

$bb = new Books();
$bconfig = $bb->TopicConfig(0);
$books_home_type = $bconfig['books_home_type'];

$trm16 = new Translator($hh->tr->id_language,16);

$row = array();
switch($books_home_type)
{
	case "0":
		echo $trm16->Translate("books_home_publishers");
	break;
	case "1":
		$num = $bb->HomepageContent( $row, 0, $books_home_type );
		$table_headers = array($trm16->Translate("publisher"),'books','');
		$table_content = array('$row[name]','<div class=\"right\">$row[counter]</div>','{LinkTitle("actions.php?from2=homepage&hometype='.$books_home_type.'&action3=delete&id=$row[id_item]","'.$hh->tr->Translate("remove").'")}');
		echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	break;
	case "2":
		$num = $bb->HomepageContent( $row, 0, $books_home_type );
		$table_headers = array('author','title',$trm16->Translate("publisher"),'');
		$table_content = array('$row[author]','{LinkTitle("book.php?id=$row[id_item]",$row[title])}','$row[name]',
		'{LinkTitle("actions.php?from2=homepage&hometype='.$books_home_type.'&action3=delete&id=$row[id_item]","'.$hh->tr->Translate("remove").'")}');
		echo $hh->ShowTable($row, $table_headers, $table_content, $num);
	break;
}

if ($module_admin && $books_home_type>0)
	echo "<p><a href=\"homepage_add.php\">" . $hh->tr->Translate("change_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

