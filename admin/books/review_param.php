<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/resources.php");

$trm16 = new Translator($hh->tr->id_language,16);

$title[] = array($trm16->Translate("reviews_configuration"),'reviews_config.php');

$id_param = $_GET['id'];

$r = new Resources();

if ($id_param>0)
{
	$row = $r->Param($id_param);
	$title[] = array($row['label'],'');
	$public = $row['public'];
}
else
{
	$title[] = array('add_new','');
	$public = 1;
}

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

$types = $r->ParamsTypes();

echo $hh->input_form_open();
echo $hh->input_hidden("from","review_param");
echo $hh->input_hidden("id_param",$id_param);
echo $hh->input_table_open();
echo $hh->input_text("name","label",$row['label'],20,0,$input_right);
$t_types = array();
foreach($types as $key=>$type)
	$t_types[$key] = $hh->tr->Translate("paramtype_" . $type);

echo $hh->input_array("type","type",array_search($row['type'],$types),$t_types,$input_right);
if($row['type']=="dropdown" || $row['type']=="mchoice")
{
	if($row['params']=="")
		echo $hh->input_note("select_insert");
	echo $hh->input_text("parameters","param_params",$row['params'],20,0,$input_right);
}

echo $hh->input_checkbox("public","public",$public,0,$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_param>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

