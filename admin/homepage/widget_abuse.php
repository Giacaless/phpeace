<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_widget_abuse = (int)$get['id'];

if ($module_admin)
	$input_right = 1;

if($id_widget_abuse>0)
{
	$row = $wi->WidgetAbuseGet($id_widget_abuse);
    $id_p = $row['id_p'];
    $id_widget = $row['id_widget'];
    $status = $row['status'];
    $email = $row['email'];
}

$title[] = array("list widget abuses",'widget_abuses.php');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","widget_abuse");
echo $hh->input_hidden("id_widget_abuse",$id_widget_abuse);
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_hidden("email",$email);  

echo $hh->input_table_open();


if ($id_widget_abuse > 0)
{
    // update
    $url = "widget.php?id=$id_widget";
    echo "<p><a href=\"$url\">Go to widget</a></p>";
    
    echo $hh->input_text("Widget Id","id_widget",$row['id_widget'],20,0,0);
    echo $hh->input_text("Widget","widget",$row['title'],20,0,0);  
    echo $hh->input_text("Reported by","people",$row['name'],20,0,0);    
    echo $hh->input_textarea("Comments","comments",$row['comments'],50,5,"",$input_right);
    echo $hh->input_array("Status","status",$status,array("Pending", "Processed"),$input_right);    
    echo $hh->input_text("Date","insert_date",$row['insert_date'],20,0,0);    

}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $id_tab>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
