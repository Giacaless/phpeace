<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_tab = (int)$get['id_tab'];
$col = (int)$get['col'];
$pos = (int)$get['pos'];
$id_p = 0; // default widget
$fail = $get['fail'];
$id_action = (int)$get['id_action'];
$id_widget = (int)$get['id_widget'];

if ($module_admin)
	$input_right = 1;

if($id_tab>0)
{
    $tab = $wi->WidgetsTabGet($id_p, $id_tab);
	$row = $wi->WidgetsTabPositionGet($id_p, $id_tab, $col, $pos);
    
    if ($id_action == 1)
   	 	$id_widget = $row['id_widget'];
}

$title[] = array("list tabs",'widgets_tabs.php');
$title[] = array("$tab[tab_name]", "widgets_tabs_position.php?id_tab=$tab[id_tab]");
$title[] = array("Tab widget", "");
echo $hh->ShowTitle($title);

$tab_sequences = array(1=>"1", 2=>"2", 3=>"3", 4=>"4", 5=>"5", 6=>"6", 7=>"7", 8=>"8", 9=>"9", 10=>"10");
$columns = array(1=>"1", 2=>"2", 3=>"3", 4=>"4", 5=>"5");
$positions = array(1=>"1", 2=>"2", 3=>"3", 4=>"4", 5=>"5");

echo $hh->input_form_open();
echo $hh->input_hidden("from","widgets_tabs_position");
echo $hh->input_hidden("id_p",$id_p);
echo $hh->input_table_open();

if ($fail == 'duplicate')
    echo $hh->input_note("<p>Duplicate tab position</p>", TRUE); 
else if ($fail == 'widget')
    echo $hh->input_note("<p>Widget already exists in homepage</p>", TRUE); 

$wi->WidgetsApproved($rows_widget, false);
$widgets = array();
foreach ($rows_widget as $row_widget)
{
    $widgets[$row_widget['id_widget']] = $row_widget['title'];    
}

$wi->WidgetsTabsAll($rows_tab,$id_p);
$tabs = array();
foreach ($rows_tab as $row_tab)
{
    $tabs[$row_tab['id_tab']] = $row_tab['tab_name'];
}    

if ($id_action == 1)
{
    // update
    echo $hh->input_hidden("id_action", 1);
    echo $hh->input_array("Widget","id_widget",$id_widget,$widgets,$input_right,"",10);    
    echo $hh->input_array("Tab","id_tab",$id_tab,$tabs,0);    
    echo $hh->input_array("Column","col",$col,$columns,0);
    echo $hh->input_array("Position","pos",$pos,$positions,0);
}
    
else
{
    // add
    echo $hh->input_hidden("id_action", 0);
    echo $hh->input_array("Widget","id_widget",$id_widget,$widgets,$input_right,"",10);    
    echo $hh->input_array("Tab","id_tab",$id_tab,$tabs,0);    
    echo $hh->input_array("Column","col",$col,$columns,$input_right);
    echo $hh->input_array("Position","pos",$pos,$positions,$input_right);

}

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $col>0 && $pos>0);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
