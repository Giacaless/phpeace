<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$status = $get['status'];  
if(!isset($status))
    $status = 2;
$statuses = array("Failed", "Success"); 
$status_list = array_merge($statuses,array("All"));  
$wf = new WebFeeds();

$title[] = array("web feeds");

echo $hh->ShowTitle($title);

$num = $wf->WebFeedsSearchCriteria($rows,true,$status);

$table_headers = array('title', 'url', 'update interval (minutes)',  'feed items', 'last updated', 'status');
$table_content = array('{LinkTitle("web_feed.php?id=$row[id_web_feed]",$row[title])}', '$row[feed_url]', '$row[update_interval]', '$row[num_web_feed_items]', '$row[last_updated]', '{WebFeedLogStatusLookup($row[status])}');

$input_right = 1;  
echo $hh->input_form_open();
echo $hh->input_hidden("from","web_feeds");
echo $hh->input_array("Status","status",$status, $status_list,$input_right);

$actions = array();
$actions[] = array('action'=>"list",'label'=>"list",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
$hh->input_form_close(); 

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if ($module_admin)
	echo "<p><a href=\"web_feed.php?id=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

