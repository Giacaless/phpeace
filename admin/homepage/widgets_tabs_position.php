<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_tab = $_GET['id_tab'];
$id_p = 0;
$tab = $wi->WidgetsTabGet($id_p, $id_tab);

if ($module_admin)
	$input_right = 1;

$title[] = array("tabs",'widgets_tabs.php');
$title[] = array($tab['tab_name'],'');  

echo $hh->ShowTitle($title);

echo "<div>Tab: <strong>" . $tab['tab_name'] . '</strong></div>';

$num = $wi->WidgetsTabsPositionByTab($rows, $id_p, $id_tab);

$table_headers = array('widget id', 'title', 'column', 'pos');
$table_content = array('$row[id_widget]', '$row[title]',  '$row[col]', '$row[pos]' );
if ($module_admin)
{
    $table_headers[]= '&nbsp;';
    $table_content[] = '{LinkTitle("widgets_tab_position.php?id_tab=$row[id_tab]&col=$row[col]&pos=$row[pos]&id_action=1",' . $hh->tr->Translate("change") . ')}';
}

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

if ($module_admin)
    echo "<p><a href=\"widgets_tab_position.php?id_tab=$id_tab\">" . "Add new tab widget" . "</a></p>\n";


include_once(SERVER_ROOT."/include/footer.php");
?>
