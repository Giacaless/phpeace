<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");
$wi = new Widgets();

if ($module_admin)
	$input_right=1;

$title[] = array('Widgets configuration','');
echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","widgets_config");
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topic.php");
$tgroups = array();
$t = new Topic(0);
$num_groups = $t->PeopleGroups($tgroups);
if($num_groups>0)
	echo $hh->input_row("Show widgets","widgets_group",$wi->widgets_group,$tgroups,"all_option",0,$input_right);

$widgets_per_page = $wi->widgets_per_page>0? $wi->widgets_per_page : $records_per_page;
echo $hh->input_text("Widgets per category page","widgets_per_page",$widgets_per_page,5,0,$input_right);
echo $hh->input_checkbox("Widgets requires authentication","widgets_require_authentication",$wi->widgets_require_authentication,0,$input_right);
echo $hh->input_checkbox("Re-initialise empty profiles","widgets_init_empty_profiles",$wi->widgets_init_empty_profiles,0,$input_right);
echo $hh->input_checkbox("Widgets abuse ","widgets_abuse_reporting",$wi->widgets_abuse_reporting,0,$input_right);
echo $hh->input_checkbox("Widgets delete","widgets_hard_delete",$wi->widgets_hard_delete,0,$input_right);

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();
include_once(SERVER_ROOT."/include/footer.php");
?>

