<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$id_widget = $_GET['id'];

$widget = $wi->WidgetGet($id_widget);
$status = $widget['status'];

if($id_widget>0)
{
    include_once(SERVER_ROOT."/../classes/ontology.php");
    $o = new Ontology;
    include_once(SERVER_ROOT."/../classes/history.php");
    $h = new History;
    $id_user = $h->CreatorId($o->types['widget'],$id_widget);
    $id_user = $id_user['id_user'];
}
else 
{
    $id_user = $ah->current_user_id;
}

if (($module_admin || $id_user == $ah->current_user_id) && $status <> 3)
	$input_right = 1;

$title[] = array("widgets",'widgets.php');
$title[] = array($widget['title'],'widget.php?id='.$id_widget);
echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("widget",'widget.php?id='.$id_widget);
$tabs[] = array("widget content",'widget_content.php?id='.$id_widget);
$tabs[] = array("widget category",'');
$tabs[] = array('history','history.php?id_type=24&id='.$id_widget);  
echo $hh->Tabs($tabs);

?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		errorContainer: $("#category-warning"),
		rules: {
			"category[]": {
				required: true
			}
		}		
	});
});
</script>

<style type="text/css">
label.error {
	display: none;
	margin-left:0;
}
</style>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","update_categories");
echo $hh->input_hidden("id_widget",$id_widget);

$widget_categories = $wi->WidgetCategories($id_widget);
$id_categories = array();
foreach($widget_categories as $widget_category)
{
	$id_categories[] = $widget_category['id_category'];
}
$categories = $wi->Categories();

echo "<label for=\"category[]\" class=\"error\">Please choose at least one category</label>";

echo "<ul id=\"categories\">";
foreach($categories as $category)
{
	echo "<li>";
	echo "<input type=\"checkbox\" name=\"category[]\" value=\"{$category['id_category']}\"";
	if(in_array($category['id_category'],$id_categories))
		echo " checked=\"checked\" ";
	echo ">{$category['name']}";
	echo "</li>";
}
echo "</ul>";

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);

echo $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
