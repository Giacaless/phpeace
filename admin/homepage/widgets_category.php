<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");
$wi = new Widgets();

$title[] = array('Widgets Categories','widgets_categories.php');

$id_category = (int)$get['id'];
$failed = (int)$get['failed'];

if ($id_category>0)
{
	$row = $wi->CategoryGet($id_category);
	$title[] = array($row['name'],'');
}
else
{
	$row = array();
	$title[] = array('add_new','');
}

if ($failed == 0)
{
	if ($id_category > 0)
	{
		$name = $row['name'];
		$description = $row['description'];
	}
}
else
{
	$name = $ah->session->Get("categoryname");
	$description = $ah->session->Get("categorydesc");	
}

if($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","category");
echo $hh->input_hidden("id_category",$id_category);
echo $hh->input_table_open();
if ($failed == 1)
	echo $hh->input_note("<p>Duplicate category code.</p>", TRUE);  
if ($failed == 2)
	echo $hh->input_note("<p>Category is required.</p>", TRUE); 
echo $hh->input_text("Category","name",$name,25,0,$input_right,"",false,20);
echo $hh->input_textarea("description","description",$description,50,4,"",$input_right);

$actions = array();
$actions[] = array('action'=>"update",'label'=>"submit",'right'=>$input_right);
if($id_category>0)
{
	$widgets = array();
	$num_widgets = $wi->CategoryWidgets($widgets,$id_category,true);
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right && $num_widgets==0);
}
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if($id_category>0)
{
	$widgets = array();
	$num_widgets = $wi->CategoryWidgets($widgets,$id_category);
	if($num_widgets>0)
	{
		echo "<h3>Associated widgets</h3>";
		echo "<ul>";
		foreach($widgets as $widget)
		{
			echo "<li><a href=\"widget.php?id={$widget['id_widget']}\">{$widget['title']}</a> ({$widget['status_name']})</li>";
		}
		echo "</ul>";

		$approved_widgets = array();
		$num_approved_widgets = $wi->CategoryWidgets($approved_widgets,$id_category,false,true);
		if($num_approved_widgets>0)
		{
			echo "<h3>Selected widgets";
            if ($module_admin)
                echo "(<a href=\"widgets_category_widget.php?id=$id_category\">Edit</a>)";
            echo "</h3>";
			$selected_widgets = $wi->CategoryWidgetsSelection($id_category);
			if(count($selected_widgets)>0)	
			{
				echo "<ul>";
				foreach($selected_widgets as $selected_widget)
				{
					echo "<li><a href=\"widget.php?id={$selected_widget['id_widget']}\">{$selected_widget['title']}</a></li>";
				}
				echo "</ul>";
			}
		}
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>

