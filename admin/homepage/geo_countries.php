<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('Geo-IP Redirection rules','');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/geo.php");
$geo = new Geo();
$id_topic_default = $geo->TopicByCountry(0);

if($id_topic_default>0)
{
	$t = new Topic($id_topic_default);
	echo "<p>Default: {$t->name}</p>";
}
else
	echo "<p>No default topic defined</p>";
	
$rules = $geo->TopicCountriesRules();
if(count($rules)>0)
{
	echo "<h3>Rules</h3><ul>";
	foreach($rules as $rule)
	{
		echo "<li><a href=\"geo_topic_countries.php?id_topic={$rule['id_topic']}\">{$rule['name']}</a>";
		echo "<div class=\"notes\">{$rule['countries']}</div></li>";
	}
	echo "</ul>";
}

echo "<p><a href=\"geo_topic.php\">Add redirection rule</a></p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

