<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/widgets.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];
$action     = $fh->ActionGet($post);
$wi = new Widgets();
$wf = new WebFeeds();  

if ($from=="home")
{
	$content	= $post['content'];
	$h = new Home;
	if ($action2=="update")
		$h->HomeUpdate($content);
	header("Location: index.php");
}

if ($from=="widgets")
{
    $id_widget        = (int)$post['id_widget'];
    // strip html tag of widget title and description
    $title            =  strip_tags(html_entity_decode($post['title']));
    $description    =  strip_tags(html_entity_decode($post['description']));
    $widget_type    = $post['widget_type'];
    $old_widget_type = $post['old_widget_type'];
    $status        = (int)$post['status'];
    $shareable        = $fh->Checkbox2bool($post['shareable']);
    $status = $post['status'];
    $owner = $post['owner'];
    $comment = $post['comment'];
    $keywords		= $post['keywords'];
    $comment_visible = $fh->Checkbox2bool($post['comment_visible']);
    
    if ($id_widget == 0)
        $id_action = 0;
    else
        $id_action = 1;
    if($action=="store")
    {
        $failed = false;
        if ($id_action == 1 && ($status <> 1) )
        {
            // Check whether widget is used in default widget configuration in homepage    
        	$count_widgets = $wi->WidgetsTabPositionCountByIdWidget(0, $id_widget);
            if ($count_widgets['num_widgets'] > 0)
            {
                $failed = true;
                $url = "widget.php?id=$id_widget&failed=1";
            }
        }
        
        if (!$failed)
        {
            $id_widget = $wi->WidgetStore($id_widget,$title,$description,$widget_type,$status,$shareable,$comment, $comment_visible,$keywords);
            if ($id_action == 0)
                $id_widget = $wi->WidgetParamStore($id_action, $id_widget, '');
            $url = "widget_content.php?id=$id_widget";
        }
    }
    if ($action=="delete" || $action=="remove")
    {
        $failed = false;
        $widget = $wi->WidgetGet($id_widget);

        if ($widget['status'] == 1) 
        {
            // Check whether widget is used in default widget configuration in homepage    
            $count_widgets = $wi->WidgetsTabPositionCountByIdWidget($id_p, $id_widget);
            if ($count_widgets['num_widgets'] > 0)
            {
                $failed = true;
                $url = "widget.php?id=$id_widget&failed=2";
            }
        }

        if (!$failed)
        {
            if($action=="remove" && $wi->widgets_hard_delete)
            	$wi->WidgetDelete($id_widget);
            else 
            	$wi->WidgetSoftDelete($id_widget);
            $url = "widgets.php";
        }
    }
    if ($action == "list")
    {
        $url = "widgets.php?id_widget=$id_widget&widget_type=$widget_type&status=$status&owner=$owner"; 
    }
    header("Location: $url");
}

if ($from=='widget_content')
{
    $id_widget      = (int)$post['id_widget'];
    $id_parent      = (int)$post['id_parent'];
    $widget_type    = (int)$post['widget_type'];
    $id_web_feed    = (int) $post['id_web_feed'];
	$failed = false;
    if ($id_widget == 0)
        $id_action = 0;
    else
        $id_action = 1;
    
    $current_params = array();
    switch ($widget_type)
    {
        // manual
        case 0:
            $link_url = $post['link_url'];

			if ($link_url != '')
			{
				include_once(SERVER_ROOT."/../classes/link.php");
		        $link = new Link(0);
		        $link->url = $link_url;
		        $error_desc_link_url = $link->Check();
		        if ($link->error <> 0)
		        	$failed = true; 
			}
            
            if (!$failed)
            {
	            // strip html tag of manual widget content
	            $content =  strip_tags(html_entity_decode($post['content']));
	            $current_params = array("link_url"=>$link_url, "content"=>$content);
   				$file = $fh->UploadedFile("img",true);
            }
            else
            {
                $url = "widget_content.php?id=$id_widget&failed=1"; 
                $url .= isset($id_parent) ? "&id_parent=$id_parent" : "";  
            }
            break;
        // rss
        case 1:
            $layout_type = (int)$post['layout_type'];
            $items_display = (int)$post['items_display'];
            
            $current_params = array("layout_type"=>$layout_type, "items_display"=>$items_display, "id_web_feed"=>$id_web_feed);
            break;
        // custom
        case 2:
            $content = $post['content'];
            $is_html = $fh->Checkbox2Bool($post['is_html']);
            $current_params = array("content"=>$content,'is_html'=>$is_html);
            break;
        case 3:
            $layout_type = (int)$post['layout_type'];
            $current_params = array("layout_type"=>$layout_type);
            break;
        case 4:
            $id_feature = (int)$post['id_feature'];
            $update_interval = (int)$post['update_interval'];
            $current_params = array("id_feature"=>$id_feature,'update_interval'=>$update_interval);
		break;
    }
    
    if (!$failed)
    {
	    include_once(SERVER_ROOT."/../classes/varia.php"); 
	    $v = new Varia; 
	    $params = $v->Serialize($current_params);
	    
	    if ($action == 'store')
	    {
            $url = "widget_category.php?id=$id_widget";      
            if ($id_parent > 0)
            {
                $id_widget = $wi->WidgetStore($id_widget,'','',WIDGET_TYPE_MANUAL,WIDGET_STATUS_PENDING,0,'',false,"",$id_parent);
                $url = "widget_content.php?id=$id_parent";      
            }
	        $id_widget = $wi->WidgetParamStore($id_action, $id_widget, $params);   
	        if ($widget_type == 1)
	        {
	            $wi->WidgetWebFeedStore($id_widget, $id_web_feed, $items_display);    
	        }
	        
	        if($widget_type==0 && $file['ok'])
	        {
        		$wi->WidgetImageUpdate($id_widget,$file);
	        }
	    }
        else if ($action == 'delete')
        {
            if ($id_parent > 0)
            {
                $wi->PublicWidgetChildDelete($id_widget);    
                $url = "widget_content.php?id=$id_parent";      
            }
        }
    }
    header("Location: $url");               
}

if ($from=="category")
{
	$id_category	= $post['id_category'];
	$name	= trim($post['name']);
	$description	= trim($post['description']);
	$action = $fh->ActionGet($post);
	$url = "widgets_categories.php";
	$failed = 0;
	
	if($name == "")
	{
		$ah->session->Set("categoryname", $name);
		$ah->session->Set("categorydesc", $description);			
		$url = "widgets_category.php?id=" . $id_category . "&failed=2";
	}
	else
	{
		if($action=="update")
		{
			$widget_category = $wi->CategoryGetByName($name);	
			if ($id_category == 0)
			{
				// add
				if ($widget_category['id_category'] != 0 )
					$failed = 1;
			}
			else
			{
				// update
				if (($widget_category['id_category'] > 0) && ($widget_category['id_category'] != $id_category))
					$failed = 1;
			}
			if ($failed == 0)
				$wi->CategoryStore($id_category,$name,$description);
			else
			{
				$ah->session->Set("categoryname", $name);
				$ah->session->Set("categorydesc", $description);			
				$url = "widgets_category.php?id=" . $id_category . "&failed=1";
			}
		}
	}
	if($action=="delete")
		$url = "widgets_category_delete.php?id=$id_category";
	header("Location: $url");
}

if ($from=="category_delete")
{
	$id_category	= $post['id_category'];
	$wi->CategoryDelete($id_category);
	header("Location: widgets_categories.php");
}

if ($from=="update_categories")
{
    $id_widget      = (int)$post['id_widget'];
	$widget_categories = is_array($post['category'])? $post['category'] : array();
	$wi->WidgetCategoriesDelete($id_widget);
	$categories = $wi->Categories();
	foreach($categories as $category)
	{
		if(in_array($category['id_category'],$widget_categories))
		{
			$wi->WidgetCategoryAdd($id_widget,$category['id_category']);
		}
	}
	header("Location: widget.php?id=$id_widget");
}

if ($from=="category_widgets_selection")
{
    $id_category      = (int)$post['id_category'];
	$widgets = array();
	$wi->CategoryWidgets($widgets,$id_category,false,true);
	$category_widgets = is_array($post['widget'])? $post['widget'] : array();
	$wi->CategoryWidgetsSelectionDelete($id_category);
	foreach($widgets as $widget)
	{
		if(in_array($widget['id_widget'],$category_widgets))
		{
			$wi->CategoryWidgetsSelectionAdd($id_category,$widget['id_widget']);
		}
	}
	header("Location: widgets_category.php?id=$id_category");
}

if ($from == 'web_feeds')
{
    $id_web_feed        = (int)$post['id_web_feed'];
    $title              = $post['title'];
    $feed_url           = trim($post['feed_url']);
    $update_interval    = $post['update_interval'];
    $status				= (int)$post['status'];
    if($action=="store")
    {
        // add/update, need to check whether it is valid url
        $valid_url = $wf->CheckValidWebFeedUrl($feed_url);

        if ($valid_url)
        {
            $id_web_feed = $wf->WebFeedStore($id_web_feed,$title,$feed_url,$update_interval);
            $url = "web_feeds.php";
        }
        else
        {
        	$fh->HttpPost(true);
            $url = "web_feed.php?id=$id_web_feed&failed=1";
        }
    }
    if ($action=="delete")
    {
        // if delete, need to check whether any widgets using it
        $num = $wi->WidgetsByWidgetType(1, $rows_rss_widgets,false);

        include_once(SERVER_ROOT."/../classes/varia.php"); 
        $v = new Varia; 
        $widgets_found = false;
        foreach ($rows_rss_widgets as $row_rss_widget)
        {
            // get the id_web_feed, compare it
            $params = $v->Deserialize($row_rss_widget['params']);

            $id_web_feed_compare = (int)$params['id_web_feed'];
    
            if ($id_web_feed == $id_web_feed_compare)
            {
                $widgets_found = true;
                break;
            }
        }

        if ($widgets_found)
        {
        	$fh->HttpPost(true);
            $url = "web_feed.php?id=$id_web_feed&failed=2";
        }
        else
        {
        	$wf->WebFeedStatusLogDeleteByWebFeed($id_web_feed);
        	$wf->WebFeedItemContentDeleteByWebFeed($id_web_feed);
            $wf->WebFeedItemDeleteByWebFeed($id_web_feed);
            $wf->WebFeedDelete($id_web_feed);
            
            $url = "web_feeds.php";
        }
    }
    if ($action == 'verifyfeedurl')
    {
    	$ah->session->Set("webfeedurl",$feed_url);

        $valid_url = $wf->CheckValidWebFeedUrl($feed_url);
        $fh->HttpPost(true);
        
        if ($valid_url)
            $url = "web_feed.php?id=$id_web_feed&verify=1";
        else
            $url = "web_feed.php?id=$id_web_feed&verify=2";
    }
	if ($action == 'import')
	{
		$feed = $wf->WebFeedGet($id_web_feed);
		$wfi = new WebFeedsImporter();
		$wfi->ImportFeed($feed['feed_url'],$id_web_feed,$feed['update_interval']);
		$ah->MessageSet("Imported {$wfi->insertedCount} feed items, updated {$wfi->updatedCount}");
		$url = "web_feed.php?id=$id_web_feed";
	}
	if ($action == "list")
    {
        $url = "web_feeds.php?status=$status"; 
    }
    header("Location: $url");
}
if ($from == 'web_feed_items')
{
    $id_web_feed        = (int) $post['id_web_feed'];
    $id_web_feed_item   = (int) $post['id_web_feed_item'];
    $title              = $post['title'];
    $is_approved           = (int)$post['is_approved'];
    $status        = $fh->Checkbox2bool($post['status']);
    $filter = (int)$post['filter'];
    
    if($action=="store")
    {
        if ($is_approved == 0)
            $is_filtered = 1;
        else
            $is_filtered = 0;
        $id_web_feed_item = $wf->WebFeedItemStore($id_web_feed_item,$is_approved,$is_filtered,$status);
        $url = "web_feed_items.php?id=$id_web_feed";
    }
    if ($action == "delete")
    {
        $wf->WebFeedItemDelete($id_web_feed_item);          
        $url = "web_feed_items.php?id=$id_web_feed";
    }
    if ($filter == 1)
        $url = "filtered_web_feed_items.php";
    
    header("Location: $url");       
}
if ($from == "filtered_web_feed_items")
{
    $from_date = $post['from_date'];
    $to_date = $post['to_date'];
    
    if ($action == "list")
    {
        $url = "filtered_web_feed_items.php?from_date=$from_date&to_date=$to_date"; 
    }
    header("Location: $url");
}
if ($from == "widgets_tabs")
{
    $id_p       = (int)$post['id_p'];
    $old_id_tab     = (int)$post['old_id_tab'];
    $tab_name   = $post['tab_name'];
    $id_tab     = (int)$post['id_tab'];
    
    if($action=="store")
    {
        if ($old_id_tab == 0)
        {
            // need to check duplication of data if insert a new tab
			$row = $wi->WidgetsTabGet($id_p, $id_tab);
            if ($tab_name == '')
            	$url = "widgets_tab.php?id=$old_id_tab&fail=emptytab";
            else if ($row['id_tab'] > 0)
                $url = "widgets_tab.php?id=$old_id_tab&fail=duplicate";
            else
            {
                $id_action = 0;
                $id_tab = $wi->WidgetsTabStore($id_action,$id_p,$id_tab,$tab_name);
                $url = "widgets_tabs.php";
            }
        }
        else
        {
            // update action
            $id_action = 1;
            if ($tab_name == '')
            	$url = "widgets_tab.php?id=$id_tab&fail=emptytab";
            else
            {
	            $id_tab = $wi->WidgetsTabStore($id_action,$id_p,$id_tab,$tab_name);
	            $url = "widgets_tabs.php";
            }
        }
    }
    if ($action=="delete")
    {
        $wi->WidgetsTabDelete($id_p, $id_tab);
        // delete tab, need to delete all the widget under the tab as well
        $wi->WidgetsTabPositionDeleteByTab($id_p, $id_tab);
        
        $url = "widgets_tabs.php";
    }
    header("Location: $url");       
}    
if ($from == "widgets_tabs_position")
{
    $id_p       = (int)$post['id_p'];
    $id_widget     = (int)$post['id_widget'];
    $id_tab     = (int)$post['id_tab'];
    $col     = (int)$post['col'];
    $pos     = (int)$post['pos'];
    $id_action     = (int)$post['id_action'];
    
    if($action=="store")
    {
        if ($id_action == 0)
        {
            // need to check duplication of data if insert a new tab
            $row = $wi->WidgetsTabPositionGet($id_p, $id_tab, $col, $pos);
            // need to check whether existing widget already been add to homepage
            if ($row['id_tab'] > 0)
            {
                $url = "widgets_tab_position.php?id_tab=$id_tab&col=$col&pos=$pos&id_widget=$id_widget&fail=duplicate";
            }
            else if ($wi->IsWidgetAvailable($id_p, $id_widget))
            {
				$url = "widgets_tab_position.php?id_tab=$id_tab&col=$col&pos=$pos&id_widget=$id_widget&fail=widget";
            }
            else
            {
                // add action
                $id_action = 0;
                $id_tab = $wi->WidgetsTabPositionStore($id_action,$id_p,$id_tab,$id_widget,$col,$pos);
                $url = "widgets_tabs_position.php?id_tab=$id_tab";
            }
        }
        else
        {
            // update action
            $id_action = 1;
            if ($wi->IsWidgetAvailableExcludeTabPosition($id_p, $id_widget, $id_tab, $col, $pos))
				$url = "widgets_tab_position.php?id_tab=$id_tab&col=$col&pos=$pos&id_widget=$id_widget&id_action=1&fail=widget";
			else
			{
	            $id_tab = $wi->WidgetsTabPositionStore($id_action,$id_p,$id_tab,$id_widget,$col,$pos);
	            $url = "widgets_tabs_position.php?id_tab=$id_tab";
			}                	
        }
    }
    if ($action=="delete")
    {
        $wi->WidgetsTabPositionDelete($id_p, $id_tab, $col, $pos);
        $url = "widgets_tabs_position.php?id_tab=$id_tab";
    }
    header("Location: $url");       
}    
if ($from == 'widget_abuse')
{
    $id_widget_abuse        = (int) $post['id_widget_abuse'];
    $id_p                   = (int) $post['id_p'];
    $id_widget              = (int) $post['id_widget'];
    $comments               = $post['comments'];
    $status                 = (int) $post['status'];
    $email                 = $post['email']; 

    if($action=="store")
    {
        $id_widget_abuse = $wi->WidgetAbuseStore($id_widget_abuse, $id_widget, $id_p, $comments, $status, $email);
        $url = "widget_abuses.php";
    }
    header("Location: $url");       
}
if ($from == 'web_feed_reports')
{
    $id_report = (int) $post['id_report'];
    $id_day = (int) $post['id_day'];
    $num_items = (int) $post['num_items'];
    
    if ($action == 'search')
    {
        $url = 'web_feed_reports.php?id_report=' . $id_report . '&id_day=' . $id_day . '&num_items=' . $num_items;
    }
    header("Location: $url");       
}

if ($from=="widgets_config" && $module_admin)
{
	$ini = new Ini();
	$ini->SetModule("homepage","widgets_group",$fh->String2Number($post['widgets_group']));
	$ini->SetModule("homepage","widgets_per_page",$fh->String2Number($post['widgets_per_page'],15));
	$ini->SetModule("homepage","widgets_require_authentication",$fh->Checkbox2Bool($post['widgets_require_authentication']));
	$ini->SetModule("homepage","widgets_init_empty_profiles",$fh->Checkbox2Bool($post['widgets_init_empty_profiles']));
	$ini->SetModule("homepage","widgets_abuse_reporting",$fh->Checkbox2Bool($post['widgets_abuse_reporting']));
	$ini->SetModule("homepage","widgets_hard_delete",$fh->Checkbox2Bool($post['widgets_hard_delete']));
	header("Location: index.php");
}

if ($from=="config" && $module_admin)
{
	$hometype	= $fh->Null2Zero($post['hometype']);
	$highlight_days	= $fh->Null2Zero($post['highlight_days']);
	$ini = new Ini;
	$ini->SetModule("homepage","hometype",$hometype);
	$ini->SetModule("homepage","highlight_days",$highlight_days);
	header("Location: index.php");
}

if ($from=="geo_topic_default" && $module_admin)
{
    $id_topic = (int)$post['id_topic'];
    include_once(SERVER_ROOT."/../classes/geo.php");
    $geo = new Geo();
    if($id_topic>0)
    	$geo->RedirectDefaultSet($id_topic);
	header("Location: index.php");
}

if ($from=="geo_topic" && $module_admin)
{
    $id_topic = (int)$post['id_topic'];
	header("Location: geo_topic_countries.php?id_topic=$id_topic");
}

if($from=="geo_topic_countries" && $module_admin)
{
	$id_topic 				= (int)$post['id_topic'];
	$id_competition_group	= $post['id_competition_group'];
	if ($id_topic>0)
	{
		include_once(SERVER_ROOT."/../classes/geo.php");
		$geo = new Geo();
		$countries = $geo->Countries(0);
		$group_countries = array();
		foreach($countries as $country)
		{
			if($fh->CheckBox2Bool($post['countries_' . $country['id_country'] ]))
			{
				$group_countries[] = $country['id_country'];
			}
		}
		$geo->TopicCountriesUpdate($id_topic, $group_countries);
	}
	header("Location: geo_countries.php");
}

?>
