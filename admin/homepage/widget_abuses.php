<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/widgets.php");

$wi = new Widgets();

$title[] = array("widget abuses");

echo $hh->ShowTitle($title);

$num = $wi->WidgetAbusesAll($rows);

$table_headers = array('widget id', 'widget','reported by','report date','status');
$table_content = array('$row[id_widget]', '{LinkTitle("widget_abuse.php?id=$row[id_widget_abuse]",$row[title])}', '$row[name]', '$row[insert_date]', '{WidgetAbuseStatusLookup($row[status])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

