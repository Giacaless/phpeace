<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/web_feeds.php");

$wf = new WebFeeds();

$id_web_feed = $_GET['id'];

$feed = $wf->WebFeedGet($id_web_feed);

if ($module_admin)
	$input_right = 1;

$title[] = array("web feeds",'web_feeds.php');
$title[] = array($feed['title'],'web_feed.php?id='.$id_web_feed);

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array("web feed",'web_feed.php?id='.$id_web_feed);
$tabs[] = array("web feed items",'');
echo $hh->Tabs($tabs);

$num = $wf->WebFeedItems($rows,$id_web_feed);

$table_headers = array('title','published date','approved/filtered','active','last updated');
$table_content = array('{LinkTitle("web_feed_item.php?id=$row[id_web_feed_item]&id_web_feed='.$id_web_feed.'",$row[title])}', '$row[published_date]', '{WebFeedItemsApprovedLookup($row[is_approved])}','{WebFeedItemsStatusLookup($row[status])}','$row[last_updated]');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);


$back_btn = "<input type='button' value='Back' onclick='javascript:history.go(-1);'";
echo $back_btn;

include_once(SERVER_ROOT."/include/footer.php");
?>
