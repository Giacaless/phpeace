<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$default = isset($_GET['default']);

$input_right = $module_admin;

$title[] = array('Geo redirection topic','');
echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/topics.php");
include_once(SERVER_ROOT."/../classes/geo.php");
$tt = new Topics;
$topics = $tt->AllTopics(false,true);
$geo = new Geo();
$id_topic_default = $default? $geo->TopicByCountry(0) : 0;

echo $hh->input_form_open();
echo $hh->input_hidden("from",$default?"geo_topic_default":"geo_topic");
echo $hh->input_table_open();
echo $hh->input_note($default? "Select default topic to redirect to" : "Select topic to use for redirection rule");

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->AllTopics(false,true);
echo $hh->input_topics($id_topic_default,0,$topics,"choose_option",$input_right);
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>

