<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/tracker.php");

$id_p = (int)$_GET['id_p'];
$id_visit = (int)$_GET['id'];
$day = (int)$_GET['d'];

if($id_p>0)
{
	include_once(SERVER_ROOT."/../classes/people.php");
	$pe = new People();
	$row = $pe->UserGetDetailsById($id_p);
	$title[] = array($row['name1'] . " " . $row['name2'],'');
}
elseif($day>0) 
{
	$month = (int)$_GET['m'];
	$year = (int)$_GET['y'];
	$months = $hh->tr->Translate("month");
	
	$title[] = array("visitors",'stats.php');
	$title[] = array($months[$month-1] . " " . $year,'stats_month.php?m=' . $month . '&y=' . $year);
	$title[] = array($day,"visits.php?d=$day&m=$month&y=$year");
	$title[] = array("visit",'');
}
else
{
	$title[] = array("visits",'visits.php');
	$title[] = array("visit",'');
}
echo $hh->ShowTitle($title);

if($id_p>0)
{
	$tabs = array();
	$tabs[] = array('user_data','/people/person.php?id='.$id_p);
	$tabs[] = array('user_admin','/people/person_admin.php?id='.$id_p);
	$tabs[] = array('user_stats','/people/person_stats.php?id='.$id_p);
	$tabs[] = array('visits','/people/visits.php?id_p='.$id_p);
	echo $hh->Tabs($tabs);
}

$tk = new Tracker();

$visit = $tk->Visit($id_visit);

echo "<ul>";
echo "<li>" . $hh->tr->Translate("name") . ": " . $visit['name'] . "</li>";
echo "<li>" . $hh->tr->Translate("date") . ": " . $hh->FormatDateTimeSeconds($visit['visit_date_ts']) . "</li>";
echo "<li>" . $hh->tr->Translate("length") . ": " .$visit['length'] . " " . $hh->tr->Translate("seconds") . "</li>";
echo "<li>IP: " . $visit['ip'] . "</li>";
echo "<li>Cookie: " .  (($visit['cookie']=="1")? $hh->tr->Translate("yes") : $hh->tr->Translate("no")) . "</li>";
echo "<li>Browser: " . $visit['user_agent'] . "</li>";
echo "<li>Screen: {$visit['width']} x {$visit['height']} x {$visit['color']}</li>";
echo "<li>Referer: <a href=\"{$visit['referer']}\" target=\"_blank\">" . $visit['referer'] . "</a></li>";
echo "</ul>";

$num = $tk->Pages( $row, $id_visit );

$table_headers = array('date','group','topic','page');

$table_content = array('{FormatDateTimeSeconds($row[page_date_ts])}','$row[group_name]','$row[topic_name]','{LinkTitle($row[url],$row[title])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
