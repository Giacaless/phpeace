<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

echo $hh->ShowTitle($title);

if($conf->Get("track") || $conf->Get("track_all"))
{
	echo "<h3>" . $hh->tr->Translate("visitors") . "</h3>";
	echo "<ul>";
	echo "<li><a href=\"visits.php\">" . $hh->tr->Translate("visits") . "</a></li>\n";
	echo "<li><a href=\"stats.php\">" . $hh->tr->Translate("stats") . ": " . $hh->tr->Translate("periodic") . "</a></li>\n";
	echo "<li><a href=\"stats_topics.php\">" . $hh->tr->Translate("stats") . ": " . $hh->tr->Translate("topics") . "</a></li>\n";
	echo "</ul>";
}

echo "<h3>Statistiche interne</h3>";
echo "<table border=0 cellpadding=2 cellspacing=0>\n";

include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users();
$last_login = $uu->LastLogin();
if ($last_login>0)
	echo "<p>" . $hh->tr->Translate("lastupdate") . ": " . $hh->FormatDate($last_login) . "</p>\n";

echo "<tr><td align=\"right\">".count($uu->Active())."</td><td>utenti abilitati</td></tr>\n";

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics();
echo "<tr><td align=\"right\">".count($tt->Visible())."</td><td>tematiche (<a href=\"stat.php?s=5\">articoli</a> - <a href=\"stat.php?s=6\">gruppi</a>)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/galleries.php");
$gg = new Galleries();
echo "<tr><td align=\"right\">".count($gg->Visible())."</td><td>gallerie (<a href=\"stat.php?s=7\">immagini</a>)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/articles.php");
echo "<tr><td align=\"right\">".(Articles::CountApproved())."</td><td>articoli (<a href=\"stat.php?s=2\">utenti</a> - <a href=\"stat.php?s=18\">autori</a> - <a href=\"stat.php?s=20\">periodo</a><!-- - <a href=\"stat.php?s=14\">lingue</a>-->)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/links.php");
$links = new Links();
echo "<tr><td align=\"right\">".$links->CountApproved()."</td><td>links (<a href=\"stat.php?s=16\">tematiche</a><!-- - <a href=\"stat.php?s=17\">lingue</a> -->)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology();
echo "<tr><td align=\"right\">".$o->Count()."</td><td>parole chiave approvate</td></tr>\n";

include_once(SERVER_ROOT."/../classes/docs.php");
echo "<tr><td align=\"right\">".Docs::Count()."</td><td>documenti (<a href=\"stat.php?s=4\">utenti</a>)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/images.php");
$i = new Images();
echo "<tr><td align=\"right\">".$i->Count()."</td><td>immagini (<a href=\"stat.php?s=3\">utenti</a>)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/banners.php");
$banners = new Banners();
$array = array();
$num = $banners->AllClicks($array);
echo "<tr><td align=\"right\">".$banners->Count()."</td><td>banners (<a href=\"stat.php?s=15\">$num clicks</a>)</td></tr>\n";

include_once(SERVER_ROOT."/../classes/events.php");
$ee = new Events();
echo "<tr><td align=\"right\">".$ee->Count()."</td><td>eventi nel calendario</td></tr>\n";

include_once(SERVER_ROOT."/../classes/mail.php");
$mail = new Mail();
$num = $mail->AllFriends( $row );
echo "<tr><td align=\"right\">$num</td><td>segnalazioni di <a href=\"stat.php?s=19\">articoli</a> via email</td></tr>\n";

echo "</table>\n";
include_once(SERVER_ROOT."/include/footer.php");
?>
