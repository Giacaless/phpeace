<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

if ($module_admin)
	$input_right=1;

$trm14 = new Translator($hh->tr->id_language,14);
$title[] = array('configuration','');
echo $hh->ShowTitle($title);

?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			pub_web: "required",
			admin_web: "required",
			title: "required",
			max_file_size: {
				required: true,
				min: 1000000
			},
			staff_email: {
				required: true,
				email: true
			}
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","config_update");
echo $hh->input_table_open();
echo $hh->input_separator("URL");
echo $hh->input_text($trm14->Translate("pub_web"),"pub_web",$hh->ini->Get("pub_web"),30,0,$input_right);
echo $hh->input_text($trm14->Translate("admin_web"),"admin_web",$hh->ini->Get("admin_web"),30,0,$input_right);

echo $hh->input_separator($trm14->Translate("portal"));
echo $hh->input_text("name","title",$hh->ini->Get("title"),40,0,$input_right);
echo $hh->input_text("description","description",$hh->ini->Get("description"),40,0,$input_right);
echo $hh->input_text("email staff","staff_email",$hh->ini->Get("staff_email"),40,0,$input_right);
echo $hh->input_text($trm14->Translate("initial_year"),"init_year",$hh->ini->Get("init_year"),4,0,$input_right);
echo $hh->input_array("geo_location","geo_location",$hh->ini->Get("geo_location"),$hh->tr->Translate("geo_options"),$input_right);
include_once(SERVER_ROOT."/../classes/payment.php");
$p = new Payment();
echo $hh->input_array("currency","default_currency",$hh->ini->Get("default_currency"),$p->currencies,$input_right);

include_once(SERVER_ROOT."/../classes/rss.php");
$rss = new Rss();
echo $hh->input_array("feed_type","feed_type",$hh->ini->Get("feed_type"),$rss->feed_types,$input_right);

$max_upload_size = min(ini_get("post_max_size"),ini_get("upload_max_filesize"));
echo $hh->input_text($trm14->Translate("max_upload"),"max_file_size",$hh->ini->Get("max_file_size"),10,0,$input_right," (php.ini: $max_upload_size)");

echo $hh->input_separator("schedules");
echo $hh->input_checkbox($trm14->Translate("notify_execution"),"sched_notify",$hh->ini->Get("sched_notify"),0,$input_right);
echo $hh->input_text("IP scheduler","scheduler_ip",$hh->ini->Get("scheduler_ip"),15,0,$input_right);

echo $hh->input_separator("Logging");
include_once(SERVER_ROOT."/../classes/log.php");
$log = new Log();
echo $hh->input_array($trm14->Translate("log_level"),"log_level",$hh->ini->Get("log_level"),$log->levels,$input_right);

echo $hh->input_checkbox($trm14->Translate("log_banners"),"log_banners",$hh->ini->Get("log_banners"),0,$input_right);

echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
