<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/scheduler.php");

$sc = new Scheduler();

$id = $_GET['id'];

$trm14 = new Translator($hh->tr->id_language,14);
$title[] = array($trm14->Translate("schedules"),'schedules.php');

if ($module_admin)
	$input_right = 1;

if ($id>0)
{
	$row = $sc->ScheduleGet($id);
	$title[] = array($sc->actions[$row['id_action']],'');
	$id_user = $row['id_user'];
	$active = $row['active'];
	if ($id_user == "0")
	{
		$is_system = true;
		$input_right = 0;
	}
}
else
{
	$title[] = array('add_new','');
	$id_user = $ah->current_user_id;
	$active = 1;
}

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","scheduler");
echo $hh->input_hidden("id_schedule",$id);
echo $hh->input_hidden("id_user",$id_user);
echo $hh->input_table_open();

echo $hh->input_checkbox("active","active",$active,0,$input_right);

$slots = $sc->SchedulerSlots();
echo $hh->input_row("scheduler","id_scheduler",$row['id_scheduler'],$slots,"",0,$input_right || $is_system);

$desc = $hh->tr->Translate("action");
if ($id>0)
{
	$params = $sc->SchedulableActionParams($row['id_action']);
	if (is_array($params) && count($params)>0)
	{
		$desc .= " (<a href=\"schedule_params.php?id=$id\">" . $hh->tr->Translate("parameters") . "</a>)";
	}
}
echo $hh->input_array($desc,"id_action",$row['id_action'],$sc->actions,$input_right);

$hhf = new HHFunctions();
echo $hh->input_text("author","user",$hhf->UserLookup($id_user),50,0,0);

$actions[] = array('action'=>"store",'label'=>"submit",'right'=>($input_right  || $is_system));
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && !$is_system && $id>0));
$actions[] = array('action'=>"activeswap",'label'=>(($row['active'])?"deactivate":"activate"),'right'=>$is_system && $row['id_action']>0);
echo $hh->input_actions($actions,$input_right || $is_system);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
