<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/images.php");

$title[] = array('images_orphans','');

echo $hh->ShowTitle($title);

echo "<p>" . $hh->tr->Translate("images_orphans_desc") . "</p>\n";

$i = new Images();

$row = array();
$num = $i->Orphans( $row );
$table_headers = array('image','&nbsp;','caption');
$table_content = array('<img src=\"/images/upload.php?src=images/0/$row[id_image].'.$i->convert_format.'&format=$row[format]\" border=\"0\">',
'{LinkTitle("../articles/image.php?id=$row[id_image]","open")}','$row[caption]');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if($num>0 && $module_admin)
	echo "<p><a href=\"image_orphans_delete.php\">" . $hh->tr->Translate("delete_forever") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>


