<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$id = $_GET['id'];

$title[] = array('users_list','users.php?p='.$current_page);

$u = new User;
$u->id = $id;
$row = $u->UserGet();
$active = $row['active'];
if ($active!=1)
{
	include_once(SERVER_ROOT."/../classes/adminhelper.php");
	$ah = new AdminHelper;
	$ah->MessageSet("user_no_active_warn");
}
$title[] = array($row['name'],'user.php?id='.$id);
$title[] = array('stats','');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/users/user.php?id='.$id);
if($hh->ini->Get("user_show"))
	$tabs[] = array('user_link','/users/user_page.php?id='.$id);
$tabs[] = array('user_admin','/users/user_admin.php?id='.$id);
$tabs[] = array('image_associated','/users/user_image.php?id='.$id);
$tabs[] = array('modules','/users/modules_add.php?id_user='.$id);
$tabs[] = array('topics','/users/topics_add.php?id_user='.$id);
$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id);
$tabs[] = array('services','/users/user_services.php?id='.$id);
$tabs[] = array('user_stats','');
echo $hh->Tabs($tabs);

if ($ah->current_user_id==$id || $module_admin)
	$input_right = 1;
if ($module_admin)
	$input_super_right = 1;

if($row['start_date_ts']>0 && $row['id_tutor']>0)
{
	$u2 = new User();
	$u2->id = $row['id_tutor'];
	$tutor = $u2->UserGet();
	echo "<p>" . $hh->tr->TranslateParams("account_creation",array($hh->FormatDate($row['start_date_ts']),$tutor['name'])) . "</p>";
}

echo "<p>" . $hh->tr->Translate("connections") . ": " . $u->Connections();
if ($row['last_conn_ts']>0)
	echo "<br>" . $hh->tr->Translate("last_time") . ": " . $hh->FormatDateTime($row['last_conn_ts']) . "\n";
echo "</p>\n";
$rows = array();
$num = $u->Articles( $rows );
echo "<p>" . $hh->tr->Translate("articles_inserted") . ": $num";
if($num>0)
	echo " (<a href=\"/articles/articles.php?id_user=$id\">" . $hh->tr->Translate("see_list") . "</a>)";
echo "</p>";

$num = $u->ArticlesVisible( $rows );
echo "<p>" . $hh->tr->Translate("articles_written") . ": $num";

include_once(SERVER_ROOT."/include/footer.php");
?>


