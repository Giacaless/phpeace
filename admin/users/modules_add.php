<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topics.php");
include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;

$id_user = $_GET['id_user'];
$restricted = (int)$_GET['showall'];

$title[] = array('users_list','users.php?p='.$current_page);

$u->id = $id_user;
$row = $u->UserGet();
$title[] = array($row['name'],'user.php?id='.$id_user);
$title[] = array('modules','');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
elseif($id_user>0)
{
	$tabs = array();
	$tabs[] = array('user_data','/users/user.php?id='.$id_user);
	if($hh->ini->Get("user_show"))
		$tabs[] = array('user_link','/users/user_page.php?id='.$id_user);
	$tabs[] = array('user_admin','/users/user_admin.php?id='.$id_user);
	$tabs[] = array('image_associated','/users/user_image.php?id='.$id_user);
	$tabs[] = array('modules','');
	$tabs[] = array('topics','/users/topics_add.php?id_user='.$id_user);
	$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id_user);
	$tabs[] = array('user_stats','/users/user_stats.php?id='.$id_user);
	echo $hh->Tabs($tabs);
	
	echo $hh->input_form_open();
	echo $hh->input_hidden("from","user_modules");
	echo $hh->input_hidden("action2","module_add");
	echo $hh->input_hidden("id_user",$id_user);
	echo $hh->input_hidden("restricted",$restricted);
	echo $hh->input_table_open();

	$modules = $u->ModulesRight($restricted==0);
	$modules_user = array();
	$modules_names = $hh->tr->Translate("modules_names");
	echo "<tr><td colspan=\"2\"><ul id=\"modules-user\">";
	$i = 1;
	foreach($modules as $module)
	{
		echo "<li class=\"mod-open{$module['restricted']} row" . ((($i%2)==1)?"0":"1") . "\"><label>" . $modules_names[$module['id_module']] . "</label>";
		echo "<input type=\"checkbox\" class=\"input-checkbox\" name=\"moduser{$module['id_module']}\" " . ($module['id_user']==$id_user?"checked":"") . "><label>" . $hh->tr->Translate("user") . "</label>";
		echo "<input type=\"checkbox\" class=\"input-checkbox\" name=\"modadmin{$module['id_module']}\" " . ($module['is_admin']==1?"checked":"") . "><label>" . $hh->tr->Translate("administrator") . "</label>";
		echo "</li>\n";
		$i++;
	}
	echo "</ul></td></tr>";
	echo $hh->ColumnsMaker($modules_user,4);

	$actions = array();
	$actions[] = array('action'=>"submit",'label'=>"submit",'right'=>$input_right);
	
	echo $hh->input_actions($actions,$input_right);
	
	echo $hh->input_table_close() . $hh->input_form_close();
	
	if (!$restricted>0)
		echo "<p><a href=\"modules_add.php?id_user=$id_user&showall=1\">" . $hh->tr->Translate("show_restricted_modules") . "</a></p>";
}
include_once(SERVER_ROOT."/include/footer.php");
?>


