<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/geo.php");

$id = $_GET['id'];

$changepass = $_GET['changepass'];

$title[] = array('users_list','users.php?p='.$current_page);

$trm28 = new Translator($hh->tr->id_language,28);

if ($id>0)
{
	include_once(SERVER_ROOT."/../classes/user.php");
	$u = new User;
	$u->id = $id;
	$row = $u->UserGet();
	$action2 = "update";
	$id_language = $row['id_language'];
	$active = $row['active'];
	if ($active!=1)
	{
		$ah->MessageSet("user_no_active_warn");
	}
	if ($row['bounces']>=$conf->Get("bounces_threshold"))
	{
		$ah->MessageSet("bounces_warning",array($conf->Get("bounces_threshold")));
	}
	$title[] = array($row['name'],'user.php?id='.$id);
	$title[] = array('administration','');
}
else
{
	$active = 1;
	$action2 = "insert";
	$id_language = $hh->ini->Get("id_language");
	$title[] = array($hh->tr->Translate("user_add"),'');
    
    include_once(SERVER_ROOT."/../classes/session.php");
    $session = new Session();
    $postback = $session->Get("postvars");
    $session->Delete("postvars");
    $row = (isset($postback) && is_array($postback))? $postback : array();
}

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
else
{
	if ($ah->current_user_id==$id || $module_admin)
		$input_right = 1;
	if ($module_admin)
		$input_super_right = 1;
	if ($id>0)
	{
		$tabs = array();
		$tabs[] = array('user_data','/users/user.php?id='.$id);
		if($hh->ini->Get("user_show"))
			$tabs[] = array('user_link','/users/user_page.php?id='.$id);
		$tabs[] = array('user_admin','');
		$tabs[] = array('image_associated','/users/user_image.php?id='.$id);
		$tabs[] = array('modules','/users/modules_add.php?id_user='.$id);
		$tabs[] = array('topics','/users/topics_add.php?id_user='.$id);
		$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id);
		$tabs[] = array('services','/users/user_services.php?id='.$id);
		$tabs[] = array('user_stats',($id>0)?'/users/user_stats.php?id='.$id:'');
		echo $hh->Tabs($tabs);
	}
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			login: "required"
<?php if ($id==0) { ?>
			,name: "required",
			email: {
				required: true,
				email: true
			},
			password: {
				required: true,
				minlength: <?=$conf->Get("password_length_min")?>
			},
			password_verify: {
				required: true,
				minlength: <?=$conf->Get("password_length_min")?>,
				equalTo: "#password-field"
			}
<?php } ?>
		}
	});
});
</script>

<?php
	if($conf->Get("user_auth")=="ldap")
	{
		if($id>0)
		{
			echo $hh->input_form_open();
			echo $hh->input_hidden("from","user_admin");
			echo $hh->input_hidden("action2","update");
			echo $hh->input_hidden("id_user",$id);
			echo $hh->input_hidden("p",$current_page);
			echo $hh->input_table_open();
			
			echo $hh->input_date("date","start_date",$row['start_date_ts'],$input_right);
			echo $hh->input_text("login","login",$row['login'],20,0,0);
			if ($input_super_right)
				echo $hh->input_textarea("user_notes","admin_notes",$row['admin_notes'],50,4,"",$input_super_right);
			echo $hh->input_array("language","id_language",$id_language,$ah->Languages(),$input_right);
			echo $hh->input_checkbox("active","active",$active,0,$input_super_right);

			$actions = array();
			$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
			echo $hh->input_actions($actions,$input_right);
			echo $hh->input_table_close() . $hh->input_form_close();
		}
		else
		{
			include_once(SERVER_ROOT."/../classes/users.php");
			$uu = new Users();
			$ausers = $uu->Available();
			$num = count($ausers);
			if($num>0)
			{
				$row = array_slice($ausers,(($current_page - 1 ) * $records_per_page),$records_per_page);
				$ldap_config = $conf->Get("ldap");
				echo $hh->tr->TranslateParams("users_available",array($ldap_config['domain']));
				$table_headers = array('name','email');
				$table_content = array('{LinkTitle("actions.php?from2=user&action3=addldap&login=$row[login]",$row[name])}','$row[email]');
				echo $hh->ShowTable($row, $table_headers, $table_content,$num);
			}
		}
	}
	else
	{
		echo $hh->input_form_open();
		echo $hh->input_hidden("from","user_admin");
		echo $hh->input_hidden("action2",$action2);
		echo $hh->input_hidden("id_user",$id);
		echo $hh->input_hidden("p",$current_page);
		echo $hh->input_table_open();

		echo $hh->input_date("date","start_date",$row['start_date_ts'],$input_right);
		if ($id==0)
		{
			echo $hh->input_text("Full Name","name",$row['name'],30,0,$input_right);
			echo $hh->input_text("email","email",$row['email'],30,0,$input_right);
		}
		else 
		{
			echo $hh->input_text("email","email",$row['email'],30,0,0);
			echo $hh->input_text("bounces","bounces",$row['bounces'],5,0,0);
			echo $hh->input_checkbox($trm28->Translate("verified"),"verified",$row['verified'],0,$input_right);
		}
		echo $hh->input_text("login","login",$row['login'],20,0,$input_super_right,"",true);
		
		if ($id>0) 
			echo $hh->input_text("password","pass","******",20,0,0,($id==$ah->current_user_id || $input_super_right==1)? " (<a href=\"user_password.php?id=".$id."\">" . $hh->tr->Translate("password_change") . "</a>)" : "",true);
		else
		{
			echo $hh->input_text("password","password","",20,0,$input_right);
			echo $hh->input_text("password_verify","password_verify","",20,0,$input_right);
		}
		if ($input_super_right)
			echo $hh->input_textarea("user_notes","admin_notes",$row['admin_notes'],20,4,"",$input_super_right);
		
		echo $hh->input_array("language","id_language",$id_language,$ah->Languages(),$input_right);
		echo $hh->input_array("group","id_group",$row['id_group'],$hh->tr->Translate("users_groups"),$input_right);
		
		echo $hh->input_checkbox("active","active",$active,0,$input_super_right);
	
		$actions = array();
		$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
		if($id>0)
		{
			$actions[] = array('action'=>"reminder",'label'=>"send_password",'right'=>$input_right && $module_admin && $active==1);
			if($row['verified']!="1")
			{
				$actions[] = array('action'=>"email_verify",'label'=>$trm28->Translate("send_email_verify"),'right'=>($input_right && $active==1));
			}
			$actions[] = array('action'=>"emulate",'label'=>"simulation",'right'=>($ah->current_user_id!=$id && ($module_admin || $ah->session->Get("real_user_id")>0)));
		}
		
		echo $hh->input_actions($actions,$input_right);

		echo $hh->input_table_close() . $hh->input_form_close();
	}
}
include_once(SERVER_ROOT."/include/footer.php");
?>


