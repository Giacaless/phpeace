<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

$id = $_GET['id'];

$u = new User;
$mod = Modules::ModuleGet($id);
$modules_names = $hh->tr->Translate("modules_names");

$title[] = array('modules_mng','modules.php');
$title[] = array($hh->tr->TranslateParams("module_name",array($modules_names[$id])),'');

echo $hh->ShowTitle($title);

if ($module_admin)
	$input_right = 1;

if (!$mod['active'] || $mod['admin']!="1")
{
	echo "<p>" . $hh->tr->Translate("module_locked") . "</p>\n";
	$input_right = 0;
}

echo "<div class=\"box\"><h2>" . $hh->tr->Translate("module") . "</h2>";
?>
<script type="text/javascript">
function check_form(action)
{
	f=document.forms['form1'];
	if (f.name.value=="")
		alert("<?=$hh->tr->Translate("missing_name");?>");
	else
		f.submit();
}
</script>
<form name="form1" action="actions.php" method="post">
<input type="hidden" name="id_module" value="<?=$id;?>">
<input type="hidden" name="from" value="module">
<input type="hidden" name="action2" value="update">
<table border="0">
<?php
echo $hh->input_text("module","name",$modules_names[$id],30,0,0);
echo $hh->input_checkbox("module_open","restricted",(($mod['restricted']=="1")?0:1),0,$input_right);
echo $hh->input_submit("submit","check_form()",$input_right);
?>
</table>
</form>
</div>
<?php
echo "<div class=\"box\"><h2>" . $hh->tr->Translate("users") . "</h2>";

if ($mod['restricted']==0)
{
	if ($module_admin || $u->ModuleAdmin($id))
		echo "<p>" . $hh->tr->Translate("module_generic_warn") . "</p>\n";
	else
		echo "<p>" . $hh->tr->Translate("module_generic") . "</p>\n";
}

$num = Modules::ModuleUsers( $row, $id );

$table_headers = array('user','administrator');
$table_content = array('{LinkTitle("user.php?id=$row[id_user]",$row[name])}','{Bool2YN($row[is_admin])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if (($input_right || $u->ModuleAdmin($id)) && $mod['active'])
{
  echo "<p><a href=\"module_add.php?id=$id&g=user\">" . $hh->tr->Translate("module_user_add") . "</a></p>\n";
  echo "<p><a href=\"module_add.php?id=$id&g=admin\">" . $hh->tr->Translate("module_admin_add") . "</a></p>\n";
}

echo "</div>";
include_once(SERVER_ROOT."/include/footer.php");
?>
