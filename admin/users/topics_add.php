<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/topics.php");
include_once(SERVER_ROOT."/../classes/user.php");

$u = new User;

$id_user = $_GET['id_user'];

$title[] = array('users_list','users.php?p='.$current_page);

$u->id = $id_user;
$row = $u->UserGet();
$title[] = array($row['name'],'user.php?id='.$id_user);
$title[] = array('topics','');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
elseif($id_user>0)
{
	$tabs = array();
	$tabs[] = array('user_data','/users/user.php?id='.$id_user);
	if($hh->ini->Get("user_show"))
		$tabs[] = array('user_link','/users/user_page.php?id='.$id_user);
	$tabs[] = array('user_admin','/users/user_admin.php?id='.$id_user);
	$tabs[] = array('image_associated','/users/user_image.php?id='.$id_user);
	$tabs[] = array('modules','/users/modules_add.php?id_user='.$id_user);
	$tabs[] = array('topics','');
	$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id_user);
	$tabs[] = array('user_stats','/users/user_stats.php?id='.$id_user);
	echo $hh->Tabs($tabs);
?>
		
<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="user_topics">
<input type="hidden" name="action2" value="topic_add">
<input type="hidden" name="id_user" value="<?=$id_user;?>">
<?php
$topics = new Topics;

$topics_user = array();
$topics_all = $topics->TopicsUser($id_user);

$topic_group = "";
$counter = 0;
$tot = count($topics_all);
echo "<ul>";
foreach($topics_all as $topic)
{
	if($topic['group_name']!=$topic_group)
	{
		if($counter>0)
			echo "</ul></li>\n";
		echo "<li>{$topic['group_name']}<ul>\n";
		$topic_group = $topic['group_name'];
	}
	echo "<li>";
	if ($input_right==1)
	{
		
		
		echo "<input type=\"checkbox\"  class=\"input-checkbox\" name=\"{$topic['id_topic']}\"";
		if ($topic['id_user']>0)
			echo " checked ";
		echo ">";
	}
	echo "{$topic['name']}</li>";
	$counter ++;
	if($counter==$tot)
	{
		$input .= "</ul></li>\n";
	}
}
echo "</ul>";
if ($input_right==1)
	echo "<p><input type=\"submit\" class=\"input-submit\" value=\"" . $hh->tr->Translate("submit") . "\"></p>\n";

?>
</form>

<?php
}
include_once(SERVER_ROOT."/include/footer.php");
?>


