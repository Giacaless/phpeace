<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");


echo $hh->ShowTitle($title);
?>

<p><a href="users.php"><?=$hh->tr->Translate("users_list");?></a></p>

<p><a href="modules.php"><?=$hh->tr->Translate("modules_mng");?></a></p>

<p><a href="topics.php"><?=$hh->tr->Translate("topics_mng");?></a></p>

<p><a href="services.php"><?=$hh->tr->Translate("services");?></a></p>

<?php
if ($module_admin)
{
	echo "<p><a href=\"user_admin.php?id=0\">" . $hh->tr->Translate("user_add") . "</a></p>\n";
}

if ($module_admin || $ah->session->Get("real_user_id")>0)
	echo "<p><a href=\"emulation.php\">" . $hh->tr->Translate("simulation") . "</a></p>\n";

if ($ah->session->Get("real_user_id")>0)
{
	echo "<p>" . $hh->tr->TranslateParams("simulation_running",array($ah->session->Get("current_user_login"),$ah->session->Get("real_user_login"))) . "<a href=\"actions.php?from2=user&action3=emulationend\">" . $hh->tr->Translate("simulation_end") . "</a></p>\n";
}
if ($module_admin)
	echo "<p><a href=\"config.php\">" . $hh->tr->Translate("configuration") . "</a></p>\n";

?>
<form action="find.php" method="get">
<table border=0 cellpadding=1 cellspacing=3>
<?php
echo $hh->input_text("<input type=\"submit\" class=\"input-submit\" value=\"" . $hh->tr->Translate("search") . "\">","u","",20,0,1);
?>
</table>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
