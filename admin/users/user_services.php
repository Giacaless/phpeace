<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");
include_once(SERVER_ROOT."/../classes/services.php");

$id = $get['id'];

$se = new Services();

$u = new User;
$u->id = $id;
$row = $u->UserGet();
$active = $row['active'];
if ($active!=1)
{
	$ah->MessageSet("user_no_active_warn");
}
$title[] = array('users_list','users.php?p='.$current_page);
$title[] = array($row['name'],'user.php?id='.$id);
$title[] = array('services','');

echo $hh->ShowTitle($title);

if ($ah->session->Get("real_user_id")>0)
	echo "<p>" . $hh->tr->Translate("simulation_no_user") . "</p>\n";
else
{
	if ($id>0)
	{
		$tabs = array();
		$tabs[] = array('user_data','/users/user.php?id='.$id);
		if($hh->ini->Get("user_show"))
			$tabs[] = array('user_link','/users/user_page.php?id='.$id);
		$tabs[] = array('user_admin','/users/user_admin.php?id='.$id);
		$tabs[] = array('image_associated','/users/user_image.php?id='.$id);
		$tabs[] = array('modules','/users/modules_add.php?id_user='.$id);
		$tabs[] = array('topics','/users/topics_add.php?id_user='.$id);
		$tabs[] = array('responsibilities','/users/user_roles.php?id='.$id);
		$tabs[] = array('services','');
		$tabs[] = array('user_stats',($id>0)?'/users/user_stats.php?id='.$id:'');
		echo $hh->Tabs($tabs);
	}
}

$rows = array();
$num = $se->UserServices($rows,$id);

$table_headers = array('name','mobile');
$table_content = array('{LinkTitle("user_service.php?id='.$id.'&id_user_service=$row[id_user_service]",$row[service_name])}','{Bool2YN($row[mobile])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

$services = $se->UserServicesAvailable($id);
if($module_admin && count($services)>0)
	echo "<p><a href=\"user_service.php?id=$id&id_user_service=0\">" . $hh->tr->Translate("add_new") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>


