<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

if ($module_admin)
	$input_right=1;

$title[] = array('Registration','');
echo $hh->ShowTitle($title);

echo "<p>";

if ($phpeace->AmIAdmin())
{
	$trm17 = new Translator($hh->tr->id_language,17);

	if (!empty($phpeace->phpeace_server))
	{
		if ($phpeace->IsRegistered())
		{
	    	if (!$phpeace->registered)
	    	{
		    	$ini->Set("registered","1");
	    		$phpeace->ServerUpdate();
	    	}
			echo $trm17->TranslateParams("registered",array($phpeace->phpeace_server));
		}
	    else
		{
	    	if ($phpeace->registered)
		    	$ini->Set("registered","0");
			echo $trm17->TranslateParams("register",array($phpeace->RegisterLink($hh->tr->id_language)));
		}
	}
	else
	{
		echo $trm17->Translate("server_missing");
	}
}

echo "</p>";

include_once(SERVER_ROOT."/include/footer.php");
?>

