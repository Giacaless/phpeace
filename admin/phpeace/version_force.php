<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('force_update','');

echo $hh->ShowTitle($title);

$phpeace = new PhPeace;

$server = $ini->Get("phpeace_server");

$build = $phpeace->build;

$build_server = $phpeace->ServerBuildNumber();

if ($build == $build_server)
	echo $phpeace->VersionInstall($build, TRUE, TRUE);
else
	echo $hh->tr->Translate("phpeace_install_force_no");

include_once(SERVER_ROOT."/include/footer.php");
?>

