<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$title[] = array('MySQL','');
echo $hh->ShowTitle($title);

if($module_admin)
{
	include_once(SERVER_ROOT."/../classes/varia.php");
	$conf = new Configuration();
	$dbconf = $conf->Get("dbconf");
	
	$va = new Varia();
	$command = "mysqladmin -u {$dbconf['user']} -p{$dbconf['password']} status";
	$output = $va->Exec($command);
	if($va->return_status==0)
	{
		echo "<h3>Status</h3><p>{$output[0]}</p>";
	}

	$db =& Db::globaldb();
	$processes = array();
	$sqlstr = "SHOW FULL PROCESSLIST";
	$db->QueryExe($processes, $sqlstr,false,1);
	
	$table = "<table width='*' border='1' cellspacing='1' cellpadding='3'>\n";
	$table .= "<tr><th>ID</th><th>User</th><th>Host</th><th>DB</th><th>Command</th><th>Time</th><th>State</th><th>Info</th></tr>";
	$threads = array();
	foreach($processes as $process)
	{
		$threads[$l[3]]++;
		$table .= "<tr>\n";
		foreach($process as $val)
			$table .= "<td>$val&nbsp;</td>\n";
		$table .= "</tr>\n";
	}
	$table .= "</table>\n";

	echo "<h3>Threads</h3>\n";
	arsort($threads);
	echo "<ul>";
	foreach($threads as $key=>$value)
	{
		echo "<li>" . (($key!="")?$key:"root") .": $value</li>";
	}
	echo "</ul>";

	echo "<h3>Processes</h3>\n";
	echo $table;

	$rows = array();
	$sqlstr = "SHOW STATUS LIKE 'Qcache%'";
	$db->QueryExe($rows, $sqlstr,false,1);
	if(count($rows)>0)
	{
		echo "<h3>Querycache Status:</h3>\n";
		echo "<ul>";
		foreach($rows as $row)
		{
			echo $hh->ListItem($row['Variable_name'],$row['Value']);
		}
		echo "</ul>";
		
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>
