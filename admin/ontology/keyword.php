<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/ontology.php");
include_once(SERVER_ROOT."/../classes/varia.php");

$trm13 = new Translator($hh->tr->id_language,13);

$id = $_GET['id'];
$id_type = (int)$_GET['id_type'];
$qid_type = $_GET['qid_type'];
$qapproved = $_GET['qapproved'];

$title[] = array('list','keywords.php?id_type=' . $id_type);
if ($id>0)
{
	include_once(SERVER_ROOT."/../classes/keyword.php");
	$k = new Keyword();
	$row = $k->KeywordGet($id);
	$action2 = "update";
	$id_type = $row['id_type'];
	$title[] = array($row['keyword'],'');
	$relations = $k->Relations();
}
else
{
	$action2 = "insert";
	if ($id_type==0)
		$id_type = 1;
	$title[] = array('add_new','');
}

if ($module_admin || $module_right)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			keyword: "required"
		}
	});
});
</script>
<?php
if($id>0) {
    $keyword_url = $ini->Get("pub_web") . "/tools/keyword.php?k={$row['keyword']}";
    echo "<p>Link: <a href=\"$keyword_url\" target=\"_blank\">$keyword_url</a></p>\n";
}

echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("from","keyword");
echo $hh->input_hidden("id_keyword",$id);
echo $hh->input_hidden("qid_type",$id_type);
echo $hh->input_hidden("p",$current_page);
echo $hh->input_table_open();

if($id>0)
	echo $hh->input_text("ID","id",$id,"5",0,0);
echo $hh->input_text($trm13->Translate("keyword"),"keyword",$row['keyword'],"50",0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],80,3,"",$input_right);
echo $hh->input_array("type","id_type",$id_type,$hh->tr->Translate("keyword_types"),$input_right);

if ($id==0)
	echo $hh->input_upload("image","img",50,$input_right);
else
{
	$filename = "keywords/orig/$id".".gif";
	include_once(SERVER_ROOT."/../classes/file.php");
	$fm = new FileManager();
	if ($fm->Exists("uploads/$filename"))
	{
		echo "<tr><td align=\"right\">" . $hh->tr->Translate("image") . "</td><td><img src=\"/images/upload.php?src=$filename\"></td></tr>\n";
		echo $hh->input_upload("substitute_with","img",50,$input_right);
	}
}
if ($id>0)
	echo $hh->input_array("language","id_language",$row['id_language'],$hh->tr->Translate("languages"),$input_right);

$actions = array();
$actions[] = array('action'=>$action2,'label'=>"submit",'right'=>$input_right);
if ($id>0)
{
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$input_right);
}
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

if ($id>0)
{
	echo "<div class=\"box\">";
	if (count($relations)>0)
	{
		echo "<h3>" . $trm13->Translate("statements") . "</h3>\n";
		echo "<ul>";
		foreach($relations as $row)
			echo "<li>{$row['keyword']} / <a href=\"statement.php?id={$row['id_statement']}&from=keyword&id_keyword=$id\">{$row['name']}</a> / {$row['range']}</li>\n";
		echo "</ul>";
	}
	if($id_type=="4")
	{
		$ikeywords = $k->KeywordsInternal($id);
		if(count($ikeywords)>0)
		{
			echo "<h3>" . $hh->tr->Translate("internal_use") . "</h3>\n";
			echo "<ul>";
			foreach($ikeywords as $ikeyword)
				echo "<li><a href=\"/topics/topic_keywords.php?id={$ikeyword['id_topic']}\">{$ikeyword['name']} {$ikeyword['id_keyword']}</a></li>\n";
			echo "</ul>";
		}
	}
	if($ah->ModuleAdmin(10))
	{
		$features = $k->Features();
		if(count($features)>0)
		{
			$v = new Varia();
			$kfeatures = array();
			foreach($features as $feature)
			{
				$kparams = $v->Deserialize($feature['params']);
				if(is_array($kparams) && (int)$kparams['id_keyword']==(int)$id)
					$kfeatures[] = $feature;
			}
			if(count($kfeatures)>0)
			{
				echo "<h3>Features</h3>\n";
				echo "<ul>";
				foreach($kfeatures as $kfeature)
				{
					if (is_numeric($kfeature['gtype']))
					{
						echo "<li>Global: <a href=\"/layout/feature_global.php?id={$kfeature['id_feature']}\">{$kfeature['name']}</a></li>\n";
					}
					else 
					{
						echo "<li>" . ($kfeature['id_style']>0? $kfeature['style_name'] : "Common") . ": <a href=\"/layout/feature.php?id={$kfeature['id_feature']}\">{$kfeature['name']}</a></li>\n";
					}
				}
				echo "</ul>";
			}
		}
	}
	$uses = $k->Types();
	if (count($uses)>0)
	{
		echo "</div><div class=\"box\">\n";
		echo "<h3>" . $trm13->TranslateParams("keyword_use",array(count($uses))) . "</h3>\n<ul>";
		foreach($uses as $use)
			echo $hh->KeywordUse($id,$use['id_type']);
		echo "</ul>\n";
	}
	echo "</div>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>

