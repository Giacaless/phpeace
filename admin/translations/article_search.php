<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_topic = $_GET['id_topic'];

$title[] = array('Cerca','');
echo $hh->ShowTitle($title);
?>

<form method="get" action="article_search2.php" name="form1">
<table border=0 cellpadding=0 cellspacing=7>
<?php
echo $hh->input_text("titolo","title","","30",0,1);
echo $hh->input_text("autore","author","","30",0,1);
echo $hh->input_text("testo","content","","30",0,1);

include_once(SERVER_ROOT."/../classes/articles.php");
$period = Articles::Period();
echo $hh->input_date("dal","written1",$period['min_art_ts'],1);
echo $hh->input_date("al","written2",$period['max_art_ts'],1);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->AllTopics();
echo $hh->input_topics($id_topic,0,$topics,"all_option",1);

?>
<tr><td>&nbsp;</td><td><input type="submit" class="input-submit" value="cerca">&nbsp;&nbsp;<input type="reset" value="cancella"></td></tr>
</table>
</form>

<p><b>titolo</b> cerca nel titolo, nell'occhiello e nel sommario
<br><b>testo</b> in tutti i campi testuali
</p>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
