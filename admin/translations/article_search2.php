<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/articles.php");

$title[] = array('search','article_search.php');
$title[] = array('results','');
echo $hh->ShowTitle($title);

$params = array(	'title' => $get['title'],
					'author' => $get['author'],
					'content' => $get['content'],
					'id_topic' => $get['id_topic'],
					'id_subtopic' => $get['id_subtopic'],
					'id_article' => $get['id_article'],
					'written1' => $get['written1_y']."-".$get['written1_m']."-".$get['written1_d'],
					'written2' => $get['written2_y']."-".$get['written2_m']."-".$get['written2_d']
					);

$num = Articles::Search( $row, $params );

$table_headers = array('date','author','tematica','in','article',' ');
$table_content = array('{FormatDate($row[written_ts])}','$row[author]','$row[topic_name]','{PathToSubtopic($row[id_topic],$row[id_subtopic])}',
'<div>$row[halftitle]</div>
<div><a href=\"/articles/article.php?id=$row[id_article]&w=tra\">$row[headline]</a> </div><div>$row[subhead]</div>',
'{LinkTitle("/translations/translation.php?id=0&id_a=$row[id_article]","traduci")}');

echo "<p>Clicca sul titolo dell'articolo del quale vuoi richiedere la traduzione.<br/>
In alternativa puoi aprire l'articolo per visualizzarlo e richiederne in seguito la traduzione cliccando sull'apposito link in testa alla pagina dell'articolo.</p>\n";
echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

