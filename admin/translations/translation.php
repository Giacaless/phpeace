<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/translations.php");
include_once(SERVER_ROOT."/../classes/article.php");

$tra = new Translations();

$id_translation = $_GET['id'];
$id_article = $_GET['id_a'];

if ($id_translation>0)
{
	$row = $tra->TranslationGet( $id_translation );
	$action2 = "update";
	$id_article = $row['id_article'];
	$priority = $row['priority'];
	$id_language = $row['id_language'];
	$source_url = $row['source_url'];
}
else
{
	$action2 = "insert";
	$priority = 1;
	$source_url = '';
}
$title[] = array('Traduzione','');

if ($module_right || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

if ($id_article>0)
{
	$a = new Article($id_article);
	$art = $a->ArticleLoad();
	$id_language = $art['id_language'];
	$existing_translations = $tra->TranslationsGetByArticleOriginal($id_article,$id_translation);
	if(count($existing_translations))
	{
		echo "<h3>Traduzioni gia' richieste per questo articolo</h3><ul>";
		foreach($existing_translations as $extra)
			echo "<li>" . $hh->FormatDate($extra['start_date_ts']) .": <a href=\"translation.php?id={$extra['id_translation']}\"><strong>" . $tra->languages[$extra['id_language_trad']] . "</strong></a>" . ($extra['completed']=="1"?" (Completato)":"") . "</li>";
		echo "</ul>";
	}
	if(!$id_translation>0) {
	    $source_url = $art['source'];
	}
}
?>
<script type="text/javascript">
function doaction(myaction)
{
	f=document.forms['form1'];
	f.action2.value = myaction;
	boolOK = true;
	strAlert = "";
	if (!f.id_language.value>0)
	{
		strAlert = strAlert + "Manca la lingua originale\n";
		boolOK = false;
	}
	if (!f.id_language_trad.value>0)
	{
		strAlert = strAlert + "Manca la lingua in cui tradurre\n";
		boolOK = false;
	}
	if (boolOK==true)
		f.submit();
	else
		alert(strAlert);
}
</script>

<form action="actions.php" method="post" name="form1">
<input type="hidden" name="from" value="translation">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="id_translation" value="<?=$id_translation;?>">
<input type="hidden" name="id_article" value="<?=$id_article;?>">
<input type="hidden" name="id_language_trad2" value="<?=$row['id_language_trad'];?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
if ($row['start_date_ts']>0)
	echo $hh->input_date("date","start_date",$row['start_date_ts'],$input_right);

if ($id_article>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($art['id_topic']);
	echo $hh->input_text("articolo da tradurre","headline",$t->name . ": " . $art['headline'],40,0,0,"(<a href=\"/articles/article.php?id=$id_article&w=tra\">vedi originale</a>)");
}
else
	echo $hh->input_note("Inserisci nelle note il link all'articolo da tradurre e ogni altra informazione utile");
echo $hh->input_array("lingua originale","id_language",$id_language,$tra->languages,($id_article>0)? 0:$input_right);
echo $hh->input_text("link","source_url",$source_url,70,0,$input_right);
echo $hh->input_textarea("informazioni aggiuntive,<br/>note e commenti<br/>da inviare ai traduttori","notes",$row['notes'],75,4,"",$input_right);
echo $hh->input_array("priority","priority",$priority,$hh->tr->Translate("priorities"),$input_right);

$lang_available = $tra->LanguagesAvailable($id_language);
$lang_available[0] = $hh->tr->Translate("choose_option");

echo $hh->input_array(($id_article>0)?"lingue disponibili<br/>per la traduzione":"lingua della traduzione","id_language_trad",$row['id_language_trad'],$lang_available,$input_right);

if ($row['id_language_trad']>0)
{
	$translators = $tra->TranslatorsLanguage($row['id_language'],$row['id_language_trad']);	
	echo $hh->input_row("traduttore","id_user",$row['id_user']>0? $row['id_user'] : $ah->current_user_id,$translators,"--",0,$input_right);
	$tra_users = $tra->Users();
	
	if ($row['id_article_trad']>0)
	{
		$atra = new Article($row['id_article_trad']);
		$atra->ArticleLoad();
		echo $hh->input_text("oppure","translator",$row['translator'],40,0,$input_right);
		echo $hh->input_note("Articolo tradotto: <a href=\"/articles/article.php?w=tra&id_translation=$id_translation&id={$row['id_article_trad']}\">{$atra->headline}</a>");
		echo $hh->input_row("revisore","id_rev",$row['id_rev'],$tra_users,"--",0,$input_right);
		echo $hh->input_row("adattatore","id_ad",$row['id_ad'],$tra_users,"--",0,$input_right);
		echo $hh->input_textarea("note sulla traduzione","comments",$row['comments'],75,6,"",$input_right);
		echo $hh->input_checkbox("completato","completed",$row['completed'],0,$input_right);
	}		
}

echo "<tr><td>&nbsp;</td><td>";

if ($input_right==1 && count($tra->LanguagesAvailable($id_language))>0)
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") . "\" onClick=\"doaction('$action2')\">";

if (!($row['id_user']>0) && $row['id_language_trad']>0)
{
	$can_translate = FALSE;
	foreach($translators as $translator)
		if ($translator['id_user']==$ah->current_user_id)
			$can_translate = TRUE;
	if ($can_translate)
		echo "<input type=\"button\" value=\"" . $hh->tr->Translate("take_translation") . "\" onClick=\"doaction('take')\">";
}

if ($row['id_user']>0 && $row['id_language_trad']>0 && !$row['id_article_trad']>0)
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("translation_insert") . "\" onClick=\"doaction('translate')\">";

if ($module_admin && !$row['completed'] && !$row['id_article_trad']>0)
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("delete") . "\" onClick=\"doaction('delete')\">";

echo "</td></tr>\n";

?>
</table>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>
