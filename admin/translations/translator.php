<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/translations.php");

$id_user = $_GET['id'];

$title[] = array('Elenco traduttori','translators.php');

include_once(SERVER_ROOT."/../classes/user.php");
$u = new User;
$u->id = $id_user;
$row = $u->UserGet();

$title[] = array($row['name'],'');

if ($ah->current_user_id==$id_user || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
$tra = new Translations();

echo "<h3>{$row['name']}</h3>";
if($ah->ModuleAdmin(1))
	echo "<div class=\"notes\">(<a href=\"/users/user.php?id=$id_user\">scheda utente</a>)</div>\n";
echo "<ul>";
echo "<li><a href=\"translations.php?s=atwork&id_user=$id_user\">Traduzioni in corso</a> (" . $tra->TranslationsUserP( $row, "atwork", $id_user ) . ")</li>";
echo "<li><a href=\"translations.php?id_user=$id_user\">Traduzioni completate</a> (" . $tra->TranslationsUserP( $row, "", $id_user ) . ")</li>";
echo "</ul>";

echo "<h3>Competenze linguistiche</h3>\n";
$languages = $tra->TranslatorLanguages($id_user);
if (count($languages)>0)
{
	echo "<ul>\n";
	foreach($languages as $language)
		echo "<li>" . $hh->tr->Translate("from") . " " . ($tra->languages[$language['id_language_from']])  . " " . $hh->tr->Translate("to") . " " . ($tra->languages[$language['id_language_to']]). "</li>\n";
	echo "</ul>\n";
}
echo "<a href=\"translator_languages.php?id=$id_user\">modifica</a>\n";


include_once(SERVER_ROOT."/include/footer.php");
?>
