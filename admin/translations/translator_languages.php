<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/translations.php");


$id_user = $_GET['id'];

$title[] = array('Elenco traduttori','translators.php');

include_once(SERVER_ROOT."/../classes/user.php");
$u = new User;
$u->id = $id_user;
$row = $u->UserGet();

$title[] = array($row[name],'translator.php?id='.$id_user);
$title[] = array("Lingue",'');

if ($ah->current_user_id==$id_user || $module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
$tra = new Translations();

echo "<h3>Lingue</h3>\n";
$languages = $tra->TranslatorLanguages($id_user);
if (count($languages)>0)
{
	echo "<ul>\n";
	foreach($languages as $language)
	{
		echo "<li>" . $hh->tr->Translate("from") . " " . ($tra->languages[$language['id_language_from']])  . " " . $hh->tr->Translate("to") . " " . ($tra->languages[$language['id_language_to']]);
		if ($input_right)
			echo " (<a href=\"actions.php?from2=translator_language&action3=remove&id=$id_user&id_f={$language['id_language_from']}&id_t={$language['id_language_to']}\">rimuovi</a>)";
		echo "</li>\n";
	}
	echo "</ul>\n";
}

if ($input_right)
{
?>
<form action="actions.php" method="post" name="form1">
<input type="hidden" name="from" value="translator_language">
<input type="hidden" name="action2" value="update">
<input type="hidden" name="id_user" value="<?=$id_user;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
echo $hh->input_separator("Competenze linguistiche");
$languages = $hh->tr->Translate("languages");
asort($languages);
echo $hh->input_array("from","id_language_from","",$languages,$input_right);
echo $hh->input_array("to","id_language_to","",$languages,$input_right);
echo $hh->input_submit("submit","",$input_right);
?>
</table>
</form>

<?php
}

include_once(SERVER_ROOT."/include/footer.php");
?>
