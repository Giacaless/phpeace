<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/events.php");
$ee = new Events();

$months = $hh->tr->Translate("month");

$title[] = array('anniversaries_per_months','');
echo $hh->ShowTitle($title);

$num = $ee->RecurringMonths( $row );
foreach($row as &$row_item)
{
	$row_item['month_name'] = $months[$row_item['r_month']-1];
}

$table_headers = array('month_item','anniversary');
$table_content = array('{LinkTitle("recurring_events.php?approved=1&id_month=$row[r_month]",$row[month_name])}','<div class=\"right\">$row[num_events]</div>');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

if ($module_right || $module_admin)
	echo "<p><a href=\"recurring_event.php?id=0&id_month=0\">" . $hh->tr->Translate("anniversary_add") . "</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
