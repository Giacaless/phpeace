<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/events.php");

$title[] = array('search','search.php');
$title[] = array('results','');

echo $hh->ShowTitle($title);

$params = array(	'start_date1' => $get['start_date1_y']."-".$get['start_date1_m']."-".$get['start_date1_d'],
					'start_date2' => $get['start_date2_y']."-".$get['start_date2_m']."-".$get['start_date2_d']
					);

$search_words = $hh->th->SearchWords($_GET['content'],$hh->tr->id_language);

$rows = array();
$ee = new Events();
$num = $ee->Search($rows,$search_words,$params,(int)$get['id_topic'],false);

$table_headers = array('when','type','title','placing','approved');
$table_content = array('{FormatDateTime($row[start_date_ts])','$row[type]','{LinkTitle("event.php?id=$row[id_event]",$row[title])}','$row[place]','{Bool2YN($row[approved])}');

echo $hh->ShowTable($rows, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

