<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");

$id = $_GET['id'];
$w = $_GET['w'];

$a = new Article($id);
$article = $a->ArticleGet();
if ($article['id_user']==$ah->current_user_id)
	$input_right = 1;

$id_topic = $article['id_topic'];
include_once(SERVER_ROOT."/../classes/topic.php");
$t = new Topic($id_topic);

if ($t->AmIAdmin() || $module_admin || $ah->ModuleAdmin(4))
	$input_right = 1;

if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id=' . $id_topic);
	$title[] = array('articles_list','/topics/articles.php?id=' . $id_topic);
}
else
	$title[] = array('list','articles.php');

$title[] = array($article['headline'],'/articles/article.php?w='.$w.'&id='.$id.'&p='.$current_page);
$title[] = array("delete",'');

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("id_article",$id);
echo $hh->input_hidden("id_topic",$id_topic);
echo $hh->input_hidden("from","article_delete");
echo $hh->input_hidden("w",$w);
echo $hh->input_hidden("p",$current_page);

echo "<p>" . $hh->tr->TranslateParams("article_delete",array($article['headline'])) . "</p>";

echo $hh->input_table_open();
echo $hh->input_separator("are_you_sure");
$actions = array();
$actions[] = array('action'=>"delete_ok",'label'=>"yes",'right'=>$input_right);
$actions[] = array('action'=>"delete_no",'label'=>"no",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


