<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/doc.php");

$id = (int)$_GET['id'];
$id_article = (int)$_GET['id_article'];
$id_topic = (int)$_GET['id_topic'];
$w = (!isset($_GET['w']))? $_GET['w'] : "topics";

if ($id_article>0)
{
	include_once(SERVER_ROOT."/../classes/article.php");
	$a = new Article($id_article);
	$a->ArticleLoad();
	$title1[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
	$id_topic = $a->id_topic;
	if ($a->id_user==$ah->current_user_id)
		$input_right = 1;
}
else
	$title1[] = array('docs_archive','/topics/docs.php?id='.$id_topic);

if ($id_topic>0)
{
	include_once(SERVER_ROOT."/../classes/topic.php");
	$t = new Topic($id_topic);
	if ($t->AmIAdmin() || $ah->ModuleAdmin(4))
	{
		$input_right = 1;
	}
	if ($w=="topics")
	{
		$ah->ModuleForce(4);
		$module_admin = $ri->ModuleAdmin();
		$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
		if($id_article>0)
			$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
	}
	else
		$title[] = array('list','articles.php');
	$title = array_merge($title,$title1);
}
else
{
	$ah->ModuleForce(14);
	$module_admin = $ri->ModuleAdmin();
	$title[] = array('doc_orphans','/admin/doc_orphans.php');
}

if ($module_admin)
{
	$input_right = 1;
}

$d = new Doc($id);
$doc = $d->DocGet();
if ($ah->current_user_id==($d->CreatorId()))
	$input_right = 1;
if($d->AdminRight())
	$input_right = 1;

$title[] = array($doc['title'],"doc.php?id=$id&id_topic=$id_topic&id_article=$id_article&w=$w");
$title[] = array('tokens','');

echo $hh->ShowTitle($title);

$row = array();
$num_tokens = $d->Tokens($row);

$table_headers = array('token','expire_date','max_downloads','counter','active');
$table_content = array('{LinkTitle("/articles/doc_token.php?id=$row[id_doc]&id_article='.$id_article.'&token=$row[token]&id_topic='.$id_topic.'&w=topics",$row[token])}',
'{FormatDate($row[expire_date_ts],$row[expires])}','<div class=\"right\">$row[max_downloads]</div>','<div class=\"right\">$row[counter]</div>','{Bool2YN($row[active])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num_tokens);

if($input_right)
{
	echo "<p><a href=\"doc_token.php?id=$id&id_article=$id_article&id_topic=$id_topic&w=$w\">" . $hh->tr->Translate("add_new") . "</a></p>";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
