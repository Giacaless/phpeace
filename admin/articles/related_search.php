<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/topic.php");

// INIT
$id_article = $_GET['id_article'];
$w = $_GET['w'];
if (!isset($w))
	$w = "topics";
$view = $_GET['view'];

$a = new Article($id_article);
$a->ArticleLoad();
$id_topic = $a->id_topic;
$t = new Topic($id_topic);

// TITLES
if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
}
else
	$title[] = array('articles_list','articles.php');
$title[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$title[] = array('articles_related','');
echo $hh->ShowTitle($title);

echo $hh->tr->TranslateParams("article_related_search",array($t->name,"id_article=$id_article&id_topic=$id_topic&w=$w","id_article=$id_article&w=$w"));

echo $hh->input_form("get","related_search2.php");
echo $hh->input_hidden("id_article",$id_article);
echo $hh->input_hidden("w",$w);
echo $hh->input_table_open();
echo $hh->input_text("title","title","","30",0,1);
echo $hh->input_text("author","author","","30",0,1);
echo $hh->input_text("text","content","","30",0,1);

include_once(SERVER_ROOT."/../classes/articles.php");
$period = Articles::Period();
$min = $period['min_art_ts']<0? 1 : $period['min_art_ts'];
echo $hh->input_date("from","written1",$min,1);
echo $hh->input_date("to","written2",$period['max_art_ts'],1);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
$topics = $tt->AllTopics();
echo $hh->input_topics($id_topic,0,$topics,"all_option",1);

echo $hh->input_link("subtopic","id_subtopic","'article_subtopic.php?id_topic='+document.forms['form1'].id_topic.value+'&id_subtopic='+document.forms['form1'].id_subtopic.value",0,"",1);
echo $hh->input_submit("submit","",1);
echo $hh->input_table_close() . $hh->input_form_close();

echo  $hh->tr->Translate("search_notes");

include_once(SERVER_ROOT."/include/footer.php");
?>

