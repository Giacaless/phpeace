<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/htmlhelper.php");
include_once(SERVER_ROOT."/../classes/articles.php");
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth(false);
$hh = new HtmlHelper();
$fh = new FormHelper();
$get = $fh->HttpGet();

$page_title = $hh->tr->Translate("article_related");
include_once(SERVER_ROOT."/../admin/include/head.php");
?>
<body>
<script type="text/javascript">
function set_article(id_article,headline)
{
	opener.document.forms['form1'].id_article.value = id_article;
	opener.document.forms['form1'].descriz_id_article.value = headline;
	self.close();
}
</script>
<p><?=$hh->tr->Translate("article_related_choose");?></p>

<?php
$w = $get['w'];

// PAGING GLOBALS
$conf = new Configuration();
$records_per_page = $conf->Get("records_per_page");
$current_page = ($get['p'] > 0)? $get['p'] : 1;

$params = array(	'title' => $get['title'],
					'author' => $get['author'],
					'content' => $get['content'],
					'id_topic' => $get['id_topic'],
					'id_subtopic' => $get['id_subtopic'],
					'id_article' => $get['id_article'],
					'written1' => $get['written1_y']."-".$get['written1_m']."-".$get['written1_d'],
					'written2' => $get['written2_y']."-".$get['written2_m']."-".$get['written2_d']
					);

$num = Articles::Search( $rows, $params );

foreach($rows as $row)
{
	$row['headline_js'] = $hh->th->TextReplaceForJavascript($row['topic_name'].": ".$row['headline']);
	$rows2[] = $row;
}

$table_headers = array('date','author','topic','in','title');
if ($w=="book" || $w=="event")
	$table_content = array('{FormatDate($row[written_ts])}','$row[author]','$row[topic_name]','{PathToSubtopic($row[id_topic],$row[id_subtopic])}',
	'<div>$row[halftitle]</div><div><a href=\"#\" onClick=\"set_article(\'$row[id_article]\',\'$row[headline_js]\')\">$row[headline]</a></div><div>$row[subhead]</div>');

echo $hh->showTable($rows2, $table_headers, $table_content, $num);

?>
</body>
</html>
