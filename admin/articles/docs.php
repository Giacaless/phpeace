<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/article.php");
include_once(SERVER_ROOT."/../classes/topic.php");
include_once(SERVER_ROOT."/../classes/docs.php");

$id_article = $_GET['id_article'];
$id_topic = $_GET['id_topic'];
$id_topic2 = $_GET['id_topic2'];

$w = $_GET['w'];
if (!isset($w))
	$w = "topics";

$a = new Article($id_article);
$a->ArticleLoad();
if (!isset($id_topic))
	$id_topic = $a->id_topic;

$t = new Topic($id_topic);
if ($w=="topics")
{
	$ah->ModuleForce(4);
	$title[] = array($t->name,'/topics/ops.php?id='.$id_topic);
	$title[] = array('articles_list','/topics/articles.php?id='.$id_topic);
}
else
	$title[] = array('list','articles.php');
$title[] = array($a->headline,'article.php?w='.$w.'&id='.$id_article);
$title[] = array('document','');

echo $hh->ShowTitle($title);

if ($id_topic2>0)
{
	$t2 = new Topic($id_topic2);
	$topic_name = $t2->name;
	$my_id_topic = $id_topic2;
}
else
{
	$topic_name = $t->name;
	$my_id_topic = $id_topic;
}
echo "<p>" . $hh->tr->TranslateParams("topic_docs",array($topic_name));

echo $hh->tr->TranslateParams("choose_doc",array($a->headline,$id_article,$w));

$row = array();
$num = Docs::Topic( $row, $my_id_topic );

$table_headers = array('document','description','file');
$table_content = array('{LinkTitle("../articles/doc.php?id=$row[id_doc]&id_article='.$id_article.'&w='.$w.'",$row[title])}','$row[description]','{FileSize("docs/$row[id_doc].$row[format]")}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

