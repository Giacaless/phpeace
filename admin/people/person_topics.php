<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/people.php");

$pe = new People();

$id_p = $_GET['id'];

$row = $pe->UserGetDetailsById($id_p);

$title[] = array($row['name1'] . " " . $row['name2'],'');

echo $hh->ShowTitle($title);

$tabs = array();
$tabs[] = array('user_data','/people/person.php?id='.$id_p);
if($ah->IsModuleActive(29))
    $tabs[] = array('Associations',$id_p>0? '/people/person_association.php?id='.$id_p:'');
$tabs[] = array('user_admin','/people/person_admin.php?id='.$id_p);
$tabs[] = array('groups','/people/person_groups.php?id='.$id_p);
$tabs[] = array('history','/people/person_history.php?id='.$id_p);
$tabs[] = array('topics','');
$tabs[] = array('user_stats','/people/person_stats.php?id='.$id_p);
if($conf->Get("track") || $conf->Get("track_all"))
	$tabs[] = array('visits','/people/visits.php?id_p='.$id_p);
echo $hh->Tabs($tabs);

$trm28 = new Translator($hh->tr->id_language,28);

if ($module_admin)
	$input_right = 1;

?>
		
<form name="form1" action="actions.php" method="post">
<input type="hidden" name="from" value="person_topics">
<input type="hidden" name="action2" value="topic_add">
<input type="hidden" name="id_p" value="<?=$id_p;?>">
<?php


$topics_user = array();
$topics_all = $pe->UserTopicsAll($id_p);

$topic_group = "";
$counter = 0;
$tot = count($topics_all);
echo "<ul>";
foreach($topics_all as $topic)
{
	if($topic['group_name']!=$topic_group)
	{
		if($counter>0)
			echo "</ul></li>\n";
		echo "<li>{$topic['group_name']}<ul>\n";
		$topic_group = $topic['group_name'];
	}
	echo "<li>";
	if ($input_right==1)
	{
		echo "<input type=\"checkbox\"  class=\"input-checkbox\" name=\"{$topic['id_topic']}\"";
		if ($topic['contact']>0)
			echo " checked ";
		echo ">";
	}
	echo $hh->Wrap($topic['name'],"<a href=\"/topics/visitor.php?id={$topic['id_topic']}&id_p=$id_p\">","</a></li>",$topic['id_p']>0);
	$counter ++;
	if($counter==$tot)
	{
		$input .= "</ul></li>\n";
	}
}
echo "</ul>";
if ($input_right==1 && $tot>0)
	echo "<p><input type=\"submit\" class=\"input-submit\" value=\"" . $hh->tr->Translate("submit") . "\"></p>\n";

?>
</form>

<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

