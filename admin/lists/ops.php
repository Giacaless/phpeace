<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/list.php");

$id = $_GET['id'];

$ml = new MailingList($id);
$list = $ml->GetList();

if ($ah->current_user_id==$list['id_user'] || $module_admin)
	$input_right = 1;

$title[] = array($list['email'],'');
echo $hh->ShowTitle($title);

if ($input_right==1)
{
	echo "<p>Operazioni sulla mailing-list <b>{$list['email']}</b></p>\n";

	echo $hh->input_form_open();
	echo $hh->input_hidden("from","list_command");
	echo $hh->input_hidden("id_list",$id);
	echo $hh->input_table_open();
	echo $hh->input_text("email","email","",40,0,$input_right);
	
	$actions = array();
	$actions[] = array('action'=>"subscribe",'label'=>"Iscrizione",'right'=>$input_right);
	$actions[] = array('action'=>"unsubscribe",'label'=>"Cancellazione",'right'=>$input_right);
	if ($list['impl']=="1")
		$actions[] = array('action'=>"checkdist",'label'=>"Verifica iscrizione",'right'=>$input_right);
	$actions[] = array('action'=>"showdist",'label'=>"Elenco iscritti",'right'=>$input_right);
	if ($list['impl']=="1")
	{
		$actions[] = array('action'=>"showlog",'label'=>"Invia log all'owner",'right'=>$input_right);
		$actions[] = array('action'=>"wipelog",'label'=>"Cancella log",'right'=>$input_right);
	}
	echo $hh->input_actions($actions,$input_right);
	echo $hh->input_table_close() . $hh->input_form_close();
	
	$owner_email = $list['owner_email'];
?>

<p>Tutte queste operazioni vengono effettuate direttamente, senza richiesta di un messaggio di conferma
<br>E' possibile effettuare operazioni solo per le liste delle quali si e' owner, o nel caso si sia amministratore del modulo.
<br>I risultati di queste operazioni verranno sempre inviati all'owner <b><?=$owner_email; ?></b>.
</p>

<p><a href="list.php?id=<?=$id; ?>">Modifica parametri fondamentali della mailing list</a>

<?php
}
else
{
	echo "<p>Non risulti abilitato per questa lista</p>\n";
}

include_once(SERVER_ROOT."/include/footer.php");
?>
