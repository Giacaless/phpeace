<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/lists.php");

$id_group = $_GET['id'];

if ($module_admin)
	$input_right = 1;

$li = new Lists();

$title[] = array('Gruppi','groups.php');

if ($id_group>0)
{
	$row = $li->GroupGet($id_group);
	$title[] = array('Modifica gruppo','');
}
else
{
	$title[] = array('Inserisci nuovo gruppo','');
}

echo $hh->ShowTitle($title);

$lists = $li->GroupLists($id_group, true,false);
if (count($lists)>0)
	echo "<p>Liste appartenenti a questo gruppo: " . $hh->th->ArrayMulti2StringIndex($lists,'email') . "</p>";

echo $hh->input_form_open();
echo $hh->input_hidden("from","group");
echo $hh->input_hidden("id_group",$id_group);
echo $hh->input_table_open();

echo $hh->input_text("nome","name",$row['name'],30,0,$input_right);
echo $hh->input_submit("submit","",$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


