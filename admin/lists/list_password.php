<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../modules/list.php");

$id = $_GET['id'];

$ml = new MailingList($id);
$list_params = $ml->UserAdmin();

$input_right = 0;
$input_super_right = 0;
if ($ah->current_user_id==$list_params['id_user'] || $module_admin)
	$input_right = 1;

if ($module_admin)
	$input_super_right = 1;

$title[] = array($list_params['email'],'ops.php?id='.$id);
$title[] = array('Cambia password','');

echo $hh->ShowTitle($title);
?>

<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			password: {
				required: true
			},
			password_verify: {
				required: true,
				equalTo: "#password-field"
			}
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("id_list",$id);
echo $hh->input_hidden("from","list");
echo $hh->input_table_open();

if ($input_super_right)
	echo $hh->input_note("In qualita' di amministratore puoi reimpostare la password di $list_params[email]. La password precedente verra' cancellata.");
else	
	echo $hh->input_text("vecchia password","password_old","",20,0,$input_right);
	
echo $hh->input_text("nuova password","password","",20,0,$input_right);
echo $hh->input_text("nuova password<br>(per verifica)","password_verify","",20,0,$input_right);

$actions = array();
$actions[] = array('action'=>"change_password",'label'=>"submit",'right'=>$input_right);
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();
include_once(SERVER_ROOT."/include/footer.php");
?>


