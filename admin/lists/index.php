<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/user.php");

echo $hh->ShowTitle($title);

$user = new User();
$lists = $user->Lists();
if (count($lists)>0)
{
	echo "<ul>Le tue mailing list:\n";
	foreach($lists as $list)
		echo "<li><a href=\"ops.php?id=$list[0]\">$list[1]</a></li>\n";
	echo "</ul>";
}

echo "<p><a href=\"lists.php\">Elenco di tutte le liste</a></p>\n";

if ($module_admin)
	echo "<p><a href=\"list.php?id=0\">Crea nuova lista</a>\n";

echo "<p><a href=\"groups.php\">Gruppi</a></p>\n";

if ($module_admin)
	echo "<p><a href=\"config.php\">Configurazione</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>
