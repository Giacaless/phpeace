<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../modules/list.php");

$ah = new AdminHelper;
$ah->CheckAuth();
$module_admin = $ah->CheckModule();

$fh = new FormHelper(true,11);
$post = $fh->HttpPost();

$from		= $post['from'];
$action 	= $fh->ActionGet($post);

if ($from=="list")
{
	$id_list	= $post['id_list'];
	$ml = new MailingList($id_list);
	$email		= $post['email'];
	$id_user	= $post['id_user'];
	$id_topic	= $fh->Null2Zero($post['id_topic']);
	$id_group	= $fh->Null2Zero($post['id_group']);
	$public	= $fh->Checkbox2bool($post['public']);
	$name		= $post['name'];
	$description	= $post['description'];
	$impl		= $post['impl'];
	$owner_email	= $post['owner_email'];
	$archive	= $post['archive'];
	$feed		= $post['feed'];
	if ($action=="store")
	{
		if($id_list>0)
			$ml->Update($email, $id_user, $public, $name, $description, $impl, $id_topic, $id_group,$owner_email,$archive,$feed);
		if($id_list==0)
			$id_list = $ml->Insert($email, $id_user, $public, $name, $description, $impl, $id_topic, $id_group,$owner_email,$archive,$feed);
	}
	$url 		= "ops.php?id=$id_list";
	if ($action=="delete" && $id_list>0)
	{
		$ml->Delete();
		$url = "index.php";
	}
	if ($action=="change_password")
	{
		$password	= $post['password'];
		$password_old	= $post['password_old'];
		$ml->ChangePassword($password_old,$password);
	}
	header("Location: $url");
}

if ($from=="list_command")
{
	$id_list	= $post['id_list'];
	$ml = new MailingList($id_list);
	$email		= $post['email'];
	$list = $ml->GetList();
	if ($ah->current_user_id==$list['id_user'] || $module_admin)
	{
		$message = $ml->Command($action,$email);
		$fh->va->MessageSet("notice",$message['string'],$message['params']);
	}
	header("Location: ops.php?id=$id_list");
}

if ($from=="group")
{
	include_once(SERVER_ROOT."/../modules/lists.php");
	$li = new Lists();
	$id_group	= $post['id_group'];
	$name		= $post['name'];
	if ($id_group==0)
		$li->GroupInsert($name);
	if ($id_group>0)
		$li->GroupUpdate($id_group, $name);
	header("Location: groups.php");
}

if ($from=="config_update")
{
	$default_list 	= $post['default_list'];
	$path 		= $post['path'];
	include_once(SERVER_ROOT."/../modules/lists.php");
	$li = new Lists();
	$li->ConfigurationUpdate($default_list, $path);
	header("Location: index.php");
}
?>


