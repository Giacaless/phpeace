<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");
include_once(SERVER_ROOT."/../classes/styles.php");

$s = new Styles();
$xslm = new XslManager("custom");

$id_xsl = $_GET['id'];

$title[] = array('Page types','xsls.php');
$title[] = array('Custom XSL','xsl_customs.php');

if ($id_xsl>0)
{
	$row = $xslm->XslGet($id_xsl,0);
	$id_style = $row['id_style'];
	$title[] = array($row['name'],'xsl_custom.php?id=1');
	$xsl = $xslm->XslGetLocal($id_xsl,$id_style);
	$xslm->xh->Check($xsl,FALSE);
}
else
{
	$title[] = array("New",'xsl_custom.php?id=0');
	$xsl = $xslm->TypeInit("root");
	$id_style = 0;
}

if ($id_xsl>0 && $id_style>0)
{
	$input_right = $s->InputRight($id_style);
}
if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo $hh->input_form_open();
echo $hh->input_hidden("from","xsl_custom");
echo $hh->input_hidden("id_xsl",$id_xsl);
echo $hh->input_table_open();

include_once(SERVER_ROOT."/../classes/topics.php");

if ($id_xsl>0)
{
	echo $hh->input_note("This XSL is never used directly; it can only be included with &lt;xsl:include href=\"../$id_style/custom_$id_xsl.xsl\" /&gt; ");
	$num = $xslm->Revisions($rows,$id_xsl,$id_style);
	if ($num>0)
		echo $hh->input_note("Revisions: <a href=\"xsl_revisions.php?id_style=$id_style&type=custom&id_pagetype=$id_xsl\">$num</a>");
	echo $hh->input_note("Warning: &_amp; substitutes &amp;amp; and &lt;_textarea&gt; substitutes  &lt;textarea&gt;; no need to fix them, it's going to be done after form submit");
}

echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_row("graphic_style","id_style",$id_style,$s->StylesAll(),"all",0,$input_right);
echo $hh->input_textarea("xsl","xsl",$xsl,80,30,"",$input_right);

$actions = array();
$actions[] = array('action'=>(($id_xsl>0)?"store":"insert"),'label'=>"submit",'right'=>$input_right);
if ($id_xsl>0)
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && $id_xsl>0));
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
