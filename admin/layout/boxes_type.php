<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/styles.php");

$s = new Styles();

$id_type = $_GET['id'];
$id_style = $_GET['id_style'];

$title[] = array('Boxes types','boxes_types.php');

if ($id_type>0)
{
	$row = $s->BoxesTypeGet($id_type);
	$id_style = (int)$row['id_style'];
	$title[] = array($row['name'],'');
}
else
{
	$title[] = array('New box type','');
}

echo $hh->ShowTitle($title);

if ($id_style>0)
{
	$input_right = $s->InputRight($id_style);
}
if ($module_admin)
	$input_right = 1;

echo $hh->input_form_open();
echo $hh->input_hidden("from","boxes_type");
echo $hh->input_hidden("id_type",$id_type);
echo $hh->input_hidden("id_style",$id_style);
echo $hh->input_table_open();
if($id_type>0)
	echo $hh->input_text("ID","id_box2",$id_type,5,0,0);
if($id_style>0)
{
	$style = $s->StyleGet($id_style);
	echo $hh->input_text("Style","style_name",$style['name'],15,0,0);
}
echo $hh->input_text("name","name","{$row['name']}",20,0,$input_right);
$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if($id_type>0)
{
	$uses = $s->BoxesTypeUse($id_type);
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>$id_type>1 && $uses==0 && $input_right);
}
echo $hh->input_actions($actions,$input_right);
echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>
