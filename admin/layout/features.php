<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xmlhelper.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
$pt = new PageTypes();

$xh = new XmlHelper();
$hhf = new HHFunctions ;

$id_type = (int)$_GET['id_type'];
$id_style = (int)$_GET['id_style'];
$id_module = (int)$_GET['id_module'];

$types = $hh->tr->Translate("page_types");

$title[] = array('Page types',$ui?'page_types.php':'xsls.php');

$t_modules = $hh->tr->Translate("modules_names");
if($id_module>0)
{
	$title[] = array($t_modules[$id_module],'xsl_module_styles.php?id=' . $id_module);
}
else 
{
	$title[] = array($types[$id_type],'xsl_styles.php?id=' . $id_type);
}

if ($id_style>0)
{
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	$style = $s->StyleGet($id_style);
	$title[] = array($style['name'],'xsl.php?id='.$id_type.'&id_style='.$id_style);
}

$title[] = array('Features','');
echo $hh->ShowTitle($title);

$num = $pt->ft->PageFeatures($row,$id_type,$id_style,$id_module);

$table_headers = array('feature','description','active','author','public_xml');
$table_content = array('{LinkTitle("feature.php?id=$row[id_feature]&id_type='.$id_type.'&id_style='.$id_style.'&id_module='.$id_module.'",$row[name])}',
'$row[description]','{Bool2YN($row[active])}','{UserLookup($row[id_user])}','{Bool2YN($row[public])}');
echo $hh->showTable($row, $table_headers, $table_content, $num);

echo "<p><a href=\"feature.php?id=0&id_type=$id_type&id_style=$id_style&id_module=$id_module\">add</a></p>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

