<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");
include_once(SERVER_ROOT."/../classes/styles.php");
$pt = new PageTypes();
$s = new Styles();

$id_style = (int)$_GET['id'];

$pfunctions = $hh->tr->Translate("page_functions");

$hhf = new HHFunctions ;

$types = $hh->tr->Translate("page_types");

$title[] = array('styles','styles.php');

$row = $s->StyleGet($id_style);
$title[] = array($row['name'],'style.php?id='.$id_style);
$title[] = array('features','');

echo $hh->ShowTitle($title);

$t_modules = $hh->tr->Translate("modules_names");
$num3 = $pt->ft->PageFeaturesStyle($rows3,$id_style,-1,false);
if ($num3>0)
{
	echo "<ul>";
	foreach($rows3 as $row3)
	{
		$type = $row3['id_module']>0? $t_modules[$row3['id_module']] : $types[$row3['id_type']];
		echo "<li>$type: <a href=\"feature.php?id=$row3[id_feature]&id_type=$row3[id_type]&id_style=$id_style\">$row3[name]</a>" . (($row3['id_user']>0)? "":" (sys)");
		echo "<div class=\"notes\">" . $pfunctions[$row3['id_function']] . "</div></li>";
	}
	echo "</ul>";
}


include_once(SERVER_ROOT."/include/footer.php");
?>

