<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/file.php");

$id_style = $_GET['id_style'];

$title[] = array('labels','label_styles.php');

if ($id_style>0)
{
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles;
	$style = $s->StyleGet($id_style);
	$title[] = array($style['name'],'');
}
else
	$title[] = array('Generic','');

echo $hh->ShowTitle($title);

$active_modules = Modules::AvailableModules();
$t_modules = $hh->tr->Translate("modules_names");
$fm = new FileManager();

echo "<ul>\n";
echo "<li><a href=\"labels.php?id_style=$id_style&id_module=0\">Common</a></li>";
foreach($active_modules as $module)
{
	$file = $hh->tr->Filename($hh->tr->id_language,$module['id_module'],false);
	if ($fm->Exists($file) && (!$id_style>0 || ($module['path']!="homepage") && $module['path']!="phpeace"))
	{
		echo "<li><a href=\"labels.php?id_style=$id_style&id_module={$module['id_module']}\">" . trim($t_modules[$module['id_module']]) . "</a></li>\n";
	}
}
echo "</ul>\n";

include_once(SERVER_ROOT."/include/footer.php");
?>

