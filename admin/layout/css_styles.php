<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");

$id_type = $_GET['id'];

$title[] = array('CSS','csss.php');
$types = $hh->tr->Translate("page_types");
$title[] = array($types[$id_type],'');
echo $hh->ShowTitle($title);

echo "<p><a href=\"css.php?id=$id_type&id_style=0\">Generic CSS</a></p>\n";
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
$num = $s->StylesP( $row );

$table_headers = array('css','style');
$table_content = array('{LinkTitle("css.php?id=' . $id_type . '&id_style=$row[id_style]","css")}',
'{LinkTitle("style.php?id=$row[id_style]",$row[name])}');

echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>

