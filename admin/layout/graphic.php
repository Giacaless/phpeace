<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");


$id = $_GET['id'];
$from_style = $_GET['from_style'];
$title[] = array('Graphics','graphics.php');

if ($id>0)
{
	include_once(SERVER_ROOT."/../classes/graphic.php");
	$gr = new Graphic($id);
	$action2 = "update";
	$row = $gr->GraphicGet();
	$title[] = array($row['name'],'');
	$id_style = $row['id_style'];
}
else
{
	$id_style = $_GET['id_style'];
	$action2 = "insert";
	$title[] = array('Insert new graphic','');
}

if ($module_right)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
function doaction(myaction)
{
	f=document.forms['form1'];
	f.action2.value = myaction;
	boolOK = true;
	strAlert = "";
	if (f.name.value=="") {
		strAlert=strAlert+"Missing caption\n";
		boolOK = false;
	}
	if (boolOK==true) {
		f.submit();
	}
	else {
		alert(strAlert);
	}
}
</script>
<form method="post" action="actions.php" name="form1" enctype="multipart/form-data">
<input type="hidden" name="action2" value="<?=$action2; ?>">
<input type="hidden" name="from" value="graphic">
<input type="hidden" name="id_graphic" value="<?=$id; ?>">
<input type="hidden" name="from_style" value="<?=$from_style; ?>">
<table border=0 cellpadding=0 cellspacing=7>
<?php
if ($id==0)
	echo $hh->input_upload("choose_file","img",50,$input_right);
else
{
	echo $hh->input_text("ID","ID",$id,5,0,0); 
	echo $hh->show_graphic($id.".".$row['format'],$row['width'],$row['height'],$row['format']);
	echo $hh->input_upload("substitute_with","img",50,$input_right);
}
echo $hh->input_text("description / caption","name",$row['name'],50,26,$input_right);
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles;
echo $hh->input_row($hh->Wrap("Style","<a href=\"style.php?id=$id_style\">","</a>",$id>0 && $id_style>0),"id_style",$id_style,$s->StylesUser($ah->current_user_id,$module_admin),$module_admin?"all":"",0,$input_right);
echo $hh->input_checkbox("Exclude from XML","exclude_lookup",$row['exclude_lookup'],0,$input_right);

if ($input_right==1)
{
	echo "<tr><td>&nbsp;</td><td>";
	echo "<input type=\"button\" value=\"Submit\" onClick=\"doaction('$action2')\">";
	if ($id>0)
		echo "&nbsp;<input type=\"button\" value=\"Delete\" onClick=\"doaction('delete')\">";
	echo "</td></tr>\n";
}

?>
</table>
</form>
<?php
if($id>0)
{
	$paths = $conf->Get("paths");
	echo "<p>URL: " . $ini->Get("pub_web") . "/" . $paths['graphic'] . "$id.{$row['format']}</p>";
	echo $hh->input_code("graphic_xsl","graphic_xsl","<xsl:call-template name=\"graphic\"><xsl:with-param name=\"id\" select=\"$id\"/></xsl:call-template>",60,2);
}
include_once(SERVER_ROOT."/include/footer.php");
?>

