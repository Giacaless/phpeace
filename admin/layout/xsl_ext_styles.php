<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/xsl.php");

$xslm = new XslManager("ext");

$id = $_GET['id'];

$title[] = array('Page types','xsls.php');
$title[] = array('XSL extensions','xsl_exts.php');

if ($id>0)
{
	$row = $xslm->XslGet($id,0);
	$title[] = array($row['name'],'xsl_custom.php?id=1');
}
else
	$title[] = array("New",'');

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);

echo "<p><a href=\"xsl_ext.php?id=$id&id_style=0\">Generic XSL</a></p>\n";
include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
$num = $s->StylesP( $row );

$table_headers = array('xsl','style');
$table_content = array('{LinkTitle("xsl_ext.php?id='.$id.'&id_style=$row[id_style]","xsl")}',
'{LinkTitle("style.php?id=$row[id_style]",$row[name])}');
echo $hh->ShowTable($row, $table_headers, $table_content, $num);

include_once(SERVER_ROOT."/include/footer.php");
?>
