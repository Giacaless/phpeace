<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
include_once(SERVER_ROOT."/../classes/formhelper.php");
include_once(SERVER_ROOT."/../classes/images.php");
include_once(SERVER_ROOT."/../classes/config.php");
$conf = new Configuration();
$ui = $conf->Get("ui");

$ah = new AdminHelper;
$ah->CheckAuth();

$fh = new FormHelper;
$post = $fh->HttpPost();

$action2	= $post['action2'];
$from		= $post['from'];

$i = new Images();

if ($from=="style")
{
	$action 		= $fh->ActionGet($post);
	$id_style	= $post['id_style'];
	$id_style_copy	= $post['id_style_copy'];
	$name		= $post['name'];
	$description	= $post['description'];
	$id_parent 	= $fh->String2Number($post['id_parent']);
	include_once(SERVER_ROOT."/../classes/styles.php");
	$ss = new Styles();
	if ($action=="insert") {
		$id_style = $ss->StyleInsert( $name,$description,$id_parent,$id_style_copy,$ui );
		if($ui) {
			$ah->MessageSet("Please add style $id_style to the UI repository");
		}
	}
	if ($action=="update")
		$ss->StyleUpdate( $id_style,$name,$description );
	if ($action=="delete") {
		$ss->StyleDelete($id_style,$ui);
	}
	header("Location: styles.php");
}

if ($from=="boxes_type")
{
	$action 	= $fh->ActionGet($post);
	$id_type	= $post['id_type'];
	$id_style	= $post['id_style'];
	$name		= $post['name'];
	include_once(SERVER_ROOT."/../classes/styles.php");
	$s = new Styles();
	if ($action=="store")
	{
		$s->BoxesTypeStore($id_type,$id_style,$name);
	}
	if ($action=="delete")
	{
		$s->BoxesTypeDelete($id_type);
	}
	header("Location: boxes_types.php?id_style=$id_style");
}

if ($from=="css")
{
	$action 	= $fh->ActionGet($post);
	$id_css	    = $post['id_css'];
	$id_type	= $post['id_type'];
	$id_style	= $post['id_style'];
	$ucss		= $fh->HttpPostUnescapedVar("css");
	include_once(SERVER_ROOT."/../classes/css.php");
	$csm = new CssManager();
	$csm->id_pagetype = $id_type;
	if ($action=="store")
	{
		$csm->CssStore($id_css,$id_style,"",$ucss);
		$backto     = "css.php?id=$id_type&id_style=$id_style";
	}
	if ($action=="store_close")
	{
		$csm->CssStore($id_css,$id_style,"",$ucss);
		$backto     = "css_styles.php?id=$id_type";
	}
	header("Location: $backto");
}

if ($from=="css_module")
{
	$action 	= $fh->ActionGet($post);
	$id_css	= $post['id_css'];
	$id_module	= $post['id_module'];
	$id_style	= $post['id_style'];
	$ucss		= $fh->HttpPostUnescapedVar("css");
	include_once(SERVER_ROOT."/../classes/css.php");
	$csm = new CssManager("module");
	$csm->id_pagetype = $id_module;
	if ($action=="store")
	{
		$csm->CssStore($id_css,$id_style,"",$ucss);
		header("Location: css_module.php?id=$id_module&id_style=$id_style");
	}
	if ($action=="store_close")
	{
		$csm->CssStore($id_css,$id_style,"",$ucss);
		header("Location: css_module_styles.php?id=$id_module");
	}
}

if ($from=="css_global")
{
	$action     = $fh->ActionGet($post);
	$id_css	= $post['id_css'];
	$ucss		= $fh->HttpPostUnescapedVar("css");
	include_once(SERVER_ROOT."/../classes/css.php");
	$csm = new CssManager("global");
	$csm->id_pagetype = $id_css;	
	$csm->CssStore($id_css,0,"",$ucss);
	if ($action=="store")
		header("Location: css_global.php?id=$id_css");
	else
		header("Location: csss.php");
}

if ($from=="css_ext")
{
	$action 	= $fh->ActionGet($post);
	$id_css	    = $post['id_css'];
	$name		= $post['name'];
	$id_style	= $fh->Null2Zero($post['id_style']);
	$css		= $post['css'];
	$ucss		= $fh->HttpPostUnescapedVar("css");
	$backto     = "css_ext_styles.php?id=$id_css";
	include_once(SERVER_ROOT."/../classes/css.php");
	$csm = new CssManager("ext");
	if ($action=="delete")
		$csm->CssDelete($id_css,$id_style);
	if ($action=="update")
	{
		$id_css2 = $csm->CssStore( $id_css,$id_style,$name,$ucss );
		$backto     = "css_ext.php?id=$id_css2&id_style=$id_style";
	}
	if ($action=="update_close")
	{
		$id_css2 = $csm->CssStore( $id_css,$id_style,$name,$ucss );
		$backto     = "css_module_styles.php?id=$id_css";
	}
	if ($action=="create")
	{
		$id_css2 = $csm->CssCreate( $name,$ucss );
		$backto     = "css_ext.php?id=$id_css2&id_style=$id_style";
	}
	header("Location: $backto");
}

if ($from=="css_custom")
{
	$action 	= $fh->ActionGet($post);
	$id_css	    = $post['id_css'];
	$name		= $post["name"];
	$id_style	= $fh->Null2Zero($post['id_style']);
	$css		= $post["css"];
	$ucss		= $fh->HttpPostUnescapedVar("css");
	include_once(SERVER_ROOT."/../classes/css.php");
	$csm = new CssManager("custom");
	if ($action=="delete")
		$csm->CssDelete($id_css,$id_style);
	if (($action=="store") or ($action=='store_close'))
		$csm->CssStore($id_css,$id_style,$name,$ucss);
	if ($action=="insert")
		$csm->CssInsert($id_css,$id_style,$name,$ucss);
	if ($action=='store')
		header("Location: css_custom.php?id=$id_css");
	else
		header("Location: css_customs.php");
}

if ($from=="favicon")
{
	include_once(SERVER_ROOT."/../classes/graphic.php");
	$id_topic	= $post['id_topic'];
	$gr = new Graphic(0);
	$file	= $fh->UploadedFile("img");
	if ($file['ok'])
		$gr->FaviconUpdate($id_topic,$file);
	header("Location: favicons.php");
}

if ($from=="graphic")
{
	include_once(SERVER_ROOT."/../classes/graphic.php");
	$id_graphic	= $post['id_graphic'];
	$name		= $post['name'];
	$id_style	= $post['id_style'];
	$from_style	= $fh->Null2Zero($post['from_style']);
	$exclude_lookup 	= $fh->Checkbox2bool($post['exclude_lookup']);
	$gr = new Graphic($id_graphic);
	$file		= $fh->UploadedFile("img",true);
	if ($action2=="insert" && $file['ok'])
		$gr->GraphicInsert($file,$name,$id_style,$exclude_lookup);
	if ($action2=="update")
		$gr->GraphicUpdate($file,$name,$id_style,$exclude_lookup);
	if ($action2=="delete")
		$gr->GraphicDelete();
	header("Location: graphics.php" . ($from_style>0?"?id_style=$from_style":""));
}

if ($from=="js")
{
	$action 	= $fh->ActionGet($post);
	$id_js		= $post['id_js'];
	$name		= $post["name"];
	$id_style	= $fh->Null2Zero($post['id_style']);
	$script		= $fh->HttpPostUnescapedVar("script");
	include_once(SERVER_ROOT."/../classes/javascript.php");
	$jsm = new JavascriptManager();
	if ($action=="delete")
		$jsm->JsDelete($id_js,$id_style);
	if ($action=="store")
		$jsm->JsStore( $id_js,$id_style,$name,$script );
	if ($action=="insert")
		$jsm->JsInsert( $id_js,$id_style,$name,$script );
	header("Location: jss.php");
}

if ($from=="xsl")
{
	$action 	= $fh->ActionGet($post);
	$id_xsl		= $post['id_xsl'];
	$id_type	= $post['id_type'];
	$id_style	= $post['id_style'];
	$uxsl		= $fh->Text($fh->HttpPostUnescapedVar("xsl"));
	include_once(SERVER_ROOT."/../classes/xsl.php");
	$xslm = new XslManager();
	$xslm->id_pagetype = $id_type;
	if ($action=="store")
	{
		$id_xsl = $xslm->XslStoreWrite($id_xsl,$id_style,"",$uxsl);
		header("Location: xsl_styles.php?id=$id_type");
	}
	if ($action=="fork")
	{
		$outsourced 	= $fh->Checkbox2bool($post['outsourced']);
		$xslm->XslForkSwap($id_xsl,$outsourced);
		header("Location: xsl.php?id=$id_type&id_style=$id_style");
	}
}

if ($from=="xsl_module")
{
	$action 	= $fh->ActionGet($post);
	$id_xsl		= $post['id_xsl'];
	$id_module	= $post['id_module'];
	$id_style	= $post['id_style'];
	$uxsl		= $fh->Text($fh->HttpPostUnescapedVar("xsl"));
	include_once(SERVER_ROOT."/../classes/xsl.php");
	$xslm = new XslManager("module");
	$xslm->id_pagetype = $id_module;
	if ($action=="store")
	{
		$id_xsl = $xslm->XslStoreWrite($id_xsl,$id_style,"",$uxsl);
		header("Location: xsl_module_styles.php?id=$id_module");
	}
	if ($action=="fork")
	{
		$outsourced 	= $fh->Checkbox2bool($post['outsourced']);
		$xslm->XslForkSwap($id_xsl,$outsourced);
		header("Location: xsl_module.php?id=$id_module&id_style=$id_style");
	}
}

if ($from=="xsl_global")
{
	$action 	= $fh->ActionGet($post);
	$id_xsl		= $post['id_xsl'];
	$uxsl		= $fh->Text($fh->HttpPostUnescapedVar("xsl"));
	include_once(SERVER_ROOT."/../classes/xsl.php");
	$xslm = new XslManager("global");
	$xslm->id_pagetype = $id_xsl;
	if ($action=="store")
	{
		$id_xsl = $xslm->XslStoreWrite($id_xsl,0,"",$uxsl);
		header("Location: xsls.php");
	}
	if ($action=="fork")
	{
		$outsourced 	= $fh->Checkbox2bool($post['outsourced']);
		$xslm->XslForkSwap($id_xsl,$outsourced);
		header("Location: xsl_global.php?id=$id_xsl");
	}
}

if ($from=="xsl_ext")
{
	$action 	= $fh->ActionGet($post);
	$id_xsl		= $post['id_xsl'];
	$name		= $post['name'];
	$id_style	= $fh->Null2Zero($post['id_style']);
	$uxsl		= $fh->Text($fh->HttpPostUnescapedVar("xsl"));
	include_once(SERVER_ROOT."/../classes/xsl.php");
	$xslm = new XslManager("ext");
	if ($action=="delete")
	{
		$xslm->XslDelete($id_xsl,$id_style);
	}
	if ($action=="update")
	{
		$id_xsl2 = $xslm->XslStoreWrite($id_xsl,$id_style,$name,$uxsl);
	}
	if ($action=="create")
		$id_xsl2 = $xslm->XslCreate( $name,$uxsl );
	header("Location: xsl_ext_styles.php?id=$id_xsl2");
}

if ($from=="xsl_custom")
{
	$action 	= $fh->ActionGet($post);
	$id_xsl		= $post['id_xsl'];
	$name		= $post['name'];
	$id_style	= $fh->Null2Zero($post['id_style']);
	$uxsl		= $fh->Text($fh->HttpPostUnescapedVar("xsl"));
	include_once(SERVER_ROOT."/../classes/xsl.php");
	$xslm = new XslManager("custom");
	if ($action=="delete")
	{
		$xslm->XslDelete($id_xsl,$id_style);
	}
	if ($action=="store")
	{
		$id_xsl = $xslm->XslStoreWrite($id_xsl,$id_style,$name,$uxsl);
	}
	if ($action=="insert")
		$xslm->XslInsert($id_xsl,$id_style,$name,$uxsl);
	header("Location: xsl_customs.php");
}

if ($from=="feature")
{
	$action 	= $fh->ActionGet($post);
	$id_feature	= $post['id_feature'];
	$name		= $post['name'];
	$active		= $fh->Checkbox2bool($post['active']);
	$public		= $fh->Checkbox2bool($post['public']);
	$description	= $post['description'];
	$id_style	= $post['id_style'];
	if(substr($post['id_type'],0,1)=="m")
	{
		$id_type 	= 0;
		$id_module	= $fh->Null2Zero(substr($post['id_type'],1));
	}
	else 
	{
		$id_type	= $fh->Null2Zero($post['id_type']);
		$id_module	= 0;
	}
	$id_function	= $post['id_function'];
	$condition_var	= $post['condition_var'];
	$condition_type	= $fh->Null2Zero($post['condition_type']);
	$condition_value	= $post['condition_value'];
	$condition_id	= $fh->Null2Zero($post['condition_id']);
	$old_id_type	= $fh->Null2Zero($post['old_id_type']);
	$old_id_function	= $fh->Null2Zero($post['old_id_function']);
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="delete")
	{
		$pt->FeatureDelete($id_feature,$id_style,$id_type);
		header("Location: features.php?id_type=$id_type&id_style=$id_style");
	}
	if ($action=="activeswap")
	{
		$pt->ft->FeatureActiveSwap($id_feature,$active);
		header("Location: features.php?id_type=$id_type&id_style=$id_style");
	}
	if ($action=="store")
	{
		$changed = $id_type!=$old_id_type || $id_function!=$old_id_function;
		$id_feature = $pt->FeatureStore( $id_feature,$name,$description,$id_style,$id_type,$id_module,$id_function,$active,$public,$condition_var,$condition_type,$condition_value,$condition_id,$ah->current_user_id,$changed );
		$params = $pt->ft->params[$id_function];
		if (count($params)>0)
			header("Location: feature_params.php?id=$id_function&id_feature=$id_feature&id_type=$id_type&id_style=$id_style");
		else
			header("Location: features.php?id_type=$id_type&id_style=$id_style");
	}
}

if ($from=="feature_global")
{
	$action 	= $fh->ActionGet($post);
	$id_feature	= $post['id_feature'];
	$name		= $post['name'];
	$active		= $fh->Checkbox2bool($post['active']);
	$public		= $fh->Checkbox2bool($post['public']);
	$description	= $post['description'];
	$id_type	= $post['id_type'];
	$id_function	= $post['id_function'];
	$old_id_type	= $fh->Null2Zero($post['old_id_type']);
	$old_id_function	= $fh->Null2Zero($post['old_id_function']);
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="delete")
	{
		$pt->GlobalFeatureDelete($id_feature,$id_type);
		header("Location: features_global.php?id_type=$id_type");
	}
	if ($action=="activeswap")
	{
		$pt->ft->FeatureActiveSwap($id_feature,$active);
		header("Location: features_global.php");
	}
	if ($action=="store")
	{
		$id_feature = $pt->GlobalFeatureStore( $id_feature,$name,$description,$id_type,$id_function,$old_id_type,$old_id_function,$active,$public,$ah->current_user_id );
		$params = $pt->ft->params[$id_function];
		if (count($params)>0)
			header("Location: feature_global_params.php?id=$id_feature");
		else
			header("Location: features_global.php");
	}
}

if ($from=="feature_params")
{
	$action 	= $fh->ActionGet($post);
	$id_feature	= $post['id_feature'];
	$id_style	= $post['id_style'];
	$id_type	= $post['id_type'];
	$id_function	= $post['id_function'];
	$params	= $fh->SerializeParams();
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="store")
	{
		$pt->PageFunctionsParamsStore($id_feature,$params);
	}
	header("Location: feature.php?id=$id_feature&id_type=$id_type&id_style=$id_style");
}

if ($from=="feature_global_params")
{
	$action 	= $fh->ActionGet($post);
	$id_feature	= $post['id_feature'];
	$id_type	= $post['id_type'];
	$id_function	= $post['id_function'];
	$params		= $fh->SerializeParams();
	include_once(SERVER_ROOT."/../classes/pagetypes.php");
	$pt = new PageTypes();
	if ($action=="store")
	{
		$pt->PageFunctionsParamsStore($id_feature,$params);
	}
	header("Location: feature_global.php?id=$id_feature&id_type=$id_type");
}

if ($from=="label")
{
	$id_style	= $post['id_style'];
	$id_module	= $post['id_module'];
	$id_language	= $post['id_language'];
	$label		= $post['label'];
	$label_custom	= $fh->HttpPostUnescapedVar('label_custom');
	$action 	= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/labels.php");
	$la = new Labels(0,false,$id_style);
	if ($action=="store")
		$la->CustomStore($id_module,$label,$label_custom,$id_language);
	if ($action=="delete")
	{
		if(strstr($label,"c_")===false)
			$la->CustomDelete($id_module,$label,$id_language);
		else 
			$la->CustomAddedDelete($id_module,$label);
	}
	header("Location: labels.php?id_style=$id_style&id_module=$id_module&id_language=$id_language");
}

if ($from=="custom_label")
{
	$id_style	= $post['id_style'];
	$id_module	= $post['id_module'];
	$id_language	= $post['id_language'];
	include_once(SERVER_ROOT."/../classes/phpeace.php");
	$phpeace = new PhPeace();
	$languages = $phpeace->Languages();
	$labels_custom = array();
	foreach($languages as $key=>$language)
		if($key>0)
			$labels_custom[]  = array('id_language'=>$key,'label'=>$fh->HttpPostUnescapedVar('label_custom_' . $key));
	$action 	= $fh->ActionGet($post);
	include_once(SERVER_ROOT."/../classes/labels.php");
	$la = new Labels(0,false,$id_style);
	if ($action=="store")
	{
		$label = $la->CustomAdd($id_module,$labels_custom);
		$ah->MessageSet("New custom label added: \"$label\"");
	}
	header("Location: labels.php?id_style=$id_style&id_module=$id_module&id_language=$id_language");
}

if ($from=="config_update")
{
	$ini = new Ini;
	$ini->SetModule("layout","feature_propagate",$fh->Checkbox2bool($post['feature_propagate']));
	$ini->SetModule("layout","gra_versioning",$fh->Checkbox2bool($post['gra_versioning']));
	$ini->SetModule("layout","css_version",$post['css_version']);
	$ini->SetModule("layout","js_version",$post['js_version']);
	header("Location: index.php");
}

if ($from=="config_xsl_update")
{
	$xsl_output_indent	= $fh->Checkbox2bool($post['xsl_output_indent']);
	$xsl_output_doctype	= $post['xsl_output_doctype'];
	include_once(SERVER_ROOT."/../classes/xsl.php");
	$xslm = new XslManager();
	$changed = $xslm->ConfigurationUpdate($xsl_output_indent,$xsl_output_doctype);
	if ($changed)
		$ah->MessageSet("changes_republish",array());
	header("Location: index.php");
}

?>
