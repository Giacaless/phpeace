<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/pagetypes.php");

$pt = new PageTypes();

$id_type = (int)$_GET['id_type'];
$id_feature = $_GET['id'];

if($id_feature>0)
{
	$row2 = $pt->ft->GlobalFeatureGetType($id_feature);
	$id_type = $row2['id_type'];
}

$types = $hh->tr->Translate("page_types_global");

if($ui) {
	$title[] = array('Page types','page_types.php');
	$title[] = array($types[$id_type],'page_type_global.php?id=' . $id_type);
} else {
	$title[] = array('Page types','xsls.php');
	$title[] = array($types[$id_type],'xsl_global.php?id=' . $id_type);
}

$title[] = array('Features','features_global.php?id_type='.$id_type);

if ($module_admin)
	$input_right = 1;
$is_system = false;

if ($id_feature>0)
{
	$row = $pt->ft->FeatureGet($id_feature);
	if(!(isset($row['id_feature'])))
		$hh->Stop();
	$row2 = $pt->ft->GlobalFeatureGetType($id_feature);
	$id_type = $row2['id_type'];
	$title[] = array($row['name'],'');
	$id_user = $row['id_user'];
	$active = $row['active'];
	if ($id_user == "0")
	{
		$is_system = true;
		$input_right = 0;
	}
}
else
{
	$title[] = array('New feature','');
	$id_user = $ah->current_user_id;
	$active = 1;
}

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/styles.php");
$s = new Styles();
?>
<form method="post" action="actions.php" name="form1">
<input type="hidden" name="from" value="feature_global">
<input type="hidden" name="id_feature" value="<?=$id_feature;?>">
<input type="hidden" name="old_id_function" value="<?=$row['id_function'];?>">
<input type="hidden" name="old_id_type" value="<?=$id_type;?>">
<table border="0" cellpadding="0" cellspacing="7">
<?php
if ($id_feature>0)
{
	$desc = $conf->Get("ui")? 'XSL' : "To call this feature in <a href=\"xsl_global.php?id=$id_type\">" . array_search($id_type,$pt->gtypes) . ".xsl</a>";
	echo $hh->input_note($hh->input_code("xsl_include",$desc,"<xsl:apply-templates select=\"/root/features/feature[@id='$id_feature']\" />"));
	echo $hh->input_note("<a href=\"feature_preview.php?id=$id_feature\" target=\"_blank\">XML " . $hh->tr->Translate("preview") . "</a>");
	if($row['public'])
	{
		echo $hh->input_note("Public link: " . $ini->Get("pub_web") . "/js/feature.php?id=$id_feature");
	}
}
	
echo $hh->input_text("name","name",$row['name'],50,0,$input_right);
echo $hh->input_checkbox("active","active",$active,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],60,3,"",$input_right);
$hhf = new HHFunctions();
echo $hh->input_text("author","id_user",$hhf->UserLookup($id_user),50,0,0);
echo $hh->input_checkbox("public_xml","public",$row['public'],0,$input_right);
$gtypes = $hh->tr->Translate("page_types_global");
unset($gtypes[8]);
echo $hh->input_array("page type","id_type",$id_type,$gtypes,0);

$desc = $hh->tr->Translate("function");
if ($id_feature>0)
{
	$params = $pt->ft->params[$row['id_function']];
	if (count($params)>0)
	{
		$desc .= " (<a href=\"feature_global_params.php?id=$id_feature\">" . $hh->tr->Translate("parameters") ."</a>)";
	}
}
echo $hh->input_array($desc,"id_function",$row['id_function'],$hh->tr->Translate("page_functions"),$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>($input_right && !$is_system));
$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($input_right && !$is_system && $id_feature>0));
$actions[] = array('action'=>"activeswap",'label'=>(($row['active'])?"deactivate":"activate"),'right'=>$is_system);
echo $hh->input_actions($actions,$input_right || $is_system);
?>
</table>
</form>
<?php
include_once(SERVER_ROOT."/include/footer.php");
?>

