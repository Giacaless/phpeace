<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/../classes/adminhelper.php");
$ah = new AdminHelper;
$ah->CheckAuth();

$id_gtype = $_GET['id_gtype'];
$id_type = $_GET['id_type'];
$id_topic = $_GET['id_topic'];
$id = $_GET['id'];

include_once(SERVER_ROOT."/../classes/error.php");
include_once(SERVER_ROOT."/../classes/layout.php");
$l = new Layout(true);

$params = array();
foreach($_GET as $key=>$value)
{
	if ($value!="")
	{
		$params[$key] = $value;
		if ($key=="offset")
			$params['ts'] = time() + ($value*86400);
	}
}
	

if (isset($id_gtype))
	$xml = $l->PageTypeGlobal($id_gtype,$id,$current_page,$params);
else
	$xml = $l->PageType($id_type,$id_topic,$id,$current_page,$params);

$xml = preg_replace("'<preview[^>]*?>.*?</preview>'si","",$xml);
if ($l->xh->Check($xml))
{
	header('Content-type: text/xml');
	echo $xml;
}

?>

