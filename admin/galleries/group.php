<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/galleries.php");


$id = $_GET['id'];
$title[] = array('galleries_tree','tree.php');

if ($id>0)
{
	$action2 = "update";
	$galleries = new Galleries;
	$row = $galleries->gh->GroupGet($id);
	$title[] = array($row[name],'');
	$parent_name = $row['parent_name'];
	$visible = $row['visible'];
	if ($row['id_parent']=="0")
		$parent_name = $galleries->gh->top_name;
}
else
{
	$action2 = "insert";
	$title[] = array('gallery_group_add','');
	$visible = 1;
}

if ($module_admin)
	$input_right = 1;

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
function dosubmit(action2)
{
	f = document.forms['form1'];
	boolOK = true;
	strAlert = "";
	if (f.name.value=="")
	{
		strAlert=strAlert+"Manca il nome\n";
		boolOK = false;
	}
	if (boolOK==true)
	{
		f.action2.value = action2;
		f.target = '';
		f.submit();
	}
	else
		alert(strAlert);
}

</script>

<form name="form1" action="actions.php" method="post">
<input type="hidden" name="action2" value="<?=$action2;?>">
<input type="hidden" name="from" value="group">
<input type="hidden" name="id_group" value="<?=$id;?>">
<input type="hidden" name="id_parent_old" value="<?=$row['id_parent'];?>">

<table border=0 cellpadding=0 cellspacing=7>
<?php
echo $hh->input_text("name","name",$row['name'],20,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],50,3,"",$input_right);
echo $hh->input_link("son_of","id_parent","'group_group.php?&id_group='+document.forms['form1'].id_group.value+'&id_parent='+document.forms['form1'].id_parent.value",$row['id_parent'],$parent_name,$input_right);
echo $hh->input_checkbox("visible_online","visible",$visible,0,$input_right);
if ($input_right==1)
{
	echo "<tr><td>&nbsp;</td><td>";
	echo "<input type=\"button\" value=\"" . $hh->tr->Translate("submit") ."\" onClick=\"dosubmit('$action2')\">";
	if ($id>0)
		echo "&nbsp;&nbsp;&nbsp;<input type=\"button\" value=\"" . $hh->tr->Translate("delete") ."\" onClick=\"dosubmit('delete')\">";
	echo "</td></tr>\n";
}
?>
</table>
</form>
<?php
if ($id>0)
{
	echo $hh->input_code("gallery_group_html","gallery_group_html","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/gallery.php?id_g=$id\"></script>",60,2);
	echo $hh->input_code("gallery_group_html_random","gallery_group_html_random","<script type=\"text/javascript\" src=\"" . $ini->Get('pub_web') . "/js/gallery.php?id_g=$id&r=1\"></script>",60,2);
}

include_once(SERVER_ROOT."/include/footer.php");
?>

