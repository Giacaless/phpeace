<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/gallery.php");
include_once(SERVER_ROOT."/../classes/users.php");
$uu = new Users;

$id = $_GET['id'];

if ($module_admin)
	$input_right = 1;

if ($id>0)
{
	$g = new Gallery($id);
	if ($g->AmIAdmin($ah->current_user_id))
		$input_right = 1;
	$row = $g->GalleryGet();
	$gallery_user = $row['id_user'];
	$title[] = array($g->title,'ops.php?id='.$id);
	$title[] = array('settings','');
	include_once(SERVER_ROOT."/../classes/varia.php");
	$v = new Varia();
	$params = $v->Deserialize($row['params']);
	$image_size = $params['image_size'];
	$id_licence = $row['id_licence'];
	$visible = $row['visible'];
	$images_per_page = $row['images_per_page'];
}
else
{
	$title[] = array('gallery_new','');
	$gallery_user = $ah->current_user_id;
	$image_size = 3;
	$id_licence = $hh->ini->Get("id_licence");	
	$visible = 1;
	$images_per_page = 10;
}

echo $hh->ShowTitle($title);
?>
<script type="text/javascript">
$().ready(function() {
$("#form1").validate({
		rules: {
			title: "required",
			id_group: {
				required: true,
				min: 1
			}
		}
	});
});
</script>

<?php
echo $hh->input_form_open();
echo $hh->input_hidden("from","gallery");
echo $hh->input_hidden("id_gallery",$id);
echo $hh->input_hidden("thumb_size_old",$params['thumb_size']);
echo $hh->input_hidden("image_size_old",$params['image_size']);
echo $hh->input_hidden("sort_order_old",$row['sort_order']);
echo $hh->input_table_open();
echo $hh->input_checkbox("visible_online","visible",$visible,0,$input_right);
echo $hh->input_link("placing","id_group","'gallery_group.php?&id_group='+document.forms['form1'].id_group.value+'&id_gallery='+document.forms['form1'].id_gallery.value",$row['id_group'],$row['group_name'],$input_right);
echo $hh->input_date("date","released",$row['released_ts'],$input_right);
echo $hh->input_checkbox("show_date","show_date",$row['show_date'],0,$input_right);
echo $hh->input_text("title","title",$row['title'],50,0,$input_right);
echo $hh->input_textarea("description","description",$row['description'],80,5,"",$input_right);
echo $hh->input_text("author","author",$row['author'],50,0,$input_right);
echo $hh->input_textarea("images_footer","note",$row['note'],80,3,"",$input_right);
if ($hh->ini->Get("licences"))
	echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_right);

include_once(SERVER_ROOT."/../classes/ontology.php");
$o = new Ontology;
$keywords = array();
echo $hh->input_keywords($id,$o->types['gallery'],$keywords,$input_right);

echo $hh->input_array("sort_by","sort_order",$row['sort_order'],$hh->tr->Translate("gallery_sort"),$input_right);
echo $hh->input_array("image_associated","associated_image",$row['associated_image'],$hh->tr->Translate("gallery_image_associated"),$input_right);
echo $hh->input_array("slides_order","param_slides_order",$params['slides_order'],$hh->tr->Translate("gallery_sort"),$input_right);
echo $hh->input_array("thumb_size","param_thumb_size",$params['thumb_size'],$conf->Get("img_sizes"),$input_right);
echo $hh->input_array("image_size","param_image_size",$image_size,$conf->Get("img_sizes"),$input_right);
echo $hh->input_text("images_per_page","images_per_page",$images_per_page,4,0,$input_right);
echo $hh->input_checkbox("show_originals","show_orig",$row['show_orig'],0,$input_right);
echo $hh->input_checkbox("show_images_author","show_author",$row['show_author'],0,$input_right);
echo $hh->input_checkbox("sharing_images","share_images",$row['share_images'],0,$input_right);
echo $hh->input_checkbox("captions_html","captions_html",$row['captions_html'],0,$input_right);

include_once(SERVER_ROOT."/../classes/topics.php");
$tt = new Topics;
echo $hh->input_topics($row['id_topic'],$row['id_topic_group'],$tt->User($ah->current_user_id),"none_option",$input_right,true,"id_topic_or_group");
echo $hh->input_checkbox("gallery_topic_admin","topic_admin",$row['topic_admin'],0,$input_right);

$users = $uu->ModuleUsers($ah->session->Get("module_id"));
echo $hh->input_user("administrator",$users,$gallery_user,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if($id>0)
{
	$gtopics = $g->Topics();
	$actions[] = array('action'=>"delete",'label'=>"delete",'right'=>($module_admin && count($gtopics)==0));
}
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

include_once(SERVER_ROOT."/include/footer.php");
?>


