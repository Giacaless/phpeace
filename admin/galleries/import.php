<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/gallery.php");

if ($module_admin)
	$input_right = 1;
else
	exit;
	
$id = $_GET['id'];

$g = new Gallery($id);
$row = $g->GalleryGet();

$title[] = array($g->title,'ops.php?id='.$id);
$title[] = array('import','');

echo $hh->ShowTitle($title);

include_once(SERVER_ROOT."/../classes/file.php");
$fm = new FileManager();

$directory = "import/g$id";

if($fm->Exists($directory))
{
	$files = $fm->DirFiles($directory);
	
	if(count($files)>0)
	{
		foreach($files as $file)
		{
			$ext = $fm->Extension($file);
			if($fm->IsGood($file,"img",true))
			{
				$db =& Db::globaldb();
				$source = "";
				$author = "";
				$id_licence = "";
				$caption = $db->SqlQuote(str_replace(".$ext","",basename($file)));
				$image_date = "";
				$db->begin();
				$db->lock( "images" );
				$id_image = $db->nextId( "images", "id_image" );
				$res[] = $db->query( "INSERT INTO images (id_image,source,author,id_licence,format,caption,image_date) 
					VALUES ($id_image,'$source','$author','$id_licence','$ext','$caption','$image_date')" );
				Db::finish( $res, $db);
				include_once(SERVER_ROOT."/../classes/image.php");
				$im = new Image($id_image);
				$filename = "{$im->path}/orig/{$im->id}.$ext";
				copy($file,"$fm->path/$filename");
				$im->ImageSizeSet();
				$im->Convert($filename, $im->id);
				$im->h->HistoryAdd($im->h->types['image'],$im->id,$im->h->actions['create']);
				$im->GalleryInsert($id,"",0,false);
				echo "inserted $caption<br>";
			}
			else
				echo "ERROR $file<br>";
		}
		$g->PrevNext();
	}
	else
		echo "No files in import directory $directory";
}
else 
	echo "Directory missing: $directory";


include_once(SERVER_ROOT."/include/footer.php");
?>

