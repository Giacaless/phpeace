<?php
/********************************************************************

   PhPeace - Portal Management System

   Copyright notice
   (C) 2003-2020 Francesco Iannuzzelli <francesco@phpeace.org>
   All rights reserved

   This script is part of PhPeace.
   PhPeace is free software; you can redistribute it and/or modify 
   it under the terms of the GNU General Public License as 
   published by the Free Software Foundation; either version 2 of 
   the License, or (at your option) any later version.

   PhPeace is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   The GNU General Public License (GPL) is available at
   http://www.gnu.org/copyleft/gpl.html.
   A copy can be found in the file COPYING distributed with 
   these scripts.

   This copyright notice MUST APPEAR in all copies of the script!

********************************************************************/

if (!defined('SERVER_ROOT'))
	define('SERVER_ROOT',$_SERVER['DOCUMENT_ROOT']);
include_once(SERVER_ROOT."/include/header.php");
include_once(SERVER_ROOT."/../classes/gallery.php");
include_once(SERVER_ROOT."/../classes/file.php");
include_once(SERVER_ROOT."/../classes/ontology.php");

$id = $_GET['id'];
$id_gallery = $_GET['id_gallery'];
$id_gallery_from = $_GET['id_gallery_from'];

$g = new Gallery($id_gallery);
$fm = new FileManager;

if ($module_admin || $g->AmIUser($ah->current_user_id))
	$input_right = 1;

$title[] = array($g->title,'ops.php?id='.$id_gallery);
$title[] = array('images','images.php?id='.$id_gallery."&p=".$current_page);

$keywords = array();
$o = new Ontology;
$o->GetKeywords($id_gallery,$o->types['gallery'], $keywords,0,true);

if($id_gallery_from>0)
	$g_from = new Gallery($id_gallery_from);

if ($id>0)
{
	include_once(SERVER_ROOT."/../classes/image.php");
	$im = new Image($id);
	$irow = $g->ImageGet($id,false);
	if ($module_admin || ($im->AdminRight() && $irow['id_gallery_from']=="0"))
		$input_super_right = 1;
	$source = $irow['source'];
	$author = $irow['author'];
	$caption = $irow['caption'];
	$id_licence = $irow['id_licence'];
	$image_date = $irow['image_date'];
	$filename_orig = "images/orig/{$id}.{$irow['format']}";
	$filename_copy = "images/2/{$id}." . $conf->Get("convert_format");
	if($id_gallery_from>0)
		$title[] = array('image_import','');
	else
		$title[] = array('image_change','');
}
else
{
	$input_super_right = $input_right;
	$title[] = array('image_add','');
	$source = $g->row['source'];
	$author = $g->author;
	$caption = $g->row['description'];
	$id_licence = $g->id_licence;
	if($g->show_date)
	{
		$db =& Db::globaldb();
		$image_date = $db->getTodayDate($g->row['released_ts']);
	}
}

echo $hh->ShowTitle($title);

$maxfilesize = $fm->MaxFileSize();
?>

<script type="text/javascript">
$(document).ready(function() {
$("#form1").validate({
		rules: {
			caption: "required"
<?php	if($id_gallery_from>0) { ?>
			,caption_import: "required"
<?php	} ?>	
		}
	});
});
</script>

<?php
echo $hh->input_form("post","actions.php",true);
echo $hh->input_hidden("MAX_FILE_SIZE",$maxfilesize);
echo $hh->input_hidden("id_gallery",$id_gallery);
echo $hh->input_hidden("from","image");
echo $hh->input_hidden("p",$current_page);
echo $hh->input_hidden("id_image",$id);
if($id_gallery_from>0)
	echo $hh->input_hidden("id_gallery_from",$id_gallery_from);
echo $hh->input_table_open();

if ($id>0)
{
	$img_sizes = $conf->Get("img_sizes");
	echo "<tr><td>&nbsp;</td><td><img src=\"/images/upload.php?src=$filename_copy&format={$irow['format']}\" width=\"{$img_sizes[2]}\">";
	if ($fm->Exists("uploads/$filename_orig"))
		echo "&nbsp;<a href=\"/images/upload.php?src=$filename_orig\" target=\"_blank\">" . $hh->tr->Translate("see_original") . "</a> (" . $fm->Size("uploads/$filename_orig") . ")";
	echo "</td></tr>\n";
	echo $hh->input_upload("substitute_with","img",50,$input_right);
	if($id_gallery_from>0)
	{
		echo $hh->input_separator("original");
		echo $hh->input_text("gallery","gallery_from",$g_from->title,50,0,0);
	}
}
else
{
	echo $hh->input_note($hh->tr->TranslateParams("upload_limit",array(floor($maxfilesize/1024))));
	echo $hh->input_upload("choose_file","img",50,$input_right);
}
echo $hh->input_date_text("date_original","image_date",$image_date,10,0,$input_super_right);
echo $hh->input_text("author","author",$author,50,0,$input_super_right);
echo $hh->input_textarea("source","source",$source,70,3,"",$input_super_right);
echo $hh->input_note("caption_instr");
echo $hh->input_textarea("caption","caption",$caption,80,10,"",$input_right);
echo $hh->input_keywords($id,$o->types['image'],$keywords,$input_right);
if ($hh->ini->Get("licences"))
	echo $hh->input_array("licence","id_licence",$id_licence,$hh->tr->Translate("licences"),$input_super_right);
if($id_gallery_from>0)
{
	echo $hh->input_separator("image_gallery");
	echo $hh->input_text("link","link",$irow['link'],50,0,$input_right);
	echo $hh->input_textarea("caption","caption_import",($irow['id_image_import']>0)? $irow['caption_import'] : $irow['caption'],80,10,"",$input_right);
}
else 
	echo $hh->input_text("link","link",$irow['link'],50,0,$input_right);

$actions = array();
$actions[] = array('action'=>"store",'label'=>"submit",'right'=>$input_right);
if ($id>0)
{
	$actions[] = array('action'=>"delete",'label'=>"delete_from_gallery",'right'=>$input_right && ($irow['id_image_import']>0 || $id_gallery_from==0));
	$articles = $im->Articles();
	$galleries = $im->Galleries($id_gallery);
	$subtopics = $im->Subtopics();
	$actions[] = array('action'=>"remove",'label'=>"delete_forever",'right'=>$input_right && count($articles)==0 && count($subtopics)==0 && count($galleries)==0 && ($irow['id_image_import']>0 || $id_gallery_from==0) && $irow['id_image_import']==0);
}
echo $hh->input_actions($actions,$input_right);

echo $hh->input_table_close() . $hh->input_form_close();

if ($id>0)
{
	$tot_articles = count($articles);
	if ($tot_articles > 0)
	{
		echo "<h3>" . $hh->tr->Translate("image_articles") . "</h3>";
		echo "<ul>";
		foreach($articles as $art)
			echo "<li>{$art['topic_name']}: <a href=\"/articles/article.php?id={$art['id_article']}&w=articles\">{$art['headline']}</a></li>\n";
		echo  "</ul>\n";
	}
	else
		echo "<p>" . $hh->tr->Translate("image_articles_no") . "</p>\n";
	$tot_galleries = count($galleries);
	if ($tot_galleries > 0)
	{
		echo "<h3>" . $hh->tr->Translate("image_galleries") . "</h3>";
		echo "<ul>";
		foreach($galleries as $gallery)
			echo "<li><a href=\"/galleries/image.php?id=$id&id_gallery={$gallery['id_gallery']}&id_gallery_from=$id_gallery\">{$gallery['title']}</a></li>\n";
		echo  "</ul>\n";
	}
}

include_once(SERVER_ROOT."/include/footer.php");
?>

